/**
 * Created by wendyliu on 2017/9/12.
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLogin: false,
    navData: [],
      token: ''
  },
  mutations: {
    SET_LOGIN (state, data) {
      state.isLogin = data[0],
      state.navData = data[1],
      state.token = data[2]
    }
  }
})
