import axios from 'axios'
import qs from 'qs'
import Vue from 'vue'
import store from '@/store'
import router from '../router'
// import * as _ from '../util/tool'

// axios 配置
axios.defaults.timeout = 10000;
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';
axios.defaults.baseURL = 'http://www.corpsteward.com:9080/steward';
// axios.defaults.baseURL = 'http://localhost:9080/steward';
// axios.defaults.baseURL = 'http://bluecoffee.s1.natapp.cc/steward';
axios.defaults.withCredentials = true
//
//POST传参序列化
axios.interceptors.request.use((config) => {
    var myToken = window.localStorage.getItem('token')
    console.log('my token:' + myToken)
    if (config.method === 'post' || config.method === 'get') {
        if (myToken) {
            config.headers = {
                'Content-Type': 'application/json;charset=UTF-8',
                'accessToken': myToken
            }
        } else {
            config.headers = {
                'Content-Type': 'application/json;charset=UTF-8',
            }
        }

    }
    console.log('return')
    return config;
}, (error) => {
    _.toast("错误的传参", 'fail');
    return Promise.reject(error);
});

//返回状态判断
axios.interceptors.response.use((res) => {
    if (!res.data.status === 200) {
        // _.toast(res.data.msg);
        return Promise.reject(res);
    } else if (res.data.status === 401) {
        router.push({name: 'login'})
    }
    return res;
}, (error) => {
    _.toast("网络异常", 'fail');
    return Promise.reject(error);
});


// this.$store.state.user_name = this.username;
Vue.prototype.setCookie = (c_name, value, expiredays) => {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
}

//获取cookie、
function getCookie(name) {
    console.log(name)
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return (unescape(arr[2]));
    else
        return null;
}


export function post(url, params) {
    var _this = this
    return new Promise((resolve, reject) => {
        axios.post(url, params)
            .then(response => {
                resolve(response.data);
            }, err => {
                removeSessionCookie()

                reject(err);
            })
            .catch((error) => {

            })
    })
}

function removeSessionCookie() {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);

    var arr, reg = new RegExp("(^| )" + 'session' + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        var cval = (unescape(arr[2]));

    if (cval != null)
        document.cookie = 'session' + "=" + cval + ";expires=" + exp.toGMTString();
}

export function get(url, params) {
    return new Promise((resolve, reject) => {
        axios.get(url, params)
            .then(response => {
                resolve(response.data);
            }, err => {
                resolve(response.data);

                reject(err);
            })
            .catch((error) => {

            })
    })
}


export function delete_request(url, params) {
    return new Promise((resolve, reject) => {
        axios.delete(url, params)
            .then(response => {
                console.log(response)
                resolve(response.data);
            }, err => {

                reject(err);
            })
            .catch((error) => {
                console.log(error)
            })
    })
}

export default {
    /**
     * 用户登录
     */
    Login(params) {
        return post('/login', params)
    },
    /**
     * 用户登录 第三方登录
     */
    LoginWithToken(token) {
        return get('/ssoAuth?msg=' + token)
    },
    /*
    * 上传文件
    * */
    Upload(params) {
        console.log('params:' + JSON.stringify(params))
        return post('/upload', params)
    },

    /*
    * 查询管理员信息
    * */
    Admins(params) {
        return post('/admins', params)
    },


    AdminInfo(adminId) {
        return get('/admin/' + adminId)
    },

    /*
    * 创建管理员信息
    * */
    CreateAdmin(params) {
        return post('/admin', params)
    },


    /*
    * 修改管理员信息
    * */
    UpdateAdmin(adminId, params) {
        return post('/admin/' + adminId, params)
    },

    /*
    * 获取产品信息
    * */
    Products(params) {
        return post('/products', params)
    },

    /*
    * 新增产品信息
    * */
    AddProduct(params) {
        return post('/product', params)
    },

    /*
     * 更新产品信息
     * */
    UpdateProduct(productId, params) {
        return post('/product/' + productId, params)
    },

    /*
    * 获取用户订单信息
    * */
    Orders(params) {
        return post('/orders', params)
    },

    /*
     * 获取用户最新订单信息
    * */
    LastOrders(params) {
        return post('/lastOrders', params)
    },

    /*
    * 用户订单订购
    * */
    Order(params) {
        return post('/order', params)
    },


    /*
    * 用户退订订单产品
    * */
    DeleteOrder(subscriptionId, params) {
        return post('/order/' + subscriptionId, params)
    },

    /*
    * 查询黑名单
    * */
    BlackUsers(phoneNumber) {
        return get("/blackUsers/" + phoneNumber, '')
    },

    /*
     * 移除黑名单
     * */
    RemoveBlackUser(phoneNumber) {
        console.log('order param :' + phoneNumber)

        return post('/blackUser/' + phoneNumber, '')
    },

    /*
     * 添加黑名单
     * */
    AddBlackUser(param) {
        return post('blackUser', param)
    },

    /*
    * 查询批量操作
    * */
    BatchOptLogs(param) {
        return post('batchOptLogs', param)
    },

    /*
    * 批量操作添加
    * */
    BatchOpt(param) {
        return post('batchOpt', param)
    },

    /*
     * 查询省份
     * */
    GetProvinces() {
        return get('provinces')
    },

    /**
     * 获取ISMP订单的验证码
     * @param param
     * @constructor
     */
    GetIsmpOtp(chargeId, phoneNumber) {
        return get('/ismp/order/otp?chargeId=' + chargeId + "&phoneNumber=" + phoneNumber)
    },

    /**
     * 用户发起ISMP产品订购
     * @param params
     * @constructor
     */
    SubIsmpProduct(params) {
        return post('/ismp/product/subscribe', params)
    },

    /**
     * 用户发起ISMP产品退订
     * @param params
     * @constructor
     */
    UnSubIsmpProduct(params) {
        return post('/ismp/product/unsubscribe', params)
    },

    /**
     * 查询用户绑定权限
     * @param {} adminId
     */
    QueryUserBind (adminId) {
        return get(`/admin/${adminId}/products`)
    },

    /**
     * 提交用户绑定
     * @param {*} params
     */
    SubmitBind (params) {
        return post('/admin/bindProduct', params)
    },

    QueryStatChannel(provinceId, startDate, endDate, productId) {
        var param = {
            "provinceId": provinceId,
            "productId": productId,
            "startDate": startDate,
            "endDate": endDate
        }
        return post('stat/channel', param)
    },

    QueryStatProduct(provinceId) {
        var date = new Date()
        var year = date.getFullYear();
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1)
            : date.getMonth() + 1;
        var day = date.getDate() < 10 ? "0" + date.getDate() : date
            .getDate();
        var dateStr = year + '' + month + '' + day;
        var param = {
            "provinceId": provinceId,
            "statDate": dateStr
        }
        return post('stat/product', param)
    },

    ExportUserSubs(phoneNumber, startDate, endDate) {
        var myToken = window.localStorage.getItem('token')
        let exportUrl = axios.defaults.baseURL + '/orders/export?phoneNumber=' + phoneNumber
            + "&startDate=" + startDate
            + "&endDate=" + endDate + "&accessToken=" + myToken

        window.location.href = exportUrl;
    }
}
