import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/common/Home.vue'
import ProductManager from '../components/pages/product/ProductManager.vue'
// import UserManager from '../components/pages/user/UserManager.vue'
import AdminManager from '../components/pages/admin/AdminManager.vue'
import MyInfo from '../components/pages/admin/MyInfo.vue'
import OrderManager from '../components/pages/user/OrderManager.vue'
import BlackUser from '../components/pages/user/BlackUser.vue'
import UserSubsExport from '../components/pages/user/UserSubsExport.vue'
import UserBatchOrder from '../components/pages/batch/BatchOperate.vue'
import ProductStat from '../components/pages/table/productStat.vue'
import UserStat from '../components/pages/table/userStat.vue'
import ThirdLogin from '../components/pages/login/ThirdLogin.vue'
import Login from '../components/pages/login/Login.vue'
import store from '../store'
Vue.use(Router)

// var base_url = 'http://106.15.199.12:9080/steward'
const router = new Router({
  mode: 'history',
  routes: [

      {
          path: '/',
          redirect: '/login'
      },
      {
          path: '/login',
          name: 'login',
          meta: {
              requireAuth: false,  // 不允许用户直接访问
              scrollToTop: true
          },
          component: Login
      },
    {
      path: '/home',
      name: 'home',
      meta: {
        requireAuth: true,  // 不允许用户直接访问
        scrollToTop: true
      },
      component: Home,
        children: [
            {
                path: '/productManager',
                name: 'productManager',
                meta: {
                    requireAuth: true,  // 不允许用户直接访问
                    scrollToTop: true
                },
                component: ProductManager
            },

            {
                path: '/adminManager',
                name: 'adminManager',
                meta: {
                    requireAuth: true,  // 不允许用户直接访问
                    scrollToTop: true
                },
                component: AdminManager
            },
            {
                path: '/userOrder',
                name: 'userOrder',
                meta: {
                    requireAuth: true,  // 不允许用户直接访问
                    scrollToTop: true
                },
                component: OrderManager
            },
            {
                path: '/myinfo',
                name: 'myInfo',
                meta: {
                    requireAuth: true,  // 不允许用户直接访问
                    scrollToTop: true
                },
                component: MyInfo
            },
            {
                path: '/blackUser',
                name: 'blackUser',
                meta: {
                    requireAuth: true,  // 不允许用户直接访问
                    scrollToTop: true
                },
                component: BlackUser
            },
            {
                path: '/userSubsExport',
                name: 'userSubsExport',
                meta: {
                    requireAuth: true,  // 不允许用户直接访问
                    scrollToTop: true
                },
                component: UserSubsExport
            },
            {
                path: '/userBatchOrder',
                name: 'userBatchOrder',
                meta: {
                    requireAuth: true,  // 不允许用户直接访问
                    scrollToTop: true
                },
                component: UserBatchOrder
            },
            {
                path: '/productStat',
                name: 'productStat',
                meta: {
                    requireAuth: true,  // 不允许用户直接访问
                    scrollToTop: true
                },
                component: ProductStat
            },
            {
                path: '/userStat',
                name: 'userStat',
                meta: {
                    requireAuth: true,  // 不允许用户直接访问
                    scrollToTop: true
                },
                component: UserStat
            }
        ]
    },
      {
          path: '/ssoAuth',
          component: ThirdLogin,
          meta: {
              requireAuth: false,  // 允许用户直接访问
              scrollToTop: true
          },
          props: (route) => ({ token: route.query.msg })
      }
  ]
})

router.beforeEach((to, from, next) => {
    if (to.name !== null) {
        // TODO requireAuth
        if (to.meta.requireAuth) {
            if (store.state.isLogin) {
                next()
            } else {
                if (checkLocalLogin () === true) {
                    next()
                }else {
                    next({
                        name: 'login'
                    })
                }
            }
        } else {
            next()
        }
    } else {
        next({
            name: 'notFound-home'
        })
    }
})

function checkLocalLogin() {
    var loginResponse = window.localStorage.getItem('loginResponse')
    if (loginResponse !== undefined && loginResponse.length > 0) {
        var res = JSON.parse(loginResponse)
        store.state.myUserInfo = res.result.admin
        store.commit('SET_LOGIN', [true,res.result.adminMenus])
        router.push({name: 'userOrder'})
        return true
    } else {
        return false
    }
}
export default router;