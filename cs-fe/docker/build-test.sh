#!/usr/bin/env bash

./stop.sh
docker rmi agent-view

cd ../
cnpm install
npm run build

rm -rf ./docker/dist
mv ./dist ./docker
cd ./docker

docker build -t agent-view ./
