#!/usr/bin/env bash

./stop.sh
docker rmi steward-fe-prod

cd ../
cnpm install
npm run build

rm -rf ./docker/dist
mv ./dist ./docker
cd ./docker

docker build -t steward-fe-prod ./
