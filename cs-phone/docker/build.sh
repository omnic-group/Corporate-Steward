rm -rf dist
mkdir dist
docker rmi -f steward-wechat
docker rm -f steward-wechat

cp -r ../css ../html ../js ./dist
mv dist/html/serviceProvision.html  dist/html/index.html

docker build -t steward-wechat ./
docker images | grep steward-wechat