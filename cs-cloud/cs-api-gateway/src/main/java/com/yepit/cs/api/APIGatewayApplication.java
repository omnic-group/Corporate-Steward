package com.yepit.cs.api;

import com.yepit.cs.api.filter.AccessFilter;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

/**
 * Created by qianlong on 2017/10/9.
 */
@EnableZuulProxy
@SpringCloudApplication
public class APIGatewayApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(APIGatewayApplication.class).web(true).run(args);
    }

    @Bean
    public AccessFilter accessFilter() {
        return new AccessFilter();
    }

}
