package com.yepit.cs.api.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by qianlong on 2017/10/9.
 */
public class AccessFilter extends ZuulFilter {

    @Value("${api.user.accessTokens}")
    private String accessTokens;

    private static Logger log = LoggerFactory.getLogger(AccessFilter.class);

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        log.info(String.format("%s request to %s", request.getMethod(), request.getRequestURL().toString()));
        Object accessTokenObj = request.getHeader("accessToken");
        if (accessTokenObj == null) {
            log.warn("access token is empty");
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            return null;
        }

        String accessToken = (String)accessTokenObj;
        //校验accessToken是否正确
        String[] accessTokenArray = accessTokens.split(",");
        for(String token:accessTokenArray){
            if(accessToken.equals(token)){
                log.info("access token ok");
                return null;
            }
        }

        log.warn("access token is error");
        ctx.setSendZuulResponse(false);
        ctx.setResponseStatusCode(401);
        return null;
    }
}
