rm -rf dist
mkdir dist
docker rmi -f steward-crbt2-mp
docker rm -f steward-crbt2-mp

cp -r ../css ../html ../js ./dist
mv dist/html/serviceProvision.html  dist/html/index.html

docker build -t steward-crbt2-mp ./
docker images | grep steward-crbt2-mp