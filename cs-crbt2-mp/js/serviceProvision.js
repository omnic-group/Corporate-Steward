
$(function () {
    $('.content-title-child:first').addClass('content-title-child-focus')
    $('.content-main-all').hide()
    $('.content-main-all:first').show()
    $('.circle:first').addClass('circle-active')
    $('.input-value').hide()
    $('.input-value:first').show()

})

function changeType(index) {
    $('.content-title-child').removeClass('content-title-child-focus')
    $('.content-title-child:' + index).addClass('content-title-child-focus')
    $('.content-main-all').hide()
    $('.content-main-all:' + index).show()
}

// 密码类型变更
function changeNumType(index) {
    $("#alertMessage").text("")
    $("#passwordValue").val("")
    $("#optValue").val("")
    $('.circle').removeClass('circle-active')
    $('.circle:' + index).addClass('circle-active')
    $('.input-value').hide()
    $('.input-value:' + index).show()
}

// 获取短信验证码
function getOpt() {
    timer(60)
    var phoneNum=$("input[name=phoneNum]").val()
    $.ajax({  // ajax登陆请求
        url: commonUrl + "/otp?tplId=208035&phoneNumber=" + phoneNum,
        type:"get",
        async:false,
        success:function(res){
            $("#alertMessage").text(res.resultDesc)
           if (res.resultCode === "000000") {
               $('.alert-message').removeClass('error')
           }else {
                if (res.resultCode === "999999") { // 999999-手机号码不为空
                    clearInterval()
                }
               $('.alert-message').addClass('error')
           }
        }
    })
}

// 倒计时按钮
function timer(time) {
    var hander = setInterval(function() {
        if (time <= 0) {
            clearInterval(hander); //清除倒计时
            $("#optButton").text('获取验证码')
            $('.vaild-number-click').removeClass('disable')
            return false;
        }else {
            $('.vaild-number-click').addClass('disable')
            var textContent = (time--) + "秒后重新获取"
            $("#optButton").text(textContent)
        }
    }, 1000)
}

//确认开通操作
function confirmClick() {
    var verifyType = ''
    var password = ''
    var productCode = ''
    var phoneNumber = $("input[name=phoneNum]").val()
    if($('.content-title-child:first').hasClass('content-title-child-focus')) {
        productCode = '9999000062' // 基础版
    } else {
        productCode = '9999000063' // 高级版
    }
    if($('.circle:first').hasClass('circle-active')) {
        verifyType = 1
        password = $("input[name=passwordValue]").val()
    } else {
        verifyType = 2
        password = $("input[name=optValue]").val()
    }
    $.ajax({  // ajax登陆请求
        url: commonUrl + "/subscriberViaWechat",
        type:"post",
        async:false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data:JSON.stringify({
            "productCode":productCode,
            "phoneNumber":phoneNumber,
            "verifyType":verifyType,
            "password":password
        }),
        success:function(res){
            if (res.resultCode === "000000") {
                $('.alert-message').removeClass('error')
                $("#content").text(res.resultDesc)
                popBox()
                cleanAll()
            }else {
                $('.alert-message').addClass('error')
                $("#alertMessage").text(res.resultDesc)
            }
        }
    })
}

/*点击弹出按钮*/
function popBox() {
    $('.ui-dialog').addClass("show")
}

/*点击关闭按钮*/
function closeBox() {
    $('.ui-dialog').removeClass("show")
}

// 清除倒计时按钮
function clearInterval() {
    var hander = setInterval(function() {
        clearInterval(hander); //清除倒计时
        $("#optButton").text('获取验证码')
        $('.vaild-number-click').removeClass('disable')
        return false;
    });
}

function cleanAll() {
    // 套餐内容与标题重置
    $('.content-title-child').removeClass('content-title-child-focus')
    $('.content-title-child:first').addClass('content-title-child-focus')
    $('.content-main-all').hide()
    $('.content-main-all:first').show()
    // 密码类型重置
    $('.circle').removeClass('circle-active')
    $('.circle:first').addClass('circle-active')
    $('.input-value').hide()
    $('.input-value:first').show()
    // 输入框重置
    $("#alertMessage").text("")
    $("#phoneNum").val("")
    $("#passwordValue").val("")
    $("#optValue").val("")
    clearInterval()
}