package test.com.yepit.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.domain.SysAdmin;
import com.yepit.cs.dto.admin.AdminLoginRequest;
import com.yepit.cs.dto.admin.AdminLoginResponse;
import com.yepit.cs.service.AdminService;
import com.yepit.cs.util.JsonUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by qianlong on 2017/8/19.
 */
public class TestAdminService extends BaseTest {

    @Autowired
    private AdminService adminService;

    @Test
    public void testCreateAdmin(){
        try{
            SysAdmin sysAdmin = new SysAdmin();
            sysAdmin.setLoginName("tina");
            sysAdmin.setLoginPwd("123456");
//            sysAdmin.setPwdType(1);

            BaseResponse resp = adminService.createAdmin(sysAdmin,new Long(1));
            System.out.println("resp.resultCode="+resp.getResultCode());
            Assert.assertEquals(true,resp.isSuccess());
        }catch (Exception ex){
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testAdminLogin(){
        try{
            AdminLoginRequest request = new AdminLoginRequest();
            request.setLoginName("qianlong");
            request.setLoginPwd("123456");
            BaseResponse<AdminLoginResponse> resp = adminService.adminLogin(request);
            System.out.println("resp:"+ JsonUtils.Object2Json(resp));
//            System.out.println("resp.resultCode="+resp.getResultCode());
            Assert.assertEquals(true,resp.isSuccess());
        }catch (Exception ex){
            Assert.fail(ex.getMessage());
        }

    }

    @Test
    public void testUpdateAdmin(){
        try{
            SysAdmin sysAdmin = new SysAdmin();
            sysAdmin.setAdminId(new Long(8));
            sysAdmin.setLoginPwd("111111");
            sysAdmin.setRealname("钱龙");
            BaseResponse resp = adminService.updateAdmin(sysAdmin);
            System.out.println("resp.resultCode="+resp.getResultCode());
            Assert.assertEquals(true,resp.isSuccess());
        }catch (Exception ex){
            Assert.fail(ex.getMessage());
        }

    }
}
