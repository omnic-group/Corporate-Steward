package test.com.yepit.service;

import com.yepit.cs.service.SequenceDefService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by qianlong on 2017/8/17.
 */
public class TestSequenceService extends BaseTest{

    @Autowired
    private SequenceDefService sequenceDefService;

    @Test
    public void testNextId(){
        try{
            for(int i = 0;i<10;i++){
                String nextId = sequenceDefService.nextId("user_info","user_id");
                System.out.println("nextId="+nextId);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void testNextIdFor(){

    }
}
