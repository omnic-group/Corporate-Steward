package test.com.yepit.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.common.PageArg;
import com.yepit.cs.common.PageResult;
import com.yepit.cs.constant.BatchOptTypeEnum;
import com.yepit.cs.dto.batch.CreateBatchRequest;
import com.yepit.cs.dto.batch.CreateBatchResponse;
import com.yepit.cs.dto.batch.SearchBatchLogRequest;
import com.yepit.cs.dto.batch.SearchBatchLogResponse;
import com.yepit.cs.dto.file.FileUploadRequest;
import com.yepit.cs.dto.file.FileUploadResponse;
import com.yepit.cs.dto.file.UploadFileDTO;
import com.yepit.cs.service.BatchService;
import com.yepit.cs.service.FileService;
import com.yepit.cs.util.ExcelUtils;
import com.yepit.cs.util.FileUtils;
import com.yepit.cs.util.JsonUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qianlong on 2017/9/2.
 */
public class TestBatchService extends BaseTest {

    @Autowired
    private BatchService batchService;

    @Autowired
    private FileService fileService;

    @Test
    public void testListBatchOptLogByCond() {
        try {
            SearchBatchLogRequest request = new SearchBatchLogRequest();
            PageArg pageArg = new PageArg();
            pageArg.setPageNum(1);
            pageArg.setPageSize(5);


//            request.setStatus(1);
            request.setStartTime("2017-09-01");
            request.setEndTime("2017-09-02");
            request.setPage(pageArg);
            BaseResponse<PageResult<SearchBatchLogResponse>> resp = batchService.listBatchOptLogByCond(request);
            System.out.println(JsonUtils.Object2Json(resp));
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    /**
     * 过滤黑名单批量处理
     */
    @Test
    public void testCreateBatchBlackUserFilter() {
        try {
            //上传一个黑名单文件
            List<List<String>> phoneList = new ArrayList<List<String>>();
            List<String> phone1 = new ArrayList<String>();
            phone1.add("18551855920");
            phoneList.add(phone1);

            List<String> phone2 = new ArrayList<String>();
            phone2.add("185518559201");
            phoneList.add(phone2);

            ExcelUtils.createXlsx("", null, phoneList, "blackUser.xlsx");

            String base64Str = FileUtils.fileToBase64("blackUser.xls");
            List<UploadFileDTO> uploadFileDTOS = new ArrayList<UploadFileDTO>();
            UploadFileDTO file1 = new UploadFileDTO();
            file1.setBase64Str(base64Str);
            file1.setFileName("blackUser.xls");
            uploadFileDTOS.add(file1);

            FileUploadRequest request = new FileUploadRequest();
            request.setUploadFiles(uploadFileDTOS);
            BaseResponse<FileUploadResponse> resp = fileService.upload(request);
            System.out.println(JsonUtils.Object2Json(resp));

            String fileId = resp.getResult().getFileList().get(0).getFileId();

            CreateBatchRequest createBatchRequest = new CreateBatchRequest();
            createBatchRequest.setFileId(fileId);
            createBatchRequest.setBatchType(BatchOptTypeEnum.BlackUserFilter.getValue());
            createBatchRequest.setOperName("qianlong");

            BaseResponse<CreateBatchResponse> createBatchRespone = batchService.createBatch(createBatchRequest);
            Assert.assertNotNull(createBatchRespone);
            Assert.assertEquals(true,createBatchRespone.isSuccess());
            System.out.println(JsonUtils.Object2Json(createBatchRespone));
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testCreateBatchBlackUserFilter1(){
        try{
            String fileId = "0000000007";

            CreateBatchRequest createBatchRequest = new CreateBatchRequest();
            createBatchRequest.setFileId(fileId);
            createBatchRequest.setBatchType(BatchOptTypeEnum.BlackUserFilter.getValue());
            createBatchRequest.setOperName("qianlong");

            BaseResponse<CreateBatchResponse> createBatchRespone = batchService.createBatch(createBatchRequest);
            Assert.assertNotNull(createBatchRespone);
            Assert.assertEquals(true,createBatchRespone.isSuccess());
            System.out.println(JsonUtils.Object2Json(createBatchRespone));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * 黑名单导入批量处理
     */
    @Test
    public void testCreateBatchBlackUserImport() {
        try {
            //上传一个黑名单文件
            List<List<String>> phoneList = new ArrayList<List<String>>();
            List<String> phone1 = new ArrayList<String>();
            phone1.add("18551855920");
            phoneList.add(phone1);

            List<String> phone2 = new ArrayList<String>();
            phone2.add("185518559201");
            phoneList.add(phone2);

            ExcelUtils.createXlsx("", null, phoneList, "blackUserImport.xlsx");

            String base64Str = FileUtils.fileToBase64("blackUser.xls");
            List<UploadFileDTO> uploadFileDTOS = new ArrayList<UploadFileDTO>();
            UploadFileDTO file1 = new UploadFileDTO();
            file1.setBase64Str(base64Str);
            file1.setFileName("blackUserImport.xls");
            uploadFileDTOS.add(file1);

            FileUploadRequest request = new FileUploadRequest();
            request.setUploadFiles(uploadFileDTOS);
            BaseResponse<FileUploadResponse> resp = fileService.upload(request);
            System.out.println(JsonUtils.Object2Json(resp));

            String fileId = resp.getResult().getFileList().get(0).getFileId();

            CreateBatchRequest createBatchRequest = new CreateBatchRequest();
            createBatchRequest.setFileId(fileId);
            createBatchRequest.setBatchType(BatchOptTypeEnum.BlackUserImport.getValue());
            createBatchRequest.setOperName("qianlong");

            BaseResponse<CreateBatchResponse> createBatchRespone = batchService.createBatch(createBatchRequest);
            Assert.assertNotNull(createBatchRespone);
            Assert.assertEquals(true,createBatchRespone.isSuccess());
            System.out.println(JsonUtils.Object2Json(createBatchRespone));
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    /**
     * 批量开户
     */
    @Test
    public void testCreateBatchOpenAccount(){
        try{
            String productCode = "0000000001";
            List<String> header = new ArrayList<String>();
            header.add("用户号码");
            header.add("名称");
            header.add("属地");
            header.add("运营商");
            header.add("产品编号");
            header.add("是否发送短信");
//            header.add("操作类型");

            List<List<String>> dataList = new ArrayList<List<String>>();
            List<String> row1 = new ArrayList<String>();
            row1.add("15301588194");
            row1.add("戴志强");
            row1.add("025");
            row1.add("电信");
            row1.add(productCode);
            row1.add("1");
//            row1.add("1");
            dataList.add(row1);

            List<String> row2 = new ArrayList<String>();
            row2.add("15301588194");
            row2.add("戴志强");
            row2.add("025");
            row2.add("电信");
            row2.add(productCode);
            row2.add("1");
//            row2.add("2");
            dataList.add(row2);

            List<String> row3 = new ArrayList<String>();
            row3.add("18551855920");
            row3.add("钱龙");
            row3.add("025");
            row3.add("电信");
            row3.add(productCode);
            row3.add("1");
            dataList.add(row3);

            ExcelUtils.createXlsx("批量开户", header, dataList, "batchOrder.xlsx");

            String base64Str = FileUtils.fileToBase64("batchOrder.xls");
            List<UploadFileDTO> uploadFileDTOS = new ArrayList<UploadFileDTO>();
            UploadFileDTO file1 = new UploadFileDTO();
            file1.setBase64Str(base64Str);
            file1.setFileName("batchOrder.xls");
            uploadFileDTOS.add(file1);

            FileUploadRequest request = new FileUploadRequest();
            request.setUploadFiles(uploadFileDTOS);
            BaseResponse<FileUploadResponse> resp = fileService.upload(request);
            System.out.println(JsonUtils.Object2Json(resp));

            String fileId = resp.getResult().getFileList().get(0).getFileId();

            CreateBatchRequest createBatchRequest = new CreateBatchRequest();
            createBatchRequest.setFileId(fileId);
            createBatchRequest.setBatchType(BatchOptTypeEnum.OpenAccount.getValue());
            createBatchRequest.setOperName("qianlong");

            BaseResponse<CreateBatchResponse> createBatchRespone = batchService.createBatch(createBatchRequest);
            Assert.assertNotNull(createBatchRespone);
            Assert.assertEquals(true,createBatchRespone.isSuccess());
            System.out.println(JsonUtils.Object2Json(createBatchRespone));
        }catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    /**
     * 批量销户
     */
    @Test
    public void testCreateBatchCloseAccount(){
        try{
            String productCode = "0000000001";
            List<String> header = new ArrayList<String>();
            header.add("用户号码");
            header.add("产品编号");

            List<List<String>> dataList = new ArrayList<List<String>>();
            List<String> row1 = new ArrayList<String>();
            row1.add("15301588194");
            row1.add(productCode);
            dataList.add(row1);

            List<String> row2 = new ArrayList<String>();
            row2.add("15301588194");
            row2.add(productCode);
            dataList.add(row2);

            List<String> row3 = new ArrayList<String>();
            row3.add("18551855920");
            row3.add(productCode);
            dataList.add(row3);

            ExcelUtils.createXlsx("批量销户", header, dataList, "batchCloseAccount.xlsx");

            String base64Str = FileUtils.fileToBase64("batchCloseAccount.xls");
            List<UploadFileDTO> uploadFileDTOS = new ArrayList<UploadFileDTO>();
            UploadFileDTO file1 = new UploadFileDTO();
            file1.setBase64Str(base64Str);
            file1.setFileName("batchCloseAccount.xls");
            uploadFileDTOS.add(file1);

            FileUploadRequest request = new FileUploadRequest();
            request.setUploadFiles(uploadFileDTOS);
            BaseResponse<FileUploadResponse> resp = fileService.upload(request);
            System.out.println(JsonUtils.Object2Json(resp));

            String fileId = resp.getResult().getFileList().get(0).getFileId();

            CreateBatchRequest createBatchRequest = new CreateBatchRequest();
            createBatchRequest.setFileId(fileId);
            createBatchRequest.setBatchType(BatchOptTypeEnum.CloseAccount.getValue());
            createBatchRequest.setOperName("qianlong");

            BaseResponse<CreateBatchResponse> createBatchRespone = batchService.createBatch(createBatchRequest);
            Assert.assertNotNull(createBatchRespone);
            Assert.assertEquals(true,createBatchRespone.isSuccess());
            System.out.println(JsonUtils.Object2Json(createBatchRespone));
        }catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }
}
