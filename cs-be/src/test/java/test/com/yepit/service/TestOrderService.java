package test.com.yepit.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.*;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.order.*;
import com.yepit.cs.dto.product.DeleteProductRequest;
import com.yepit.cs.service.OrderService;
import com.yepit.cs.service.ProductService;
import com.yepit.cs.util.JsonUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by qianlong on 2017/8/27.
 */
public class TestOrderService extends BaseTest {

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductService productService;

    private final String product_code_steward = "00000123";
    private final String product_id_steward = "0000000013";
    private final String product_id_crbt = "0000000009";

    /**
     * 订购一个不存在的产品
     */
    @Test
    public void testSubscribeProduct1() {
        try {
            SubscribeProductRequest request = new SubscribeProductRequest();
            request.setPhoneNumber("15301588194");
            request.setProductId("121212");
            request.setOperateOrigin(2);

            BaseResponse<SubscribeProductResponse> resp = orderService.subscribeProduct(request);
            System.out.println("resp:" + JsonUtils.Object2Json(resp));
            Assert.assertNotNull(resp);
            Assert.assertEquals("000001", resp.getResultCode());
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    /**
     * 订购一个状态不正常的产品
     */
    @Test
    public void testSubscribeProduct2() {
        try {
            //更新产品状态为下架
            ProductInfo productInfo = new ProductInfo();
            productInfo.setProductId(product_id_steward);
            productInfo.setStatus(ProductStatusEnum.Cancel.getValue());
            productService.updateProduct(productInfo);

            SubscribeProductRequest request = new SubscribeProductRequest();
            request.setPhoneNumber("15301588194");
            request.setProductId(product_id_steward);
            request.setOperateOrigin(2);

            BaseResponse<SubscribeProductResponse> resp = orderService.subscribeProduct(request);
            System.out.println("resp:" + JsonUtils.Object2Json(resp));
            Assert.assertNotNull(resp);
            Assert.assertEquals("000002", resp.getResultCode());

        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    /**
     * 江苏联通用户(该用户不存在)订购电信平台产品
     */
    @Test
    public void testSubscribeProduct3() {
        try {
            //更新产品状态为正常
            ProductInfo productInfo = new ProductInfo();
            productInfo.setProductId(product_id_steward);
            productInfo.setStatus(ProductStatusEnum.Normal.getValue());
            productInfo.setProductType(ProductTypeEnum.CRBT.getValue());
            productService.updateProduct(productInfo);

            SubscribeProductRequest request = new SubscribeProductRequest();
            request.setPhoneNumber("18551855920");
            request.setProductId(product_id_steward);
            request.setOperateOrigin(2);

            BaseResponse<SubscribeProductResponse> resp = orderService.subscribeProduct(request);
            System.out.println("resp:" + JsonUtils.Object2Json(resp));
            Assert.assertNotNull(resp);
            Assert.assertEquals("000005", resp.getResultCode());

        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    /**
     * 是江苏电信用户订购本平台产品,之前已订购过该产品
     */
    @Test
    public void testSubscribeProduct4() {
        try {
            orderService.deleteProductSubscribe("15301588194",product_id_steward);

            //更新产品状态为正常
            ProductInfo productInfo = new ProductInfo();
            productInfo.setProductId(product_id_steward);
            productInfo.setStatus(ProductStatusEnum.Normal.getValue());
            productInfo.setProductType(ProductTypeEnum.BaiWang.getValue());
            productService.updateProduct(productInfo);

            SubscribeProductRequest request = new SubscribeProductRequest();
            request.setPhoneNumber("15301588194");
            request.setProductId(product_id_steward);
            request.setOperateOrigin(OperateOriginEnum.StewardPlatform.getValue());
            orderService.subscribeProduct(request);

            BaseResponse<SubscribeProductResponse> resp = orderService.subscribeProduct(request);
            System.out.println("resp:" + JsonUtils.Object2Json(resp));
            Assert.assertNotNull(resp);
            Assert.assertEquals("000004", resp.getResultCode());

        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    /**
     * 是江苏电信用户订购电信平台产品,之前未订购过该产品
     */
    @Test
    public void testSubscribeProduct5() {
        try {
            String phoneNumber = "19951717099";
            phoneNumber = "15301588194";
            orderService.deleteProductSubscribe("19951717099","0000000013");

            //更新产品状态为正常
            ProductInfo productInfo = new ProductInfo();
            productInfo.setProductId("0000000013");
            productInfo.setStatus(ProductStatusEnum.Normal.getValue());
            productInfo.setProductType(ProductTypeEnum.CRBT.getValue());
            productService.updateProduct(productInfo);

            SubscribeProductRequest request = new SubscribeProductRequest();
            request.setPhoneNumber("19951717099");
            request.setPhoneType(PhoneTypeEnum.Mobile.getValue());
            request.setTelecomOperator(TelecomTypeEnum.CTC.getValue());
            request.setProductId("0000000013");
            request.setOperateOrigin(OperateOriginEnum.StewardPlatform.getValue());

            BaseResponse<SubscribeProductResponse> resp = orderService.subscribeProduct(request);
            System.out.println("resp:" + JsonUtils.Object2Json(resp));
            Assert.assertNotNull(resp);
            Assert.assertEquals("000000", resp.getResultCode());

        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    /**
     * 退订
     */
    @Test
    public void testUnSubscribeProduct1() {
        try {
            orderService.deleteProductSubscribe("15301588194",product_id_crbt);

            //更新产品状态为正常
            ProductInfo productInfo = new ProductInfo();
            productInfo.setProductId(product_id_crbt);
            productInfo.setStatus(ProductStatusEnum.Normal.getValue());
            productInfo.setProductType(ProductTypeEnum.CRBT.getValue());
            productService.updateProduct(productInfo);

            //订购该产品
            SubscribeProductRequest request = new SubscribeProductRequest();
            request.setPhoneNumber("15301588194");
            request.setProductId(product_id_crbt);
            request.setOperateOrigin(OperateOriginEnum.StewardPlatform.getValue());

            BaseResponse<SubscribeProductResponse> resp = orderService.subscribeProduct(request);
            System.out.println("resp:" + JsonUtils.Object2Json(resp));
            Assert.assertNotNull(resp);
            Assert.assertEquals("000000", resp.getResultCode());
            String subscriptionId = resp.getResult().getSubscriptionId();

            //退订该产品
            UnsubscribeProductRequest unsubscribeProductRequest = new UnsubscribeProductRequest();
            unsubscribeProductRequest.setSubscriptionId(subscriptionId);
            unsubscribeProductRequest.setOperateOrigin(OperateOriginEnum.StewardPlatform.getValue());
            BaseResponse unsubscribeResp = orderService.unSubscribeProduct(unsubscribeProductRequest);
            System.out.println("unsubscribeResp:" + JsonUtils.Object2Json(unsubscribeResp));
            Assert.assertEquals("000000", resp.getResultCode());

        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testSearchUserSubscription(){
        try{
            SearchSubscriptionRequest request = new SearchSubscriptionRequest();
            request.setPhoneNumber("15301588194");
            BaseResponse<List<SearchSubscriptionResponse>> resp = orderService.searchUserSubscription(request);
            Assert.assertNotNull(resp);
            System.out.println("resp:" + JsonUtils.Object2Json(resp));

        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }
}
