package test.com.yepit.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.common.PageArg;
import com.yepit.cs.common.PageResult;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.admin.PageSearchAdminRequest;
import com.yepit.cs.dto.product.CreateProductRequest;
import com.yepit.cs.dto.product.PageSearchProductRequest;
import com.yepit.cs.rest.ProductController;
import com.yepit.cs.service.ProductService;
import com.yepit.cs.util.JsonUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by qianlong on 2017/8/20.
 */
public class TestProductService extends BaseTest{

    @Autowired
    private ProductService productService;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders.standaloneSetup(new ProductController()).build();
        webRequest = null;
    }

    @Test
    public void testWebCreateProduct(){
        try{
            CreateProductRequest request = new CreateProductRequest();
            request.setProductName("测试产品");
            request.setProductType(1);

            webRequest = post("/product")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(JsonUtils.Object2Json(request).getBytes());

            mvc.perform(webRequest)
                    .andExpect(status().isOk())
//                    .andExpect(content().string(equalTo("success")))
                    .andDo(MockMvcResultHandlers.print());
        }catch (Exception ex){
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testCreateProduct(){
        try{
            ProductInfo productInfo = new ProductInfo();
            productInfo.setProductName("测试产品");
            productInfo.setProductType(1);

            BaseResponse resp = productService.createProduct(productInfo);
            System.out.println(JsonUtils.Object2Json(resp));
            Assert.assertEquals(true,resp.isSuccess());
        }catch (Exception ex){
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testUpdateProduct(){
        try{
            ProductInfo productInfo = new ProductInfo();
            productInfo.setProductName("测试产品1");
            productInfo.setProductId("0000000003");
            productInfo.setProductType(1);

            BaseResponse<ProductInfo> resp = productService.updateProduct(productInfo);
            System.out.println(JsonUtils.Object2Json(resp));
            Assert.assertEquals(true,resp.isSuccess());
        }catch (Exception ex){
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void testPageListProduct(){
        try{
            PageSearchProductRequest request = new PageSearchProductRequest();
            PageArg pageArg = new PageArg();
            pageArg.setPageNum(1);
            pageArg.setPageSize(10);
            request.setPage(pageArg);
            request.setProductType(1);

            BaseResponse<PageResult<ProductInfo>> resp = productService.listProductByCond(request);
            System.out.println(JsonUtils.Object2Json(resp));
        }catch (Exception ex){

        }
    }
}
