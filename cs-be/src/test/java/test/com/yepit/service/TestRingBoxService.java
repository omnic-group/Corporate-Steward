package test.com.yepit.service;

import com.linkage.lccmp.client.MusicSetsSubscribeInterface.MusicSetsSubscribeInterfaceProxy;
import com.linkage.lccmp.client.UserInterface.UserInterface;
import com.linkage.lccmp.client.UserInterface.UserInterfaceProxy;
import com.linkage.lccmp.common.ClientObject;
import com.linkage.lccmp.common.ResultObject;
import com.linkage.lccmp.webservice.encpdata.user.MusicSetsSubscribeInfo;
import com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo;
import com.yepit.cs.util.JsonUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * Created by qianlong on 2017/8/10.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class TestRingBoxService {

    private static SimpleUserInfo userInfo;

    private static ClientObject co;

    static final String SETS_ID = "9999000033";

    @BeforeClass
    public static void beforeClass() {
        userInfo = new SimpleUserInfo();
        userInfo.setCallusertype("2");
        userInfo.setPhonenumber("15301588194");

        co = new ClientObject();
        co.setOperator("qiyeguanjia");
        co.setOrigin(40);
    }

    @Test
    public void getMusicSetsSubscribeTest() {
        try {
            MusicSetsSubscribeInterfaceProxy proxy = new MusicSetsSubscribeInterfaceProxy();
            MusicSetsSubscribeInfo[] musicSetsSubscribeInfos = proxy.getMusicSetsSubscribe(userInfo, co);
            System.out.println("musicSetsSubscribeInfos.length=" + musicSetsSubscribeInfos.length);
            Assert.assertEquals(true, musicSetsSubscribeInfos.length > 0);
            for(MusicSetsSubscribeInfo info:musicSetsSubscribeInfos){
                System.out.println("setsId:"+info.getSetsid()+"/spid:"+info.getSpid());
            }
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void addMusicSetsSubscribeTest() {
        try {
            MusicSetsSubscribeInterfaceProxy proxy = new MusicSetsSubscribeInterfaceProxy();
            ResultObject ro = proxy.addMusicSetsSubscribe(userInfo, SETS_ID, null, co);
            System.out.println(JsonUtils.Object2Json(ro));
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void delMusicSetsSubscribeTest() {
        try {
            MusicSetsSubscribeInterfaceProxy proxy = new MusicSetsSubscribeInterfaceProxy();
            ResultObject ro = proxy.delMusicSetsSubscribe(userInfo, SETS_ID, co);
            System.out.println(JsonUtils.Object2Json(ro));
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void testAuthUser(){
        try{
            UserInterfaceProxy proxy = new UserInterfaceProxy();
            ResultObject ro = proxy.authUser(userInfo,"852281",co);
            System.out.println(JsonUtils.Object2Json(ro));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
