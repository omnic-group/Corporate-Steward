package test.com.yepit.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.dto.file.FileUploadRequest;
import com.yepit.cs.dto.file.FileUploadResponse;
import com.yepit.cs.dto.file.UploadFileDTO;
import com.yepit.cs.service.FileService;
import com.yepit.cs.util.FileUtils;
import com.yepit.cs.util.JsonUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qianlong on 2017/8/23.
 */
public class TestFileService extends BaseTest{

    @Autowired
    private FileService fileService;
    @Test
    public void testUpload(){
        try{
            String filePath = "/Users/qianlong/OscGithub/test.txt";
            String base64Str = FileUtils.fileToBase64(filePath);

            FileUploadRequest request = new FileUploadRequest();
            List<UploadFileDTO> uploadFileDTOS = new ArrayList<UploadFileDTO>();
            UploadFileDTO file1 = new UploadFileDTO();
            file1.setBase64Str(base64Str);
            file1.setFileName("test.txt");
            uploadFileDTOS.add(file1);

            request.setUploadFiles(uploadFileDTOS);
            System.out.println("base64Str:"+base64Str);

            BaseResponse<FileUploadResponse> resp = fileService.upload(request);
            System.out.println(JsonUtils.Object2Json(resp));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
