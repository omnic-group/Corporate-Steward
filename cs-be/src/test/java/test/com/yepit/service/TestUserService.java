package test.com.yepit.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.dto.file.FileUploadRequest;
import com.yepit.cs.dto.file.FileUploadResponse;
import com.yepit.cs.dto.file.UploadFileDTO;
import com.yepit.cs.dto.user.BlackUserFilterRequest;
import com.yepit.cs.dto.user.BlackUserFilterResponse;
import com.yepit.cs.service.FileService;
import com.yepit.cs.service.UserService;
import com.yepit.cs.util.ExcelUtils;
import com.yepit.cs.util.FileUtils;
import com.yepit.cs.util.JsonUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qianlong on 2017/9/2.
 */
public class TestUserService extends BaseTest{

    @Autowired
    private UserService userService;

    @Autowired
    private FileService fileService;

    /**
     * 黑名单过滤
     */
    @Test
    public void testBlackUserFilter(){
        try{
            //上传一个黑名单文件
            List<List<String>> phoneList = new ArrayList<List<String>>();
            List<String> phone1 = new ArrayList<String>();
            phone1.add("18551855920");
            phoneList.add(phone1);

            List<String> phone2 = new ArrayList<String>();
            phone2.add("185518559201");
            phoneList.add(phone2);

            ExcelUtils.createXlsx("",null,phoneList,"blackUser.xlsx");

            String base64Str = FileUtils.fileToBase64("blackUser.xls");
            List<UploadFileDTO> uploadFileDTOS = new ArrayList<UploadFileDTO>();
            UploadFileDTO file1 = new UploadFileDTO();
            file1.setBase64Str(base64Str);
            file1.setFileName("blackUser.xls");
            uploadFileDTOS.add(file1);

            FileUploadRequest request = new FileUploadRequest();
            request.setUploadFiles(uploadFileDTOS);
            BaseResponse<FileUploadResponse> resp = fileService.upload(request);
            System.out.println(JsonUtils.Object2Json(resp));

            String fileId = resp.getResult().getFileList().get(0).getFileId();
            BlackUserFilterRequest request1 = new BlackUserFilterRequest();
            request1.setFileId(fileId);

            BaseResponse<BlackUserFilterResponse> blackUserFilterResp = userService.blackUserFilter(request1);
            Assert.assertNotNull(blackUserFilterResp);
            Assert.assertEquals(true,blackUserFilterResp.isSuccess());
            System.out.println(JsonUtils.Object2Json(blackUserFilterResp));
        }catch (Exception ex){
            Assert.fail(ex.getMessage());
        }
    }

}
