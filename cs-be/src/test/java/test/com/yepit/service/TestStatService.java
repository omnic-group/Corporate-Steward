package test.com.yepit.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.domain.StatChannelSubDay;
import com.yepit.cs.dto.stat.StatByChannelRequest;
import com.yepit.cs.dto.stat.StatByChannelResponse;
import com.yepit.cs.dto.stat.StatByProductRequest;
import com.yepit.cs.dto.stat.StatByProductResponse;
import com.yepit.cs.mapper.StatChannelSubDayMapper;
import com.yepit.cs.service.StatService;
import com.yepit.cs.util.JsonUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by qianlong on 2017/10/2.
 */
public class TestStatService extends BaseTest {

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private StatService statService;

    @Autowired
    private StatChannelSubDayMapper statChannelSubDayMapper;

    /**
     * 按产品维度统计
     */
    @Test
    public void testStatProductSubscriptions() {
        try {
//            statService.statProductSubscriptions("20170901");
//            for (int i = 20170902; i <= 20170930; i++) {
//                statService.statProductSubscriptions(String.valueOf(i));
//            }
//            for (int i = 20171001; i <= 20171008; i++) {
//                statService.statProductSubscriptions(String.valueOf(i));
//            }
            statService.statProductSubscriptions("20171008");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }

    }

    /**
     * 按渠道维度统计新增用户数
     */
    @Test
    public void testStatChannelSubscriptions() {
        try {
//            for (int i = 20170901; i <= 20170930; i++) {
//                statService.statChannelSubscriptions(String.valueOf(i));
//            }
//            for (int i = 20171001; i <= 20171008; i++) {
//                statService.statChannelSubscriptions(String.valueOf(i));
//            }
//            statService.statChannelSubscriptions("20170907");
//            statService.statChannelSubscriptions("20170908");
//            statService.statChannelSubscriptions("20170911");
//            statService.statChannelSubscriptions("20170913");
            statService.statChannelSubscriptions("20171008");

//            ArrayList<StatChannelSubDay> results = new ArrayList<StatChannelSubDay>();
//            for(int i=0;i<5;i++){
//                StatChannelSubDay result = new StatChannelSubDay();
//                result.setStatDay("20171012");
//                result.setStatTime(new Date());
//                result.setProductId("1");
//                result.setProductCode("1");
//                result.setProductName("1111");
//                result.setAreaCode("025");
//                result.setAreaName("南京");
//                result.setStatNum(new Long(10));
//                result.setChannel(1);
//                results.add(result);
//            }
//
//            statChannelSubDayMapper.insertBatch(results);
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }

    }

    /**
     * 按全渠道维度统计用户到达数,退订数
     */
    @Test
    public void testStatAllChannel() {
        try {
//            statService.statChannelSubscriptions(SDF.format(new Date()));
//            statService.statAllChannelDay("20170907");
//            statService.statAllChannelDay("20170908");
//            statService.statAllChannelDay("20170911");
//            statService.statAllChannelDay("20170913");
//            statService.statAllChannelDay("20170927");
//            statService.statAllChannelDay("20171007");
            statService.statAllChannelDay("20171008");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }

    }

    @Test
    public void testSearchStatResultByProduct() {
        try {
            StatByProductRequest request = new StatByProductRequest();
            request.setStatDate("20171006");
            request.setProvinceId(115);

            BaseResponse<StatByProductResponse> resp = statService.searchStatResultByProduct(request);
            System.out.println(JsonUtils.Object2Json(resp));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testSearchStatResultByChannel() {
        try {
            StatByChannelRequest request = new StatByChannelRequest();
            request.setStartDate("20170901");
            request.setEndDate("20171007");
            request.setProvinceId(115);
            request.setProductId("0000000010");

            BaseResponse<StatByChannelResponse> resp = statService.searchStatResultByChannel(request);
            System.out.println(JsonUtils.Object2Json(resp));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
