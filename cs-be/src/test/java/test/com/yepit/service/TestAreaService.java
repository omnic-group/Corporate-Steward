package test.com.yepit.service;

import com.yepit.cs.domain.Area;
import com.yepit.cs.dto.system.AreaInfo;
import com.yepit.cs.mapper.AreaMapper;
import com.yepit.cs.service.AreaService;
import com.yepit.cs.util.AreaUtils;
import com.yepit.cs.util.JsonUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;

/**
 * Created by qianlong on 2017/10/1.
 */
public class TestAreaService extends BaseTest {

    @Autowired
    private AreaService areaService;

    static List<AreaInfo> allAreas;

    @BeforeClass
    public static void beforeClass() throws Exception {
        allAreas = AreaUtils.getAllAreas();
    }

    @Test
    public void testInit() {
        try {
            HashMap<String, AreaInfo> allAreaInfo = AreaUtils.getAreaInfoMap();
            List<String> provinceNameList = AreaUtils.provinceNameList;
            int areaId = 1;
            for (int i = 0; i < provinceNameList.size(); i++) {
                StringBuffer sql = new StringBuffer();
                sql.append("insert into s_area(area_id,parent_area_id,area_name,area_level,area_code) values");
                String provinceName = provinceNameList.get(i);
                Area province = new Area();
                province.setAreaId(areaId);
                province.setAreaName(provinceName);
                province.setParentAreaId(0);
                province.setAreaLevel(1);
                String provinceAreaCode = "";
                AreaInfo areaInfoDTO = AreaUtils.getAreaByName(provinceName);
                if (areaInfoDTO != null) {
                    provinceAreaCode = areaInfoDTO.getCode();
                }
                province.setAreaCode(provinceAreaCode);
                sql.append("(" + province.getAreaId() + ",0,'" + provinceName + "',1,'" + provinceAreaCode + "');");
                System.out.println(sql.toString());

                areaId ++;
                //下级地市
                List<AreaInfo> cityList = AreaUtils.getCityListByProvince(provinceName);
                for (int j = 0; j < cityList.size(); j++) {
                    AreaInfo cityInfo = cityList.get(j);
                    if(cityInfo.getCity().equals(provinceName)){
                        continue;
                    }
                    StringBuffer citySql = new StringBuffer();
                    citySql.append("insert into s_area(area_id,parent_area_id,area_name,area_level,area_code) values");
                    citySql.append("(" + areaId + "," + province.getAreaId()
                            + ",'" + cityInfo.getCity() + "',2,'" + cityInfo.getCode() + "');");
                    System.out.println(citySql);
                    areaId ++;
                }

//                areaService.addArea(province);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testGetAllArea(){
        try{
            List<Area> allAreas = areaService.getAllAreas();
            Assert.assertNotNull(allAreas);
            Assert.assertTrue(allAreas.size() > 0);
            allAreas = areaService.getAllAreas();
            System.out.println(JsonUtils.Object2Json(allAreas));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Test
    public void testGetAllProvinces(){
        try{
            List<Area> allProvinces = areaService.getAllProvinces();
            Assert.assertNotNull(allProvinces);
            Assert.assertTrue(allProvinces.size() > 0);
            allProvinces = areaService.getAllProvinces();
            System.out.println(JsonUtils.Object2Json(allProvinces));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
