package test.com.yepit.util;

import com.yepit.cs.cache.AdminCache;
import com.yepit.cs.domain.SysAdmin;
import com.yepit.cs.dto.admin.AdminDTO;
import com.yepit.cs.service.AdminService;
import com.yepit.cs.util.RedisUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.com.yepit.service.BaseTest;

/**
 * Created by qianlong on 2017/10/18.
 */
public class TestRedisUtil extends BaseTest{

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private AdminService adminService;

    @Autowired
    private AdminCache adminCache;

    @Test
    public void testSet(){
        try{
            redisUtils.set("user_qianlong","qianlong");

            Object user = redisUtils.get("user_qianlong");
            Assert.assertNotNull(user);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Test
    public void hmSet(){
        try{
            adminCache.reload();
            AdminDTO admin = adminCache.getAdminByLoginName("qianlong");
            Assert.assertNotNull(admin);
            System.out.println(admin.getRealname());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
