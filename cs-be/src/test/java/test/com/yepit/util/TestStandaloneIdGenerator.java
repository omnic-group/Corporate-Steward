package test.com.yepit.util;

import com.yepit.cs.util.idgenerate.DefaultIdGenerator;
import com.yepit.cs.util.idgenerate.IdGenerator;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by qianlong on 2017/8/17.
 */
public class TestStandaloneIdGenerator {

    @Test
    public void test1(){
        IdGenerator idGenerator = new DefaultIdGenerator();

        System.out.println("--------简单测试------------------");
        for (int i=0; i<100; i++){
            System.out.println(idGenerator.next().length());
        }
    }

    @Test
    public void test2(){
        IdGenerator idGenerator = new DefaultIdGenerator();

        //多线程测试
        System.out.println("--------多线程测试不重复------------------");
        Set<String> idSet = Collections.synchronizedSet(new HashSet<>());
        ExecutorService es = Executors.newFixedThreadPool(100);
        for (int i=0; i<2000000; i++){
            es.submit(() -> {
                String val = idGenerator.next();
                if (idSet.contains(val)){
                    System.out.println("重复了: " + val);
                }else{
                    idSet.add(val);
                }
            });
        }
        es.shutdown();
        System.out.println("启用顺序关闭");
        while(true){
            if(es.isTerminated()){
                System.out.println("所有的子线程都结束了！");
                break;
            }
            try {
                System.out.println("子线程的任务还没运行完");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("共生成: " + idSet.size() + "个");
    }
}
