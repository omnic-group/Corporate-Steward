package test.com.voicetalk;

import com.voicetalk.CallResponseOfInt32;
import com.voicetalk.CallResponseOfString;
import com.voicetalk.VoiceTask2SoapProxy;
import com.yepit.cs.util.encrypt.MD5Utils;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/2/20 9:05 AM
 **/
public class VoiceTalkTest {

    @Test
    public void sendVoiceOTPTest() {
        try {
            VoiceTask2SoapProxy voiceTask = new VoiceTask2SoapProxy();
            String userCode = "yxtitf";
            String pwd = "sP9VCwYU@e";
//            String timestamp = String.valueOf(System.currentTimeMillis()/1000);
            long timeStampSec = System.currentTimeMillis() / 1000;
            String timestamp = String.format("%010d", timeStampSec);
            System.out.println("timestamp=" + timestamp);
            System.out.println("timestamp.len=" + timestamp.length());
            String encodePwd = MD5Utils.encode(MD5Utils.encode(userCode + MD5Utils.encode(pwd)) + timestamp);
            int voiceType = 0;
            String callNumber = "02566040809";
            String[] recvNumber = {"15301588194"};
            String[] recvMessage = {"您的验证码为123456,1分钟内有效"};
            int retryNum = 1;
            int retryInterval = 30;
            System.out.println("encodePwd:" + encodePwd);
            CallResponseOfString resp = voiceTask.sendVCode(userCode,encodePwd,timestamp,"voice","15301588194","123456");
//            CallResponseOfInt32 resp = voiceTask.createSelfTask(userCode, encodePwd, timestamp, voiceType, callNumber, recvNumber, recvMessage, retryNum, retryInterval);
            Assert.assertTrue(resp != null);
            System.out.println(resp.isSuccess());
            System.out.println(resp.getResult());
            System.out.println(resp.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void main(String args[]) throws Exception {


    }
}
