package test.com.imsp.service;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.yepit.cs.constant.IsmpOrderTradeTypeEnum;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.domain.ThirdPartyCallLog;
import com.yepit.cs.dto.ismp.*;
import com.yepit.cs.mapper.ThirdPartyCallLogMapper;
import com.yepit.cs.service.IsmpOrderService;
import com.yepit.cs.common.BaseResponse;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.com.yepit.service.BaseTest;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/8/3 8:47 PM
 **/
public class IsmpOrderServiceTest extends BaseTest {

    @Autowired
    private IsmpOrderService ismpOrderService;

    @Autowired
    private ThirdPartyCallLogMapper thirdPartyCallLogMapper;

    @Test
    public void getOrderInfoTest() {
        GetIsmpOrderRequest getOrderIdRequest = new GetIsmpOrderRequest();
        getOrderIdRequest.setChargeId("2110");
        BaseResponse resp = ismpOrderService.getOrderInfo(getOrderIdRequest);
        System.out.println(JSONUtil.toJsonStr(resp));
    }

    @Test
    public void getOrderIdTest() {
        BaseResponse resp = ismpOrderService.getOrderId("2110", null, null);
        System.out.println(JSONUtil.toJsonStr(resp));
    }

    @Test
    public void getOTPTest() {
        ValidSubRequest request = new ValidSubRequest();
        request.setChargeId("2110");
        request.setPhoneNumber("15301588194");
        BaseResponse<String> resp = ismpOrderService.getOTP(request);
        System.out.println(JSONUtil.toJsonStr(resp));
    }

    @Test
    public void subscribeProductTest() {
        String orderId = "201908032255031128741";
        String otp = "6759";
        IsmpSubscribeProductRequest subRequest = new IsmpSubscribeProductRequest();
        subRequest.setOrderId(orderId);
        subRequest.setPhoneNumber("15301588194");
        subRequest.setOtp(otp);
        BaseResponse resp = ismpOrderService.subscribeProduct(subRequest);
        System.out.println(JSONUtil.toJsonStr(resp));
    }

    @Test
    public void unSubscribeProductTest() {
        IsmpUnSubscribeProductRequest request = new IsmpUnSubscribeProductRequest();
        request.setChargeId("2110");
        request.setPhoneNum("15301588194");
        BaseResponse resp = ismpOrderService.unSubscribeProduct(request);
        System.out.println(JSONUtil.toJsonStr(resp));
    }

    @Test
    public void createPreOrderTest() {
        IsmpPreOrderRequest preOrderRequest = new IsmpPreOrderRequest();
        preOrderRequest.setPhoneNumber("15301588194");
        preOrderRequest.setIsmpSerial(IdUtil.fastSimpleUUID());
        preOrderRequest.setProductCode("111000000000000231853");
        preOrderRequest.setTradeType(IsmpOrderTradeTypeEnum.OrderMonthly.getValue());
        BaseResponse resp = ismpOrderService.createPreOrder(preOrderRequest);
        System.out.println(JSONUtil.toJsonStr(resp));
    }

    @Test
    public void placeOrderTest(){
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductCode("111000000000000232099");
        String phoneNumber = "15301588194";
        Integer tradeType = 2;
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("productCode", productInfo.getProductCode());
        params.put("phoneNumber", phoneNumber);
        params.put("tradeType", tradeType);

        Date now = new Date();
        Date startTimeDate = DateUtil.offset(now, DateField.MINUTE, -30);
        String startTime = DateUtil.formatDateTime(startTimeDate);
        String endTime = DateUtil.formatDateTime(now);
        List<ThirdPartyCallLog> logList = thirdPartyCallLogMapper.listByCond(2, JSON.toJSONString(params), startTime, endTime);
        System.out.println(logList.size());
    }
}
