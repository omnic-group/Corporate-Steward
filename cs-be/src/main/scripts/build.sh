#!/usr/bin/env bash
cd ../../../
docker stop steward-dev && docker rm steward-dev
docker rmi -f steward
mvn clean package docker:build -Dmaven.test.skip=true