/**
 * TaskResultEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.voicetalk;

public class TaskResultEntity  implements java.io.Serializable {
    private int serialNumber;

    private String displayNumber;

    private String calleeNumber;

    private java.util.Calendar time;

    private Integer duration;

    private String pressKey;

    private String failedReason;

    private String numRemark;

    private Integer resultCode;

    public TaskResultEntity() {
    }

    public TaskResultEntity(
           int serialNumber,
           String displayNumber,
           String calleeNumber,
           java.util.Calendar time,
           Integer duration,
           String pressKey,
           String failedReason,
           String numRemark,
           Integer resultCode) {
           this.serialNumber = serialNumber;
           this.displayNumber = displayNumber;
           this.calleeNumber = calleeNumber;
           this.time = time;
           this.duration = duration;
           this.pressKey = pressKey;
           this.failedReason = failedReason;
           this.numRemark = numRemark;
           this.resultCode = resultCode;
    }


    /**
     * Gets the serialNumber value for this TaskResultEntity.
     * 
     * @return serialNumber
     */
    public int getSerialNumber() {
        return serialNumber;
    }


    /**
     * Sets the serialNumber value for this TaskResultEntity.
     * 
     * @param serialNumber
     */
    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }


    /**
     * Gets the displayNumber value for this TaskResultEntity.
     * 
     * @return displayNumber
     */
    public String getDisplayNumber() {
        return displayNumber;
    }


    /**
     * Sets the displayNumber value for this TaskResultEntity.
     * 
     * @param displayNumber
     */
    public void setDisplayNumber(String displayNumber) {
        this.displayNumber = displayNumber;
    }


    /**
     * Gets the calleeNumber value for this TaskResultEntity.
     * 
     * @return calleeNumber
     */
    public String getCalleeNumber() {
        return calleeNumber;
    }


    /**
     * Sets the calleeNumber value for this TaskResultEntity.
     * 
     * @param calleeNumber
     */
    public void setCalleeNumber(String calleeNumber) {
        this.calleeNumber = calleeNumber;
    }


    /**
     * Gets the time value for this TaskResultEntity.
     * 
     * @return time
     */
    public java.util.Calendar getTime() {
        return time;
    }


    /**
     * Sets the time value for this TaskResultEntity.
     * 
     * @param time
     */
    public void setTime(java.util.Calendar time) {
        this.time = time;
    }


    /**
     * Gets the duration value for this TaskResultEntity.
     * 
     * @return duration
     */
    public Integer getDuration() {
        return duration;
    }


    /**
     * Sets the duration value for this TaskResultEntity.
     * 
     * @param duration
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }


    /**
     * Gets the pressKey value for this TaskResultEntity.
     * 
     * @return pressKey
     */
    public String getPressKey() {
        return pressKey;
    }


    /**
     * Sets the pressKey value for this TaskResultEntity.
     * 
     * @param pressKey
     */
    public void setPressKey(String pressKey) {
        this.pressKey = pressKey;
    }


    /**
     * Gets the failedReason value for this TaskResultEntity.
     * 
     * @return failedReason
     */
    public String getFailedReason() {
        return failedReason;
    }


    /**
     * Sets the failedReason value for this TaskResultEntity.
     * 
     * @param failedReason
     */
    public void setFailedReason(String failedReason) {
        this.failedReason = failedReason;
    }


    /**
     * Gets the numRemark value for this TaskResultEntity.
     * 
     * @return numRemark
     */
    public String getNumRemark() {
        return numRemark;
    }


    /**
     * Sets the numRemark value for this TaskResultEntity.
     * 
     * @param numRemark
     */
    public void setNumRemark(String numRemark) {
        this.numRemark = numRemark;
    }


    /**
     * Gets the resultCode value for this TaskResultEntity.
     * 
     * @return resultCode
     */
    public Integer getResultCode() {
        return resultCode;
    }


    /**
     * Sets the resultCode value for this TaskResultEntity.
     * 
     * @param resultCode
     */
    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof TaskResultEntity)) return false;
        TaskResultEntity other = (TaskResultEntity) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.serialNumber == other.getSerialNumber() &&
            ((this.displayNumber==null && other.getDisplayNumber()==null) || 
             (this.displayNumber!=null &&
              this.displayNumber.equals(other.getDisplayNumber()))) &&
            ((this.calleeNumber==null && other.getCalleeNumber()==null) || 
             (this.calleeNumber!=null &&
              this.calleeNumber.equals(other.getCalleeNumber()))) &&
            ((this.time==null && other.getTime()==null) || 
             (this.time!=null &&
              this.time.equals(other.getTime()))) &&
            ((this.duration==null && other.getDuration()==null) || 
             (this.duration!=null &&
              this.duration.equals(other.getDuration()))) &&
            ((this.pressKey==null && other.getPressKey()==null) || 
             (this.pressKey!=null &&
              this.pressKey.equals(other.getPressKey()))) &&
            ((this.failedReason==null && other.getFailedReason()==null) || 
             (this.failedReason!=null &&
              this.failedReason.equals(other.getFailedReason()))) &&
            ((this.numRemark==null && other.getNumRemark()==null) || 
             (this.numRemark!=null &&
              this.numRemark.equals(other.getNumRemark()))) &&
            ((this.resultCode==null && other.getResultCode()==null) || 
             (this.resultCode!=null &&
              this.resultCode.equals(other.getResultCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSerialNumber();
        if (getDisplayNumber() != null) {
            _hashCode += getDisplayNumber().hashCode();
        }
        if (getCalleeNumber() != null) {
            _hashCode += getCalleeNumber().hashCode();
        }
        if (getTime() != null) {
            _hashCode += getTime().hashCode();
        }
        if (getDuration() != null) {
            _hashCode += getDuration().hashCode();
        }
        if (getPressKey() != null) {
            _hashCode += getPressKey().hashCode();
        }
        if (getFailedReason() != null) {
            _hashCode += getFailedReason().hashCode();
        }
        if (getNumRemark() != null) {
            _hashCode += getNumRemark().hashCode();
        }
        if (getResultCode() != null) {
            _hashCode += getResultCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TaskResultEntity.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "TaskResultEntity"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serialNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "SerialNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DisplayNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calleeNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CalleeNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("time");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("duration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Duration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pressKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "PressKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("failedReason");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "FailedReason"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numRemark");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "NumRemark"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ResultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
