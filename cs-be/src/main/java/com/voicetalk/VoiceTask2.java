/**
 * VoiceTask2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.voicetalk;

public interface VoiceTask2 extends javax.xml.rpc.Service {
    public String getVoiceTask2SoapAddress();

    public VoiceTask2Soap getVoiceTask2Soap() throws javax.xml.rpc.ServiceException;

    public VoiceTask2Soap getVoiceTask2Soap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
