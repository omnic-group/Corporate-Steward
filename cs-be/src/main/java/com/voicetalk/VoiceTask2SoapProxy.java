package com.voicetalk;

public class VoiceTask2SoapProxy implements VoiceTask2Soap {
  private String _endpoint = null;
  private VoiceTask2Soap voiceTask2Soap = null;
  
  public VoiceTask2SoapProxy() {
    _initVoiceTask2SoapProxy();
  }
  
  public VoiceTask2SoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initVoiceTask2SoapProxy();
  }
  
  private void _initVoiceTask2SoapProxy() {
    try {
      voiceTask2Soap = (new VoiceTask2Locator()).getVoiceTask2Soap();
      if (voiceTask2Soap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)voiceTask2Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)voiceTask2Soap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (voiceTask2Soap != null)
      ((javax.xml.rpc.Stub)voiceTask2Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public VoiceTask2Soap getVoiceTask2Soap() {
    if (voiceTask2Soap == null)
      _initVoiceTask2SoapProxy();
    return voiceTask2Soap;
  }
  
  public CallResponseOfInt32 createCommonTask(String userCode, String pwd, String timestamp, String callNumber, String[] recvNumber, String recvMessage, int retryNum, int retryInterval) throws java.rmi.RemoteException{
    if (voiceTask2Soap == null)
      _initVoiceTask2SoapProxy();
    return voiceTask2Soap.createCommonTask(userCode, pwd, timestamp, callNumber, recvNumber, recvMessage, retryNum, retryInterval);
  }
  
  public CallResponseOfInt32 createSelfTask(String userCode, String pwd, String timestamp, int voiceType, String callNumber, String[] recvNumber, String[] recvMessage, int retryNum, int retryInterval) throws java.rmi.RemoteException{
    if (voiceTask2Soap == null)
      _initVoiceTask2SoapProxy();
    return voiceTask2Soap.createSelfTask(userCode, pwd, timestamp, voiceType, callNumber, recvNumber, recvMessage, retryNum, retryInterval);
  }
  
  public CallResponseOfArrayOfTaskResultEntity getResult(String userCode, String pwd, String timestamp, int taskId, int page, int size) throws java.rmi.RemoteException{
    if (voiceTask2Soap == null)
      _initVoiceTask2SoapProxy();
    return voiceTask2Soap.getResult(userCode, pwd, timestamp, taskId, page, size);
  }
  
  public CallResponseOfString sendVCode(String userCode, String pwd, String timestamp, String type, String receiveNumber, String vcode) throws java.rmi.RemoteException{
    if (voiceTask2Soap == null)
      _initVoiceTask2SoapProxy();
    return voiceTask2Soap.sendVCode(userCode, pwd, timestamp, type, receiveNumber, vcode);
  }
  
  
}