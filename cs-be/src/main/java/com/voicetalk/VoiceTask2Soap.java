/**
 * VoiceTask2Soap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.voicetalk;

public interface VoiceTask2Soap extends java.rmi.Remote {

    /**
     * 创建普通外呼任务
     */
    public CallResponseOfInt32 createCommonTask(String userCode, String pwd, String timestamp, String callNumber, String[] recvNumber, String recvMessage, int retryNum, int retryInterval) throws java.rmi.RemoteException;

    /**
     * 创建个性外呼任务
     */
    public CallResponseOfInt32 createSelfTask(String userCode, String pwd, String timestamp, int voiceType, String callNumber, String[] recvNumber, String[] recvMessage, int retryNum, int retryInterval) throws java.rmi.RemoteException;

    /**
     * 获取呼叫结果
     */
    public CallResponseOfArrayOfTaskResultEntity getResult(String userCode, String pwd, String timestamp, int taskId, int page, int size) throws java.rmi.RemoteException;

    /**
     * 发送验证码
     */
    public CallResponseOfString sendVCode(String userCode, String pwd, String timestamp, String type, String receiveNumber, String vcode) throws java.rmi.RemoteException;
}
