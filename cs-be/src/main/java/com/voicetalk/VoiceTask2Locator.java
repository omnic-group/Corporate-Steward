/**
 * VoiceTask2Locator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.voicetalk;

public class VoiceTask2Locator extends org.apache.axis.client.Service implements VoiceTask2 {

    public VoiceTask2Locator() {
    }


    public VoiceTask2Locator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public VoiceTask2Locator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for VoiceTask2Soap
    private String VoiceTask2Soap_address = "http://47.97.124.228:9200/ws/VoiceTask2.asmx";

    public String getVoiceTask2SoapAddress() {
        return VoiceTask2Soap_address;
    }

    // The WSDD service name defaults to the port name.
    private String VoiceTask2SoapWSDDServiceName = "VoiceTask2Soap";

    public String getVoiceTask2SoapWSDDServiceName() {
        return VoiceTask2SoapWSDDServiceName;
    }

    public void setVoiceTask2SoapWSDDServiceName(String name) {
        VoiceTask2SoapWSDDServiceName = name;
    }

    public VoiceTask2Soap getVoiceTask2Soap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(VoiceTask2Soap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getVoiceTask2Soap(endpoint);
    }

    public VoiceTask2Soap getVoiceTask2Soap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            VoiceTask2SoapStub _stub = new VoiceTask2SoapStub(portAddress, this);
            _stub.setPortName(getVoiceTask2SoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setVoiceTask2SoapEndpointAddress(String address) {
        VoiceTask2Soap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (VoiceTask2Soap.class.isAssignableFrom(serviceEndpointInterface)) {
                VoiceTask2SoapStub _stub = new VoiceTask2SoapStub(new java.net.URL(VoiceTask2Soap_address), this);
                _stub.setPortName(getVoiceTask2SoapWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("VoiceTask2Soap".equals(inputPortName)) {
            return getVoiceTask2Soap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "VoiceTask2");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "VoiceTask2Soap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {
        
if ("VoiceTask2Soap".equals(portName)) {
            setVoiceTask2SoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
