package com.yepit.cs.vo;

import com.yepit.cs.domain.ProductBlackList;
import lombok.Data;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/6/26 4:18 下午
 **/
@Data
public class ProductBlackRuleVO extends ProductBlackList implements Serializable {

    private static final long serialVersionUID = -4742962569705193915L;

    private String productCode;

    private String productName;
}
