package com.yepit.cs.interceptor;

import com.yepit.cs.cache.AdminCache;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.constant.SessionConstant;
import com.yepit.cs.domain.SysAdmin;
import com.yepit.cs.dto.admin.AdminDTO;
import com.yepit.cs.util.JsonUtils;
import com.yepit.cs.util.RedisUtils;
import com.yepit.cs.util.token.JWTUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by qianlong on 2017/8/20.
 */
public class AppInterceptor implements HandlerInterceptor {

    private static final Logger LOGGER = Logger.getLogger(AppInterceptor.class);

    @Value("${system.sessionVaild}")
    private Integer sessionVaildSwitch;//session校验开关

    @Autowired
    private JWTUtils jwtUtils;

    @Autowired
    private AdminCache adminCache;

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object handler) throws Exception {
        //如果不是映射到方法直接通过
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        String url = httpServletRequest.getRequestURL().toString();
        if (url.indexOf("login") >= 0 || url.indexOf("auth") >= 0
                || url.indexOf("logout") >= 0 || url.indexOf("files") >= 0
                || url.indexOf("druid") >= 0 || url.indexOf("sso") >= 0
                || url.indexOf("otp") >= 0 || url.indexOf("subscriberViaWechat") >= 0
                || url.indexOf("ismp") >= 0 || url.indexOf("ismp/v2") >= 0
                || url.indexOf("test") >= 0 ) {
            return true;
        }
        Object accessTokenObj = httpServletRequest.getHeader("accessToken");
        //如果没有accessToken参数,则不允许访问
        if (accessTokenObj == null) {
            accessTokenObj = httpServletRequest.getParameter("accessToken");
            if (accessTokenObj == null){
                this.setNoAuthority(httpServletResponse, "无权限访问");
                return false;
            }
        } else {
            String accessToken = accessTokenObj.toString();
            //校验token是否已登记
            if (redisUtils.get(accessToken) == null) {
                this.setNoAuthority(httpServletResponse, "token已过期");
                return false;
            }
            //判断token中的数据是否正确
            String loginName = jwtUtils.getUsernameFromToken(accessToken);
            AdminDTO admin = adminCache.getAdminByLoginName(loginName);
            if (admin == null) {
                this.setNoAuthority(httpServletResponse, "无权限访问");
                return false;
            }
            HttpSession httpSession = httpServletRequest.getSession();
            httpSession.setAttribute(SessionConstant.CURRENT_ADMIN, admin);
        }
        return true;
    }

    private void setNoAuthority(HttpServletResponse httpServletResponse, String errMsg) throws Exception {
        BaseResponse resp = new BaseResponse();
        resp.setSuccess(false);
        resp.setResultCode(ResultCodeConstant.LOGIN_TIMEOUT);
        resp.setResultDesc(errMsg);
        httpServletResponse.setHeader("Content-type", "text/html;charset=UTF-8");
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.getOutputStream().write(JsonUtils.Object2Json(resp).getBytes());
        httpServletResponse.setStatus(401);
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
