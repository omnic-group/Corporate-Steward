package com.yepit.cs.rabbit;

import cn.hutool.core.text.StrFormatter;
import com.alibaba.fastjson.JSON;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.IsmpPlaceOrderRequestStatusEnum;
import com.yepit.cs.domain.IsmpPlaceOrderRequest;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.service.IsmpOrderService;
import com.yepit.cs.service.IsmpPlaceOrderRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.yepit.cs.rabbit.RabbitConstant.ISMP_PLACE_ORDER_QUEUE;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2021/6/19 9:48 下午
 **/
@Component
@Slf4j
public class IsmpPlaceOrderConsumer {

    @Autowired
    private IsmpOrderService ismpOrderService;

    @Autowired
    private IsmpPlaceOrderRequestService ismpPlaceOrderRequestService;

    @RabbitHandler
    @RabbitListener(queues = ISMP_PLACE_ORDER_QUEUE)
    public void issmpPlaceOrderRequestOnMessage(String message) {
        log.info(StrFormatter.format("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message));
        IsmpPlaceOrderRequest ismpPlaceOrderRequest = JSON.parseObject(message, IsmpPlaceOrderRequest.class);
        BaseResponse resp = null;
        try {
            resp = ismpOrderService.placeOrder(ismpPlaceOrderRequest);
        } catch (ServiceException ex) {
            resp = new BaseResponse().setFailResp(ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            resp = new BaseResponse().setFailResp("999999", ex.getMessage());
        }
        log.info(StrFormatter.format("处理ISMP订单结果:{}", JSON.toJSONString(resp)));
        ismpPlaceOrderRequest.setProcessTime(new Date());
        ismpPlaceOrderRequest.setStatus(IsmpPlaceOrderRequestStatusEnum.FINISH.getValue());
        ismpPlaceOrderRequest.setProcessResult(resp.getResultDesc());
        ismpPlaceOrderRequestService.updateByPrimaryKey(ismpPlaceOrderRequest);
    }
}
