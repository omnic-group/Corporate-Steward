package com.yepit.cs.rabbit;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.yepit.cs.rabbit.RabbitConstant.ISMP_PLACE_ORDER_EXCHANGE;
import static com.yepit.cs.rabbit.RabbitConstant.ISMP_PLACE_ORDER_QUEUE;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2021/6/19 9:41 下午
 **/
@Configuration
public class RabbitConfig {

    /**
     * Direct Exchange配置类
     */
    public static class DirectExchangeConfiguration {
        // 创建 Queue
        @Bean
        public Queue ismpPlaceOrderQueue() {
            return new Queue(ISMP_PLACE_ORDER_QUEUE, // Queue 名字
                    true, // durable: 是否持久化
                    false, // exclusive: 是否排它
                    false); // autoDelete: 是否自动删除
        }

        // 创建 Direct Exchange
        @Bean
        public DirectExchange ismpPlaceOrderExchange() {
            return new DirectExchange(ISMP_PLACE_ORDER_EXCHANGE,
                    true,  // durable: 是否持久化
                    false);  // exclusive: 是否排它
        }
    }
}
