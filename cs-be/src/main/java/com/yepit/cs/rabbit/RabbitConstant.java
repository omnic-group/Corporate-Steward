package com.yepit.cs.rabbit;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2021/6/19 9:40 下午
 **/
public interface RabbitConstant {

    String ISMP_PLACE_ORDER_QUEUE = "ismp-place-order-queue";

    String ISMP_PLACE_ORDER_EXCHANGE = "ismp-place-order-exchange";
}
