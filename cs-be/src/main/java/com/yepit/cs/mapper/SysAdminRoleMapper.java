package com.yepit.cs.mapper;

import com.yepit.cs.domain.SysAdminRole;

import java.util.List;

public interface SysAdminRoleMapper {
    int deleteByPrimaryKey(Long adminRoleId);

    int insert(SysAdminRole record);

    int insertSelective(SysAdminRole record);

    SysAdminRole selectByPrimaryKey(Long adminRoleId);

    int updateByPrimaryKeySelective(SysAdminRole record);

    int updateByPrimaryKey(SysAdminRole record);

    List<SysAdminRole> listByCond(SysAdminRole cond);
}