package com.yepit.cs.mapper;
import org.apache.ibatis.annotations.Param;

import com.yepit.cs.domain.IsmpPlaceOrderRequest;

import java.util.List;

public interface IsmpPlaceOrderRequestMapper {
    /**
     * delete by primary key
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(IsmpPlaceOrderRequest record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(IsmpPlaceOrderRequest record);

    /**
     * select by primary key
     * @param id primary key
     * @return object by primary key
     */
    IsmpPlaceOrderRequest selectByPrimaryKey(Long id);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(IsmpPlaceOrderRequest record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(IsmpPlaceOrderRequest record);

    /**
     * 根据状态查询数据
     * @param status
     * @return
     */
    List<IsmpPlaceOrderRequest> findByStatus(@Param("status")Integer status);


}