package com.yepit.cs.mapper;

import com.yepit.cs.domain.StatProductSubDay;

import java.util.List;

public interface StatProductSubDayMapper {
    int deleteByPrimaryKey(Long statId);

    int insert(StatProductSubDay record);

    int insertSelective(StatProductSubDay record);

    StatProductSubDay selectByPrimaryKey(Long statId);

    int updateByPrimaryKeySelective(StatProductSubDay record);

    int updateByPrimaryKey(StatProductSubDay record);

    List<StatProductSubDay> executeStat(StatProductSubDay statCondition);

    int deleteByStatDay(String statDay);

    int insertBatch(List<StatProductSubDay> recordList);

    List<StatProductSubDay> selectByCond(StatProductSubDay statCondition);
}