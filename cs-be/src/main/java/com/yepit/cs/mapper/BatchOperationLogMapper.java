package com.yepit.cs.mapper;

import com.yepit.cs.domain.BatchOperationLog;
import com.yepit.cs.dto.batch.SearchBatchLogRequest;

import java.util.List;

public interface BatchOperationLogMapper {
    int deleteByPrimaryKey(String batchId);

    int insert(BatchOperationLog record);

    int insertSelective(BatchOperationLog record);

    BatchOperationLog selectByPrimaryKey(String batchId);

    int updateByPrimaryKeySelective(BatchOperationLog record);

    int updateByPrimaryKey(BatchOperationLog record);

    public List<BatchOperationLog> listByCond(SearchBatchLogRequest request);
}