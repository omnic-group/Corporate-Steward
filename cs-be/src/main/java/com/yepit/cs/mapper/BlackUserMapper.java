package com.yepit.cs.mapper;

import com.yepit.cs.domain.BlackUser;

import java.util.List;

public interface BlackUserMapper {
    int deleteByPrimaryKey(String blackUserId);

    int insert(BlackUser record);

    int insertSelective(BlackUser record);

    BlackUser selectByPrimaryKey(String blackUserId);

    int updateByPrimaryKeySelective(BlackUser record);

    int updateByPrimaryKey(BlackUser record);

    List<BlackUser> listByCond(BlackUser cond);
}