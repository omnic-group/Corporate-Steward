package com.yepit.cs.mapper;

import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.product.PageSearchProductRequest;

import java.util.List;

public interface ProductInfoMapper {
    int deleteByPrimaryKey(String productId);

    int deleteByProductCode(String productCode);

    int insert(ProductInfo record);

    int insertSelective(ProductInfo record);

    ProductInfo selectByPrimaryKey(String productId);

    int updateByPrimaryKeySelective(ProductInfo record);

    int updateByPrimaryKey(ProductInfo record);

    List<ProductInfo> listByCond(ProductInfo cond);

    List<ProductInfo> pageList(PageSearchProductRequest cond);

}