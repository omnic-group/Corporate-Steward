package com.yepit.cs.mapper;

import com.yepit.cs.domain.OrderAttachment;

public interface OrderAttachmentMapper {
    int deleteByPrimaryKey(String orderAttachmentId);

    int insert(OrderAttachment record);

    int insertSelective(OrderAttachment record);

    OrderAttachment selectByPrimaryKey(String orderAttachmentId);

    int updateByPrimaryKeySelective(OrderAttachment record);

    int updateByPrimaryKey(OrderAttachment record);
}