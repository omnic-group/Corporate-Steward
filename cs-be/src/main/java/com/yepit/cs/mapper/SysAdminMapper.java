package com.yepit.cs.mapper;

import com.yepit.cs.domain.SysAdmin;
import com.yepit.cs.dto.admin.PageSearchAdminResponse;

import java.util.List;

public interface SysAdminMapper {
    int deleteByPrimaryKey(Long adminId);

    int insert(SysAdmin record);

    int insertSelective(SysAdmin record);

    SysAdmin selectByPrimaryKey(Long adminId);

    int updateByPrimaryKeySelective(SysAdmin record);

    int updateByPrimaryKey(SysAdmin record);

    List<SysAdmin> listByCond(SysAdmin cond);

    List<PageSearchAdminResponse> pageListByCond(SysAdmin cond);
}