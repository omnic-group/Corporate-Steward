package com.yepit.cs.mapper;

import com.yepit.cs.domain.UserSubscribeInfo;

import java.util.List;

public interface UserSubscribeMapper {
    List<UserSubscribeInfo> listByCond(UserSubscribeInfo cond);
}