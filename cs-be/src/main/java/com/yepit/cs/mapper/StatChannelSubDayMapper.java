package com.yepit.cs.mapper;

import com.yepit.cs.domain.StatChannelSubDay;

import java.util.List;

public interface StatChannelSubDayMapper {
    int deleteByPrimaryKey(Long statId);

    int insert(StatChannelSubDay record);

    int insertSelective(StatChannelSubDay record);

    StatChannelSubDay selectByPrimaryKey(Long statId);

    int updateByPrimaryKeySelective(StatChannelSubDay record);

    int updateByPrimaryKey(StatChannelSubDay record);

    int deleteByStatDay(String statDay);

    List<StatChannelSubDay> executeStat(StatChannelSubDay statCondition);

    void insertBatch(List<StatChannelSubDay> recordList);

    List<StatChannelSubDay> sumByCond(StatChannelSubDay queryCond);

}