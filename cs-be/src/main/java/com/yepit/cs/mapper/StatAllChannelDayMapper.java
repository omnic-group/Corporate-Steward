package com.yepit.cs.mapper;

import com.yepit.cs.domain.StatAllChannelDay;

import java.util.List;

public interface StatAllChannelDayMapper {
    int deleteByPrimaryKey(Long statId);

    int insert(StatAllChannelDay record);

    int insertSelective(StatAllChannelDay record);

    StatAllChannelDay selectByPrimaryKey(Long statId);

    int updateByPrimaryKeySelective(StatAllChannelDay record);

    int updateByPrimaryKey(StatAllChannelDay record);

    int deleteByStatDay(String statDay);

    List<StatAllChannelDay> executeStat(StatAllChannelDay statCondition);

    int insertBatch(List<StatAllChannelDay> recordList);

    List<StatAllChannelDay> selectByCond(StatAllChannelDay queryCond);

}