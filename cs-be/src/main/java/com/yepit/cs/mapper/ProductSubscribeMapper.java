package com.yepit.cs.mapper;

import com.yepit.cs.domain.ProductSubscribe;
import com.yepit.cs.dto.order.SearchSubscriptionRequest;

import java.util.List;

public interface ProductSubscribeMapper {
    int deleteByPrimaryKey(String subscriptionId);

    int deleteProductSubscribe(ProductSubscribe record);

    int insert(ProductSubscribe record);

    int insertSelective(ProductSubscribe record);

    ProductSubscribe selectByPrimaryKey(String subscriptionId);

    int updateByPrimaryKeySelective(ProductSubscribe record);

    int updateByPrimaryKey(ProductSubscribe record);

    List<ProductSubscribe> listByCond(ProductSubscribe cond);

    List<ProductSubscribe> listByProduct(SearchSubscriptionRequest cond);
}