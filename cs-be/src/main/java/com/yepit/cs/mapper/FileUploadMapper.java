package com.yepit.cs.mapper;

import com.yepit.cs.domain.FileUpload;

public interface FileUploadMapper {
    int deleteByPrimaryKey(String fileId);

    int insert(FileUpload record);

    int insertSelective(FileUpload record);

    FileUpload selectByPrimaryKey(String fileId);

    int updateByPrimaryKeySelective(FileUpload record);

    int updateByPrimaryKey(FileUpload record);
}