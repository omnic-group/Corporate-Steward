package com.yepit.cs.mapper;

import com.yepit.cs.domain.OrderLog;

public interface OrderLogMapper {
    int deleteByPrimaryKey(String orderLogId);

    int insert(OrderLog record);

    int insertSelective(OrderLog record);

    OrderLog selectByPrimaryKey(String orderLogId);

    int updateByPrimaryKeySelective(OrderLog record);

    int updateByPrimaryKey(OrderLog record);
}