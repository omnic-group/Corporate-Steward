package com.yepit.cs.mapper;

import com.yepit.cs.domain.SysMenu;
import com.yepit.cs.domain.SysRoleAuth;

import java.util.List;

public interface SysRoleAuthMapper {
    int deleteByPrimaryKey(Long roleAuthId);

    int insert(SysRoleAuth record);

    int insertSelective(SysRoleAuth record);

    SysRoleAuth selectByPrimaryKey(Long roleAuthId);

    int updateByPrimaryKeySelective(SysRoleAuth record);

    int updateByPrimaryKey(SysRoleAuth record);

    List<SysMenu> listByRoleId(Long roleId);
}