package com.yepit.cs.dto.stat;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by qianlong on 2017/10/7.
 */
public class StatByChannelResponse implements Serializable {

    private static final long serialVersionUID = -5392655849857150863L;

    private LinkedHashMap<String,List<StatByChannelResponse.StatResult>> statResult;

    public LinkedHashMap<String, List<StatByChannelResponse.StatResult>> getStatResult() {
        return statResult;
    }

    public void setStatResult(LinkedHashMap<String, List<StatByChannelResponse.StatResult>> statResult) {
        this.statResult = statResult;
    }

    public class StatResult implements Serializable {

        private static final long serialVersionUID = -5392655849857150863L;

        private String statObject;

        private Long statNum;

        public String getStatObject() {
            return statObject;
        }

        public void setStatObject(String statObject) {
            this.statObject = statObject;
        }

        public Long getStatNum() {
            return statNum;
        }

        public void setStatNum(Long statNum) {
            this.statNum = statNum;
        }
    }
}
