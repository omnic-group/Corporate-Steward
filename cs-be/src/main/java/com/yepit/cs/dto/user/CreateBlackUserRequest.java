package com.yepit.cs.dto.user;

import com.yepit.cs.common.BaseRequest;

/**
 * Created by qianlong on 2017/8/31.
 */
public class CreateBlackUserRequest extends BaseRequest{

    private String phonenumber;

    private Integer blackType;

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Integer getBlackType() {
        return blackType;
    }

    public void setBlackType(Integer blackType) {
        this.blackType = blackType;
    }
}
