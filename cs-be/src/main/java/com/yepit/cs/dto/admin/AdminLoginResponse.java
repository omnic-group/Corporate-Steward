package com.yepit.cs.dto.admin;

import com.yepit.cs.domain.SysAdmin;

import java.util.List;

/**
 *
 * @author qianlong
 * @date 2017/8/19
 */
public class AdminLoginResponse{

    private String accessToken;
    private SysAdmin admin;
    private List<MenuDTO> adminMenus;

    public SysAdmin getAdmin() {
        return admin;
    }

    public void setAdmin(SysAdmin admin) {
        this.admin = admin;
    }

    public List<MenuDTO> getAdminMenus() {
        return adminMenus;
    }

    public void setAdminMenus(List<MenuDTO> adminMenus) {
        this.adminMenus = adminMenus;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
