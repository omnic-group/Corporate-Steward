package com.yepit.cs.dto.admin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/5/30 9:47 上午
 **/
@Data
public class AdminProductRelRequest implements Serializable {

    @ApiModelProperty("操作员ID")
    private Long adminId;

    @ApiModelProperty("产品ID列表")
    private List<String> productList;
}
