package com.yepit.cs.dto.user;

import java.util.Date;

/**
 * Created by qianlong on 2017/8/31.
 */
public class CreateBlackUserResponse {

    private String blackUserId;

    private String userId;

    private String phonenumber;

    private Integer blackType;

    private String blackTypeName;

    private Date createTime;

    private String createOperator;

    private Date updateTime;

    private String updateOperator;

    public String getBlackUserId() {
        return blackUserId;
    }

    public void setBlackUserId(String blackUserId) {
        this.blackUserId = blackUserId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Integer getBlackType() {
        return blackType;
    }

    public void setBlackType(Integer blackType) {
        this.blackType = blackType;
    }

    public String getBlackTypeName() {
        return blackTypeName;
    }

    public void setBlackTypeName(String blackTypeName) {
        this.blackTypeName = blackTypeName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateOperator() {
        return createOperator;
    }

    public void setCreateOperator(String createOperator) {
        this.createOperator = createOperator;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateOperator() {
        return updateOperator;
    }

    public void setUpdateOperator(String updateOperator) {
        this.updateOperator = updateOperator;
    }
}
