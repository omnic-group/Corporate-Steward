package com.yepit.cs.dto.product;

import com.yepit.cs.common.BaseRequest;
import lombok.Data;

/**
 * Created by qianlong on 2017/8/20.
 */
@Data
public class PageSearchProductRequest extends BaseRequest {

    private String productId;

    private String productCode;

    private String chargeId;

    private String productName;

    private Integer productType;

    private Integer status;

    private Long adminId;

}
