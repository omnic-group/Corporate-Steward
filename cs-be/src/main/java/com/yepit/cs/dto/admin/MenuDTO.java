package com.yepit.cs.dto.admin;

import com.yepit.cs.domain.SysMenu;

import java.util.List;

/**
 * Created by qianlong on 2017/8/20.
 */
public class MenuDTO {

    private SysMenu parentMenu;
    private List<SysMenu> childrenMenu;

    public SysMenu getParentMenu() {
        return parentMenu;
    }

    public void setParentMenu(SysMenu parentMenu) {
        this.parentMenu = parentMenu;
    }

    public List<SysMenu> getChildrenMenu() {
        return childrenMenu;
    }

    public void setChildrenMenu(List<SysMenu> childrenMenu) {
        this.childrenMenu = childrenMenu;
    }
}
