package com.yepit.cs.dto.ismp;

import cn.hutool.crypto.SecureUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/6/27 7:57 上午
 **/
@Data
public class NewIsmpPreOrderRequest implements Serializable {

    private static final long serialVersionUID = 1656467502999936621L;

    @ApiModelProperty("产品编码")
    private String productCode;

    @ApiModelProperty("用户号码")
    private String phoneNumber;

    @ApiModelProperty("订购时间,yyyyMMddHHmmss")
    private String orderTimestamp;

    @ApiModelProperty(value = "签名", notes = "生成算法是SHA1(productCode+phoneNumber+orderTimestamp+sercet)")
    private String signature;

    /**
     * 校验订单请求签名是否正确
     *
     * @param secret
     * @return
     */
    public boolean validRequestSign(String secret) {
        String encodeStr = SecureUtil.sha1(this.productCode + this.phoneNumber + this.orderTimestamp + secret);
        String signature = this.signature;
        if (signature.equalsIgnoreCase(encodeStr)) {
            return true;
        }
        return false;
    }

    public static boolean checkSign(String secret, NewIsmpPreOrderRequest request) {
        String encodeStr = SecureUtil.sha1(request.getProductCode() + request.getPhoneNumber() + request.getOrderTimestamp() + secret);
        String signature = request.getSignature();
        if (signature.equalsIgnoreCase(encodeStr)) {
            return true;
        }
        return false;
    }
}
