package com.yepit.cs.dto.admin;

import com.yepit.cs.common.BaseRequest;

/**
 * Created by qianlong on 2017/8/19.
 */
public class AdminLoginRequest extends BaseRequest {

    private String loginName;
    private String loginPwd;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }
}
