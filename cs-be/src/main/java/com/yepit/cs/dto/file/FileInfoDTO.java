package com.yepit.cs.dto.file;

import java.io.Serializable;

/**
 * Created by qianlong on 2017/8/24.
 */
public class FileInfoDTO implements Serializable {

    private static final long serialVersionUID = 3940175898365866969L;

    private String fileId;

    private String fileName;

    private String link;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
