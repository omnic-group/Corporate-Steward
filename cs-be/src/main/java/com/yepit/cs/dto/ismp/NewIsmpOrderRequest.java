package com.yepit.cs.dto.ismp;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.SecureUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author qianlong
 * @description 订购ISMP平台请求
 * @Date 2020/6/27 7:57 上午
 **/
@Data
public class NewIsmpOrderRequest implements Serializable {

    private static final long serialVersionUID = 5149389222354236293L;

    @ApiModelProperty("用户号码")
    private String phoneNumber;

    @ApiModelProperty("ISMP平台订单号")
    private String orderId;

    @ApiModelProperty("验证码")
    private String otp;

    @ApiModelProperty("订购时间,yyyyMMddHHmmss")
    private String orderTimestamp;

    @ApiModelProperty(value = "签名",notes = "生成算法是SHA1(orderId+phoneNumber+orderTimestamp+sercet)")
    private String signature;

    /**
     * 校验订单请求签名是否正确
     *
     * @param secret
     * @return
     */
    public boolean validRequestSign(String secret) {
        String encodeStr = SecureUtil.sha1(this.orderId + this.phoneNumber + this.orderTimestamp + secret);
        if (this.signature.equalsIgnoreCase(encodeStr)){
            return true;
        }
        return false;
    }

//    public static void main(String[] args) throws Exception{
//        String encodeStr = SecureUtil.sha1("202008082220381333737" + "15301588194" + "20200627111312" + "123456");
//        System.out.println(encodeStr);
//    }
}
