package com.yepit.cs.dto.product;

import lombok.Data;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/6/26 3:56 下午
 **/
@Data
public class CreateProductBlackRuleRequest implements Serializable {

    private static final long serialVersionUID = 5360540158828826498L;

    private String productId;

    private String areaCode;
}
