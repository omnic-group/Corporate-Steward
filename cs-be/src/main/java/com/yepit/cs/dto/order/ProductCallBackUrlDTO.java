package com.yepit.cs.dto.order;

import lombok.Data;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/7/26 9:48 上午
 **/
@Data
public class ProductCallBackUrlDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String productId;

    private String clientId;

    private Integer callbackUrlId;

    private String callbackUrlName;

    private String callbackUrl;
}
