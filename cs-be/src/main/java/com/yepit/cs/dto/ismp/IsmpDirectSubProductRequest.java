package com.yepit.cs.dto.ismp;

import com.yepit.cs.common.BaseRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //直接订购ISMP平台产品
 * @Date 2019/10/20 2:26 PM
 **/
@Data
@ApiModel
public class IsmpDirectSubProductRequest extends BaseRequest implements Serializable {

    @ApiModelProperty(value = "用户手机号码,仅限江苏电信用户")
    private String phoneNumber;

    @ApiModelProperty(value = "产品编码")
    private String productCode;

    @ApiModelProperty(value = "短信验证码")
    private String otp;
}
