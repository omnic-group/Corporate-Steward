package com.yepit.cs.dto.sms;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author qianlong
 * @description //短信发送对象
 * @Date 2019/10/26 2:49 PM
 **/
@Data
public class SmsInfo implements Serializable {

    /**
     * 用户号码
     */
    private String phoneNumber;

    /**
     * 模板ID
     */
    private String tplId;

    /**
     * 模板参数
     */
    private HashMap<String,Object> tplParams;

    public SmsInfo(String phoneNumber,String tplId,HashMap<String,Object> tplParams){
        this.phoneNumber = phoneNumber;
        this.tplId = tplId;
        this.tplParams = tplParams;
    }
}
