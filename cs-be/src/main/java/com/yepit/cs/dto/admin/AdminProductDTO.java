package com.yepit.cs.dto.admin;

import com.yepit.cs.domain.AdminProduct;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/5/30 9:45 上午
 **/
@Data
public class AdminProductDTO implements Serializable {

    private static final long serialVersionUID = 1974340543639212378L;

    @ApiModelProperty("产品ID")
    private String productId;

    @ApiModelProperty("产品编码")
    private String productCode;

    @ApiModelProperty("产品名称")
    private String productName;

    @ApiModelProperty("是否绑定")
    private boolean isBind = false;
}
