package com.yepit.cs.dto.file;

import java.util.List;

/**
 * Created by qianlong on 2017/8/23.
 */
public class FileUploadResponse {

    private List<FileInfoDTO> fileList;

    public List<FileInfoDTO> getFileList() {
        return fileList;
    }

    public void setFileList(List<FileInfoDTO> fileList) {
        this.fileList = fileList;
    }
}
