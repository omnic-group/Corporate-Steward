package com.yepit.cs.dto.stat;

import com.yepit.cs.common.BaseRequest;

/**
 * Created by qianlong on 2017/10/7.
 */
public class StatByProductRequest extends BaseRequest{

    private Integer provinceId;

    private String statDate;

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public String getStatDate() {
        return statDate;
    }

    public void setStatDate(String statDate) {
        this.statDate = statDate;
    }
}
