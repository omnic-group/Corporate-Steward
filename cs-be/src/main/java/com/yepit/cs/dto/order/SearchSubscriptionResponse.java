package com.yepit.cs.dto.order;

import lombok.Data;

import java.util.Date;

/**
 * Created by qianlong on 2017/8/30.
 */
@Data
public class SearchSubscriptionResponse{

    private String subscriptionId;

    private String userId;

    private String phonenumber;

    private Integer telecomOperator;

    private String telecomOperatorName;

    private Integer phoneType;

    private String phoneTypeName;

    private String userName;

    private String area;

    private String areaName;

    private String productId;

    private String productCode;

    private String productName;

    private Integer productPrice;

    private Integer productType;

    private String productTypeName;

    private Date openTime;

    private Date closeTime;

    private Integer status;

    private String statusName;

    private Integer operateOrigin;

    private String operateOriginName;

    private Date createTime;

    private String createOperator;

    private Date updateTime;

    private String updateOperator;

    private String fileId;

    private String fileUrl;

    private String chargeId;

}
