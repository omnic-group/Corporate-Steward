package com.yepit.cs.dto.order;

import com.yepit.cs.common.BaseRequest;

/**
 * Created by qianlong on 2017/8/28.
 */
public class UnsubscribeProductRequest extends BaseRequest {

//    private String phoneNumber;

    private String subscriptionId;

    private Integer operateOrigin;

    private Integer devOps;

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

//    public String getPhoneNumber() {
//        return phoneNumber;
//    }
//
//    public void setPhoneNumber(String phoneNumber) {
//        this.phoneNumber = phoneNumber;
//    }

    public Integer getOperateOrigin() {
        return operateOrigin;
    }

    public void setOperateOrigin(Integer operateOrigin) {
        this.operateOrigin = operateOrigin;
    }

    public Integer getDevOps() {
        return devOps;
    }

    public void setDevOps(Integer devOps) {
        this.devOps = devOps;
    }
}
