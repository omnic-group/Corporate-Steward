package com.yepit.cs.dto.order;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class GetPartnerOrderRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long orderId;
    private String phoneNumber;
    private String productCode;
    private Integer tradeType;
    private Integer status;
    private String clientId;
    private Date createTime;
    private Date confirmTime;
    private String ismpOrderId;

    private String startTime;
    private String endTime;


}