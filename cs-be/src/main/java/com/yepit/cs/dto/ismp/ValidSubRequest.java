package com.yepit.cs.dto.ismp;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //校验用户是否可以订购产品请求
 * @Date 2019/8/3 5:59 PM
 **/
@Data
public class ValidSubRequest implements Serializable {

    private static final long serialVersionUID = -4372624601710747673L;

    private String phoneNumber;

    private String chargeId;

    public String toJsonString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.JSON_STYLE);
    }}
