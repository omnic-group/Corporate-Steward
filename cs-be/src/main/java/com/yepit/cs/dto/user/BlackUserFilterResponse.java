package com.yepit.cs.dto.user;

/**
 * Created by qianlong on 2017/9/2.
 */
public class BlackUserFilterResponse{

    private String downloadUrl;

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
}
