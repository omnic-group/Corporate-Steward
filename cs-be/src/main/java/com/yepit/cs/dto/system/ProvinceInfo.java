package com.yepit.cs.dto.system;

/**
 * Created by qianlong on 2017/10/8.
 */
public class ProvinceInfo {

    private Integer provinceId;

    private String provinceName;

    private String areaCode;

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
}
