package com.yepit.cs.dto.ismp;

import cn.hutool.crypto.SecureUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //用户发起退订
 * @Date 2020/8/8 5:59 PM
 **/
@Data
@ApiModel
public class NewIsmpUnSubRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private String phoneNumber;

    private String productCode;

    @ApiModelProperty(value = "签名",notes = "生成算法是SHA1(phoneNumber+productCode+sercet)")
    private String signature;

    /**
     * 校验订单请求签名是否正确
     *
     * @param secret
     * @return
     */
    public boolean validRequestSign(String secret) {
        String encodeStr = SecureUtil.sha1( this.phoneNumber + this.productCode + secret);
        if (this.signature.equalsIgnoreCase(encodeStr)){
            return true;
        }
        return false;
    }
//
//    public static void main(String[] args) throws Exception{
//        String encodeStr = SecureUtil.sha1("15301588194" + "111000000000000231853"  + "123456");
//        System.out.println(encodeStr);
//    }
}
