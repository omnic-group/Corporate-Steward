package com.yepit.cs.dto.file;

import com.yepit.cs.common.BaseRequest;

import java.util.List;

/**
 * Created by qianlong on 2017/8/23.
 */
public class FileUploadRequest extends BaseRequest {

    private List<UploadFileDTO> uploadFiles;

    public List<UploadFileDTO> getUploadFiles() {
        return uploadFiles;
    }

    public void setUploadFiles(List<UploadFileDTO> uploadFiles) {
        this.uploadFiles = uploadFiles;
    }
}
