package com.yepit.cs.dto.user;

/**
 * Created by qianlong on 2017/8/27.
 */
public class PhoneInfo {

    private String areaCode;

    private String areaName;

    private String phoneNumber;

    private Integer telecomOperatorType;

    private String telecomOperatorName;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getTelecomOperatorType() {
        return telecomOperatorType;
    }

    public void setTelecomOperatorType(Integer telecomOperatorType) {
        this.telecomOperatorType = telecomOperatorType;
    }

    public String getTelecomOperatorName() {
        return telecomOperatorName;
    }

    public void setTelecomOperatorName(String telecomOperatorName) {
        this.telecomOperatorName = telecomOperatorName;
    }
}
