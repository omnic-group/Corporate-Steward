package com.yepit.cs.dto.file;

import java.io.Serializable;

/**
 * Created by qianlong on 2017/8/24.
 */
public class UploadFileDTO implements Serializable {

    private static final long serialVersionUID = 623126645996088807L;

    private String base64Str;

    private String fileName;

//    private String fileSuffix;

    public String getBase64Str() {
        return base64Str;
    }

    public void setBase64Str(String base64Str) {
        this.base64Str = base64Str;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

//    public String getFileSuffix() {
//        return fileSuffix;
//    }
//
//    public void setFileSuffix(String fileSuffix) {
//        this.fileSuffix = fileSuffix;
//    }
}
