package com.yepit.cs.dto.order;

import com.yepit.cs.common.BaseRequest;
import lombok.Data;

/**
 * Created by qianlong on 2017/8/30.
 */
@Data
public class SearchSubscriptionRequest extends BaseRequest {

    private String phoneNumber;

    private Integer status;

    private String startDate;

    private String endDate;

    private Integer superFlag = 0;

    private String userId;

    private Integer productType;

    private String subscriptionId;

    private String productId;

    private Long adminId;
}
