package com.yepit.cs.dto.order;

import com.yepit.cs.common.BaseRequest;
import com.yepit.cs.constant.OperateOriginEnum;

import java.util.List;

/**
 * Created by qianlong on 2019/1/17.
 */
public class SubscribeProductViaWechatRequest extends BaseRequest {

    private static final long serialVersionUID = 2296729234637432748L;

    private String productId;

    private String productCode;

    private Integer operateOrigin = OperateOriginEnum.Wechart.getValue();

    private String phoneNumber;

    private String userName;

    private String password;

    /**
     * 1-10000号密码验证
     * 2-OTP
     */
    private Integer verifyType;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getOperateOrigin() {
        return operateOrigin;
    }

    public void setOperateOrigin(Integer operateOrigin) {
        this.operateOrigin = operateOrigin;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getVerifyType() {
        return verifyType;
    }

    public void setVerifyType(Integer verifyType) {
        this.verifyType = verifyType;
    }
}
