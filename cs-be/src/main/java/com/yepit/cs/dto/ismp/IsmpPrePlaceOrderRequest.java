package com.yepit.cs.dto.ismp;

import lombok.Data;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/8/4 5:11 PM
 **/
@Data
public class IsmpPrePlaceOrderRequest implements Serializable {

    private static final long serialVersionUID = 3974494260249709067L;

    private String ismpSerial;

    private String phoneNumber;

    private Integer result;

}
