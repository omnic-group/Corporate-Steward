package com.yepit.cs.dto.stat;

import com.yepit.cs.domain.ProductInfo;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by qianlong on 2017/10/7.
 */
public class StatByProductResponse implements Serializable{

    private static final long serialVersionUID = 3487837802582914377L;

    private LinkedHashMap<String,List<StatResult>> statResult;

    public LinkedHashMap<String, List<StatResult>> getStatResult() {
        return statResult;
    }

    public void setStatResult(LinkedHashMap<String, List<StatResult>> statResult) {
        this.statResult = statResult;
    }

    public class StatResult implements Serializable {

        private static final long serialVersionUID = 8848706327372554949L;

        private String statObject;

        private Long statNum;

        public String getStatObject() {
            return statObject;
        }

        public void setStatObject(String statObject) {
            this.statObject = statObject;
        }

        public Long getStatNum() {
            return statNum;
        }

        public void setStatNum(Long statNum) {
            this.statNum = statNum;
        }
    }
}
