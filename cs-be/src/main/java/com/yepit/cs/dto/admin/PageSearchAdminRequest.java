package com.yepit.cs.dto.admin;

import com.yepit.cs.common.BaseRequest;

/**
 * Created by qianlong on 2017/8/20.
 */
public class PageSearchAdminRequest extends BaseRequest {

    private Long adminId;

    private String loginName;

    private String mobile;

    private String email;

    private Integer status;

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
