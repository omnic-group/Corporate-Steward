package com.yepit.cs.dto.order;

import com.yepit.cs.common.BaseRequest;
import lombok.Data;

import java.util.List;

/**
 * Created by qianlong on 2017/8/26.
 */
@Data
public class SubscribeProductRequest extends BaseRequest {

    private String productId;

    private String productCode;

    private Integer operateOrigin;

//    private String openTime;

    private String phoneNumber;

    private String userName;

    private Integer telecomOperator;

    private Integer phoneType;

    private List<String> fileIds;

    private String ismpOrderId;

    private String otp;

    private Integer productType;

    /**
     * 是否是运维处理,0则不向彩铃平台发送指令,1则发送
     */
    private Integer devOps;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getOperateOrigin() {
        return operateOrigin;
    }

    public void setOperateOrigin(Integer operateOrigin) {
        this.operateOrigin = operateOrigin;
    }

}
