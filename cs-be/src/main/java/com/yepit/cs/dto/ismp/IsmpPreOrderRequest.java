package com.yepit.cs.dto.ismp;

import lombok.Data;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/8/4 5:11 PM
 **/
@Data
public class IsmpPreOrderRequest implements Serializable {

    private static final long serialVersionUID = -7534120542574079189L;

    private String productCode;

    private Integer tradeType;

    private String ismpSerial;

    private String phoneNumber;

}
