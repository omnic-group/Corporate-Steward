package com.yepit.cs.dto.ismp;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //用户发起订购
 * @Date 2019/8/3 5:59 PM
 **/
@Data
public class IsmpSubscribeProductRequest implements Serializable {

    private static final long serialVersionUID = -3222035973171547931L;

    private String phoneNumber;

    private String orderId;

    private String otp;

    private Integer orderType;

    public String toJsonString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.JSON_STYLE);
    }}
