package com.yepit.cs.dto.batch;

import com.yepit.cs.common.BaseRequest;

/**
 * Created by qianlong on 2017/9/2.
 */
public class CreateBatchRequest extends BaseRequest{

    private String fileId;

    private Integer batchType;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public Integer getBatchType() {
        return batchType;
    }

    public void setBatchType(Integer batchType) {
        this.batchType = batchType;
    }
}
