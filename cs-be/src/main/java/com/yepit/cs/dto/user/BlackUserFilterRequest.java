package com.yepit.cs.dto.user;

import com.yepit.cs.common.BaseRequest;

/**
 * Created by qianlong on 2017/9/2.
 */
public class BlackUserFilterRequest extends BaseRequest{

    private String fileId;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
}
