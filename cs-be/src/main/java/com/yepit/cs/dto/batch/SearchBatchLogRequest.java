package com.yepit.cs.dto.batch;

import com.yepit.cs.common.BaseRequest;

/**
 * Created by qianlong on 2017/9/2.
 */
public class SearchBatchLogRequest extends BaseRequest{

    private Integer status;

    private Integer batchType;

    private String startTime;

    private String endTime;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getBatchType() {
        return batchType;
    }

    public void setBatchType(Integer batchType) {
        this.batchType = batchType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
