package com.yepit.cs.dto.ismp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/8/3 5:59 PM
 **/
@Data
@ApiModel
public class GetIsmpOrderRequest implements Serializable {

    private static final long serialVersionUID = -1403553389945053778L;

    @ApiModelProperty(hidden = true)
    private String spId;

    private String chargeId;

    private String orderType;

    @ApiModelProperty(hidden = true)
    private String timestamp;

    @ApiModelProperty(hidden = true)
    private String accessToken;

    public String toJsonString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.JSON_STYLE);
    }}
