package com.yepit.cs.dto.ismp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author qianlong
 * @description //用户发起退订
 * @Date 2019/8/3 5:59 PM
 **/
@Data
@ApiModel
public class IsmpUnSubscribeProductRequest implements Serializable {

    private static final long serialVersionUID = 1769184448994908076L;

    private String phoneNum;

    private String chargeId;

    @ApiModelProperty(hidden = true)
    private String spId;

    @ApiModelProperty(hidden = true)
    private String timestamp;

    @ApiModelProperty(hidden = true)
    private String accessToken;

    public String toJsonString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.JSON_STYLE);
    }}
