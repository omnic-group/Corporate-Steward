package com.yepit.cs.dto.order;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/9/1 12:02 PM
 **/
@Data
public class ExportOrderInfo implements Serializable {

    @ExcelProperty("用户号码")
    private String phonenumber;

    @ExcelProperty("产品名称")
    private String productName;

    @ExcelProperty("开通时间")
    private Date openTime;

    @ExcelProperty("关闭时间")
    private Date closeTime;

    @ExcelProperty("状态")
    private String statusName;

    @ExcelProperty("最近更新时间")
    private Date updateTime;
}
