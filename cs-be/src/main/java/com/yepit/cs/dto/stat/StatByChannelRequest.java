package com.yepit.cs.dto.stat;

import com.yepit.cs.common.BaseRequest;

/**
 * Created by qianlong on 2017/10/5.
 */
public class StatByChannelRequest extends BaseRequest{

    private String productId;

    private String startDate;

    private String endDate;

    private Integer provinceId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }
}
