package com.yepit.cs.dto.product;

import com.yepit.cs.common.BaseRequest;

/**
 * Created by qianlong on 2017/8/27.
 */
public class DeleteProductRequest extends BaseRequest {

    private String productId;

    private String productCode;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
