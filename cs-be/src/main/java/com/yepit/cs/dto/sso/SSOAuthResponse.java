package com.yepit.cs.dto.sso;

/**
 * Created by qianlong on 2018/1/28.
 */
public class SSOAuthResponse {

    private String loginUrl;

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }
}
