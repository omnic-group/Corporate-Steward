package com.yepit.cs.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by qianlong on 2017/8/19.
 */
@ApiModel
public class BaseRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    public BaseRequest() {
        super();
        init();
    }

    private void init() {
        // 当没有时，自动生成一个。发生在web新建对象时。当到服务提供者端时，已经有了。
        if (null == traceId || "".equals(traceId))
            traceId = UUID.randomUUID().toString().replaceAll("\\-", "").toUpperCase();
    }

    public BaseRequest(PageArg page) {
        super();
        init();
        this.page = page;
    }

    public BaseRequest(int pageNum, int pageSize) {
        super();
        init();
        this.page = new PageArg(pageNum, pageSize);
    }

    /**
     * traceId，必填
     */
    @ApiModelProperty(hidden = true)
    private String traceId;

    private PageArg page;

    /**
     * 工号
     */
    @ApiModelProperty(hidden = true)
    private String operId;

    /**
     * 操作员登录名
     */
    @ApiModelProperty(hidden = true)
    private String operName;

    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public PageArg getPage() {
        return page;
    }

    public void setPage(PageArg page) {
        this.page = page;
    }

    public String getOperId() {
        return operId;
    }

    public void setOperId(String operId) {
        this.operId = operId;
    }
}
