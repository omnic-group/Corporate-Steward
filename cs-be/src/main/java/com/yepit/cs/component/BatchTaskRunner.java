package com.yepit.cs.component;

import com.yepit.cs.constant.BatchOptStatusEnum;
import com.yepit.cs.constant.BatchOptTypeEnum;
import com.yepit.cs.domain.BatchOperationLog;
import com.yepit.cs.dto.batch.SearchBatchLogRequest;
import com.yepit.cs.mapper.BatchOperationLogMapper;
import com.yepit.cs.service.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by qianlong on 2017/9/3.
 * //
 */
@Component
@Order(value = 2)
public class BatchTaskRunner implements CommandLineRunner {

    final static org.slf4j.Logger logger = LoggerFactory.getLogger(BatchTaskRunner.class);

    @Autowired
    private BatchOperationLogMapper batchOperationLogMapper;

    @Override
    public void run(String... strings) throws Exception {
        try {
            while (true) {
                //搜索待执行的批处理任务
                SearchBatchLogRequest cond = new SearchBatchLogRequest();
                cond.setStatus(BatchOptStatusEnum.Pending.getValue());
                List<BatchOperationLog> pendingTaskList = batchOperationLogMapper.listByCond(cond);
//                logger.info("当待处理的批操作任务有:"+pendingTaskList.size()+"条");
                for (BatchOperationLog batchOperationLog : pendingTaskList) {
                    Integer batchType = batchOperationLog.getBatchType();
                    if (batchType == BatchOptTypeEnum.OpenAccount.getValue()) {
                        logger.info("批量开户线程开始");
                        OpenAccountTask task = ApplicationContextProvider.getBean(OpenAccountTask.class);
                        task.setBatchOperationLog(batchOperationLog);
                        task.run();
                    } else if (batchType == BatchOptTypeEnum.BlackUserImport.getValue()) {
                        logger.info("批量导入黑名单线程开始");
                        BlackUserImportTask task = ApplicationContextProvider.getBean(BlackUserImportTask.class);
                        task.setBatchOperationLog(batchOperationLog);
                        task.run();
                    } else if (batchType == BatchOptTypeEnum.BlackUserFilter.getValue()) {
                        logger.info("黑名单过滤线程开始");
                        BlackUserFilterTask task = ApplicationContextProvider.getBean(BlackUserFilterTask.class);
                        task.setBatchOperationLog(batchOperationLog);
                        task.start();
                    } else if (batchType == BatchOptTypeEnum.CloseAccount.getValue()) {
                        logger.info("批量销户线程开始");
                        CloseAccountTask task = ApplicationContextProvider.getBean(CloseAccountTask.class);
                        task.setBatchOperationLog(batchOperationLog);
                        task.start();
                    } else if (batchType == BatchOptTypeEnum.SmsGroupSend.getValue()) {
                        logger.info("短信群发线程开始");
                        NewSmsGroupSendTask task = ApplicationContextProvider.getBean(NewSmsGroupSendTask.class);
                        task.setBatchOperationLog(batchOperationLog);
                        task.start();
                    } else if (batchType == BatchOptTypeEnum.IsmpOpenAccount.getValue()) {
                        logger.info("ISMP批量开户线程开始");
                        IsmpOpenAccountTask task = ApplicationContextProvider.getBean(IsmpOpenAccountTask.class);
                        task.setBatchOperationLog(batchOperationLog);
                        task.start();
                    } else if (batchType == BatchOptTypeEnum.IsmpCloseAccount.getValue()) {
                        logger.info("ISMP批量销户线程开始");
                        IsmpCloseAccountTask task = ApplicationContextProvider.getBean(IsmpCloseAccountTask.class);
                        task.setBatchOperationLog(batchOperationLog);
                        task.start();
                    } else if (batchType == BatchOptTypeEnum.IsmpSubPreAuth.getValue()) {
                        logger.info("ISMP批量校验线程开始");
                        IsmpSubPreAuthTask task = ApplicationContextProvider.getBean(IsmpSubPreAuthTask.class);
                        task.setBatchOperationLog(batchOperationLog);
                        task.start();
                    } else if (batchType == BatchOptTypeEnum.OpsOpenAccount.getValue()) {
                        logger.info("开户故障处理线程开始");
                        OpsOpenAccountTask task = ApplicationContextProvider.getBean(OpsOpenAccountTask.class);
                        task.setBatchOperationLog(batchOperationLog);
                        task.start();
                    } else if (batchType == BatchOptTypeEnum.OpsCloseAccount.getValue()) {
                        logger.info("销户故障处理线程开始");
                        OpsCloseAccountTask task = ApplicationContextProvider.getBean(OpsCloseAccountTask.class);
                        task.setBatchOperationLog(batchOperationLog);
                        task.start();
                    }
                }
                Thread.sleep(1000);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
