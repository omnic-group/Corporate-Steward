package com.yepit.cs.component;

import com.yepit.cs.cache.AdminCache;
import com.yepit.cs.cache.ProductCache;
import com.yepit.cs.cache.SysClientCache;
import com.yepit.cs.domain.SysAdmin;
import com.yepit.cs.domain.SysClient;
import com.yepit.cs.dto.admin.AdminDTO;
import com.yepit.cs.service.AdminService;
import com.yepit.cs.util.RedisUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * Created by qianlong on 2017/10/22.
 */
@Component
@Order(value=1)
public class AppInitRunner implements CommandLineRunner {

    @Autowired
    private AdminCache adminCache;

    @Autowired
    private ProductCache productCache;

    @Autowired
    private SysClientCache sysClientCache;

    @Override
    public void run(String... strings) throws Exception {
        try{
            //将所有管理员信息加载到缓存中去
            adminCache.reload();

            //将所有产品数据加载到缓存中去
            productCache.reload();

            //将所有客户端授权数据加载到缓存中去
            sysClientCache.reload();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
