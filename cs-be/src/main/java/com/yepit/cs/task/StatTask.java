package com.yepit.cs.task;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.service.StatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by qianlong on 2017/10/7.
 */
@Component
public class StatTask {

    final static Logger logger = LoggerFactory.getLogger(StatTask.class);

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd");

    @Autowired
    private StatService statService;

    /**
     *  每天01:00执行按产品维度统计各地区订购数量的任务
     */
    @Scheduled(cron = "0 0 1 * * ?")
    @SuppressWarnings("all")
    public void executeStatProductSubTask() {
        try {
            long statTime = System.currentTimeMillis();
            logger.info("按产品维度统计各地区订购数量开始");
            BaseResponse resp = statService.statProductSubscriptions(SDF.format(new Date()));
            logger.info("按产品维度统计各地区订购数量结果:" + resp.getResultDesc());
            logger.info("按产品维度统计各地区订购数量结束,供耗时:" + (System.currentTimeMillis() - statTime) / 1000 + "秒");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("按产品维度统计各地区订购数量发生异常:" + ex.getMessage());
        }

    }

    /**
     *  每天01:00执行按渠道维度统计各地区每个产品订购数量的任务
     */
    @Scheduled(cron = "0 0 1 * * ?")
    @SuppressWarnings("all")
    public void executeStatChannelSubTask() {
        try {
            long statTime = System.currentTimeMillis();
            logger.info("按渠道维度统计各地区每个产品订购数量开始");
            BaseResponse resp = statService.statChannelSubscriptions(SDF.format(new Date()));
            logger.info("按渠道维度统计各地区每个产品订购数量结果:" + resp.getResultDesc());
            logger.info("按渠道维度统计各地区每个产品订购数量结束,供耗时:" + (System.currentTimeMillis() - statTime) / 1000 + "秒");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("按渠道维度统计各地区每个产品订购数量发生异常:" + ex.getMessage());
        }
    }

    /**
     *  每天01:00执行按全渠道维度统计各地区每个产品订购数量的任务
     */
    @Scheduled(cron = "0 0 1 * * ?")
    @SuppressWarnings("all")
    public void executeStatAllChannelSubTask() {
        try {
            long statTime = System.currentTimeMillis();
            logger.info("按全渠道维度统计各地区每个产品订购数量开始");
            BaseResponse resp = statService.statAllChannelDay(SDF.format(new Date()));
            logger.info("按全渠道维度统计各地区每个产品订购数量结果:" + resp.getResultDesc());
            logger.info("按全渠道维度统计各地区每个产品订购数量结束,供耗时:" + (System.currentTimeMillis() - statTime) / 1000 + "秒");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("按产品维度统计各地区订购数量发生异常:" + ex.getMessage());
        }
    }

}
