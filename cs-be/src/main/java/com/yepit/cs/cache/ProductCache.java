package com.yepit.cs.cache;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.domain.SysAdmin;
import com.yepit.cs.dto.admin.AdminDTO;
import com.yepit.cs.mapper.ProductInfoMapper;
import com.yepit.cs.mapper.SysAdminMapper;
import com.yepit.cs.util.JsonUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by qianlong on 2020/05/30.
 */
@Service
public class ProductCache extends BaseCache {

    @Autowired
    private ProductInfoMapper productInfoMapper;

    private static final String PRODUCT_ID = "product:id";
    private static final String PRODUCT_CODE = "product:code";

    @Override
    public void reload() throws Exception {
        redisUtils.remove(PRODUCT_ID);
        redisUtils.remove(PRODUCT_CODE);
        List<ProductInfo> productList = productInfoMapper.listByCond(new ProductInfo());
        if (CollUtil.isNotEmpty(productList)) {
            productList.stream().forEach(productInfo -> {
                redisUtils.hmset(PRODUCT_ID, productInfo.getProductId(), JSON.toJSONString(productInfo));
                redisUtils.hmset(PRODUCT_CODE, productInfo.getProductCode(), JSON.toJSONString(productInfo));
            });
        }
    }

    /**
     * 根据产品ID获取对象
     *
     * @param productId
     * @return
     * @throws Exception
     */
    public ProductInfo getProductInfoById(String productId) throws Exception {
        Object value = redisUtils.hmget(PRODUCT_ID, productId);
        if (value == null) {
            return null;
        }
        ProductInfo productInfo = JsonUtils.jsonToJavaBean(value.toString(), ProductInfo.class);
        return productInfo;
    }

    /**
     * 根据产品CODE获取对象
     *
     * @param productCode
     * @return
     * @throws Exception
     */
    public ProductInfo getProductInfoByCode(String productCode) throws Exception {
        Object value = redisUtils.hmget(PRODUCT_CODE, productCode);
        if (value == null) {
            return null;
        }
        ProductInfo productInfo = JsonUtils.jsonToJavaBean(value.toString(), ProductInfo.class);
        return productInfo;
    }
}
