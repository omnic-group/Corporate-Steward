package com.yepit.cs.cache;

import com.yepit.cs.domain.SysAdmin;
import com.yepit.cs.dto.admin.AdminDTO;
import com.yepit.cs.mapper.SysAdminMapper;
import com.yepit.cs.util.JsonUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by qianlong on 2017/10/22.
 */
@Service
public class AdminCache extends BaseCache {

    @Autowired
    private SysAdminMapper sysAdminMapper;

    @Override
    public void reload() throws Exception {
        redisUtils.remove("sysAdmin");
        List<SysAdmin> adminList = sysAdminMapper.listByCond(new SysAdmin());
        if (adminList != null && adminList.size() > 0) {
            HashMap<String, String> sysAdminMap = new HashMap<String, String>();
            for (SysAdmin sysAdmin : adminList) {
                AdminDTO adminDTO = new AdminDTO();
                BeanUtils.copyProperties(sysAdmin,adminDTO);
                sysAdminMap.put(sysAdmin.getLoginName(), JsonUtils.Object2Json(adminDTO));
            }
            redisUtils.hmset("sysAdmin", sysAdminMap);
        }
    }

    /**
     * 根据登录名获取管理员对象
     * @param loginName
     * @return
     * @throws Exception
     */
    public AdminDTO getAdminByLoginName(String loginName) throws Exception {
        Object value = redisUtils.hmget("sysAdmin", loginName);
        if(value == null){
            return null;
        }
        AdminDTO admin = JsonUtils.jsonToJavaBean(value.toString(),AdminDTO.class);
        return admin;
    }
}
