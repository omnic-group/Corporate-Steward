package com.yepit.cs.cache;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.yepit.cs.domain.SysClient;
import com.yepit.cs.mapper.SysClientMapper;
import com.yepit.cs.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/6/27 9:34 上午
 **/
@Component
public class SysClientCache extends BaseCache {

    @Autowired
    private SysClientMapper sysClientMapper;

    private static final String CLIENT_ID = "sys:clientId";

    /**
     * 由子类实现
     */
    @Override
    public void reload() throws Exception {
        redisUtils.remove(CLIENT_ID);
        List<SysClient> allSysClient = sysClientMapper.selectAll();
        if (CollUtil.isNotEmpty(allSysClient)) {
            allSysClient.stream().forEach(sysClient -> {
                redisUtils.hmset(CLIENT_ID, sysClient.getClientId(), JSON.toJSONString(sysClient));
            });
        }
    }

    /**
     * 根据客户端ID获取客户端配置信息
     * @param clientId
     * @return
     * @throws Exception
     */
    public SysClient getSysClientByClientId(String clientId) throws Exception{
        Object value = redisUtils.hmget(CLIENT_ID,clientId);
        if(value == null){
            return null;
        }
        SysClient sysClient = JsonUtils.jsonToJavaBean(value.toString(), SysClient.class);
        return sysClient;
    }

}
