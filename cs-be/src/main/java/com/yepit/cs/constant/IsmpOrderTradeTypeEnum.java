package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/20.
 */
public enum IsmpOrderTradeTypeEnum implements EnumValue<Integer, String> {

    OrderSingle(1, "点播"), OrderMonthly(2, "包月"), Unsubscribe(3, "退订");

    private int value;
    private String desc;

    private IsmpOrderTradeTypeEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for (IsmpOrderTradeTypeEnum enumObj : IsmpOrderTradeTypeEnum.values()) {
            if (enumObj.getValue() == value) {
                return enumObj.getDesc();
            }
        }
        return "";
    }
}