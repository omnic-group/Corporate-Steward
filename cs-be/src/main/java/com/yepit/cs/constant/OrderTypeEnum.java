package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/27.
 */
public enum OrderTypeEnum implements EnumValue<Integer,String>{

    Subscribe(1,"订购"),Cancel(2,"退订");

    private int value;
    private String desc;

    private OrderTypeEnum(int value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for(OrderTypeEnum enumObj:OrderTypeEnum.values()){
            if(enumObj.getValue() == value){
                return enumObj.getDesc();
            }
        }
        return "";
    }
}
