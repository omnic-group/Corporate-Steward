package com.yepit.cs.constant;

/**
 * @author qianlong
 * @Description //TODO
 * @Date 2021/5/17 11:28 上午
 **/
public interface OrderConstant {

    interface DevOpsDef {
        Integer NO_DEV_OPS = 0;
        Integer DEV_OPS = 1;
    }
}
