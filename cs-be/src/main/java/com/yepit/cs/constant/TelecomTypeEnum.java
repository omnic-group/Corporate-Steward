package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/27.
 */
public enum TelecomTypeEnum implements EnumValue<Integer,String>{

    CTC(1,"中国电信"),CMC(2,"中国移动"),CUC(3,"中国联通");

    private int value;
    private String desc;

    private TelecomTypeEnum(int value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for(TelecomTypeEnum enumObj:TelecomTypeEnum.values()){
            if(enumObj.getValue() == value){
                return enumObj.getDesc();
            }
        }
        return "";
    }
}
