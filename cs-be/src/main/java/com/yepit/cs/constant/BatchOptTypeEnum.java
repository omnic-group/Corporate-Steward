package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/27.
 */
public enum BatchOptTypeEnum implements EnumValue<Integer, String> {

    OpenAccount(1, "开户"), BlackUserImport(2, "黑名单导入"),
    BlackUserFilter(3, "黑名单过滤"), CloseAccount(4, "销户"),
    SmsGroupSend(5, "短信群发"),IsmpOpenAccount(6, "ISMP平台订购"),
    IsmpCloseAccount(7, "ISMP平台退订"),
    IsmpSubPreAuth(8, "ISMP批量校验"),
    OpsOpenAccount(9, "开户故障处理"),
    OpsCloseAccount(10, "销户故障处理");

    private int value;
    private String desc;

    private BatchOptTypeEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for (BatchOptTypeEnum enumObj : BatchOptTypeEnum.values()) {
            if (enumObj.getValue() == value) {
                return enumObj.getDesc();
            }
        }
        return "";
    }
}
