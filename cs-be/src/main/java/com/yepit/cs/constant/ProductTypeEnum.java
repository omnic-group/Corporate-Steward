package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/20.
 */
public enum ProductTypeEnum implements EnumValue<Integer,String>{

    BaiWang(1,"百旺托收"),
    CRBT(2,"电信能力开放平台托收"),
    ISMP(3,"ISMP平台托收"),
    Other(4,"其他"),
    CRBT2(5,"电信能力开放平台托收-2");

    private int value;
    private String desc;

    private ProductTypeEnum(int value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for(ProductTypeEnum enumObj:ProductTypeEnum.values()){
            if(enumObj.getValue() == value){
                return enumObj.getDesc();
            }
        }
        return "";
    }
}
