package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/20.
 */
public enum ProductStatusEnum implements EnumValue<Integer,String>{

    Normal(1,"正常"),Cancel(2,"下架");

    private int value;
    private String desc;

    private ProductStatusEnum(int value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for(ProductStatusEnum enumObj:ProductStatusEnum.values()){
            if(enumObj.getValue() == value){
                return enumObj.getDesc();
            }
        }
        return "";
    }
}