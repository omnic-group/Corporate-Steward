package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/27.
 */
public enum PhoneTypeEnum implements EnumValue<Integer,String>{

    Mobile(1,"手机"),FixedPhone(2,"固话");

    private int value;
    private String desc;

    private PhoneTypeEnum(int value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for(PhoneTypeEnum enumObj:PhoneTypeEnum.values()){
            if(enumObj.getValue() == value){
                return enumObj.getDesc();
            }
        }
        return "";
    }
}
