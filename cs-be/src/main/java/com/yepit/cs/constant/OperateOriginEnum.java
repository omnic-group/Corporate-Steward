package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/27.
 */
public enum OperateOriginEnum implements EnumValue<Integer,String>{

    BaiwangIntface(1,"百旺接口同步"),StewardPlatform(2,"管理平台开通"),
    Batch(3,"批量开通"),Wechart(4,"微信开通"),Ismp(5,"ISMP开通");

    private int value;
    private String desc;

    private OperateOriginEnum(int value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for(OperateOriginEnum enumObj:OperateOriginEnum.values()){
            if(enumObj.getValue() == value){
                return enumObj.getDesc();
            }
        }
        return "";
    }
}
