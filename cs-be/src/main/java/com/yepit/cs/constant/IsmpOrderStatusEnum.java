package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/20.
 */
public enum IsmpOrderStatusEnum implements EnumValue<Integer, String> {

    Initial(0, "初始状态"), WaitingForConfirm(1, "待确认"),
    Confirmed(2, "已确认"), Fail(3, "订购失败");

    private int value;
    private String desc;

    private IsmpOrderStatusEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for (IsmpOrderStatusEnum enumObj : IsmpOrderStatusEnum.values()) {
            if (enumObj.getValue() == value) {
                return enumObj.getDesc();
            }
        }
        return "";
    }
}