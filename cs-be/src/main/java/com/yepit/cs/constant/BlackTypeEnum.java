package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/19.
 */
public enum BlackTypeEnum implements EnumValue<Integer,String>{

    CTC(1,"电信黑名单");

    private int value;
    private String desc;

    private BlackTypeEnum(int value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for(BlackTypeEnum adminStateEnum: BlackTypeEnum.values()){
            if(adminStateEnum.getValue() == value){
                return adminStateEnum.getDesc();
            }
        }
        return "";
    }

}
