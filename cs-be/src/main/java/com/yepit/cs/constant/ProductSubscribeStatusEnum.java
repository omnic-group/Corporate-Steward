package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/20.
 */
public enum ProductSubscribeStatusEnum implements EnumValue<Integer,String>{

    Normal(1,"正常"),Cancel(2,"退订");

    private int value;
    private String desc;

    private ProductSubscribeStatusEnum(int value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for(ProductSubscribeStatusEnum enumObj:ProductSubscribeStatusEnum.values()){
            if(enumObj.getValue() == value){
                return enumObj.getDesc();
            }
        }
        return "";
    }
}