package com.yepit.cs.constant;

/**
 * Created by qianlong on 2017/8/20.
 */
public class SequenceDefConstant {

    public static class ProductInfoDef{
        public final static String TABLE_NAME = "p_product_info";
        public final static String COLUMN_NAME = "product_id";
    }

    public static class FileUpload{
        public final static String TABLE_NAME = "s_file_upload";
        public final static String COLUMN_NAME = "file_id";
    }

    public static class BatchOperationLog{
        public final static String TABLE_NAME = "b_batch_opeation_log";
        public final static String COLUMN_NAME = "batch_id";
    }

    public static class UserInfo{
        public final static String TABLE_NAME = "u_user_info";
        public final static String COLUMN_NAME = "user_id";
    }

    public static class BlackUser{
        public final static String TABLE_NAME = "u_black_user";
        public final static String COLUMN_NAME = "black_user_id";
    }

    public static class OrderLog{
        public final static String TABLE_NAME = "o_order_log";
        public final static String COLUMN_NAME = "order_log_id";
    }

    public static class ProductSubscribe{
        public final static String TABLE_NAME = "o_product_subscribe";
        public final static String COLUMN_NAME = "subscription_id";
    }

    public static class OrderAttachment{
        public final static String TABLE_NAME = "o_order_attachment";
        public final static String COLUMN_NAME = "order_attachment_id";
    }
}
