package com.yepit.cs.constant;

/**
 * Created by qianlong on 2021/6/19.
 */
public enum IsmpPlaceOrderRequestStatusEnum implements EnumValue<Integer,String>{

    WAITING_FOR_HANDLE(1,"待处理"),FINISH(2,"已处理");

    private int value;
    private String desc;

    private IsmpPlaceOrderRequestStatusEnum(int value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(Integer value) {
        for(IsmpPlaceOrderRequestStatusEnum enumObj: IsmpPlaceOrderRequestStatusEnum.values()){
            if(enumObj.getValue() == value){
                return enumObj.getDesc();
            }
        }
        return "";
    }
}
