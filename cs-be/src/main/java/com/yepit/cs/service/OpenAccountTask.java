package com.yepit.cs.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.*;
import com.yepit.cs.domain.BatchOperationLog;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.order.SubscribeProductRequest;
import com.yepit.cs.dto.order.SubscribeProductResponse;
import com.yepit.cs.dto.user.CreateBlackUserRequest;
import com.yepit.cs.dto.user.CreateBlackUserResponse;
import com.yepit.cs.mapper.BatchOperationLogMapper;
import com.yepit.cs.mapper.BlackUserMapper;
import com.yepit.cs.mapper.FileUploadMapper;
import com.yepit.cs.mapper.ProductInfoMapper;
import com.yepit.cs.util.ExcelUtils;
import com.yepit.cs.util.NumberUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 批量开户
 * Created by qianlong on 2017/9/3.
 */
@Component("openAccountTask")
@Scope("prototype")
public class OpenAccountTask extends Thread {

    final static Logger logger = LoggerFactory.getLogger(OpenAccountTask.class);

    private BatchOperationLog batchOperationLog;

    @Autowired
    private BatchOperationLogMapper batchOperationLogMapper;

    @Autowired
    private FileUploadMapper fileUploadMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private OrderService orderService;

    @Value("${system.batch.savepath}")
    private String batchFileSavePath;

    @Value("${system.fileServerURL}")
    private String fileServerURL;

    @SuppressWarnings("all")
    @Override
    public void run() {
        try {
            logger.info("批量开户开始");
            //将文件状态改为处理中
            batchOperationLog.setStatus(BatchOptStatusEnum.Processing.getValue());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);

            String uploadFilePath = batchOperationLog.getUploadFilePath();
            File file = new File(uploadFilePath);
            if (!file.exists()) {
                logger.error("批处理文件[" + uploadFilePath + "]不存在,无法处理");
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("批处理文件不存在,无法处理");
                batchOperationLog.setUpdateTime(new Date());
                batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
                batchOperationLog.setResultFilePath(null);
                batchOperationLog.setResultFileUrl(null);
                batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
                return;
            }

            //读取待开户数据
            List<List<String>> dataList = ExcelUtils.readWorkBook(uploadFilePath, 1);
            List<List<String>> resultList = new ArrayList<List<String>>();
            if (dataList != null && dataList.size() > 0) {
                for (List<String> accountInfo : dataList) {
                    if(accountInfo.size() < 6){
                        continue;
                    }
                    List<String> result = new ArrayList<String>();

                    String phoneNumber = accountInfo.get(0);
                    String userName = accountInfo.get(1);
                    String areaCode = accountInfo.get(2);
                    Integer operatorType = null;
                    String operatorName = accountInfo.get(3);//运营商名称
                    String productCode = accountInfo.get(4);//产品编码
                    String isSms = accountInfo.get(5);

                    if (StringUtils.isBlank(phoneNumber) && StringUtils.isBlank(userName)
                            && StringUtils.isBlank(areaCode) && StringUtils.isBlank(operatorName)
                            && StringUtils.isBlank(productCode) && StringUtils.isBlank(isSms)) {
                        continue;
                    }

                    result.add(phoneNumber.trim());
                    result.add(userName);
                    result.add(areaCode);
                    result.add(operatorName);
                    result.add(productCode);
                    result.add(isSms);

                    logger.info("----------------------------------------------------");
                    logger.info("用户号码=" + phoneNumber + "|||客户名称=" + userName
                            + "|||属地=" + areaCode + "|||运营商=" + operatorName
                            + "|||产品编码=" + productCode + "|||是否发送短信=" + isSms);

                    if (!StringUtils.isBlank(operatorName)) {
                        if (operatorName.equals("电信")) {
                            operatorType = TelecomTypeEnum.CTC.getValue();
                        } else if (operatorName.equals("移动")) {
                            operatorType = TelecomTypeEnum.CMC.getValue();
                        }
                        if (operatorName.equals("联通")) {
                            operatorType = TelecomTypeEnum.CUC.getValue();
                        }
                    }

                    if (phoneNumber.startsWith("0") && StringUtils.isBlank(operatorName)) {
                        result.add("固话号码必须选择一个运营商");
                        resultList.add(result);
                        continue;
                    }
                    //根据产品编码获取产品ID
                    if (StringUtils.isBlank(productCode)) {
                        result.add("产品编码不能为空");
                        resultList.add(result);
                        continue;
                    }

                    ProductInfo cond = new ProductInfo();
                    cond.setProductCode(productCode);
                    List<ProductInfo> productInfoList = productInfoMapper.listByCond(cond);
                    if (productInfoList == null || productInfoList.size() == 0) {
                        result.add("要订购的产品不存在");
                        resultList.add(result);
                        continue;
                    }
                    phoneNumber = phoneNumber.trim();

                    BaseResponse<SubscribeProductResponse> resp = new BaseResponse<SubscribeProductResponse>();
                    try{
                        //发起开户请求
                        SubscribeProductRequest request = new SubscribeProductRequest();
                        request.setOperName(batchOperationLog.getCreateOperator());
                        request.setProductId(productInfoList.get(0).getProductId());
                        request.setOperateOrigin(OperateOriginEnum.Batch.getValue());
                        request.setTelecomOperator(operatorType);
                        request.setUserName(userName);
                        request.setPhoneNumber(phoneNumber);
                        if (NumberUtils.isMobilePhone(phoneNumber)) {
                            request.setPhoneType(PhoneTypeEnum.Mobile.getValue());
                        } else {
                            request.setPhoneType(PhoneTypeEnum.FixedPhone.getValue());
                        }
                        resp = orderService.subscribeProduct(request);
                    }catch (Exception ex){
                        ex.printStackTrace();
                        resp = resp.setFailResp(ResultCodeConstant.UNKNOWERR,ex.getMessage());
                    }

                    logger.info("用户号码=" + phoneNumber + "|||客户名称=" + userName
                            + "|||属地=" + areaCode + "|||运营商=" + operatorName
                            + "|||产品编码=" + productCode + "|||是否发送短信=" + isSms+"|||处理结果="+resp.getResultDesc());
                    //保存添加结果
                    if(resp.isSuccess()){
                        result.add("处理成功");
                    }else{
                        result.add("处理失败");
                    }
                    result.add(resp.getResultDesc());
                    resultList.add(result);
                }
            }

            //创建结果文件
            if (resultList != null && resultList.size() > 0) {
                logger.info("共处理了" + resultList.size() + "条记录");
                String newFileName = batchOperationLog.getBatchId() + "_result.xlsx";
                String newFilePath = batchFileSavePath + File.separator + newFileName;
                List<String> header = new ArrayList<String>();
                header.add("用户号码");
                header.add("名称");
                header.add("属地");
                header.add("运营商");
                header.add("产品编号");
                header.add("是否发送短信");
                header.add("处理结果");
                header.add("处理结果描述");
                ExcelUtils.createXlsx("批量开户结果", header, resultList, newFilePath);

                //获取主键
                String tableName = SequenceDefConstant.FileUpload.TABLE_NAME;
                String columnName = SequenceDefConstant.FileUpload.COLUMN_NAME;
                String nextId = sequenceDefService.nextId(tableName, columnName);

                FileUpload newFileUpload = new FileUpload();
                newFileUpload.setFileId(nextId);
                newFileUpload.setFileName(newFileName);
                newFileUpload.setFileSavePath(newFilePath);
                newFileUpload.setFileUrl(fileServerURL + nextId);
                newFileUpload.setCreateTime(new Date());
                fileUploadMapper.insertSelective(newFileUpload);

                batchOperationLog.setResultFilePath(newFileUpload.getFileSavePath());
                batchOperationLog.setResultFileUrl(newFileUpload.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Successful.getValue());
                batchOperationLog.setResultDesc("批理开户成功");
            } else {
                logger.info("所有号码批量开户失败");
                FileUpload uploadFile = fileUploadMapper.selectByPrimaryKey(batchOperationLog.getFileId());
                batchOperationLog.setResultFilePath(uploadFile.getFileSavePath());
                batchOperationLog.setResultFileUrl(uploadFile.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("所有号码批量开户失败");
            }
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
            logger.info("批量开户结束");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());
            logger.info("处理结果="+ex.getMessage());
            batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLog.setResultDesc(ex.getMessage());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
        }
    }

    public BatchOperationLog getBatchOperationLog() {
        return batchOperationLog;
    }

    public void setBatchOperationLog(BatchOperationLog batchOperationLog) {
        this.batchOperationLog = batchOperationLog;
    }
}
