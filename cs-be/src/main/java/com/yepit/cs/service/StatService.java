package com.yepit.cs.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.OperateOriginEnum;
import com.yepit.cs.domain.*;
import com.yepit.cs.dto.product.PageSearchProductRequest;
import com.yepit.cs.dto.stat.StatByChannelRequest;
import com.yepit.cs.dto.stat.StatByChannelResponse;
import com.yepit.cs.dto.stat.StatByProductRequest;
import com.yepit.cs.dto.stat.StatByProductResponse;
import com.yepit.cs.dto.system.AreaInfo;
import com.yepit.cs.mapper.ProductInfoMapper;
import com.yepit.cs.mapper.StatAllChannelDayMapper;
import com.yepit.cs.mapper.StatChannelSubDayMapper;
import com.yepit.cs.mapper.StatProductSubDayMapper;
import com.yepit.cs.util.AreaUtils;
import com.yepit.cs.util.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 统计相关Service
 * Created by qianlong on 2017/10/1.
 */
@Service
public class StatService {

    final static Logger logger = LoggerFactory.getLogger(StatService.class);

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private StatProductSubDayMapper statProductSubDayMapper;

    @Autowired
    private StatChannelSubDayMapper statChannelSubDayMapper;

    @Autowired
    private StatAllChannelDayMapper statAllChannelDayMapper;

    @Autowired
    private AreaService areaService;

    final static List<Integer> channelList = new ArrayList<Integer>();

    static {
        channelList.add(1);
        channelList.add(2);
        channelList.add(3);
        channelList.add(4);
    }

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd");

    /**
     * 查询按产品维度统计订购数量的结果
     *
     * @param request
     * @return
     */
    public BaseResponse<StatByProductResponse> searchStatResultByProduct(StatByProductRequest request) throws Exception {
        BaseResponse<StatByProductResponse> resp = new BaseResponse<StatByProductResponse>();
        Integer provinceId = request.getProvinceId();
        String statDay = request.getStatDate();
        String queryStatDay = statDay.substring(0, 4) + "-" + statDay.substring(4, 6) + "-" + statDay.substring(6, statDay.length());

        StatByProductResponse statByProductResp = new StatByProductResponse();
        LinkedHashMap<String, List<StatByProductResponse.StatResult>> statResults = new LinkedHashMap<String, List<StatByProductResponse.StatResult>>();

        List<ProductInfo> productList = productInfoMapper.pageList(new PageSearchProductRequest());
        for (ProductInfo productInfo : productList) {
            List<StatByProductResponse.StatResult> productStatResults = new ArrayList<StatByProductResponse.StatResult>();

            //查询总到达数
            StatProductSubDay statCondition = new StatProductSubDay();
            statCondition.setStatDay(statDay);
            statCondition.setAreaCode("9999");
            statCondition.setProductId(productInfo.getProductId());
            List<StatProductSubDay> allCityResult = statProductSubDayMapper.selectByCond(statCondition);
            if (allCityResult != null && allCityResult.size() > 0) {
                StatByProductResponse.StatResult statResult = statByProductResp.new StatResult();
                statResult.setStatObject("到达用户数");
                statResult.setStatNum(allCityResult.get(0).getStatNum());
                productStatResults.add(statResult);
            } else {
                StatByProductResponse.StatResult statResult = statByProductResp.new StatResult();
                statResult.setStatObject("到达用户数");
                statResult.setStatNum(new Long(0));
                productStatResults.add(statResult);
            }

            //根据省份查询下级地市的数据
            List<Area> cityList = areaService.getCityByProvinceId(provinceId);
            for (Area city : cityList) {
                statCondition = new StatProductSubDay();
                statCondition.setStatDay(statDay);
                statCondition.setAreaCode(city.getAreaCode());
                statCondition.setProductId(productInfo.getProductId());

                List<StatProductSubDay> cityResult = statProductSubDayMapper.selectByCond(statCondition);
                if (cityResult != null && cityResult.size() > 0) {
                    for (StatProductSubDay result : cityResult) {
                        StatByProductResponse.StatResult statResult = statByProductResp.new StatResult();
                        statResult.setStatObject(result.getAreaName());
                        statResult.setStatNum(result.getStatNum());
                        productStatResults.add(statResult);
                    }
                }
            }

            statResults.put(productInfo.getProductName(), productStatResults);
        }

        statByProductResp.setStatResult(statResults);
        resp = resp.setSuccessfulResp("查询数据成功");
        resp.setResult(statByProductResp);
        return resp;
    }

    public BaseResponse<StatByChannelResponse> searchStatResultByChannel(StatByChannelRequest request) throws Exception {
        BaseResponse<StatByChannelResponse> resp = new BaseResponse<StatByChannelResponse>();

        String productId = request.getProductId();
        Integer provinceId = request.getProvinceId();
        String startDate = request.getStartDate();
        String endDate = request.getEndDate();

        if (StringUtils.isBlank(productId)) {
            resp = resp.setFailResp("0001", "请选择需要查询的产品");
            return resp;
        }

        if (provinceId == null) {
            resp = resp.setFailResp("0002", "请选择需要查询的区域");
            return resp;
        }

        if (StringUtils.isBlank(startDate)) {
            resp = resp.setFailResp("0003", "请选择需要查询的开始日期");
            return resp;
        }

        if (StringUtils.isBlank(endDate)) {
            resp = resp.setFailResp("0004", "请选择需要查询的结束日期");
            return resp;
        }

        //查询产品
        ProductInfo productInfo = productInfoMapper.selectByPrimaryKey(productId);
        if (productInfo == null) {
            resp = resp.setFailResp("0005", "需要查询的产品不存在");
            return resp;
        }

        StatByChannelResponse statByChannelResp = new StatByChannelResponse();
        LinkedHashMap<String, List<StatByChannelResponse.StatResult>> statResults = new LinkedHashMap<String, List<StatByChannelResponse.StatResult>>();

        List<Area> cityList = areaService.getCityByProvinceId(provinceId);
        for (Area city : cityList) {
            List<StatByChannelResponse.StatResult> channelStatResults = new ArrayList<StatByChannelResponse.StatResult>();

            //查询全渠道用户到达数
            StatAllChannelDay queryCond = new StatAllChannelDay();
            queryCond.setStatKpi(1);
            queryCond.setStatDay(endDate);
            queryCond.setAreaCode(city.getAreaCode());
            queryCond.setProductId(productId);
            List<StatAllChannelDay> dbAllSubsResult = statAllChannelDayMapper.selectByCond(queryCond);
            StatByChannelResponse.StatResult statAllSubsResult = statByChannelResp.new StatResult();
            statAllSubsResult.setStatObject("到达用户数");
            if (dbAllSubsResult != null && dbAllSubsResult.size() > 0
                    && dbAllSubsResult.get(0).getStatNum() != null) {
                statAllSubsResult.setStatNum(dbAllSubsResult.get(0).getStatNum());
            } else {
                statAllSubsResult.setStatNum(new Long(0));
            }
            channelStatResults.add(statAllSubsResult);

            //查询全渠道用户退订数
            queryCond = new StatAllChannelDay();
            queryCond.setStatKpi(2);
            queryCond.setStatDay(endDate);
            queryCond.setAreaCode(city.getAreaCode());
            queryCond.setProductId(productId);
            List<StatAllChannelDay> dbAllUnSubsResult = statAllChannelDayMapper.selectByCond(queryCond);
            StatByChannelResponse.StatResult statAllUnSubsResult = statByChannelResp.new StatResult();
            statAllUnSubsResult.setStatObject("退订用户数");
            if (dbAllUnSubsResult != null && dbAllUnSubsResult.size() > 0
                    && dbAllUnSubsResult.get(0).getStatNum() != null) {
                statAllUnSubsResult.setStatNum(dbAllUnSubsResult.get(0).getStatNum());
            } else {
                statAllUnSubsResult.setStatNum(new Long(0));
            }
            channelStatResults.add(statAllUnSubsResult);

            //查询每个城市每种渠道的新增用户数
            for (Integer channel : channelList) {
                StatChannelSubDay cond = new StatChannelSubDay();
                cond.setChannel(channel);
                cond.setAreaCode(city.getAreaCode());
                cond.setProductId(productId);
                cond.setStartDate(startDate);
                cond.setEndDate(endDate);

                List<StatChannelSubDay> sumResult = statChannelSubDayMapper.sumByCond(cond);
                StatByChannelResponse.StatResult statCityChannelResult = statByChannelResp.new StatResult();
                statCityChannelResult.setStatObject(OperateOriginEnum.getDescByValue(channel));
                if (sumResult != null && sumResult.size() > 0
                        && sumResult.get(0).getStatNum() != null) {
                    statCityChannelResult.setStatNum(sumResult.get(0).getStatNum());
                } else {
                    statCityChannelResult.setStatNum(new Long(0));
                }
                channelStatResults.add(statCityChannelResult);
            }

            statResults.put(city.getAreaName(), channelStatResults);
        }

        statByChannelResp.setStatResult(statResults);
        resp = resp.setSuccessfulResp("查询数据成功");
        resp.setResult(statByChannelResp);
        return resp;
    }


    /**
     * 统计各地区产品订购数量
     *
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse statProductSubscriptions(String statDay) throws Exception {
        logger.info("开始统计" + statDay + "各地区产品订购数量");
        BaseResponse resp = new BaseResponse();
        String queryStatDay = statDay.substring(0, 4) + "-" + statDay.substring(4, 6) + "-" + statDay.substring(6, statDay.length());

        //先删除旧记录
        statProductSubDayMapper.deleteByStatDay(statDay);

        Date statTime = new Date();
        //查询所有产品
        List<ProductInfo> productList = productInfoMapper.pageList(new PageSearchProductRequest());
        List<StatProductSubDay> allStatResult = new ArrayList<>();
        //查询所有省份和直辖市
        for (ProductInfo productInfo : productList) {
            String productId = productInfo.getProductId();
            String productName = productInfo.getProductName();
            String productCode = productInfo.getProductCode();

            //统计当前全国的用户到达量
            StatProductSubDay statAllCondition = new StatProductSubDay();
            statAllCondition.setProductId(productId);
            List<StatProductSubDay> statAllResult = statProductSubDayMapper.executeStat(statAllCondition);
            StatProductSubDay statCountryResult = new StatProductSubDay();
            statCountryResult.setStatDay(statDay);
            statCountryResult.setStatTime(statTime);
            statCountryResult.setAreaCode("9999");
            statCountryResult.setAreaName("全国");
            statCountryResult.setProductId(productId);
            statCountryResult.setProductCode(productCode);
            statCountryResult.setProductName(productName);
            if (statAllResult != null && statAllResult.size() > 0) {
                statCountryResult.setStatNum(statAllResult.get(0).getStatNum());
            } else {
                statCountryResult.setStatNum(new Long(0));
            }
            allStatResult.add(statCountryResult);

            //按省份查询
            HashMap<String, List<AreaInfo>> provinceCityMap = AreaUtils.getProvinceCityMap();
            Iterator iter = provinceCityMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, List<AreaInfo>> entry = (Map.Entry) iter.next();
                String provinceName = entry.getKey();
                System.out.println("provinceName:" + provinceName);
                List<AreaInfo> cityList = entry.getValue();
                long provinceSum = 0;//省份汇总
                for (AreaInfo city : cityList) {
                    logger.info("统计城市:" + city.getCity());
                    StatProductSubDay statCondition = new StatProductSubDay();
                    statCondition.setProductId(productId);
                    statCondition.setAreaCode(city.getCode());
                    List<StatProductSubDay> cityStatResult = statProductSubDayMapper.executeStat(statCondition);
                    for (StatProductSubDay statResult : cityStatResult) {
                        statResult.setStatDay(statDay);
                        statResult.setStatTime(statTime);
                        statResult.setProductName(productName);
                        statResult.setAreaCode(city.getCode());
                        statResult.setAreaName(city.getCity());
                        provinceSum = provinceSum + statResult.getStatNum();
                    }
                    allStatResult.addAll(cityStatResult);
                }
                //加入省份汇总
                StatProductSubDay provinceStatResult = new StatProductSubDay();
                provinceStatResult.setStatDay(statDay);
                provinceStatResult.setStatTime(statTime);
                provinceStatResult.setAreaCode(provinceName);
                provinceStatResult.setAreaName(provinceName);
                provinceStatResult.setProductId(productId);
                provinceStatResult.setProductCode(productCode);
                provinceStatResult.setProductName(productName);
                provinceStatResult.setStatNum(provinceSum);
                allStatResult.add(provinceStatResult);
            }

            //统计未知属地
            StatProductSubDay unknowAreaStatCondition = new StatProductSubDay();
            unknowAreaStatCondition.setProductId(productInfo.getProductId());
            unknowAreaStatCondition.setAreaCode("0000");
            List<StatProductSubDay> unknowAreaStatResult = statProductSubDayMapper.executeStat(unknowAreaStatCondition);
            if (unknowAreaStatResult != null && unknowAreaStatResult.size() > 0) {
                for (StatProductSubDay statResult : unknowAreaStatResult) {
                    statResult.setStatDay(statDay);
                    statResult.setStatTime(statTime);
                    statResult.setAreaCode("0000");
                    statResult.setAreaName("未知属地");
                }
            } else {
                StatProductSubDay statResult = new StatProductSubDay();
                statResult.setStatDay(statDay);
                statResult.setStatTime(statTime);
                statResult.setAreaCode("0000");
                statResult.setAreaName("未知属地");
                statResult.setProductId(productId);
                statResult.setProductCode(productCode);
                statResult.setProductName(productName);
                statResult.setStatNum(new Long(0));
            }
            allStatResult.addAll(unknowAreaStatResult);
        }

        logger.info("本次统计结果[" + statDay + "]:" + JsonUtils.Object2Json(allStatResult));

        //将数据插入数据库
        if(allStatResult != null && allStatResult.size() > 0){
            statProductSubDayMapper.insertBatch(allStatResult);
        }
//        for (StatProductSubDay data : allStatResult) {
//            statProductSubDayMapper.insertSelective(data);
//        }

        resp = resp.setSuccessfulResp("按产品统计订购数据成功");
        return resp;
    }

    /**
     * 统计各渠道的产品订购数量
     *
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse statChannelSubscriptions(String statDay) throws Exception {
        logger.info("开始统计" + statDay + "各渠道产品订购数量");
        BaseResponse resp = new BaseResponse();

        //先删除旧记录
        statChannelSubDayMapper.deleteByStatDay(statDay);

        Date statTime = new Date();
        //查询所有产品
        List<ProductInfo> productList = productInfoMapper.pageList(new PageSearchProductRequest());
        List<StatChannelSubDay> allStatResult = new ArrayList<StatChannelSubDay>();
        //查询所有省份和直辖市
        for (ProductInfo productInfo : productList) {
            //按省份查询
            List<Area> allProvince = areaService.getAllProvinces();
            for (Area province : allProvince) {
                if (StringUtils.isNotBlank(province.getAreaCode())) {//有区号,说明是直辖市
                    List<StatChannelSubDay> statResults = this.statChannelSubDayByCond(productInfo, province, statDay, statTime);
                    if (statResults != null && statResults.size() > 0) {
                        allStatResult.addAll(statResults);
                    }
                } else {//是省份
                    List<Area> cityList = areaService.getCityByProvinceId(province.getAreaId());
                    long provinceStatSum = 0;//省份汇总数量
                    if (cityList != null && cityList.size() > 0) {
                        for (Area city : cityList) {
                            List<StatChannelSubDay> statResults = this.statChannelSubDayByCond(productInfo, city, statDay, statTime);
                            if (statResults != null && statResults.size() > 0) {
                                allStatResult.addAll(statResults);
                                for (StatChannelSubDay result : statResults) {
                                    provinceStatSum = provinceStatSum + result.getStatNum();
                                }
                            }
                        }
                    }
                }
            }

        }
        logger.info("本次统计结果[" + statDay + "]:" + JsonUtils.Object2Json(allStatResult));

        //将数据插入数据库
        if(allStatResult != null && allStatResult.size() > 0){
            statChannelSubDayMapper.insertBatch(allStatResult);
        }
//        for (StatChannelSubDay data : allStatResult) {
//            statChannelSubDayMapper.insertSelective(data);
//        }
        resp = resp.setSuccessfulResp("统计渠道数据成功");
        return resp;
    }

    /**
     * 根据产品、区域查询各个渠道的订购数量
     *
     * @param productInfo
     * @param area
     * @param statDay
     * @param statTime
     * @return
     * @throws Exception
     */
    private List<StatChannelSubDay> statChannelSubDayByCond(final ProductInfo productInfo, final Area area,
                                                            String statDay, Date statTime) throws Exception {
        List<StatChannelSubDay> allStatResults = new ArrayList<StatChannelSubDay>();
        String queryStatDay = statDay.substring(0, 4) + "-" + statDay.substring(4, 6) + "-" + statDay.substring(6, statDay.length());

        String productId = productInfo.getProductId();
        String productCode = productInfo.getProductCode();
        String productName = productInfo.getProductName();

        Area province = null;
        //如果该城市不是省份,还需要统计省份汇总
        if (area.getAreaLevel() == 2) {
            Integer provinceId = area.getParentAreaId();
            province = areaService.getAreaById(provinceId);
        }
        //每种渠道都需要查询
        for (Integer channel : channelList) {
            StatChannelSubDay statCondition = new StatChannelSubDay();
            statCondition.setProductId(productInfo.getProductId());
            statCondition.setAreaCode(area.getAreaCode());
            statCondition.setChannel(channel);
            statCondition.setStatDay(queryStatDay);
            List<StatChannelSubDay> statResults = statChannelSubDayMapper.executeStat(statCondition);
            if (statResults != null && statResults.size() > 0) {
                String areaCode = area.getAreaCode();
                if (StringUtils.isBlank(areaCode)) {
                    areaCode = "0000";
                }
                String areaName = area.getAreaName();
                if (areaCode.equals("0000")) {
                    areaName = "未知属地";
                }
                long sumStatNum = 0;
                for (StatChannelSubDay result : statResults) {
                    if (result.getStatNum() == 0) {
                        continue;
                    }
                    result.setProductId(productId);
                    result.setProductCode(productCode);
                    result.setProductName(productName);
                    result.setAreaCode(areaCode);
                    result.setAreaName(areaName);
                    result.setChannel(channel);
                    result.setStatDay(statDay);
                    result.setStatTime(statTime);
                    sumStatNum = sumStatNum + result.getStatNum();
                    allStatResults.add(result);
                }

//                if (isSumProvince && province != null) {
//                    //加入省份汇总
//                    StatChannelSubDay statProvinceSum = new StatChannelSubDay();
//                    statProvinceSum.setProductId(productId);
//                    statProvinceSum.setProductCode(productCode);
//                    statProvinceSum.setProductName(productName);
//                    statProvinceSum.setStatDay(statDay);
//                    statProvinceSum.setStatTime(statTime);
//                    statProvinceSum.setAreaCode(province.getAreaName());
//                    statProvinceSum.setAreaName(province.getAreaName());
//                    statProvinceSum.setChannel(channel);
//                    statProvinceSum.setStatNum(sumStatNum);
//                    allStatResults.add(statProvinceSum);
//                }
            }
        }

        return allStatResults;
    }

    /**
     * 统计全渠道订购/退订数量
     *
     * @param statDay
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse statAllChannelDay(String statDay) throws Exception {
        logger.info("开始统计" + statDay + "全渠道订购/退订数量");

        BaseResponse resp = new BaseResponse();

        //删除旧数据
        statAllChannelDayMapper.deleteByStatDay(statDay);

        Date statTime = new Date();
        //查询所有产品
        List<ProductInfo> productList = productInfoMapper.pageList(new PageSearchProductRequest());
        List<StatAllChannelDay> allStatResult = new ArrayList<StatAllChannelDay>();
        //查询所有省份和直辖市
        for (ProductInfo productInfo : productList) {
            //按省份查询
            List<Area> allProvince = areaService.getAllProvinces();
            for (Area province : allProvince) {
                if (StringUtils.isNotBlank(province.getAreaCode())) {//有区号,说明是直辖市
                    List<StatAllChannelDay> statResults = this.statAllChannelDayByCond(productInfo, province, statDay, statTime);
                    if (statResults != null && statResults.size() > 0) {
                        allStatResult.addAll(statResults);
                    }
                } else {//统计下级地市
                    List<Area> cityList = areaService.getCityByProvinceId(province.getAreaId());
                    long provinceStatSum = 0;//省份汇总数量
                    if (cityList != null && cityList.size() > 0) {
                        for (Area city : cityList) {
                            List<StatAllChannelDay> statResults = this.statAllChannelDayByCond(productInfo, city, statDay, statTime);
                            if (statResults != null && statResults.size() > 0) {
                                allStatResult.addAll(statResults);
                            }
                        }
                    }

                }
            }
        }

        logger.info("本次统计结果[" + statDay + "]:" + JsonUtils.Object2Json(allStatResult));

        //将数据插入数据库
        if(allStatResult != null && allStatResult.size() > 0){
            statAllChannelDayMapper.insertBatch(allStatResult);
        }
//        for(StatAllChannelDay result:allStatResult){
//            statAllChannelDayMapper.insertSelective(result);
//        }

        resp = resp.setSuccessfulResp("统计全渠道订购/退订数量成功");
        return resp;
    }

    /**
     * @param area
     * @param statDay
     * @param statTime
     * @return
     */
    public List<StatAllChannelDay> statAllChannelDayByCond(final ProductInfo productInfo,
                                                           final Area area, String statDay, Date statTime) throws Exception {
        List<StatAllChannelDay> allStatResults = new ArrayList<StatAllChannelDay>();
        String queryStatDay = statDay.substring(0, 4) + "-" + statDay.substring(4, 6) + "-" + statDay.substring(6, statDay.length());

        String productId = productInfo.getProductId();
        String productCode = productInfo.getProductCode();
        String productName = productInfo.getProductName();

        //查询订购到达量
        StatAllChannelDay statSubCondition = new StatAllChannelDay();
        statSubCondition.setProductId(productId);
        statSubCondition.setAreaCode(area.getAreaCode());
        statSubCondition.setStatDay(queryStatDay);
        statSubCondition.setStatKpi(1);
        List<StatAllChannelDay> subResults = statAllChannelDayMapper.executeStat(statSubCondition);
        if (subResults != null && subResults.size() > 0) {
            allStatResults.addAll(subResults);
        }

        //查询退订到达量
        StatAllChannelDay statUnsubCondition = new StatAllChannelDay();
        statUnsubCondition.setProductId(productId);
        statUnsubCondition.setAreaCode(area.getAreaCode());
        statUnsubCondition.setStatDay(queryStatDay);
        statUnsubCondition.setStatKpi(2);
        List<StatAllChannelDay> unSubResults = statAllChannelDayMapper.executeStat(statUnsubCondition);
        if (unSubResults != null && unSubResults.size() > 0) {
            allStatResults.addAll(unSubResults);
        }

        for (StatAllChannelDay result : allStatResults) {
            result.setStatTime(statTime);
            result.setProductId(productId);
            result.setProductCode(productCode);
            result.setProductName(productName);
            result.setAreaCode(area.getAreaCode());
            result.setAreaName(area.getAreaName());
        }

        return allStatResults;
    }

    public static void main(String[] args) throws Exception {
        String statDay = "20170509";
        String queryStatDay = statDay.substring(0, 4) + "-" + statDay.substring(4, 6) + "-" + statDay.substring(6, statDay.length());
        System.out.println(queryStatDay);
    }
}
