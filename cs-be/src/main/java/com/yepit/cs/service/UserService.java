package com.yepit.cs.service;

import com.linkage.lccmp.client.UserInterface.UserInterfaceProxy;
import com.linkage.lccmp.common.ClientObject;
import com.linkage.lccmp.common.ResultObject;
import com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.BlackTypeEnum;
import com.yepit.cs.constant.PhoneTypeEnum;
import com.yepit.cs.constant.SequenceDefConstant;
import com.yepit.cs.domain.BlackUser;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.domain.UserInfo;
import com.yepit.cs.dto.system.AreaInfo;
import com.yepit.cs.dto.user.*;
import com.yepit.cs.mapper.BlackUserMapper;
import com.yepit.cs.mapper.FileUploadMapper;
import com.yepit.cs.mapper.UserInfoMapper;
import com.yepit.cs.util.*;
import com.yepit.cs.util.idgenerate.DefaultIdGenerator;
import com.yepit.cs.util.idgenerate.IdGenerator;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by qianlong on 2017/8/31.
 */
@Service
public class UserService {

    @Value("${system.file.savepath}")
    private String fileSavePath;

    @Autowired
    private BlackUserMapper blackUserMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Autowired
    private FileUploadMapper fileUploadMapper;

    @Value("${system.fileServerURL}")
    private String fileServerURL;

    /**
     * 创建黑名单用户
     *
     * @param request
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse<CreateBlackUserResponse> addBlackUser(CreateBlackUserRequest request) throws Exception {
        if (StringUtils.isBlank(request.getPhonenumber())) {
            return new BaseResponse<CreateBlackUserResponse>(false, "000001", "请输入号码");
        }
        if (request.getBlackType() == null) {
            return new BaseResponse<CreateBlackUserResponse>(false, "000002", "请输入黑名单类型");
        }
        BlackUser blackUser = new BlackUser();
        BeanUtils.copyProperties(request, blackUser);
        blackUser.setCreateOperator(request.getOperName());
        blackUser.setCreateTime(new Date());

        //查询是否在黑名单中
        List<BlackUser> blackUserList = blackUserMapper.listByCond(blackUser);
        if (blackUserList != null && blackUserList.size() > 0) {
            return new BaseResponse<CreateBlackUserResponse>(false, "000003", "该用户已经在黑名单中");
        }

        //获取主键
        String blackUserId = sequenceDefService.nextId(SequenceDefConstant.BlackUser.TABLE_NAME,
                SequenceDefConstant.BlackUser.COLUMN_NAME);//获取主键
        blackUser.setBlackUserId(blackUserId);

        blackUserMapper.insertSelective(blackUser);

        BaseResponse<CreateBlackUserResponse> resp = new BaseResponse<CreateBlackUserResponse>();
        resp = resp.setSuccessfulResp("添加黑名单用户成功");
        CreateBlackUserResponse createBlackUserResponse = new CreateBlackUserResponse();
        BeanUtils.copyProperties(blackUser, createBlackUserResponse);
        createBlackUserResponse.setBlackTypeName(BlackTypeEnum.getDescByValue(blackUser.getBlackType()));
        resp.setResult(createBlackUserResponse);
        return resp;
    }

    /**
     * 删除黑名单
     *
     * @param phonenumber
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse delBlackUser(String phonenumber) throws Exception {
        if (StringUtils.isBlank(phonenumber)) {
            return new BaseResponse<CreateBlackUserResponse>(false, "000001", "请输入号码");
        }

        BlackUser cond = new BlackUser();
        cond.setPhonenumber(phonenumber);
        List<BlackUser> blackUserList = blackUserMapper.listByCond(cond);
        if (blackUserList != null && blackUserList.size() > 0) {
            for (BlackUser blackUser : blackUserList) {
                blackUserMapper.deleteByPrimaryKey(blackUser.getBlackUserId());
            }
            return new BaseResponse<CreateBlackUserResponse>(true, "000000", "已从黑名单中移除");
        } else {
            return new BaseResponse<CreateBlackUserResponse>(false, "000002", "该号码不在黑名单中");
        }
    }

    /**
     * 根据号码查询是否在黑名单中
     *
     * @param phonenumber
     * @return
     * @throws Exception
     */
    public BaseResponse<BlackUserInfo> getBlackUserByPhone(String phonenumber) throws Exception {
        if (StringUtils.isBlank(phonenumber)) {
            return new BaseResponse<BlackUserInfo>(false, "000001", "请输入号码");
        }

        BlackUser cond = new BlackUser();
        cond.setPhonenumber(phonenumber);
        List<BlackUser> blackUserList = blackUserMapper.listByCond(cond);
        if (blackUserList != null && blackUserList.size() > 0) {
            BaseResponse<BlackUserInfo> resp = new BaseResponse<BlackUserInfo>();
            BlackUserInfo blackUserInfo = new BlackUserInfo();
            BeanUtils.copyProperties(blackUserList.get(0), blackUserInfo);
            resp = resp.setSuccessfulResp("查询黑名单成功");
            resp.setResult(blackUserInfo);
            return resp;
        } else {
            return new BaseResponse<BlackUserInfo>(false, "000002", "该号码不在黑名单中");
        }
    }

    /**
     * 黑名单过滤
     *
     * @param request
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse<BlackUserFilterResponse> blackUserFilter(BlackUserFilterRequest request) throws Exception {
        BaseResponse<BlackUserFilterResponse> resp = new BaseResponse<BlackUserFilterResponse>();
        String fileId = request.getFileId();

        FileUpload fileUpload = fileUploadMapper.selectByPrimaryKey(fileId);
        String filePath = fileUpload.getFileSavePath();
        File file = new File(filePath);
        if (!file.exists()) {
            resp = resp.setFailResp("000001", "要过滤的号码文件不存在");
            return resp;
        }

        //将不在黑名单中的号码过滤出来
        List<List<String>> normalPhoneList = new ArrayList<List<String>>();
        List<List<String>> phoneList = ExcelUtils.readWorkBook(filePath, 0);
        if (phoneList != null && phoneList.size() > 0) {
            for (List<String> phoneInfo : phoneList) {
                String phonenumber = phoneInfo.get(0);
                BlackUser cond = new BlackUser();
                cond.setPhonenumber(phonenumber);
                List<BlackUser> blackUserList = blackUserMapper.listByCond(cond);
                if (blackUserList == null || blackUserList.size() == 0) {
                    List<String> normalPhone = new ArrayList<String>();
                    normalPhone.add(phonenumber);
                    normalPhoneList.add(normalPhone);
                }
            }
        }

        //创建正常用户名单
        if (normalPhoneList != null && normalPhoneList.size() > 0) {
            String newFileName = fileUpload.getFileId() + "_normal.xlsx";
            String normalPhonePath = fileSavePath + File.separator + newFileName;
            ExcelUtils.createXlsx("正常用户名单", null, normalPhoneList, normalPhonePath);

            //获取主键
            String tableName = SequenceDefConstant.FileUpload.TABLE_NAME;
            String columnName = SequenceDefConstant.FileUpload.COLUMN_NAME;
            String nextId = sequenceDefService.nextId(tableName, columnName);

            FileUpload newFileUpload = new FileUpload();
            newFileUpload.setFileId(nextId);
            newFileUpload.setFileName(newFileName);
            newFileUpload.setFileSavePath(normalPhonePath);
            newFileUpload.setFileUrl(fileServerURL + newFileName);
            newFileUpload.setCreateTime(new Date());
            fileUploadMapper.insertSelective(newFileUpload);

            resp = resp.setSuccessfulResp("黑名单过滤成功");
            BlackUserFilterResponse blackUserFilterResponse = new BlackUserFilterResponse();
            blackUserFilterResponse.setDownloadUrl(newFileUpload.getFileUrl());
            resp.setResult(blackUserFilterResponse);
        } else {
            resp = resp.setFailResp("000002", "该名单全在黑名单中");
        }
        return resp;
    }

    /**
     * 校验10000密码
     *
     * @param request
     * @return
     * @throws Exception
     */
    public BaseResponse userAuth(UserAuthRequest request) throws Exception {
        SimpleUserInfo userInfo = new SimpleUserInfo();
        userInfo.setCallusertype("2");
        userInfo.setPhonenumber(request.getPhoneNumber());

        ClientObject co = new ClientObject();
        co.setOperator("qiyeguanjia");
        co.setOrigin(40);

        UserInterfaceProxy proxy = new UserInterfaceProxy();
        ResultObject ro = proxy.authUser(userInfo, request.getPassword(), co);
        BaseResponse resp = new BaseResponse();
        if (ro != null && ro.getResult().equals("000")) {
            resp = resp.setSuccessfulResp(ro.getDescription());
        } else {
            resp = resp.setFailResp("000001", ro.getDescription());
        }
        return resp;
    }
}
