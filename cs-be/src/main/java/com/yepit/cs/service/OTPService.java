package com.yepit.cs.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.OTPTypeEnum;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.util.NumberUtils;
import com.yepit.cs.util.PasswordUtils;
import com.yepit.cs.util.RedisUtils;
import com.yepit.cs.util.otp.JuHeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 验证码获取
 */
@Service
public class OTPService {

    final static Logger LOGGER = LoggerFactory.getLogger(OTPService.class);

    @Autowired
    private RedisUtils redisUtils;

    @Value("${otp.expiration}")
    private Long otpExpiration;

    @Value("${otp.switch}")
    private Integer isSendOTP;

    /**
     * 从聚合数据平台获取验证码
     *
     * @return
     */
    public BaseResponse<String> getOTPViaJuhe(Integer otpType, String phoneNumber,String tplId) {
        if (StringUtils.isBlank(phoneNumber)) {
            throw new ServiceException("000001", "用户号码不能为空");
        }

        //校验该号码的验证码是否在有效期内,只有不在有效期内才会重新发送
        String key = OTPTypeEnum.getDescByValue(otpType) + "_" + phoneNumber;
        Object oldOtp = redisUtils.get(key);
        if (oldOtp != null) {
            throw new ServiceException("000002", "您的操作太频繁了,请稍候再试!");
        }

        //生成6位随机OTP
        String otp = PasswordUtils.getNumberRandom(6);
        redisUtils.set(key, otp, otpExpiration);

        String otpConent = "您的验证码为" + otp + ",1分钟内有效.";

        String resultDesc = otpConent;
        if (isSendOTP == 1) {
            //是固话,则发送语音验证码
            if (NumberUtils.isFixedPhone(phoneNumber)) {
                LOGGER.info("向{}发送语音验证码{}", phoneNumber, otp);
                JuHeUtils.sendVoiceByYXT(phoneNumber, otp);
//                resultDesc = "已发送语音验证码,请注意来电";
                resultDesc = "验证码已呼叫您的号码" + phoneNumber + "，请注意接听";
            }

            //是手机号码,则发送短信验证码
            if (NumberUtils.isMobilePhone(phoneNumber)) {
                LOGGER.info("向{}发送短信验证码{}", phoneNumber, otp);
                if(StringUtils.isBlank(tplId)){
                    tplId = "126488";
                }
                JuHeUtils.sendSms(phoneNumber, tplId, otp);
                resultDesc = "已发送短信验证码,请注意手机短信";
            }
        }

        BaseResponse<String> resp = new BaseResponse<String>();
        resp = resp.setSuccessfulResp(resultDesc);
        resp.setResult(resultDesc);
        return resp;
    }

    /**
     * 校验验证码
     *
     * @param otpType
     * @param phoneNumber
     * @param otp
     * @return
     */
    public BaseResponse verifyOTP(Integer otpType, String phoneNumber, String otp) {
        if (StringUtils.isBlank(phoneNumber)) {
            throw new ServiceException("000001", "用户号码不能为空");
        }
        if (StringUtils.isBlank(otp)) {
            throw new ServiceException("000002", "验证码不能为空");
        }
        String key = OTPTypeEnum.getDescByValue(otpType) + "_" + phoneNumber;
        Object otpObj = redisUtils.get(key);
        if (otpObj == null) {
            throw new ServiceException("000003", "验证码已过期,请重新获取");
        }

        if (!otp.equals((String) otpObj)) {
            throw new ServiceException("000004", "验证码不正确");
        }
        BaseResponse resp = new BaseResponse();
        resp = resp.setSuccessfulResp("验证通过");
        return resp;
    }


}
