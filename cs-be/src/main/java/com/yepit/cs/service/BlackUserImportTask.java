package com.yepit.cs.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.BatchOptStatusEnum;
import com.yepit.cs.constant.BlackTypeEnum;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.constant.SequenceDefConstant;
import com.yepit.cs.domain.BatchOperationLog;
import com.yepit.cs.domain.BlackUser;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.dto.user.CreateBlackUserRequest;
import com.yepit.cs.dto.user.CreateBlackUserResponse;
import com.yepit.cs.mapper.BatchOperationLogMapper;
import com.yepit.cs.mapper.BlackUserMapper;
import com.yepit.cs.mapper.FileUploadMapper;
import com.yepit.cs.util.ExcelUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by qianlong on 2017/9/3.
 */
@Component("blackUserImportTask")
@Scope("prototype")
public class BlackUserImportTask extends Thread {

    final static Logger logger = LoggerFactory.getLogger(BlackUserImportTask.class);

    private BatchOperationLog batchOperationLog;

    @Autowired
    private BatchOperationLogMapper batchOperationLogMapper;

    @Autowired
    private FileUploadMapper fileUploadMapper;

    @Autowired
    private BlackUserMapper blackUserMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Autowired
    private UserService userService;

    @Value("${system.batch.savepath}")
    private String batchFileSavePath;

    @Value("${system.fileServerURL}")
    private String fileServerURL;

    @SuppressWarnings("all")
    @Override
    public void run() {
        try {
            logger.info("黑名单导入开始");
            //将文件状态改为处理中
            batchOperationLog.setStatus(BatchOptStatusEnum.Processing.getValue());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);

            String uploadFilePath = batchOperationLog.getUploadFilePath();
            File file = new File(uploadFilePath);
            if (!file.exists()) {
                logger.error("批处理文件[" + uploadFilePath + "]不存在,无法处理");
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("批处理文件不存在,无法处理");
                batchOperationLog.setUpdateTime(new Date());
                batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
                batchOperationLog.setResultFilePath(null);
                batchOperationLog.setResultFileUrl(null);
                batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
                return;
            }

            //读取黑名单用户
            List<List<String>> phoneNumbers = ExcelUtils.readWorkBook(uploadFilePath, 0);
            List<List<String>> resultList = new ArrayList<List<String>>();
            if (phoneNumbers != null && phoneNumbers.size() > 0) {
                for (List<String> phoneInfo : phoneNumbers) {
                    if(phoneInfo.size() < 1){
                        continue;
                    }
                    String phoneNumber = phoneInfo.get(0);
                    if(StringUtils.isBlank(phoneNumber)){
                        logger.info("读取到的用户号码为空");
                        continue;
                    }
                    logger.info("用户号码=" + phoneNumber);
                    BaseResponse<CreateBlackUserResponse> resp = new BaseResponse<CreateBlackUserResponse>();
                    try{
                        //新增黑名单用户
                        CreateBlackUserRequest request = new CreateBlackUserRequest();
                        request.setOperName(batchOperationLog.getCreateOperator());
                        request.setBlackType(BlackTypeEnum.CTC.getValue());
                        request.setPhonenumber(phoneNumber);
                        resp = userService.addBlackUser(request);
                    }catch (Exception ex){
                        ex.printStackTrace();
                        resp = resp.setFailResp(ResultCodeConstant.UNKNOWERR,ex.getMessage());
                    }

                    //保存添加结果
                    List<String> result = new ArrayList<String>();
                    result.add(phoneNumber);
                    result.add(resp.getResultDesc());
                    resultList.add(result);
                }
            }

            //创建结果文件
            if (resultList != null && resultList.size() > 0) {
                logger.info("共处理了" + resultList.size() + "条记录");
                String newFileName = batchOperationLog.getBatchId() + "_result.xlsx";
                String newFilePath = batchFileSavePath + File.separator + newFileName;
                List<String> header = new ArrayList<String>();
                header.add("用户号码");
                header.add("导入结果");
                ExcelUtils.createXlsx("黑名单导入结果", header, resultList, newFilePath);

                //获取主键
                String tableName = SequenceDefConstant.FileUpload.TABLE_NAME;
                String columnName = SequenceDefConstant.FileUpload.COLUMN_NAME;
                String nextId = sequenceDefService.nextId(tableName, columnName);

                FileUpload newFileUpload = new FileUpload();
                newFileUpload.setFileId(nextId);
                newFileUpload.setFileName(newFileName);
                newFileUpload.setFileSavePath(newFilePath);
                newFileUpload.setFileUrl(fileServerURL + nextId);
                newFileUpload.setCreateTime(new Date());
                fileUploadMapper.insertSelective(newFileUpload);

                batchOperationLog.setResultFilePath(newFileUpload.getFileSavePath());
                batchOperationLog.setResultFileUrl(newFileUpload.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Successful.getValue());
                batchOperationLog.setResultDesc("黑名单过滤成功");
            } else {
                logger.info("所有号码导入失败");
                FileUpload uploadFile = fileUploadMapper.selectByPrimaryKey(batchOperationLog.getFileId());
                batchOperationLog.setResultFilePath(uploadFile.getFileSavePath());
                batchOperationLog.setResultFileUrl(uploadFile.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("所有号码导入失败");
            }
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
            logger.info("黑名单导入结束");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());
            batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLog.setResultDesc(ex.getMessage());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
        }
    }

    public BatchOperationLog getBatchOperationLog() {
        return batchOperationLog;
    }

    public void setBatchOperationLog(BatchOperationLog batchOperationLog) {
        this.batchOperationLog = batchOperationLog;
    }
}
