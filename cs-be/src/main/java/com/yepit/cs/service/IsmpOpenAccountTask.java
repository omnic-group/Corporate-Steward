package com.yepit.cs.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.*;
import com.yepit.cs.domain.BatchOperationLog;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.ismp.IsmpDirectSubProductRequest;
import com.yepit.cs.dto.order.SubscribeProductResponse;
import com.yepit.cs.mapper.BatchOperationLogMapper;
import com.yepit.cs.mapper.FileUploadMapper;
import com.yepit.cs.mapper.ProductInfoMapper;
import com.yepit.cs.util.ExcelUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 批量ISMP开户
 * Created by qianlong on 2019/10/20.
 */
@Component("ismpOpenAccountTask")
@Scope("prototype")
public class IsmpOpenAccountTask extends Thread {

    final static Logger logger = LoggerFactory.getLogger(IsmpOpenAccountTask.class);

    private BatchOperationLog batchOperationLog;

    @Autowired
    private BatchOperationLogMapper batchOperationLogMapper;

    @Autowired
    private FileUploadMapper fileUploadMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private IsmpOrderService ismpOrderService;

    @Value("${system.batch.savepath}")
    private String batchFileSavePath;

    @Value("${system.fileServerURL}")
    private String fileServerURL;

    @SuppressWarnings("all")
    @Override
    public void run() {
        try {
            logger.info("ISMP批量开户开始");
            //将文件状态改为处理中
            batchOperationLog.setStatus(BatchOptStatusEnum.Processing.getValue());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);

            String uploadFilePath = batchOperationLog.getUploadFilePath();
            File file = new File(uploadFilePath);
            if (!file.exists()) {
                logger.error("批处理文件[" + uploadFilePath + "]不存在,无法处理");
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("批处理文件不存在,无法处理");
                batchOperationLog.setUpdateTime(new Date());
                batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
                batchOperationLog.setResultFilePath(null);
                batchOperationLog.setResultFileUrl(null);
                batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
                return;
            }

            //读取待开户数据
            List<List<String>> dataList = ExcelUtils.readWorkBook(uploadFilePath, 1);
            List<List<String>> resultList = new ArrayList<List<String>>();
            if (dataList != null && dataList.size() > 0) {
                for (List<String> accountInfo : dataList) {
                    if (accountInfo.size() < 2) {
                        continue;
                    }
                    List<String> result = new ArrayList<String>();

                    String phoneNumber = accountInfo.get(0);
                    String productCode = accountInfo.get(1);//产品编码

                    if (StringUtils.isBlank(phoneNumber) && StringUtils.isBlank(productCode)) {
                        continue;
                    }

                    result.add(phoneNumber.trim());
                    result.add(productCode);

                    logger.info("----------------------------------------------------");
                    logger.info("用户号码=" + phoneNumber + "|||产品编码=" + productCode);


                    //根据产品编码获取产品ID
                    if (StringUtils.isBlank(productCode)) {
                        result.add("产品编码不能为空");
                        resultList.add(result);
                        continue;
                    }

                    ProductInfo cond = new ProductInfo();
                    cond.setProductCode(productCode);
                    List<ProductInfo> productInfoList = productInfoMapper.listByCond(cond);
                    if (productInfoList == null || productInfoList.size() == 0) {
                        result.add("要订购的产品不存在");
                        resultList.add(result);
                        continue;
                    }
                    phoneNumber = phoneNumber.trim();

                    BaseResponse<SubscribeProductResponse> resp = new BaseResponse<SubscribeProductResponse>();
                    try {
                        //发起开户请求
                        IsmpDirectSubProductRequest request = new IsmpDirectSubProductRequest();
                        request.setOperName(batchOperationLog.getCreateOperator());
                        request.setProductCode(productCode);
//                        request.setOperateOrigin(OperateOriginEnum.Batch.getValue());
//                        request.setTelecomOperator(operatorType);
                        request.setPhoneNumber(phoneNumber);
                        resp = ismpOrderService.batchSubscribeProduct(request);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        resp = resp.setFailResp(ResultCodeConstant.UNKNOWERR, ex.getMessage());
                    }

                    logger.info("用户号码=" + phoneNumber
                            + "|||产品编码=" + productCode + "|||处理结果=" + resp.getResultDesc());
                    //保存添加结果
                    if (resp.isSuccess()) {
                        result.add("处理成功");
                    } else {
                        result.add("处理失败");
                    }
                    result.add(resp.getResultDesc());
                    resultList.add(result);

                    try{
                        //每条处理延迟2s
                        Thread.sleep(2000);
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            //创建结果文件
            if (resultList != null && resultList.size() > 0) {
                logger.info("共处理了" + resultList.size() + "条记录");
                String newFileName = batchOperationLog.getBatchId() + "_result.xlsx";
                String newFilePath = batchFileSavePath + File.separator + newFileName;
                List<String> header = new ArrayList<String>();
                header.add("用户号码");
                header.add("产品编号");
                header.add("处理结果");
                header.add("处理结果描述");
                ExcelUtils.createXlsx("批量开户结果", header, resultList, newFilePath);

                //获取主键
                String tableName = SequenceDefConstant.FileUpload.TABLE_NAME;
                String columnName = SequenceDefConstant.FileUpload.COLUMN_NAME;
                String nextId = sequenceDefService.nextId(tableName, columnName);

                FileUpload newFileUpload = new FileUpload();
                newFileUpload.setFileId(nextId);
                newFileUpload.setFileName(newFileName);
                newFileUpload.setFileSavePath(newFilePath);
                newFileUpload.setFileUrl(fileServerURL + nextId);
                newFileUpload.setCreateTime(new Date());
                fileUploadMapper.insertSelective(newFileUpload);

                batchOperationLog.setResultFilePath(newFileUpload.getFileSavePath());
                batchOperationLog.setResultFileUrl(newFileUpload.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Successful.getValue());
                batchOperationLog.setResultDesc("批理开户成功");
            } else {
                logger.info("所有号码批量开户失败");
                FileUpload uploadFile = fileUploadMapper.selectByPrimaryKey(batchOperationLog.getFileId());
                batchOperationLog.setResultFilePath(uploadFile.getFileSavePath());
                batchOperationLog.setResultFileUrl(uploadFile.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("所有号码批量开户失败");
            }
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
            logger.info("ISMP批量开户结束");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());
            logger.info("处理结果=" + ex.getMessage());
            batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLog.setResultDesc(ex.getMessage());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
        }
    }

    public BatchOperationLog getBatchOperationLog() {
        return batchOperationLog;
    }

    public void setBatchOperationLog(BatchOperationLog batchOperationLog) {
        this.batchOperationLog = batchOperationLog;
    }
}
