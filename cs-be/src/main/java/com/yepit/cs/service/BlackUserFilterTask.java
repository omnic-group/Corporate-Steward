package com.yepit.cs.service;

import com.yepit.cs.constant.BatchOptStatusEnum;
import com.yepit.cs.constant.SequenceDefConstant;
import com.yepit.cs.domain.BatchOperationLog;
import com.yepit.cs.domain.BlackUser;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.mapper.BatchOperationLogMapper;
import com.yepit.cs.mapper.BlackUserMapper;
import com.yepit.cs.mapper.FileUploadMapper;
import com.yepit.cs.util.ExcelUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by qianlong on 2017/9/3.
 */
@Component("blackUserFilterTask")
@Scope("prototype")
public class BlackUserFilterTask extends Thread {

    final static Logger logger = LoggerFactory.getLogger(BlackUserFilterTask.class);

    private BatchOperationLog batchOperationLog;

    @Autowired
    private BatchOperationLogMapper batchOperationLogMapper;

    @Autowired
    private FileUploadMapper fileUploadMapper;

    @Autowired
    private BlackUserMapper blackUserMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Value("${system.batch.savepath}")
    private String batchFileSavePath;

    @Value("${system.fileServerURL}")
    private String fileServerURL;

    @SuppressWarnings("all")
    @Override
    public void run() {
        try {
            logger.info("黑名单过滤开始");
            //将文件状态改为处理中
            batchOperationLog.setStatus(BatchOptStatusEnum.Processing.getValue());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);

            String uploadFilePath = batchOperationLog.getUploadFilePath();
            File file = new File(uploadFilePath);
            if (!file.exists()) {
                logger.error("批处理文件[" + uploadFilePath + "]不存在,无法处理");
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("批处理文件不存在,无法处理");
                batchOperationLog.setUpdateTime(new Date());
                batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
                batchOperationLog.setResultFilePath(null);
                batchOperationLog.setResultFileUrl(null);
                batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
                return;
            }

            //将不在黑名单中的号码过滤出来
            List<List<String>> normalPhoneList = new ArrayList<List<String>>();
            List<List<String>> phoneNumbers = ExcelUtils.readWorkBook(uploadFilePath, 0);
            if (phoneNumbers != null && phoneNumbers.size() > 0) {
                for (List<String> phoneInfo : phoneNumbers) {
                    String phoneNumber = phoneInfo.get(0);

                    logger.info("用户号码=" + phoneNumber);
                    if(StringUtils.isBlank(phoneNumber)){
                        continue;
                    }

                    BlackUser cond = new BlackUser();
                    cond.setPhonenumber(phoneNumber);
                    List<BlackUser> blackUserList = blackUserMapper.listByCond(cond);
                    if (blackUserList == null || blackUserList.size() == 0) {
                        List<String> normalPhone = new ArrayList<String>();
                        normalPhone.add(phoneNumber);
                        normalPhoneList.add(normalPhone);
                    }
                }
            }

            //创建正常用户名单
            if (normalPhoneList != null && normalPhoneList.size() > 0) {
                logger.info("共有正常用户" + normalPhoneList.size() + "条");
                String newFileName = batchOperationLog.getBatchId() + "_result.xlsx";
                String normalPhonePath = batchFileSavePath + File.separator + newFileName;
                ExcelUtils.createXlsx("正常用户名单", null, normalPhoneList, normalPhonePath);

                //获取主键
                String tableName = SequenceDefConstant.FileUpload.TABLE_NAME;
                String columnName = SequenceDefConstant.FileUpload.COLUMN_NAME;
                String nextId = sequenceDefService.nextId(tableName, columnName);

                FileUpload newFileUpload = new FileUpload();
                newFileUpload.setFileId(nextId);
                newFileUpload.setFileName(newFileName);
                newFileUpload.setFileSavePath(normalPhonePath);
                newFileUpload.setFileUrl(fileServerURL + nextId);
                newFileUpload.setCreateTime(new Date());
                fileUploadMapper.insertSelective(newFileUpload);

                batchOperationLog.setResultFilePath(newFileUpload.getFileSavePath());
                batchOperationLog.setResultFileUrl(newFileUpload.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Successful.getValue());
                batchOperationLog.setResultDesc("黑名单过滤成功");
            } else {
                logger.info("文件中所有号码都在黑名单中");
                FileUpload uploadFile = fileUploadMapper.selectByPrimaryKey(batchOperationLog.getFileId());
                batchOperationLog.setResultFilePath(uploadFile.getFileSavePath());
                batchOperationLog.setResultFileUrl(uploadFile.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Successful.getValue());
                batchOperationLog.setResultDesc("文件中所有号码都在黑名单中");
            }
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
            logger.info("黑名单过滤结束");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());
            batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLog.setResultDesc(ex.getMessage());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
        }
    }

    public BatchOperationLog getBatchOperationLog() {
        return batchOperationLog;
    }

    public void setBatchOperationLog(BatchOperationLog batchOperationLog) {
        this.batchOperationLog = batchOperationLog;
    }
}
