package com.yepit.cs.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.SequenceDefConstant;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.dto.file.FileInfoDTO;
import com.yepit.cs.dto.file.FileUploadRequest;
import com.yepit.cs.dto.file.FileUploadResponse;
import com.yepit.cs.dto.file.UploadFileDTO;
import com.yepit.cs.mapper.FileUploadMapper;
import com.yepit.cs.util.FileUtils;
import com.yepit.cs.util.idgenerate.DefaultIdGenerator;
import com.yepit.cs.util.idgenerate.IdGenerator;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by qianlong on 2017/8/23.
 */
@Service
public class FileService {

    @Value("${system.file.savepath}")
    private String fileSavePath;

    @Value("${system.fileServerURL}")
    private String fileServerURL;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Autowired
    private FileUploadMapper fileUploadMapper;

    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse<FileUploadResponse> upload(FileUploadRequest request) throws Exception {
        BaseResponse<FileUploadResponse> resp = new BaseResponse<FileUploadResponse>();

        List<UploadFileDTO> uploadFileList= request.getUploadFiles();
        if(uploadFileList == null || uploadFileList.size() == 0){
            resp = resp.setFailResp("000001","请选择需要上传的文件");
            return resp;
        }

        List<FileInfoDTO> fileInfoDTOList = new ArrayList<FileInfoDTO>();
        int i= 0;
        for(UploadFileDTO uploadFile:uploadFileList){
            /**
             * 由于前端转过来的数据格式为data:image/jpeg;base64,图像base64字符串,因此需要处理一下
             */
            final String subStr = "base64,";

            String base64Str = uploadFile.getBase64Str();
            if(base64Str.indexOf("base64,") > 0){
                base64Str = base64Str.substring(base64Str.indexOf(subStr) + subStr.length(), base64Str.length());
            }

            String fileName = uploadFile.getFileName();
            String filePostfix = fileName.substring(fileName.indexOf("."),fileName.length());

            //获取主键
            String tableName = SequenceDefConstant.FileUpload.TABLE_NAME;
            String columnName = SequenceDefConstant.FileUpload.COLUMN_NAME;
            String nextId = sequenceDefService.nextId(tableName, columnName);

            //获取新的文件名
            IdGenerator idGenerator = new DefaultIdGenerator();
            String fileId = idGenerator.next();
            String newFileName = fileId + i + filePostfix;
            String newFilePath = fileSavePath + File.separator + newFileName;
            FileUtils.base64ToFile(base64Str, newFilePath);

            FileUpload fileUpload = new FileUpload();
            fileUpload.setFileId(nextId);
            fileUpload.setFileName(newFileName);
            fileUpload.setFileSavePath(newFilePath);
            fileUpload.setFileUrl(fileServerURL+nextId);
            fileUpload.setCreateTime(new Date());
            fileUploadMapper.insertSelective(fileUpload);

            FileInfoDTO fileInfoDTO = new FileInfoDTO();
            fileInfoDTO.setFileId(nextId);
            fileInfoDTO.setFileName(newFileName);
            fileInfoDTO.setLink(fileUpload.getFileUrl());
            fileInfoDTOList.add(fileInfoDTO);

            i++;
        }

        FileUploadResponse fileUploadResponse = new FileUploadResponse();
        fileUploadResponse.setFileList(fileInfoDTOList);
        resp = resp.setSuccessfulResp("文件上传成功");
        resp.setResult(fileUploadResponse);
        return resp;
    }

    public BaseResponse<FileUpload> getUploadFile(String fileId) throws Exception{
        BaseResponse<FileUpload> resp = new BaseResponse<FileUpload>();
        if(StringUtils.isBlank(fileId)){
            return new BaseResponse<FileUpload>(false,"000001","文件不存在");
        }
        FileUpload fileUpload = fileUploadMapper.selectByPrimaryKey(fileId);
        if(fileUpload == null){
            return new BaseResponse<FileUpload>(false,"000001","文件不存在");
        }
        resp = resp.setSuccessfulResp("查询文件信息成功");
        resp.setResult(fileUpload);
        return resp;
    }

}
