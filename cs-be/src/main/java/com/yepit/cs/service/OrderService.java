package com.yepit.cs.service;

import com.linkage.lccmp.client.MusicSetsSubscribeInterface.MusicSetsSubscribeInterfaceProxy;
import com.linkage.lccmp.common.ClientObject;
import com.linkage.lccmp.common.ResultObject;
import com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.*;
import com.yepit.cs.domain.*;
import com.yepit.cs.dto.order.*;
import com.yepit.cs.dto.system.AreaInfo;
import com.yepit.cs.dto.user.PhoneInfo;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.mapper.*;
import com.yepit.cs.util.AreaUtils;
import com.yepit.cs.util.JsonUtils;
import com.yepit.cs.util.MobileNoTrack;
import com.yepit.cs.util.NumberUtils;
import com.yepit.cs.vo.ProductBlackRuleVO;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by qianlong on 2017/8/26.
 */
@Service
public class OrderService {

    protected static Logger log = Logger.getLogger(OrderService.class);

    @Autowired
    private ProductSubscribeMapper productSubscribeMapper;

    @Autowired
    private OrderLogMapper orderLogMapper;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private BlackUserMapper blackUserMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Autowired
    private OrderAttachmentMapper orderAttachmentMapper;

    @Autowired
    private UserSubscribeMapper userSubscribeMapper;

    @Autowired
    private ProductBlackListMapper productBlackListMapper;

    public static HashMap<String, String> JiangSuAreaMap = new HashMap<String, String>();

    static {
        JiangSuAreaMap.put("025", "南京");
        JiangSuAreaMap.put("0510", "无锡");
        JiangSuAreaMap.put("0511", "镇江");
        JiangSuAreaMap.put("0512", "苏州");
        JiangSuAreaMap.put("0513", "南通");
        JiangSuAreaMap.put("0514", "扬州");
        JiangSuAreaMap.put("0515", "盐城");
        JiangSuAreaMap.put("0516", "徐州");
        JiangSuAreaMap.put("0517", "淮安");
        JiangSuAreaMap.put("0517", "淮安");
        JiangSuAreaMap.put("0518", "连云港");
        JiangSuAreaMap.put("0519", "常州");
        JiangSuAreaMap.put("0523", "泰州");
        JiangSuAreaMap.put("0527", "宿迁");
    }

    /**
     * 订购产品基本校验
     *
     * @param request
     * @return
     */
    public BaseResponse subscribeProductValid(SubscribeProductRequest request) throws Exception {
        String productId = request.getProductId();
        String productCode = request.getProductCode();
        if (StringUtils.isBlank(productId) && StringUtils.isBlank(productCode)) {
            return new BaseResponse<SubscribeProductResponse>(false, "200001", "请选择需要订购的产品");
        }
        String phoneNumber = request.getPhoneNumber();
        if (StringUtils.isBlank(phoneNumber)) {
            return new BaseResponse<SubscribeProductResponse>(false, "200002", "用户号码不能为空");
        }
        phoneNumber = phoneNumber.trim();
        Integer phoneType = request.getPhoneType();
        if (phoneType == null) {
            return new BaseResponse<SubscribeProductResponse>(false, "200003", "请选择号码类型");
        }
        if (phoneType == PhoneTypeEnum.Mobile.getValue()) {
            if (!phoneNumber.startsWith("1") || phoneNumber.length() != 11) {
                return new BaseResponse<SubscribeProductResponse>(false, "200004", "手机号码必须以1开头并且长度为11位");
            }
        } else {
            if (!phoneNumber.startsWith("0") || phoneNumber.length() < 11
                    || phoneNumber.length() > 12) {
                return new BaseResponse<SubscribeProductResponse>(false, "200005", "固话号码不合法");
            }
            String areaCode = NumberUtils.getAreaCodeFromPhone(phoneNumber);
            if (StringUtils.isBlank(areaCode)) {
                return new BaseResponse<SubscribeProductResponse>(false, "200008", "固话号码区号错误,请检查后重新输入");
            }
            AreaInfo areaInfo = AreaUtils.getAreaInfoMap().get(areaCode);
            if (areaInfo == null) {
                return new BaseResponse<SubscribeProductResponse>(false, "200008", "固话号码区号错误,请检查后重新输入");
            }
        }
        if (!NumberUtils.isMobilePhone(phoneNumber)) {//如果是固话,需要选择是哪个运营商
            if (request.getTelecomOperator() == null) {
                return new BaseResponse<SubscribeProductResponse>(false, "200006", "固话号码需要选择运营商");
            }
        }
        if (null == request.getOperateOrigin()) {
            return new BaseResponse<SubscribeProductResponse>(false, "200007", "操作来源不能为空");
        }

        //判断要订购的产品是否存在,状态是否正常
        ProductInfo productQueryCond = new ProductInfo();
        ProductInfo productInfo = null;
        if (StringUtils.isNotBlank(productId)) {
            productQueryCond.setProductId(productId);
            productInfo = productInfoMapper.selectByPrimaryKey(productId);
        } else if (StringUtils.isNotBlank(productCode)) {
            productQueryCond.setProductCode(productCode);
            productInfo = productInfoMapper.selectByPrimaryKey(productCode);
        }
        List<ProductInfo> productInfoList = productInfoMapper.listByCond(productQueryCond);
        if (productInfoList != null && productInfoList.size() > 0) {
            productInfo = productInfoList.get(0);
        }
        if (productInfo == null) {
            return new BaseResponse<SubscribeProductResponse>(false, "000001", "要订购的产品不存在");
        }
        if (productInfo.getStatus() != ProductStatusEnum.Normal.getValue()) {
            return new BaseResponse<SubscribeProductResponse>(false, "000002", "要订购的产品已注销,不能订购");
        }

        //校验号码是否在黑名单中
        phoneNumber = phoneNumber.trim();
        BlackUser blackUserCond = new BlackUser();
        blackUserCond.setPhonenumber(phoneNumber);
        List<BlackUser> blackUserList = blackUserMapper.listByCond(blackUserCond);
        if (blackUserList != null && blackUserList.size() > 0) {
            return new BaseResponse<SubscribeProductResponse>(false, "000003", "用户[" + phoneNumber + "]在黑名单中,不能订购");
        }

        //判断用户是否已订购了该产品
        if (canSubscriberByPhoneNumber(productId, phoneNumber)) {
            throw new ServiceException("000004", "用户[" + phoneNumber + "]已经订购了该产品");
        }

        return new BaseResponse<SubscribeProductResponse>(true, ResultCodeConstant.SUCCESSFUL, "订购产品基本校验通过");
    }

    /**
     * 订购某个产品
     *
     * @param request
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, ServiceException.class, Exception.class})
    public BaseResponse<SubscribeProductResponse> subscribeProduct(SubscribeProductRequest request) throws Exception {
        BaseResponse<SubscribeProductResponse> resp = new BaseResponse<SubscribeProductResponse>();

        String phoneNumber = request.getPhoneNumber();

        /**
         * 基本信息校验
         */
        resp = this.subscribeProductValid(request);
        if (resp != null && !resp.isSuccess()) {
            return resp;
        }

        //判断要订购的产品是否存在,状态是否正常
        String productId = request.getProductId();
        String productCode = request.getProductCode();
        ProductInfo productQueryCond = new ProductInfo();
        ProductInfo productInfo = null;
        if (StringUtils.isNotBlank(productId)) {
            productQueryCond.setProductId(productId);
            productInfo = productInfoMapper.selectByPrimaryKey(productId);
        } else if (StringUtils.isNotBlank(productCode)) {
            productQueryCond.setProductCode(productCode);
            productInfo = productInfoMapper.selectByPrimaryKey(productCode);
        }
        List<ProductInfo> productInfoList = productInfoMapper.listByCond(productQueryCond);
        if (productInfoList != null && productInfoList.size() > 0) {
            productInfo = productInfoList.get(0);
        }
        if (productInfo == null) {
            return new BaseResponse<SubscribeProductResponse>(false, "000001", "要订购的产品不存在");
        }
        if (productInfo.getStatus() != ProductStatusEnum.Normal.getValue()) {
            return new BaseResponse<SubscribeProductResponse>(false, "000002", "要订购的产品已注销,不能订购");
        }

//        //校验号码是否在黑名单中
//        phoneNumber = phoneNumber.trim();
//        BlackUser blackUserCond = new BlackUser();
//        blackUserCond.setPhonenumber(phoneNumber);
//        List<BlackUser> blackUserList = blackUserMapper.listByCond(blackUserCond);
//        if (blackUserList != null && blackUserList.size() > 0) {
//            return new BaseResponse<SubscribeProductResponse>(false, "000003", "用户[" + phoneNumber + "]在黑名单中,不能订购");
//        }

        String userId = null;
        //判断用户号码是否已在系统中,如果没有则新增
        String areaCode = "";
        Integer telecomeOperator = 0;
        UserInfo queryCond = new UserInfo();
        queryCond.setPhonenumber(phoneNumber);
        List<UserInfo> userInfoList = userInfoMapper.listByCond(queryCond);
        if (userInfoList != null && userInfoList.size() > 0) {
            UserInfo userInfo = userInfoList.get(0);
            telecomeOperator = userInfo.getTelecomOperator();
            areaCode = userInfo.getArea();
            userId = userInfo.getUserId();
        } else {//新增用户
            //获取主键
            String tableName = SequenceDefConstant.UserInfo.TABLE_NAME;
            String columnName = SequenceDefConstant.UserInfo.COLUMN_NAME;
            userId = sequenceDefService.nextId(tableName, columnName);

            UserInfo newUser = new UserInfo();
            newUser.setUserId(userId);
            newUser.setPhonenumber(phoneNumber);
            newUser.setUserName(request.getUserName());

            //根据号码判断是手机还是固话
            if (NumberUtils.isMobilePhone(phoneNumber)) {
                try {
                    //调用聚合API来获取手机属地
                    PhoneInfo phoneInfo = MobileNoTrack.requestTrack(phoneNumber);
                    telecomeOperator = phoneInfo.getTelecomOperatorType();
                    areaCode = phoneInfo.getAreaCode();
                    String areaName;
                    if (StringUtils.isBlank(areaCode)) {
                        areaCode = "0000";
                        areaName = "0000";
                    } else {
                        areaName = phoneInfo.getAreaName();
                    }
                    newUser.setTelecomOperator(telecomeOperator);
                    newUser.setArea(areaCode);
                    newUser.setAreaName(areaName);
                    newUser.setPhoneType(PhoneTypeEnum.Mobile.getValue());
                } catch (Exception ex) {
                    throw new Exception("请求聚合API获取手机号码属地时发生异常");
                }
            } else {
                String areaName;
                //获取区号及属地名称
                areaCode = NumberUtils.getAreaCodeFromPhone(phoneNumber);
                if (StringUtils.isBlank(areaCode)) {
                    areaCode = "0000";
                    areaName = "0000";
                } else {
                    AreaInfo areaInfo = AreaUtils.getAreaInfoMap().get(areaCode);
                    if (areaInfo == null) {
                        areaCode = "0000";
                        areaName = "0000";
                    } else {
                        areaName = areaInfo.getCity();
                    }
                }
                newUser.setPhoneType(PhoneTypeEnum.FixedPhone.getValue());
                newUser.setTelecomOperator(request.getTelecomOperator());
                newUser.setArea(areaCode);
                newUser.setAreaName(areaName);
                telecomeOperator = newUser.getTelecomOperator();
            }

            newUser.setCreateTime(new Date());
            newUser.setCreateOperator(request.getOperName());
            userInfoMapper.insertSelective(newUser);
        }

        //校验该号码是在产品订购黑名单区域中
        ProductBlackList productBlackRuleCond = new ProductBlackList();
        productBlackRuleCond.setAreaCode(areaCode);
        productBlackRuleCond.setProductId(productInfo.getProductId());
        List<ProductBlackRuleVO> productBlackRuleVOList = productBlackListMapper.listByCond(productBlackRuleCond);
        if (productBlackRuleVOList != null && productBlackRuleVOList.size() > 0) {
            throw new ServiceException("000012", "用户[" + phoneNumber + "]在产品黑名单区域中,不能订购");
        }
//        //判断用户是否已订购了该产品
//        if (isOrderProduct(productId, userId)) {
//            throw new ServiceException("000004", "用户[" + phoneNumber + "]已经订购了该产品");
//        }

        log.info("当前号码[" + phoneNumber + "]区号:" + areaCode);
        log.info("当前号码[" + phoneNumber + "]运营商类型:" + telecomeOperator);
        log.info("当前号码[" + phoneNumber + "]运营商名称:" + TelecomTypeEnum.getDescByValue(telecomeOperator));

        Integer isDevOps = request.getDevOps();
        //如果是故障处理模式,则不需要调用彩铃平台
        if (isDevOps == null || isDevOps.equals(OrderConstant.DevOpsDef.NO_DEV_OPS)) {
            //批操作来源,如果已经有任意一次订购记录就不能再订购了
            if (request.getOperateOrigin() == OperateOriginEnum.Batch.getValue()) {
                SearchSubscriptionRequest cond = new SearchSubscriptionRequest();
                cond.setUserId(userId);
                cond.setProductType(productInfo.getProductType());
                List<ProductSubscribe> list = productSubscribeMapper.listByProduct(cond);
                if (list != null && list.size() > 0) {
                    throw new ServiceException("000007", "用户[" + phoneNumber + "]已开通过其他业务");
                }
            }

            //如果产品类型是彩铃平台代收,则先请求彩铃平台
            Integer productType = productInfo.getProductType();
            if (productType.equals(ProductTypeEnum.CRBT.getValue())
                    || productType.equals(ProductTypeEnum.CRBT2.getValue())) {
                //如果用户号码非江苏电信的号码则不能订购该产品
                if (JiangSuAreaMap.get(areaCode) == null || telecomeOperator != TelecomTypeEnum.CTC.getValue()) {
                    throw new ServiceException("000005", "用户[" + phoneNumber + "]不是江苏电信用户,无法订购该产品");
                }
                try {
                    SimpleUserInfo userInfo = new SimpleUserInfo();
                    userInfo.setPhonenumber(phoneNumber);
                    userInfo.setCallusertype("2");

                    ClientObject co = new ClientObject();
                    if (productType.equals(ProductTypeEnum.CRBT2.getValue())) {
                        co.setOperator("caishuitong");
                        co.setOrigin(50);
                    } else {
                        co.setOperator("qiyeguanjia");
                        co.setOrigin(40);
                    }

                    log.info("用户[" + phoneNumber + "]请求彩铃平台订购套餐[" + productInfo.getProductCode() + "]开始");
                    MusicSetsSubscribeInterfaceProxy proxy = new MusicSetsSubscribeInterfaceProxy();
                    ResultObject ro = proxy.addMusicSetsSubscribe(userInfo, productInfo.getProductCode(), null, co);
                    log.info("用户[" + phoneNumber + "]请求彩铃平台订购套餐[" + productInfo.getProductCode() + "]结果" + JsonUtils.Object2Json(ro));
                    if (ro != null && !ro.getResult().equals("000") && !ro.getCode().equals("1109")) {
                        throw new ServiceException("000006", "请求彩铃平台异常" + "同步电信平台失败(编码|||描述):" + ro.getCode()
                                + "|||" + ro.getDescription());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    log.error("用户[" + phoneNumber + "]请求彩铃平台订购套餐[" + productInfo.getProductCode() + "]异常：" + ex.getMessage());
                    throw new Exception("请求彩铃平台异常" + ex.getMessage());
                }
            }
        }

        //添加产品订购关系
        String subscriptionId = sequenceDefService.nextId(SequenceDefConstant.ProductSubscribe.TABLE_NAME,
                SequenceDefConstant.ProductSubscribe.COLUMN_NAME);//获取主键
        ProductSubscribe productSubscribe = new ProductSubscribe();
        productSubscribe.setSubscriptionId(subscriptionId);
        productSubscribe.setProductId(productInfo.getProductId());
        productSubscribe.setUserId(userId);
        productSubscribe.setOpenTime(new Date());
        productSubscribe.setStatus(ProductSubscribeStatusEnum.Normal.getValue());
        productSubscribe.setOperateOrigin(request.getOperateOrigin());
        productSubscribe.setCreateTime(new Date());
        productSubscribe.setCreateOperator(request.getOperName());
        productSubscribe.setUpdateTime(new Date());
        productSubscribe.setUpdateOperator(request.getOperName());
        productSubscribeMapper.insertSelective(productSubscribe);

        //如果上传了附件需要保存
        List<String> fileIds = request.getFileIds();
        if (fileIds != null && fileIds.size() > 0) {
            for (String fileId : fileIds) {
                String attachmentId = sequenceDefService.nextId(SequenceDefConstant.OrderAttachment.TABLE_NAME,
                        SequenceDefConstant.OrderAttachment.COLUMN_NAME);//获取主键
                OrderAttachment attachment = new OrderAttachment();
                attachment.setOrderAttachmentId(attachmentId);
                attachment.setSubscriptionId(subscriptionId);
                attachment.setFileId(fileId);
                attachment.setCreateTime(new Date());
                attachment.setCreateOperator(request.getOperName());
                orderAttachmentMapper.insertSelective(attachment);
            }
        }

        //添加订单操作日志
        String orderLogId = sequenceDefService.nextId(SequenceDefConstant.OrderLog.TABLE_NAME,
                SequenceDefConstant.OrderLog.COLUMN_NAME);//获取主键
        OrderLog orderLog = new OrderLog();
        orderLog.setOrderLogId(orderLogId);
        orderLog.setSubscriptionId(subscriptionId);
        orderLog.setOrderType(OrderTypeEnum.Subscribe.getValue());
        orderLog.setOperateOrigin(request.getOperateOrigin());
        orderLog.setCreateTime(new Date());
        orderLog.setCreateOperator(request.getOperName());
        orderLogMapper.insertSelective(orderLog);

        //发送短信

        SubscribeProductResponse subscribeProductResp = new SubscribeProductResponse();
        BeanUtils.copyProperties(productSubscribe, subscribeProductResp);
        resp = resp.setSuccessfulResp("用户[" + phoneNumber + "]订购产品成功");
        resp.setResult(subscribeProductResp);
        return resp;
    }

    /**
     * 退订某个产品
     *
     * @param request
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse unSubscribeProduct(UnsubscribeProductRequest request) throws ServiceException {
        BaseResponse resp = new BaseResponse();
        String subscriptionId = request.getSubscriptionId();
        if (StringUtils.isBlank(subscriptionId)) {
            return new BaseResponse(false, "000001", "请选择需要退订的产品");
        }
        ProductSubscribe cond = new ProductSubscribe();
        cond.setSubscriptionId(subscriptionId);
        List<ProductSubscribe> list = productSubscribeMapper.listByCond(cond);
        if (list == null || list.size() == 0) {
            return new BaseResponse(false, "000001", "请选择需要退订的产品");
        }

        if (request.getOperateOrigin() == null) {
            return new BaseResponse(false, "000003", "请选择操作来源");
        }

        ProductSubscribe productSubscribe = list.get(0);
        if (productSubscribe.getStatus() == ProductSubscribeStatusEnum.Cancel.getValue()) {
            return new BaseResponse(false, "000002", "您已退订了该产品");
        }
        //如果需要退订的产品类型是电信平台的产品,需要同步给电信平台
        String productId = productSubscribe.getProductId();
        String userId = productSubscribe.getUserId();
        UserInfo userInfo = userInfoMapper.selectByPrimaryKey(userId);
        String phoneNumber = userInfo.getPhonenumber();

        ProductInfo productInfo = productInfoMapper.selectByPrimaryKey(productId);
        Integer productType = productInfo.getProductType();
        //如果是故障处理的退订,则只在自己平台上操作,不需要同步到彩铃平台
        Integer devOps = request.getDevOps();
        if (devOps == null || devOps.equals(OrderConstant.DevOpsDef.NO_DEV_OPS)) {
            if (productType.equals(ProductTypeEnum.CRBT.getValue()) || productType.equals(ProductTypeEnum.CRBT2.getValue())) {
                try {
                    SimpleUserInfo simpleUserInfo = new SimpleUserInfo();
                    simpleUserInfo.setPhonenumber(phoneNumber);
                    simpleUserInfo.setCallusertype("2");

                    ClientObject co = new ClientObject();
                    if (productType.equals(ProductTypeEnum.CRBT2.getValue())) {
                        co.setOperator("caishuitong");
                        co.setOrigin(50);
                    } else {
                        co.setOperator("qiyeguanjia");
                        co.setOrigin(40);
                    }

                    log.info("用户[" + phoneNumber + "]请求彩铃平台退订套餐[" + productInfo.getProductCode() + "]开始");
                    MusicSetsSubscribeInterfaceProxy proxy = new MusicSetsSubscribeInterfaceProxy();
                    ResultObject ro = proxy.delMusicSetsSubscribe(simpleUserInfo, productInfo.getProductCode(), co);
                    log.info("用户[" + phoneNumber + "]请求彩铃平台退订套餐[" + productInfo.getProductCode() + "]结果" + JsonUtils.Object2Json(ro));
                    if (ro == null) {
                        return new BaseResponse(false, "000004", "同步电信平台失败,没有收到响应消息");
                    }
                    if (ro != null && !ro.getResult().equals("000") && !ro.getCode().equals("1106") && !ro.getCode().equals("1107")) {
                        return new BaseResponse(false, "000005", "同步电信平台失败:" + ro.getDescription());
                    }
                } catch (Exception ex) {
                    log.error("用户[" + phoneNumber + "]请求彩铃平台订购套餐[" + productInfo.getProductCode() + "]异常：" + ex.getMessage());
                    throw new ServiceException("请求彩铃平台异常" + ex.getMessage());
                }
            }
        }

        productSubscribe.setOperateOrigin(request.getOperateOrigin());
        productSubscribe.setCloseTime(new Date());
        productSubscribe.setStatus(ProductSubscribeStatusEnum.Cancel.getValue());
        productSubscribe.setUpdateTime(new Date());
        productSubscribe.setUpdateOperator(request.getOperName());
        productSubscribeMapper.updateByPrimaryKeySelective(productSubscribe);

        //添加订单操作日志
        String orderLogId = sequenceDefService.nextId(SequenceDefConstant.OrderLog.TABLE_NAME,
                SequenceDefConstant.OrderLog.COLUMN_NAME);//获取主键
        OrderLog orderLog = new OrderLog();
        orderLog.setOrderLogId(orderLogId);
        orderLog.setSubscriptionId(subscriptionId);
        orderLog.setOrderType(OrderTypeEnum.Cancel.getValue());
        orderLog.setOperateOrigin(request.getOperateOrigin());
        orderLog.setCreateTime(new Date());
        orderLog.setCreateOperator(request.getOperName());
        orderLogMapper.insertSelective(orderLog);

        resp = resp.setSuccessfulResp("用户[" + phoneNumber + "]退订产品成功");
        return resp;
    }

    /**
     * 判断用户是否已订购了产品
     *
     * @param productId
     * @param userId
     * @return true-已订购 false-未订购
     * @throws Exception
     */
    public boolean isOrderProduct(String productId, String userId) throws Exception {
        ProductSubscribe cond = new ProductSubscribe();
        cond.setUserId(userId);
        cond.setProductId(productId);
        cond.setStatus(ProductSubscribeStatusEnum.Normal.getValue());

        List<ProductSubscribe> list = productSubscribeMapper.listByCond(cond);
        if (list != null && list.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param productId
     * @param phoneNumber
     * @return
     * @throws Exception
     */
    public boolean canSubscriberByPhoneNumber(String productId, String phoneNumber) throws Exception {
        //根据用户号码查询用户
        String userId = null;
        UserInfo qryUserCond = new UserInfo();
        qryUserCond.setPhonenumber(phoneNumber);
        List<UserInfo> userInfos = userInfoMapper.listByCond(qryUserCond);
        if (userInfos != null && userInfos.size() > 0) {
            userId = userInfos.get(0).getUserId();
        } else {
            return false;
        }

        ProductSubscribe cond = new ProductSubscribe();
        cond.setUserId(userId);
        cond.setProductId(productId);
        cond.setStatus(ProductSubscribeStatusEnum.Normal.getValue());

        List<ProductSubscribe> list = productSubscribeMapper.listByCond(cond);
        if (list != null && list.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据用户号码、产品ID删除数据库中的数据(此方法仅用于单元测试时使用)
     *
     * @param phoneNumber
     * @param productId
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse deleteProductSubscribe(String phoneNumber, String productId) {
        UserInfo queryCond = new UserInfo();
        queryCond.setPhonenumber(phoneNumber);
        List<UserInfo> userInfoList = userInfoMapper.listByCond(queryCond);
        if (userInfoList != null && userInfoList.size() > 0) {
            UserInfo userInfo = userInfoList.get(0);
            ProductSubscribe data = new ProductSubscribe();
            data.setProductId(productId);
            data.setUserId(userInfo.getUserId());
            productSubscribeMapper.deleteProductSubscribe(data);
        }
        return new BaseResponse(true, ResultCodeConstant.SUCCESSFUL, "删除订购关系成功");
    }

    /**
     * 查询用户订单
     *
     * @param request
     * @return
     * @throws Exception
     */
    public BaseResponse<List<SearchSubscriptionResponse>> searchUserSubscription(SearchSubscriptionRequest request)
            throws Exception {
        BaseResponse<List<SearchSubscriptionResponse>> resp = new BaseResponse<List<SearchSubscriptionResponse>>();
        String phoneNumber = request.getPhoneNumber();
        if (StringUtils.isBlank(phoneNumber) && request.getSuperFlag() == 0) {
            return new BaseResponse<List<SearchSubscriptionResponse>>(false, "000001", "请输入用户号码");
        }

        UserSubscribeInfo cond = new UserSubscribeInfo();
        cond.setPhonenumber(phoneNumber);
        cond.setStatus(request.getStatus());
        cond.setStartDate(request.getStartDate());
        cond.setEndDate(request.getEndDate());
        cond.setAdminId(request.getAdminId());
        List<UserSubscribeInfo> list = userSubscribeMapper.listByCond(cond);
        List<SearchSubscriptionResponse> respList = new ArrayList<SearchSubscriptionResponse>();
        if (list != null && list.size() > 0) {
            for (UserSubscribeInfo userSubscribeInfo : list) {
                SearchSubscriptionResponse dto = new SearchSubscriptionResponse();
                BeanUtils.copyProperties(userSubscribeInfo, dto);
                dto.setPhoneTypeName(PhoneTypeEnum.getDescByValue(dto.getPhoneType()));
                dto.setTelecomOperatorName(TelecomTypeEnum.getDescByValue(dto.getTelecomOperator()));
                dto.setStatusName(ProductSubscribeStatusEnum.getDescByValue(dto.getStatus()));
                dto.setOperateOriginName(OperateOriginEnum.getDescByValue(dto.getOperateOrigin()));
                dto.setProductTypeName(ProductTypeEnum.getDescByValue(dto.getProductType()));
                respList.add(dto);
            }
        }
        resp = resp.setSuccessfulResp("查询数据成功");
        resp.setResult(respList);
        return resp;
    }

    public static void main(String args[]) {
        System.out.println(OrderTypeEnum.getDescByValue(2));
    }

}
