package com.yepit.cs.service;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSONObject;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.*;
import com.yepit.cs.domain.BatchOperationLog;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.order.SubscribeProductRequest;
import com.yepit.cs.dto.order.SubscribeProductResponse;
import com.yepit.cs.mapper.BatchOperationLogMapper;
import com.yepit.cs.mapper.FileUploadMapper;
import com.yepit.cs.mapper.ProductInfoMapper;
import com.yepit.cs.util.ExcelUtils;
import com.yepit.cs.util.NumberUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 批量开户（故障处理）
 * Created by qianlong on 2021/5/17
 */
@Component("opsOpenAccountTask")
@Scope("prototype")
public class OpsOpenAccountTask extends Thread {

    final static Logger logger = LoggerFactory.getLogger(OpsOpenAccountTask.class);

    private BatchOperationLog batchOperationLog;

    @Autowired
    private BatchOperationLogMapper batchOperationLogMapper;

    @Autowired
    private FileUploadMapper fileUploadMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private OrderService orderService;

    @Value("${system.batch.savepath}")
    private String batchFileSavePath;

    @Value("${system.fileServerURL}")
    private String fileServerURL;

    @SuppressWarnings("all")
    @Override
    public void run() {
        try {
            logger.info("批量开户故障处理开始");
            //将文件状态改为处理中
            batchOperationLog.setStatus(BatchOptStatusEnum.Processing.getValue());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);

            String uploadFilePath = batchOperationLog.getUploadFilePath();
            File file = new File(uploadFilePath);
            if (!file.exists()) {
                logger.error("批处理文件[" + uploadFilePath + "]不存在,无法处理");
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("批处理文件不存在,无法处理");
                batchOperationLog.setUpdateTime(new Date());
                batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
                batchOperationLog.setResultFilePath(null);
                batchOperationLog.setResultFileUrl(null);
                batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
                return;
            }

            //读取待开户数据,文件格式为txt
            List<String> lines = FileUtil.readLines(uploadFilePath, "utf8");
            List<JSONObject> resultList = new ArrayList<JSONObject>();
            if (lines != null && lines.size() > 0) {
                for (String lineStr : lines) {
                    JSONObject resultObject = new JSONObject();
                    String[] accountInfo = lineStr.split(",");
                    if (accountInfo.length < 2) {
                        continue;
                    }
                    Integer isDevOps = 1;
                    List<String> result = new ArrayList<String>();

                    String phoneNumber = accountInfo[0];
                    String productCode = accountInfo[1];
                    resultObject.put("phoneNumber", phoneNumber);
                    resultObject.put("productCode", productCode);

                    if (StringUtils.isBlank(phoneNumber)
                            && StringUtils.isBlank(productCode)) {
                        continue;
                    }

                    result.add(phoneNumber.trim());
                    result.add(productCode);
                    result.add(String.valueOf(isDevOps));

                    logger.info("----------------------------------------------------");
                    logger.info("用户号码=" + phoneNumber + "|||产品编码=" + productCode);

                    if (StringUtils.isBlank(phoneNumber.trim())) {
                        resultObject.put("result", "用户号码不能为空");
                        resultList.add(resultObject);
                        continue;
                    }
                    if (StringUtils.isBlank(productCode)) {
                        resultObject.put("result", "产品编码不能为空");
                        resultList.add(resultObject);
                        continue;
                    }

                    ProductInfo cond = new ProductInfo();
                    cond.setProductCode(productCode);
                    List<ProductInfo> productInfoList = productInfoMapper.listByCond(cond);
                    if (productInfoList == null || productInfoList.size() == 0) {
                        resultObject.put("result", "要订购的产品不存在");
                        resultList.add(resultObject);
                        continue;
                    }
                    phoneNumber = phoneNumber.trim();

                    BaseResponse<SubscribeProductResponse> resp = new BaseResponse<SubscribeProductResponse>();
                    try {
                        //发起开户请求
                        SubscribeProductRequest request = new SubscribeProductRequest();
                        request.setOperName(batchOperationLog.getCreateOperator());
                        request.setProductId(productInfoList.get(0).getProductId());
                        request.setOperateOrigin(OperateOriginEnum.Batch.getValue());
                        request.setUserName(phoneNumber);
                        request.setPhoneNumber(phoneNumber);
                        request.setDevOps(1);
                        if (NumberUtils.isMobilePhone(phoneNumber)) {
                            request.setPhoneType(PhoneTypeEnum.Mobile.getValue());
                        } else {
                            request.setPhoneType(PhoneTypeEnum.FixedPhone.getValue());
                        }
                        resp = orderService.subscribeProduct(request);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        resp = resp.setFailResp(ResultCodeConstant.UNKNOWERR, ex.getMessage());
                    }

                    logger.info("用户号码=" + phoneNumber + "|||产品编码=" + productCode + "|||处理结果=" + resp.getResultDesc());
                    //保存添加结果
                    resultObject.put("result", resp.getResultDesc());
                    resultList.add(resultObject);
                }
            }

            //创建结果文件
            if (resultList != null && resultList.size() > 0) {
                logger.info("共处理了" + resultList.size() + "条记录");
                String newFileName = batchOperationLog.getBatchId() + "_result.txt";
                String newFilePath = batchFileSavePath + File.separator + newFileName;
                List<String> finalResult = new ArrayList<String>();
                for (JSONObject resultObject : resultList) {
                    String resultContent = resultObject.getString("phoneNumber") + "," + resultObject.getString("productCode")
                            + "," + resultObject.get("result");
                    finalResult.add(resultContent);
                }
                FileUtil.writeUtf8Lines(finalResult, new File(newFilePath));

                //获取主键
                String tableName = SequenceDefConstant.FileUpload.TABLE_NAME;
                String columnName = SequenceDefConstant.FileUpload.COLUMN_NAME;
                String nextId = sequenceDefService.nextId(tableName, columnName);

                FileUpload newFileUpload = new FileUpload();
                newFileUpload.setFileId(nextId);
                newFileUpload.setFileName(newFileName);
                newFileUpload.setFileSavePath(newFilePath);
                newFileUpload.setFileUrl(fileServerURL + nextId);
                newFileUpload.setCreateTime(new Date());
                fileUploadMapper.insertSelective(newFileUpload);

                batchOperationLog.setResultFilePath(newFileUpload.getFileSavePath());
                batchOperationLog.setResultFileUrl(newFileUpload.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Successful.getValue());
                batchOperationLog.setResultDesc("批理开户故障处理成功");
            } else {
                logger.info("所有开户故障处理失败");
                FileUpload uploadFile = fileUploadMapper.selectByPrimaryKey(batchOperationLog.getFileId());
                batchOperationLog.setResultFilePath(uploadFile.getFileSavePath());
                batchOperationLog.setResultFileUrl(uploadFile.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("所有开户故障处理失败");
            }
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
            logger.info("批量开户故障处理结束");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());
            logger.info("处理结果=" + ex.getMessage());
            batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLog.setResultDesc(ex.getMessage());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
        }
    }

    public BatchOperationLog getBatchOperationLog() {
        return batchOperationLog;
    }

    public void setBatchOperationLog(BatchOperationLog batchOperationLog) {
        this.batchOperationLog = batchOperationLog;
    }
}
