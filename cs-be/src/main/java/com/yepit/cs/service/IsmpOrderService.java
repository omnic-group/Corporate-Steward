package com.yepit.cs.service;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.http.Method;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yepit.cs.cache.SysClientCache;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.*;
import com.yepit.cs.domain.*;
import com.yepit.cs.dto.ismp.*;
import com.yepit.cs.dto.order.*;
import com.yepit.cs.dto.product.PageSearchProductRequest;
import com.yepit.cs.dto.user.PhoneInfo;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.mapper.*;
import com.yepit.cs.util.MobileNoTrack;
import com.yepit.cs.util.NumberUtils;
import com.yepit.cs.vo.ProductBlackRuleVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/8/3 5:19 PM
 **/
@Service
@Slf4j
public class IsmpOrderService {

    @Value("${ismp.server.url}")
    private String ismpServerUrl;

    private final static String SUBSCRIBE_URL = "/ismp/serviceOrder?action=subscribe";

    private final static String DIRECT_SUBSCRIBE_URL = "/ismp/immediate/subscribe";

    private final static String UNSUBSCRIBE_URL = "/ismp/serviceOrder?action=unsubscribe";

    @Autowired
    private ProductService productService;

    @Autowired
    private IsmpOrderMapper ismpOrderMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private UserSubscribeMapper userSubscribeMapper;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OTPService otpService;

    @Autowired
    private ProductBlackListMapper productBlackListMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private SysClientCache sysClientCache;

    @Autowired
    private PartnerOrderMapper partnerOrderMapper;

    @Autowired
    private OrderCallbackConfigMapper orderCallbackConfigMapper;

    @Autowired
    private ThirdPartyCallLogMapper thirdPartyCallLogMapper;

    @Autowired
    private BlackUserMapper blackUserMapper;

    /**
     * 根据chargeId查询产品信息
     *
     * @param chargeId
     * @return
     */
    private ProductInfo getProductByChargeId(String chargeId) {
        PageSearchProductRequest queryCond = new PageSearchProductRequest();
        queryCond.setChargeId(chargeId);
        List<ProductInfo> productInfoList = productService.listProduct(queryCond);
        if (productInfoList != null && productInfoList.size() > 0) {
            return productInfoList.get(0);
        }
        return null;
    }

    /**
     * 生成token
     *
     * @param chargeId
     * @param timestamp
     * @return
     */
    private String generateAccessToken(String chargeId, String timestamp) {
        ProductInfo productInfo = this.getProductByChargeId(chargeId);
        if (productInfo == null) {
            throw new ServiceException("000001", "要订购的产品不存在");
        }
        String secret = productInfo.getSecretKey();
        String accessToken = SecureUtil.sha1(chargeId + timestamp + secret);
        return accessToken;
    }

    /**
     * 从ISMP平台获取订单信息
     *
     * @param getOrderRequest
     * @return
     */
    public BaseResponse<HashMap<String, Object>> getOrderInfo(GetIsmpOrderRequest getOrderRequest) {
        BaseResponse<HashMap<String, Object>> resp;
        resp = new BaseResponse<HashMap<String, Object>>().setSuccessfulResp("业务处理成功");
        String requestUrl = ismpServerUrl + SUBSCRIBE_URL;
        String chargeId = getOrderRequest.getChargeId();
        String timestamp = getOrderRequest.getTimestamp();
        if (StringUtils.isBlank(timestamp)) {
            timestamp = DateUtil.format(new Date(), "yyyyMMddHHmmss");
        }
        getOrderRequest.setTimestamp(timestamp);
        getOrderRequest.setOrderType("1");
        String accessToken = this.generateAccessToken(chargeId, timestamp);
        getOrderRequest.setAccessToken(accessToken);
        String requestStr = getOrderRequest.toJsonString();
        HashMap<String, Object> params = JSON.parseObject(requestStr, new HashMap<String, Object>().getClass());
        BaseResponse<HashMap<String, Object>> ismpResp = this.callIsmp(requestUrl, params);
        HashMap<String, Object> resultData = ismpResp.getResult();
        resp.setResult(resultData);
        return resp;
    }

    /**
     * 获取订单号
     *
     * @param chargeId
     * @return
     */
    public BaseResponse<String> getOrderId(String chargeId, String spId, String timestamp) {
        GetIsmpOrderRequest getOrderRequest = new GetIsmpOrderRequest();
        if (StringUtils.isNotBlank(spId)) {
            getOrderRequest.setSpId(spId);
        }
        getOrderRequest.setChargeId(chargeId);
        getOrderRequest.setTimestamp(timestamp);
        BaseResponse<String> resp = null;
        resp = new BaseResponse<String>().setSuccessfulResp("");
        BaseResponse<HashMap<String, Object>> getOrderResp = this.getOrderInfo(getOrderRequest);
        if (getOrderResp != null && getOrderResp.getResult() != null) {
            JSONObject orderInfo = (JSONObject) getOrderResp.getResult().get("orderinfo");
            if (orderInfo == null) {
                throw new ServiceException("系统异常,没有获取到订单对象");
            }
            String orderId = orderInfo.getString("orderId");
            if (StringUtils.isBlank(orderId)) {
                throw new ServiceException("系统异常,没有获取到订单ID");
            }
            resp.setResult(orderId);
        }
//        Snowflake snowflake = IdUtil.getSnowflake(1, 1);
//        resp.setResult(snowflake.nextIdStr());
        return resp;
    }

    /**
     * 订单基本校验,校验通过后返回可以订购的产品对象
     *
     * @param phoneNumber
     * @param productCode
     * @param chargeId
     */
    private ProductInfo basicValidOrder(String phoneNumber, String productCode, String chargeId) {
        if (StringUtils.isBlank(phoneNumber)) {
            throw new ServiceException("用户号码不能为空");
        }
        if (StringUtils.isBlank(productCode) && StringUtils.isBlank(chargeId)) {
            throw new ServiceException("产品编码不能为空");
        }

        try {
            //校验号码是否为江苏电信号码
            PhoneInfo phoneInfo = MobileNoTrack.requestTrack(phoneNumber);
            String areaCode = phoneInfo.getAreaCode();
            if (StringUtils.isBlank(areaCode)) {
                throw new ServiceException("000001", "用户[" + phoneNumber + "]不是正确的号码,无法订购该产品");
            }
            Integer telecomeOperator = phoneInfo.getTelecomOperatorType();
            if (OrderService.JiangSuAreaMap.get(areaCode) == null || telecomeOperator.intValue() != TelecomTypeEnum.CTC.getValue()) {
                throw new ServiceException("000002", "用户[" + phoneNumber + "]不是江苏电信用户,无法订购该产品");
            }

            //根据chargeId查询产品信息
            ProductInfo getProductCond = new ProductInfo();
            if (StringUtils.isNotBlank(chargeId)) {
                getProductCond.setChargeId(chargeId);
            } else {
                getProductCond.setProductCode(productCode);
            }

            List<ProductInfo> productInfos = productInfoMapper.listByCond(getProductCond);
            if (productInfos == null || productInfos.size() == 0) {
                throw new ServiceException("000003", "要订购的产品不存在");
            }

            //校验号码是否在黑名单中
            phoneNumber = phoneNumber.trim();
            BlackUser blackUserCond = new BlackUser();
            blackUserCond.setPhonenumber(phoneNumber);
            List<BlackUser> blackUserList = blackUserMapper.listByCond(blackUserCond);
            if (blackUserList != null && blackUserList.size() > 0) {
                throw new ServiceException("000003", "用户[" + phoneNumber + "]在黑名单中,不能订购");
            }

            ProductInfo productInfo = productInfos.get(0);

            //校验该号码是在产品订购黑名单区域中
            ProductBlackList productBlackRuleCond = new ProductBlackList();
            productBlackRuleCond.setAreaCode(areaCode);
            productBlackRuleCond.setProductId(productInfo.getProductId());
            List<ProductBlackRuleVO> productBlackRuleVOList = productBlackListMapper.listByCond(productBlackRuleCond);
            if (productBlackRuleVOList != null && productBlackRuleVOList.size() > 0) {
                throw new ServiceException("000012", "用户[" + phoneNumber + "]在产品黑名单区域中,不能订购");
            }

            return productInfo;
        } catch (ServiceException ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        }

    }

    /**
     * 校验用户是否可以订购产品,只有江苏电信用户才可订购,校验通过发送手机验证码
     *
     * @param validSubRequest
     * @return
     */
    public BaseResponse<String> getOTP(ValidSubRequest validSubRequest) {
        BaseResponse<String> resp;
        resp = new BaseResponse<String>().setSuccessfulResp("");
        String phoneNumber = validSubRequest.getPhoneNumber();
        String chargeId = validSubRequest.getChargeId();
        String requestUrl = ismpServerUrl + SUBSCRIBE_URL;
        try {
//            //校验号码是否为江苏电信号码
//            PhoneInfo phoneInfo = MobileNoTrack.requestTrack(phoneNumber);
//            String areaCode = phoneInfo.getAreaCode();
//            if (StringUtils.isBlank(areaCode)) {
//                throw new ServiceException("000001", "用户[" + phoneNumber + "]不是正确的号码,无法订购该产品");
//            }
//            Integer telecomeOperator = phoneInfo.getTelecomOperatorType();
//            if (OrderService.JiangSuAreaMap.get(areaCode) == null || telecomeOperator.intValue() != TelecomTypeEnum.CTC.getValue()) {
//                throw new ServiceException("000002", "用户[" + phoneNumber + "]不是江苏电信用户,无法订购该产品");
//            }
//
//            //根据chargeId查询产品信息
//            ProductInfo getProductCond = new ProductInfo();
//            getProductCond.setChargeId(chargeId);
//            List<ProductInfo> productInfos = productInfoMapper.listByCond(getProductCond);
//            if (productInfos == null || productInfos.size() == 0) {
//                throw new ServiceException("000003", "要订购的产品不存在");
//            }
//
//            ProductInfo productInfo = productInfos.get(0);
//
//            //校验该号码是在产品订购黑名单区域中
//            ProductBlackList productBlackRuleCond = new ProductBlackList();
//            productBlackRuleCond.setAreaCode(areaCode);
//            productBlackRuleCond.setProductId(productInfo.getProductId());
//            List<ProductBlackRuleVO> productBlackRuleVOList = productBlackListMapper.listByCond(productBlackRuleCond);
//            if (productBlackRuleVOList != null && productBlackRuleVOList.size() > 0) {
//                throw new ServiceException("000012", "用户[" + phoneNumber + "]在产品黑名单区域中,不能订购");
//            }

            ProductInfo productInfo = this.basicValidOrder(phoneNumber, null, chargeId);

            //获取订单ID
            BaseResponse<String> getOrderIdResp = this.getOrderId(productInfo.getChargeId(), productInfo.getSpid(), null);
            if (getOrderIdResp.isSuccess()) {
                String orderId = getOrderIdResp.getResult();
                //调用接口来验证
                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put("orderId", orderId);
                params.put("phoneNum", phoneNumber);
                BaseResponse<HashMap<String, Object>> ismpResp = this.callIsmp(requestUrl, params);
                resp = this.convertResp(ismpResp);
                resp.setResult(orderId);
            }

        } catch (ServiceException ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        }
        return resp;
    }

    /**
     * 校验用户是否可以订购产品,只有江苏电信用户才可订购,校验通过发送手机验证码
     *
     * @param phoneNumber
     * @param productCode
     * @param timestamp
     * @param sysClient
     * @return
     */
    public BaseResponse<String> partnerGetOTP(String phoneNumber, String productCode, String timestamp, SysClient sysClient) {
        BaseResponse<String> resp;
        resp = new BaseResponse<String>().setSuccessfulResp("");
        String requestUrl = ismpServerUrl + SUBSCRIBE_URL;
        try {
            ProductInfo productInfo = this.basicValidOrder(phoneNumber, productCode, null);

            //查看当前合作渠道订购次数是否超过最大次数
            Long maxLimit = sysClient.getCallMaxLimit();
            String today = DateUtil.today();
            String startTime = today + "00:00:00";
            String endTime = today + "23:59:59";
            int partnerOrderCount = partnerOrderMapper.getOrderCountByClientId(sysClient.getClientId(), startTime, endTime);
            if (partnerOrderCount > maxLimit.intValue()) {
                throw new ServiceException("当前渠道当日订购数已超过最大限制");
            }

            //获取订单ID
            BaseResponse<String> getOrderIdResp = this.getOrderId(productInfo.getChargeId(), productInfo.getSpid(), timestamp);
            if (getOrderIdResp.isSuccess()) {
                String orderId = getOrderIdResp.getResult();
                //调用接口来验证
                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put("orderId", orderId);
                params.put("phoneNum", phoneNumber);
                BaseResponse<HashMap<String, Object>> ismpResp = this.callIsmp(requestUrl, params);
                resp = this.convertResp(ismpResp);
                resp.setResult(orderId);

                //数据库中新增预订单
                PartnerOrder partnerOrder = new PartnerOrder();
                partnerOrder.setTradeType(IsmpOrderTradeTypeEnum.OrderMonthly.getValue());
                partnerOrder.setPhoneNumber(phoneNumber);
                partnerOrder.setCreateTime(new Date());
                partnerOrder.setProductCode(productCode);
                partnerOrder.setIsmpOrderId(orderId);
                partnerOrder.setClientId(sysClient.getClientId());
                //初始状态
                partnerOrder.setStatus(IsmpOrderStatusEnum.Initial.getValue());
                partnerOrderMapper.insert(partnerOrder);
            }


        } catch (ServiceException ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        }
        return resp;
    }


    /**
     * 向ISMP发起订购产品请求
     *
     * @param request
     * @return
     */
    public BaseResponse subscribeProduct(IsmpSubscribeProductRequest request) {
        BaseResponse resp = null;
        String requestUrl = ismpServerUrl + SUBSCRIBE_URL;
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("orderId", request.getOrderId());
        params.put("phoneNum", request.getPhoneNumber());
        params.put("verCode", request.getOtp());
        BaseResponse<HashMap<String, Object>> ismpResp = this.callIsmp(requestUrl, params);
        resp = this.convertResp(ismpResp);
        if (resp.isSuccess() && resp.getResultCode().equals("0")) {//只要电信受理成功就回调第三方接口
            GetPartnerOrderRequest queryCond = new GetPartnerOrderRequest();
            queryCond.setIsmpOrderId(request.getOrderId());
            List<PartnerOrder> partnerOrderList = partnerOrderMapper.listByCond(queryCond);
            if (CollUtil.isNotEmpty(partnerOrderList)) {
                partnerOrderList.stream().forEach(partnerOrder -> {
                    ProductInfo productInfo = productService.getProductByCode(partnerOrder.getProductCode());

                    //更新合作方订单状态
                    partnerOrder.setStatus(IsmpOrderStatusEnum.WaitingForConfirm.getValue());
                    partnerOrderMapper.updateByPrimaryKey(partnerOrder);

                    //发起回调
                    this.callbackThirdParty(productInfo, request.getPhoneNumber(), request.getOrderType());
                });
            }
        }
        return resp;
    }

    /**
     * 第三方订购产品
     *
     * @param request
     * @return
     */
    public BaseResponse subscribeProductBy3rd(IsmpDirectSubProductRequest request) {
        BaseResponse resp = null;
        String otp = request.getOtp();

        //校验短信验证码是否正确
        try {
            otpService.verifyOTP(OTPTypeEnum.Order.getValue(), request.getPhoneNumber(), otp);
        } catch (Exception ex) {
            throw new ServiceException("999999", "校验验证码失败:" + ex.getMessage());
        }

        resp = this.directSubscribeProduct(request);
        return resp;
    }

    /**
     * 用于批量订购的逻辑,调用号百直接订购的接口
     *
     * @param request
     * @return
     */
    public BaseResponse batchSubscribeProduct(IsmpDirectSubProductRequest request) {
        String requestUrl = ismpServerUrl + DIRECT_SUBSCRIBE_URL;

        BaseResponse resp = null;

        //校验产品是否存在
        String productCode = request.getProductCode();
        PageSearchProductRequest queryCond = new PageSearchProductRequest();
        queryCond.setProductCode(productCode);
        List<ProductInfo> productInfoList = productService.listProduct(queryCond);
        if (productInfoList == null || productInfoList.size() == 0) {
            throw new ServiceException("要订购的产品不存在");
        }
        ProductInfo productInfo = productInfoList.get(0);
        if (productInfo.getProductType().intValue() != ProductTypeEnum.ISMP.getValue()) {
            throw new ServiceException("该产品不是ISMP平台产品,不能订购");
        }

        String phoneNumber = request.getPhoneNumber();
        try {
            //校验号码是否为江苏电信号码
            PhoneInfo phoneInfo = MobileNoTrack.requestTrack(phoneNumber);
            String areaCode = phoneInfo.getAreaCode();
            if (StringUtils.isBlank(areaCode)) {
                throw new ServiceException("000001", "用户[" + phoneNumber + "]不是正确的号码,无法订购该产品");
            }
            Integer telecomeOperator = phoneInfo.getTelecomOperatorType();
            if (OrderService.JiangSuAreaMap.get(areaCode) == null || telecomeOperator.intValue() != TelecomTypeEnum.CTC.getValue()) {
                throw new ServiceException("000002", "用户[" + phoneNumber + "]不是江苏电信用户,无法订购该产品");
            }

            //校验该号码是在产品订购黑名单区域中
            ProductBlackList productBlackRuleCond = new ProductBlackList();
            productBlackRuleCond.setAreaCode(areaCode);
            productBlackRuleCond.setProductId(productInfo.getProductId());
            List<ProductBlackRuleVO> productBlackRuleVOList = productBlackListMapper.listByCond(productBlackRuleCond);
            if (productBlackRuleVOList != null && productBlackRuleVOList.size() > 0) {
                throw new ServiceException("000012", "用户[" + phoneNumber + "]在产品黑名单区域中,不能订购");
            }

            String spId = productInfo.getSpid();
            String chargeId = productInfo.getChargeId();
            String secretKey = productInfo.getSecretKey();
            String timestamp = DateUtil.format(new Date(), "yyyyMMddHHmmss");
            String accessToken = SecureUtil.sha1(phoneNumber + chargeId + timestamp + secretKey);
            HashMap<String, Object> params = new HashMap<String, Object>();
            params.put("opType", 0);
            params.put("spId", spId);
            params.put("phoneNum", phoneNumber);
            params.put("goodsId", chargeId);
            params.put("timestamp", timestamp);
            params.put("accessToken", accessToken);
            BaseResponse<HashMap<String, Object>> ismpResp = this.postIsmpRestful(requestUrl, params);
            if (ismpResp.getResult() == null) {
                throw new ServiceException("调用号百接口异常,无返回对象");
            } else {
                resp = new BaseResponse().setSuccessfulResp("successful");
                HashMap<String, Object> result = ismpResp.getResult();
                if (String.valueOf(result.get("resultCode")).equals("0")) {
                    resp.setResultDesc("处理成功");
                } else {
                    resp = resp.setFailResp(String.valueOf(result.get("resultCode")), String.valueOf(result.get("resultMsg")));
                }
            }
            log.info("直接订购产品结果:{}", JSON.toJSONString(ismpResp));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        }
        return resp;
    }

    /**
     * 直接订购产品
     *
     * @param request
     * @return
     */
    public BaseResponse directSubscribeProduct(IsmpDirectSubProductRequest request) {
        String requestUrl = ismpServerUrl + SUBSCRIBE_URL;
        BaseResponse resp = null;

        //校验产品是否存在
        String productCode = request.getProductCode();
        PageSearchProductRequest queryCond = new PageSearchProductRequest();
        queryCond.setProductCode(productCode);
        List<ProductInfo> productInfoList = productService.listProduct(queryCond);
        if (productInfoList == null || productInfoList.size() == 0) {
            throw new ServiceException("要订购的产品不存在");
        }
        ProductInfo productInfo = productInfoList.get(0);
        if (productInfo.getProductType().intValue() != ProductTypeEnum.ISMP.getValue()) {
            throw new ServiceException("该产品不是ISMP平台产品,不能订购");
        }

        String phoneNumber = request.getPhoneNumber();
        try {
            //校验号码是否为江苏电信号码
            PhoneInfo phoneInfo = MobileNoTrack.requestTrack(phoneNumber);
            String areaCode = phoneInfo.getAreaCode();
            if (StringUtils.isBlank(areaCode)) {
                throw new ServiceException("000001", "用户[" + phoneNumber + "]不是正确的号码,无法订购该产品");
            }
            Integer telecomeOperator = phoneInfo.getTelecomOperatorType();
            if (OrderService.JiangSuAreaMap.get(areaCode) == null || telecomeOperator.intValue() != TelecomTypeEnum.CTC.getValue()) {
                throw new ServiceException("000002", "用户[" + phoneNumber + "]不是江苏电信用户,无法订购该产品");
            }

            //校验该号码是在产品订购黑名单区域中
            ProductBlackList productBlackRuleCond = new ProductBlackList();
            productBlackRuleCond.setAreaCode(areaCode);
            productBlackRuleCond.setProductId(productInfo.getProductId());
            List<ProductBlackRuleVO> productBlackRuleVOList = productBlackListMapper.listByCond(productBlackRuleCond);
            if (productBlackRuleVOList != null && productBlackRuleVOList.size() > 0) {
                throw new ServiceException("000012", "用户[" + phoneNumber + "]在产品黑名单区域中,不能订购");
            }

            String spId = productInfo.getSpid();
            String chargeId = productInfo.getChargeId();
            String secretKey = productInfo.getSecretKey();
            String timestamp = DateUtil.format(new Date(), "yyyyMMddHHmmss");
            String accessToken = SecureUtil.sha1(chargeId + timestamp + secretKey);
            HashMap<String, Object> params = new HashMap<String, Object>();
            params.put("spId", spId);
            params.put("chargeId", chargeId);
            params.put("orderType", 1);
            params.put("timestamp", timestamp);
            params.put("accessToken", accessToken);
            params.put("phoneNum", phoneNumber);

            BaseResponse<HashMap<String, Object>> ismpResp = this.callIsmp(requestUrl, params);
            resp = this.convertResp(ismpResp);
            log.info("直接订购产品结果:{}", JSON.toJSONString(ismpResp));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        }
        return resp;
    }

    /**
     * 合作方渠道退订产品
     *
     * @param newIsmpUnSubRequest
     * @return
     */
    public BaseResponse partnerUnSubscribeProduct(SysClient sysClient, NewIsmpUnSubRequest newIsmpUnSubRequest) {
        //根据产品code查询chargeId
        ProductInfo productInfo = productService.getProductByCode(newIsmpUnSubRequest.getProductCode());
        if (productInfo == null) {
            throw new ServiceException("000001", "要退订的产品不存在");
        }
        //查询该用户是否订购了该产品
        UserSubscribeInfo queryCond = new UserSubscribeInfo();
        queryCond.setPhonenumber(newIsmpUnSubRequest.getPhoneNumber());
        queryCond.setProductId(productInfo.getProductId());
        queryCond.setStatus(ProductSubscribeStatusEnum.Normal.getValue());
        List<UserSubscribeInfo> subscribeInfoList = userSubscribeMapper.listByCond(queryCond);
        if (CollUtil.isEmpty(subscribeInfoList)) {
            throw new ServiceException("000002", "该用户不存在");
        }

        //数据库中新增预订单,状态是等待平台确认
        PartnerOrder partnerOrder = new PartnerOrder();
        partnerOrder.setTradeType(IsmpOrderTradeTypeEnum.Unsubscribe.getValue());
        partnerOrder.setPhoneNumber(newIsmpUnSubRequest.getPhoneNumber());
        partnerOrder.setCreateTime(new Date());
        partnerOrder.setProductCode(newIsmpUnSubRequest.getProductCode());
        partnerOrder.setClientId(sysClient.getClientId());
        //待确认状态
        partnerOrder.setStatus(IsmpOrderStatusEnum.WaitingForConfirm.getValue());
        partnerOrderMapper.insert(partnerOrder);

        IsmpUnSubscribeProductRequest request = new IsmpUnSubscribeProductRequest();
        request.setChargeId(productInfo.getChargeId());
        request.setPhoneNum(newIsmpUnSubRequest.getPhoneNumber());
        request.setSpId(productInfo.getSpid());

        BaseResponse ismpResp = this.unSubscribeProduct(request);

        return ismpResp;
    }

    /**
     * 退订产品
     *
     * @param request
     * @return
     */
    public BaseResponse unSubscribeProduct(IsmpUnSubscribeProductRequest request) {
        BaseResponse resp;
        String requestUrl = ismpServerUrl + UNSUBSCRIBE_URL;
        String chargeId = request.getChargeId();
        ProductInfo productInfo = productService.getProductByChargeId(chargeId);
        if (productInfo == null) {
            throw new ServiceException("000002", "错误的产品编码");
        }
        request.setSpId(productInfo.getSpid());
        String timestamp = DateUtil.format(new Date(), "yyyyMMddHHmmss");
        request.setTimestamp(timestamp);
        String accessToken = this.generateAccessToken(chargeId, timestamp);
        request.setAccessToken(accessToken);
        String requestStr = request.toJsonString();
        HashMap<String, Object> params = JSON.parseObject(requestStr, new HashMap<String, Object>().getClass());
        BaseResponse<HashMap<String, Object>> ismpResp = this.callIsmp(requestUrl, params);
        resp = this.convertResp(ismpResp);
        //如果ISMP平台订购关系不存在,我们也直接退订
        if (resp != null && resp.getResultCode().equals("130")) {
            UserSubscribeInfo cond = new UserSubscribeInfo();
            cond.setPhonenumber(request.getPhoneNum());
            cond.setProductId(productInfo.getProductId());
            cond.setStatus(1);
            List<UserSubscribeInfo> list = userSubscribeMapper.listByCond(cond);
            if (CollUtil.isNotEmpty(list)) {
                list.stream().forEach(userSubscribeInfo -> {
                    UnsubscribeProductRequest unSubscribeProductReq = new UnsubscribeProductRequest();
                    unSubscribeProductReq.setSubscriptionId(userSubscribeInfo.getSubscriptionId());
                    unSubscribeProductReq.setOperateOrigin(OperateOriginEnum.StewardPlatform.getValue());
                    orderService.unSubscribeProduct(unSubscribeProductReq);
                });
            }
            resp = new BaseResponse().setSuccessfulResp("0", "退订产品成功");
        } else if (resp != null && resp.getResultCode().equals("0")) {
            log.info("直接向合作方发起退订回调");
            //向发第三方平台发起回调
            this.callbackThirdParty(productInfo, request.getPhoneNum(), IsmpOrderTradeTypeEnum.Unsubscribe.getValue());
        }

        return resp;
    }

    /**
     * 直接退订购产品
     *
     * @param request
     * @return
     */
    public BaseResponse directUnSubscribeProduct(IsmpDirectUnSubProductRequest request) {
        String requestUrl = ismpServerUrl + UNSUBSCRIBE_URL;
        BaseResponse resp = null;

        //校验产品是否存在
        String productCode = request.getProductCode();
        PageSearchProductRequest queryCond = new PageSearchProductRequest();
        queryCond.setProductCode(productCode);
        List<ProductInfo> productInfoList = productService.listProduct(queryCond);
        if (productInfoList == null || productInfoList.size() == 0) {
            throw new ServiceException("要退订的产品不存在");
        }
        ProductInfo productInfo = productInfoList.get(0);
        if (productInfo.getProductType().intValue() != ProductTypeEnum.ISMP.getValue()) {
            throw new ServiceException("该产品不是ISMP平台产品,不能退订");
        }

        String phoneNumber = request.getPhoneNumber();
        try {
            String spId = productInfo.getSpid();
            String chargeId = productInfo.getChargeId();
            String secretKey = productInfo.getSecretKey();
            String timestamp = DateUtil.format(new Date(), "yyyyMMddHHmmss");
            String accessToken = SecureUtil.sha1(chargeId + timestamp + secretKey);
            HashMap<String, Object> params = new HashMap<String, Object>();
            params.put("spId", spId);
            params.put("chargeId", chargeId);
            params.put("timestamp", timestamp);
            params.put("accessToken", accessToken);
            params.put("phoneNum", phoneNumber);

            BaseResponse<HashMap<String, Object>> ismpResp = this.callIsmp(requestUrl, params);
            resp = this.convertResp(ismpResp);
            log.info("直接退订产品结果:{}", JSON.toJSONString(ismpResp));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        }
        return resp;
    }

    /**
     * 创建预订单
     *
     * @param preOrderRequest
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, ServiceException.class, Exception.class})
    public BaseResponse createPreOrder(IsmpPreOrderRequest preOrderRequest) {
        BaseResponse resp = new BaseResponse();
        resp = new BaseResponse();
        String productCode = preOrderRequest.getProductCode();
        //根据productCode查询chargeId
        PageSearchProductRequest queryCond = new PageSearchProductRequest();
        queryCond.setProductCode(productCode);
        String chargeId = null;
        List<ProductInfo> productInfoList = productService.listProduct(queryCond);
        if (productInfoList != null && productInfoList.size() > 0) {
            chargeId = productInfoList.get(0).getChargeId();
        } else {
            throw new ServiceException("000001", "要订购的产品[" + productCode + "]不存在");
        }

        try {
            //进行创建预订单校验
            SubscribeProductRequest subProductReq = new SubscribeProductRequest();
            subProductReq.setPhoneNumber(preOrderRequest.getPhoneNumber());
            subProductReq.setProductCode(productCode);
            subProductReq.setPhoneType(PhoneTypeEnum.Mobile.getValue());
            subProductReq.setOperateOrigin(OperateOriginEnum.Ismp.getValue());
            subProductReq.setOperName("ismp");

            //生成subscriberId主键
            String subscriptionId = sequenceDefService.nextId(SequenceDefConstant.ProductSubscribe.TABLE_NAME,
                    SequenceDefConstant.ProductSubscribe.COLUMN_NAME);
            IsmpOrder ismpOrder = new IsmpOrder();
            ismpOrder.setSubscriptionId(subscriptionId);
            ismpOrder.setChargeId(chargeId);
            ismpOrder.setProductCode(productCode);
            ismpOrder.setPhoneNumber(preOrderRequest.getPhoneNumber());
            ismpOrder.setIsmpSerial(preOrderRequest.getIsmpSerial());
            ismpOrder.setTradeType(preOrderRequest.getTradeType());
            ismpOrder.setStatus(IsmpOrderStatusEnum.WaitingForConfirm.getValue());
            ismpOrder.setCreateTime(new Date());
            ismpOrderMapper.insert(ismpOrder);

            Integer tradeType = preOrderRequest.getTradeType();
            if (tradeType != null && tradeType.intValue() == IsmpOrderTradeTypeEnum.OrderMonthly.getValue()) {
                BaseResponse subProductVaildResp = orderService.subscribeProductValid(subProductReq);
                if (!subProductVaildResp.isSuccess()) {
                    throw new ServiceException(subProductVaildResp.getResultCode(), subProductVaildResp.getResultDesc());
                }

                //查询该用户是否是第三方渠道的订单,如果有订单则更新订单状态
                GetPartnerOrderRequest getPartnerOrderCond = new GetPartnerOrderRequest();
                getPartnerOrderCond.setProductCode(productCode);
                getPartnerOrderCond.setPhoneNumber(preOrderRequest.getPhoneNumber());
                getPartnerOrderCond.setStatus(IsmpOrderStatusEnum.Initial.getValue());
                getPartnerOrderCond.setTradeType(preOrderRequest.getTradeType());
                Date now = new Date();
                Date startTime = DateUtil.offset(now, DateField.MINUTE, -30);
                getPartnerOrderCond.setStartTime(DateUtil.formatDateTime(startTime));
                getPartnerOrderCond.setEndTime(DateUtil.formatDateTime(now));
                getPartnerOrderCond.setTradeType(IsmpOrderTradeTypeEnum.OrderMonthly.getValue());
                List<PartnerOrder> partnerOrderList = partnerOrderMapper.listByCond(getPartnerOrderCond);
                if (CollUtil.isNotEmpty(partnerOrderList)) {
                    PartnerOrder partnerOrder = partnerOrderList.get(0);
                    partnerOrder.setStatus(IsmpOrderStatusEnum.WaitingForConfirm.getValue());
                    partnerOrder.setConfirmTime(new Date());
                    partnerOrderMapper.updateByPrimaryKey(partnerOrder);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException("处理预订单异常", ex.getMessage());
        }

        resp = resp.setSuccessfulResp("处理成功");
        return resp;
    }

    /**
     * 正式下订单
     *
     * @param request
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, ServiceException.class, Exception.class})
    public BaseResponse placeOrder(IsmpPlaceOrderRequest request) {
        BaseResponse orderResp = null;
        try {
            //查询所有未确认过的预订单
            IsmpOrder queryCond = new IsmpOrder();
            queryCond.setIsmpSerial(request.getIsmpSerial());
            queryCond.setStatus(IsmpOrderStatusEnum.WaitingForConfirm.getValue());
            List<IsmpOrder> ismpOrders = ismpOrderMapper.listByCond(queryCond);
            if (ismpOrders == null || ismpOrders.size() == 0) {
                throw new ServiceException("没有找到预订单");
            }

            //预订单状态变更
            IsmpOrder ismpOrder = ismpOrders.get(0);
            ismpOrder.setConfirmTime(new Date());
            Integer tradeType = ismpOrder.getTradeType();
            String phoneNumber = request.getPhoneNumber();
            String productCode = ismpOrder.getProductCode();

            if (tradeType.intValue() == IsmpOrderTradeTypeEnum.OrderMonthly.getValue()) {
                //订购
                SubscribeProductRequest subProductReq = new SubscribeProductRequest();
                subProductReq.setPhoneNumber(phoneNumber);
                subProductReq.setProductCode(productCode);
                subProductReq.setPhoneType(PhoneTypeEnum.Mobile.getValue());
                subProductReq.setOperateOrigin(OperateOriginEnum.Ismp.getValue());
                subProductReq.setOperName("ismp");

                orderResp = orderService.subscribeProduct(subProductReq);

            } else if (tradeType.intValue() == IsmpOrderTradeTypeEnum.Unsubscribe.getValue()) {
                //先查询该号码这个产品的订购关系,然后退订
                UserSubscribeInfo cond = new UserSubscribeInfo();
                cond.setPhonenumber(phoneNumber);
                cond.setProductCode(productCode);
                cond.setStatus(ProductSubscribeStatusEnum.Normal.getValue());
                List<UserSubscribeInfo> sublist = userSubscribeMapper.listByCond(cond);
                if (sublist != null && sublist.size() > 0) {
                    for (UserSubscribeInfo userSubscribeInfo : sublist) {
                        UnsubscribeProductRequest unSubProductReq = new UnsubscribeProductRequest();
                        unSubProductReq.setSubscriptionId(userSubscribeInfo.getSubscriptionId());
                        unSubProductReq.setOperateOrigin(OperateOriginEnum.Ismp.getValue());
                        unSubProductReq.setOperName("ismp");

//                        BaseResponse unSubResp = orderService.unSubscribeProduct(unSubProductReq);
//                        if (!unSubResp.isSuccess()) {
//                            return unSubResp;
//                        }
                        orderService.unSubscribeProduct(unSubProductReq);
                    }
                    orderResp = new BaseResponse().setSuccessfulResp("退订成功");
                } else {
                    orderResp = new BaseResponse().setFailResp("000003", "没有找到订购关系");
                }
            }

            Integer imspOrderResult = request.getIsmpResult();
            if (imspOrderResult.intValue() == 1) {
                ismpOrder.setStatus(IsmpOrderStatusEnum.Confirmed.getValue());
            } else {
                ismpOrder.setStatus(IsmpOrderStatusEnum.Fail.getValue());
            }
            ismpOrderMapper.updateByPrimaryKey(ismpOrder);

            if (orderResp.isSuccess()) {
                ProductInfo productInfo = productService.getProductByCode(productCode);
                this.callbackDefaultParty(productInfo, request.getPhoneNumber(), ismpOrder.getTradeType());
            }

            return orderResp;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        }
    }

    /**
     * 订购成功后回调第三方接口
     *
     * @param productInfo
     * @param tradeType
     */
    private void callbackThirdParty(ProductInfo productInfo, String phoneNumber, Integer tradeType) {
        String productCode = productInfo.getProductCode();
        if (productInfo == null) {
            throw new ServiceException("000003", "产品不存在");
        }
        log.info("开始处理回调逻辑");
        //查看该产品是否配置了第三方回调地址,如果有则需要调用第三方接口
        GetPartnerOrderRequest getPartnerOrderCond = new GetPartnerOrderRequest();
        getPartnerOrderCond.setProductCode(productCode);
        getPartnerOrderCond.setPhoneNumber(phoneNumber);
        getPartnerOrderCond.setStatus(IsmpOrderStatusEnum.WaitingForConfirm.getValue());
        getPartnerOrderCond.setTradeType(tradeType);
        Date now = new Date();
        Date startTime = DateUtil.offset(now, DateField.MINUTE, -30);
        getPartnerOrderCond.setStartTime(DateUtil.formatDateTime(startTime));
        getPartnerOrderCond.setEndTime(DateUtil.formatDateTime(now));
        //查询该产品的partner的回调地址
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("productCode", productCode);
        params.put("phoneNumber", phoneNumber);
        params.put("tradeType", tradeType);

        String clientId = null;
        if (tradeType.intValue() == IsmpOrderTradeTypeEnum.OrderMonthly.getValue() || tradeType.intValue() == IsmpOrderTradeTypeEnum.Unsubscribe.getValue()) {//订购
            List<PartnerOrder> partnerOrderList = partnerOrderMapper.listByCond(getPartnerOrderCond);
            log.info("partnerOrderList.size={}", partnerOrderList == null ? 0 : partnerOrderList.size());
            if (CollUtil.isNotEmpty(partnerOrderList)) {
                PartnerOrder partnerOrder = partnerOrderList.get(0);
                partnerOrder.setStatus(IsmpOrderStatusEnum.Confirmed.getValue());
                partnerOrder.setConfirmTime(new Date());
                partnerOrderMapper.updateByPrimaryKey(partnerOrder);
                clientId = partnerOrder.getClientId();
            }else{
                log.info("30分钟内没有找到预订单,不需要回调");
                return;
            }
        }

        log.info("clientId={}", clientId);
        if (StringUtils.isEmpty(clientId)) {
            return;
        }
        ProductCallBackUrlDTO queryConfigCond = new ProductCallBackUrlDTO();
        queryConfigCond.setProductId(productInfo.getProductId());
        queryConfigCond.setClientId(clientId);
        List<ProductCallBackUrlDTO> productCallBackUrlDTOList = orderCallbackConfigMapper.selectProductConfig(queryConfigCond);
        if (CollUtil.isNotEmpty(productCallBackUrlDTOList)) {
            log.info("----------回调第三方平台开始----------");
            for (int i = 0; i < productCallBackUrlDTOList.size(); i++) {
                ProductCallBackUrlDTO productCallBackUrlDTO = productCallBackUrlDTOList.get(i);
                callPartnerSystem(params, productCallBackUrlDTO);
            }
            log.info("----------回调第三方平台结束----------");
        }
    }

    /**
     * 向产品方发起回调
     *
     * @param productInfo
     * @param phoneNumber
     * @param tradeType
     */
    private void callbackDefaultParty(ProductInfo productInfo, String phoneNumber, Integer tradeType) {
        ProductCallBackUrlDTO queryConfigCond = new ProductCallBackUrlDTO();
        queryConfigCond.setProductId(productInfo.getProductId());
        queryConfigCond.setClientId("defaultid");
        List<ProductCallBackUrlDTO> productCallBackUrlDTOList = orderCallbackConfigMapper.selectProductConfig(queryConfigCond);
        if (CollUtil.isEmpty(productCallBackUrlDTOList)) {
            log.error("没有找到产品默认回调配置");
            return;
        }
        ProductCallBackUrlDTO config = productCallBackUrlDTOList.get(0);

        //查询30分钟内是否向产品方发起过回调,如果有过回调就不发了,如果没有发过就发起回调
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("productCode", productInfo.getProductCode());
        params.put("phoneNumber", phoneNumber);
        params.put("tradeType", tradeType);

        Date now = new Date();
        Date startTimeDate = DateUtil.offset(now, DateField.MINUTE, -30);
        String startTime = DateUtil.formatDateTime(startTimeDate);
        String endTime = DateUtil.formatDateTime(now);
        List<ThirdPartyCallLog> logList = thirdPartyCallLogMapper.listByCond(config.getCallbackUrlId(), JSON.toJSONString(params), startTime, endTime);
        if (CollUtil.isEmpty(logList)) {
            log.info("----------回调第三方平台开始----------");
            for (int i = 0; i < productCallBackUrlDTOList.size(); i++) {
                callPartnerSystem(params, config);
            }
            log.info("----------回调第三方平台结束----------");
        }else{
            log.info("30分钟内已经向第三方回调过,不需要再次回调");
        }

    }

    /**
     * 向第三方合作商系统发起请求
     *
     * @param params
     * @param productCallBackUrlDTO
     */
    private void callPartnerSystem(HashMap<String, Object> params, ProductCallBackUrlDTO productCallBackUrlDTO) {
        String requestUrl = productCallBackUrlDTO.getCallbackUrl();
        String requestBody = JSON.toJSONString(params);
        log.info("The requestUrl[{}] is :{}", productCallBackUrlDTO.getCallbackUrlName(), requestUrl);
        log.info("The requestBody is:{}", requestBody);

        long startTime = System.currentTimeMillis(); // 开始时间
        //TODO 调用接口
        String response = HttpUtil.get(requestUrl, params);

        long endTime = System.currentTimeMillis();
        //计算花费时间
        long spendTime = endTime - startTime;

        //记录接口调用日志
        ThirdPartyCallLog callLog = new ThirdPartyCallLog();
        callLog.setParams(JSON.toJSONString(params));
        callLog.setRequestTime(new Date());
        callLog.setRequestUrlId(productCallBackUrlDTO.getCallbackUrlId());
        callLog.setRequestUrlName(productCallBackUrlDTO.getCallbackUrlName());
        callLog.setResponse(response);
        callLog.setTime(spendTime);
        thirdPartyCallLogMapper.insert(callLog);
    }

    /**
     * 查询手机号码在系统中是否存在,如果不存在则新增
     *
     * @param mobileNumber
     * @return
     */
    private UserInfo queryAndCreateUser(String mobileNumber) throws ServiceException {
        String userId = null;
        Integer telecomeOperator;
        String areaCode = null;
        String areaName = null;
        UserInfo queryCond = new UserInfo();
        queryCond.setPhonenumber(mobileNumber);
        try {
            List<UserInfo> userInfoList = userInfoMapper.listByCond(queryCond);
            if (userInfoList != null && userInfoList.size() > 0) {
                return userInfoList.get(0);
            } else {//新增用户
                //只支持手机开户
                if (NumberUtils.isMobilePhone(mobileNumber)) {
                    //获取主键
                    String tableName = SequenceDefConstant.UserInfo.TABLE_NAME;
                    String columnName = SequenceDefConstant.UserInfo.COLUMN_NAME;
                    userId = sequenceDefService.nextId(tableName, columnName);

                    UserInfo newUser = new UserInfo();
                    newUser.setUserId(userId);
                    newUser.setPhonenumber(mobileNumber);
                    newUser.setUserName(mobileNumber);
                    //调用聚合API来获取手机属地
                    PhoneInfo phoneInfo = MobileNoTrack.requestTrack(mobileNumber);
                    telecomeOperator = phoneInfo.getTelecomOperatorType();
                    areaCode = phoneInfo.getAreaCode();
                    if (StringUtils.isBlank(areaCode)) {
                        areaCode = "0000";
                        areaName = "0000";
                    } else {
                        areaName = phoneInfo.getAreaName();
                    }
                    newUser.setTelecomOperator(telecomeOperator);
                    newUser.setArea(areaCode);
                    newUser.setAreaName(areaName);
                    newUser.setPhoneType(PhoneTypeEnum.Mobile.getValue());


                    newUser.setCreateTime(new Date());
                    newUser.setCreateOperator("ISMP");
                    userInfoMapper.insertSelective(newUser);
                    return newUser;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        }
        return null;
    }

    /**
     * 将ISMP的响应转换
     *
     * @param ismpResp
     * @return
     */
    private BaseResponse convertResp(BaseResponse<HashMap<String, Object>> ismpResp) {
        BaseResponse resp = null;
        resp = new BaseResponse().setSuccessfulResp("successful");
        if (ismpResp.isSuccess()) {
            HashMap<String, Object> resultData = ismpResp.getResult();
            String resultCode = "";
            if (resultData.get("errcode") instanceof Integer) {
                resultCode = String.valueOf((Integer) resultData.get("errcode"));
            } else {
                resultCode = (String) resultData.get("errcode");
            }
            if (resultCode.equals("101")) {
                resultCode = "000000";
            }
            String resultDesc = (String) resultData.get("errmsg");
            resp.setResultCode(resultCode);
            resp.setResultDesc(resultDesc);
        }
        return resp;
    }

    public BaseResponse<HashMap<String, Object>> postIsmpRestful(String url, Map<String, Object> params) {
        BaseResponse resp;
        HttpRequest httpRequest = null;
        HttpResponse httpResponse = null;
        String respStr = null;
        try {
            log.info("Call ISMP RequestUrl is:" + url);
            log.info("Call ISMP Params is:{})", params);
            httpRequest = HttpUtil.createRequest(Method.POST, url)
                    .body(JSONUtil.toJsonStr(params));
            System.out.println(httpRequest.toString());
            httpResponse = httpRequest.execute();
            log.info("The http response body is:{}", httpResponse.body());
            log.info("The http response status is:{}", httpResponse.getStatus());
            respStr = httpResponse.body();
            if (httpResponse.getStatus() == 401) {
                log.error(respStr);
                return new BaseResponse().setFailResp("401", "无权限访问");
            }
            log.info("The response is:{}", respStr);
            HashMap<String, Object> respMap = JSON.parseObject(respStr, new HashMap<String, Object>().getClass());
            resp = new BaseResponse();
            resp = resp.setSuccessfulResp("业务处理成功");
            resp.setResult(respMap);
            return resp;
        } catch (ServiceException ex) {
            throw new ServiceException("Call ISMP service exception:" + ex.getMessage());
        } catch (Exception ex) {
            throw new ServiceException("Call ISMP service exception:" + ex.getMessage());
        }
    }

    public BaseResponse<HashMap<String, Object>> callIsmp(String url, Map<String, Object> params) {
        BaseResponse resp;
        HttpRequest httpRequest = null;
        HttpResponse httpResponse = null;
        String respStr = null;
        try {
            log.info("Call ISMP RequestUrl is:" + url);
            log.info("Call ISMP Params is:{})", params);
            httpRequest = HttpUtil.createRequest(Method.POST, url)
                    .contentType("application/x-www-form-urlencoded;charset=UTF-8")
                    .form(params);
            System.out.println(httpRequest.toString());
            System.out.println(httpRequest.form().toString());
            httpResponse = httpRequest.execute();
            log.info("The http response body is:{}", httpResponse.body());
            log.info("The http response status is:{}", httpResponse.getStatus());
            respStr = httpResponse.body();
            if (httpResponse.getStatus() == 401) {
                log.error(respStr);
                return new BaseResponse().setFailResp("401", "无权限访问");
            }
            log.info("The response is:{}", respStr);
            HashMap<String, Object> respMap = JSON.parseObject(respStr, new HashMap<String, Object>().getClass());
            resp = new BaseResponse();
            resp = resp.setSuccessfulResp("业务处理成功");
            resp.setResult(respMap);
            return resp;
        } catch (ServiceException ex) {
            throw new ServiceException("Call ISMP service exception:" + ex.getMessage());
        } catch (Exception ex) {
            throw new ServiceException("Call ISMP service exception:" + ex.getMessage());
        }
    }

    /**
     * 校验订单请求签名是否正确
     *
     * @param ismpOrderRequest
     * @return
     */
    public boolean validRequestSign(String secret, NewIsmpPreOrderRequest ismpOrderRequest) {
        String signature = ismpOrderRequest.getSignature();
        String encodeStr = SecureUtil.sha1(ismpOrderRequest.getProductCode() + ismpOrderRequest.getPhoneNumber() + ismpOrderRequest.getOrderTimestamp() + secret);
        if (signature.equalsIgnoreCase(encodeStr)) {
            return true;
        }
        return false;
    }

    /**
     * 校验请求头中的令牌是否正确,如果正确则返回SysClient对象
     *
     * @param auth
     * @return
     */
    public SysClient validRequestAuth(String auth) {
        if (StringUtils.isBlank(auth)) {
            throw new ServiceException("000001", "请求令牌不能为空");
        }
        //进行base64解密
        String decodeStr = Base64.decodeStr(auth);
        String[] authInfo = decodeStr.split(" ");
        if (authInfo.length != 2) {
            throw new ServiceException("000002", "令牌数据不合法");
        }
        String clientId = authInfo[0];
        String secret = authInfo[1];
        try {
            SysClient sysClient = sysClientCache.getSysClientByClientId(clientId);
            if (sysClient == null) {
                throw new ServiceException("000003", "错误的令牌数据,请联系管理员");
            }
            if (!secret.equalsIgnoreCase(sysClient.getSecret())) {
                throw new ServiceException("000003", "错误的令牌数据,请联系管理员");
            }
            return sysClient;
        } catch (Exception ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    public static void main(String[] args) throws Exception {
//        DateTime dateTime = new DateTime("2017-01-05 12:34:23", DatePattern.NORM_DATETIME_FORMAT);
//        dateTime.setTime();
    }

}
