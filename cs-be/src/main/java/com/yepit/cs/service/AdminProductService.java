package com.yepit.cs.service;

import cn.hutool.core.collection.CollUtil;
import com.yepit.cs.cache.ProductCache;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.domain.AdminProduct;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.admin.AdminDTO;
import com.yepit.cs.dto.admin.AdminProductDTO;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.mapper.AdminProductMapper;
import com.yepit.cs.mapper.ProductInfoMapper;
import com.yepit.cs.util.SessionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author qianlong
 * @description 管理员产品关联配置
 * @Date 2020/5/30 9:01 上午
 **/
@Service
public class AdminProductService {

    @Autowired
    private AdminProductMapper adminProductMapper;

    @Autowired
    private ProductCache productCache;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    /**
     * 绑定产品
     *
     * @param adminId
     * @param productIdList
     * @return
     */
    public BaseResponse bindProduct(Long adminId, List<String> productIdList) {
        if (CollUtil.isEmpty(productIdList)) {
            throw new ServiceException("要绑定的产品不能为空");
        }
        //先清除原有记录再重新添加
        adminProductMapper.deleteByCond(adminId, null);

        productIdList.stream().forEach(productId -> {
            AdminProduct adminProduct = new AdminProduct();
            adminProduct.setAdminId(adminId);
            adminProduct.setProductId(productId.trim());
            adminProduct.setCreateTime(new Date());
            adminProductMapper.insert(adminProduct);
        });
        return new BaseResponse().setSuccessfulResp("绑定操作成功");
    }

    /**
     * 绑定产品
     *
     * @param adminId
     * @param productIdList
     * @return
     */
    public BaseResponse unBindProduct(Long adminId, List<String> productIdList) {
        if (CollUtil.isEmpty(productIdList)) {
            throw new ServiceException("要解绑的产品不能为空");
        }
        productIdList.stream().forEach(productId -> {
            adminProductMapper.deleteByCond(adminId, productId);
        });
        return new BaseResponse().setSuccessfulResp("解绑操作成功");
    }

    /**
     * 查询操作员绑定的产品列表
     *
     * @param adminId
     * @return
     */
    public List<AdminProductDTO> ListAdminProduct(Long adminId) {
        List<ProductInfo> allProduct = productInfoMapper.listByCond(new ProductInfo());
        List<AdminProductDTO> result = new ArrayList<>();
        if (CollUtil.isNotEmpty(allProduct)) {
            allProduct.stream().forEach(productInfo -> {
                AdminProductDTO adminProductDTO = new AdminProductDTO();
                BeanUtils.copyProperties(productInfo, adminProductDTO);
                adminProductDTO.setBind(false);
                result.add(adminProductDTO);
            });
        }
        AdminProduct queryCond = new AdminProduct();
        queryCond.setAdminId(adminId);
        List<AdminProduct> adminProductList = adminProductMapper.listByCond(queryCond);
        if (CollUtil.isNotEmpty(adminProductList)) {
            result.stream().forEach(adminProductDTO -> {
                adminProductList.stream().forEach(adminProduct -> {
                    if (adminProductDTO.getProductId().equals(adminProduct.getProductId())) {
                        adminProductDTO.setBind(true);
                    }
                });
            });
        }
        return result;
    }

    /**
     * 查询操作员绑定的产品列表
     *
     * @return
     */
    public List<String> ListCurrentUserBindProduct() {
        AdminDTO currentUser = SessionUtils.getCurrentUser();
        AdminProduct queryCond = new AdminProduct();
        queryCond.setAdminId(currentUser.getAdminId());
        List<AdminProduct> adminProductList = adminProductMapper.listByCond(queryCond);
        List<String> productList = adminProductList.stream().map(AdminProduct::getProductId).collect(Collectors.toList());
        return productList;
    }
}
