package com.yepit.cs.service;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.*;
import com.yepit.cs.domain.BatchOperationLog;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.domain.UserSubscribeInfo;
import com.yepit.cs.dto.order.SubscribeProductRequest;
import com.yepit.cs.dto.order.SubscribeProductResponse;
import com.yepit.cs.dto.order.UnsubscribeProductRequest;
import com.yepit.cs.mapper.BatchOperationLogMapper;
import com.yepit.cs.mapper.FileUploadMapper;
import com.yepit.cs.mapper.ProductInfoMapper;
import com.yepit.cs.mapper.UserSubscribeMapper;
import com.yepit.cs.util.ExcelUtils;
import com.yepit.cs.util.NumberUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 批量销户
 * Created by qianlong on 2017/9/3.
 */
@Component("closeAccountTask")
@Scope("prototype")
public class CloseAccountTask extends Thread {

    final static Logger logger = LoggerFactory.getLogger(CloseAccountTask.class);

    private BatchOperationLog batchOperationLog;

    @Autowired
    private BatchOperationLogMapper batchOperationLogMapper;

    @Autowired
    private FileUploadMapper fileUploadMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserSubscribeMapper userSubscribeMapper;

    @Value("${system.batch.savepath}")
    private String batchFileSavePath;

    @Value("${system.fileServerURL}")
    private String fileServerURL;

    @SuppressWarnings("all")
    @Override
    public void run() {
        try {
            logger.info("批量销户开始");
            //将文件状态改为处理中
            batchOperationLog.setStatus(BatchOptStatusEnum.Processing.getValue());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);

            String uploadFilePath = batchOperationLog.getUploadFilePath();
            File file = new File(uploadFilePath);
            if (!file.exists()) {
                logger.error("批处理文件[" + uploadFilePath + "]不存在,无法处理");
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("批处理文件不存在,无法处理");
                batchOperationLog.setUpdateTime(new Date());
                batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
                batchOperationLog.setResultFilePath(null);
                batchOperationLog.setResultFileUrl(null);
                batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
                return;
            }

            //读取待销户数据
            List<List<String>> resultList = new ArrayList<List<String>>();
            ExcelReader reader = ExcelUtil.getReader(uploadFilePath);
            List<List<Object>> readAll = reader.read();
            if (readAll != null && readAll.size() > 0) {
                for (List<Object> dataRow : readAll) {
                    List<String> result = new ArrayList<String>();
                    String phoneNumber = dataRow.get(0).toString();
                    String productCode = dataRow.get(1).toString();//产品编码

                    result.add(phoneNumber);
                    result.add(productCode);

                    logger.info("用户号码=" + phoneNumber + "|||产品编码=" + productCode);

                    //根据产品编码获取产品ID
                    if (StringUtils.isBlank(productCode)) {
                        result.add("产品编码不能为空");
                        resultList.add(result);
                        continue;
                    }

                    ProductInfo cond = new ProductInfo();
                    cond.setProductCode(productCode);
                    List<ProductInfo> productInfoList = productInfoMapper.listByCond(cond);
                    if (productInfoList == null || productInfoList.size() == 0) {
                        result.add("要退订的产品不存在");
                        resultList.add(result);
                        continue;
                    }

                    //查询用户的订购关系,用户只可能有一条状态为正常的订购关系
                    UserSubscribeInfo queryCond = new UserSubscribeInfo();
                    queryCond.setPhonenumber(phoneNumber);
                    queryCond.setProductId(productInfoList.get(0).getProductId());
                    queryCond.setStatus(ProductSubscribeStatusEnum.Normal.getValue());
                    List<UserSubscribeInfo> userSubscribeInfos = userSubscribeMapper.listByCond(queryCond);
                    if (userSubscribeInfos == null || userSubscribeInfos.size() == 0) {
                        result.add("该产品已经退订");
                        resultList.add(result);
                        continue;
                    }

                    //发起销户请求
                    UnsubscribeProductRequest request = new UnsubscribeProductRequest();
                    request.setSubscriptionId(userSubscribeInfos.get(0).getSubscriptionId());
                    request.setOperateOrigin(OperateOriginEnum.Batch.getValue());
                    request.setOperName(batchOperationLog.getCreateOperator());

                    BaseResponse<SubscribeProductResponse> resp = orderService.unSubscribeProduct(request);

                    //保存添加结果
                    result.add(resp.getResultDesc());
                    resultList.add(result);
                }
            }

//            List<List<String>> dataList = ExcelUtils.readWorkBook(uploadFilePath, 1);
//            List<List<String>> resultList = new ArrayList<List<String>>();
//            if (dataList != null && dataList.size() > 0) {
//                for (List<String> accountInfo : dataList) {
//                    List<String> result = new ArrayList<String>();
//
//                    String phoneNumber = accountInfo.get(0);
//                    String productCode = accountInfo.get(1);//产品编码
//
//                    result.add(phoneNumber);
//                    result.add(productCode);
//
//                    logger.info("用户号码=" + phoneNumber + "|||产品编码=" + productCode);
//
//                    //根据产品编码获取产品ID
//                    if (StringUtils.isBlank(productCode)) {
//                        result.add("产品编码不能为空");
//                        resultList.add(result);
//                        continue;
//                    }
//
//                    ProductInfo cond = new ProductInfo();
//                    cond.setProductCode(productCode);
//                    List<ProductInfo> productInfoList = productInfoMapper.listByCond(cond);
//                    if (productInfoList == null || productInfoList.size() == 0) {
//                        result.add("要退订的产品不存在");
//                        resultList.add(result);
//                        continue;
//                    }
//
//                    //查询用户的订购关系,用户只可能有一条状态为正常的订购关系
//                    UserSubscribeInfo queryCond = new UserSubscribeInfo();
//                    queryCond.setPhonenumber(phoneNumber);
//                    queryCond.setProductId(productInfoList.get(0).getProductId());
//                    queryCond.setStatus(ProductSubscribeStatusEnum.Normal.getValue());
//                    List<UserSubscribeInfo> userSubscribeInfos = userSubscribeMapper.listByCond(queryCond);
//                    if (userSubscribeInfos == null || userSubscribeInfos.size() == 0) {
//                        result.add("该产品已经退订");
//                        resultList.add(result);
//                        continue;
//                    }
//
//                    //发起销户请求
//                    UnsubscribeProductRequest request = new UnsubscribeProductRequest();
//                    request.setSubscriptionId(userSubscribeInfos.get(0).getSubscriptionId());
//                    request.setOperateOrigin(OperateOriginEnum.Batch.getValue());
//                    request.setOperName(batchOperationLog.getCreateOperator());
//
//                    BaseResponse<SubscribeProductResponse> resp = orderService.unSubscribeProduct(request);
//
//                    //保存添加结果
//                    result.add(resp.getResultDesc());
//                    resultList.add(result);
//                }
//            }

            //创建结果文件
            if (resultList != null && resultList.size() > 0) {
                logger.info("共处理了" + resultList.size() + "条记录");
                String newFileName = batchOperationLog.getBatchId() + "_result.xlsx";
                String newFilePath = batchFileSavePath + File.separator + newFileName;
                List<String> header = new ArrayList<String>();
                header.add("用户号码");
                header.add("产品编号");
                header.add("处理结果");
                ExcelUtils.createXlsx("批理开户结果", header, resultList, newFilePath);

                //获取主键
                String tableName = SequenceDefConstant.FileUpload.TABLE_NAME;
                String columnName = SequenceDefConstant.FileUpload.COLUMN_NAME;
                String nextId = sequenceDefService.nextId(tableName, columnName);

                FileUpload newFileUpload = new FileUpload();
                newFileUpload.setFileId(nextId);
                newFileUpload.setFileName(newFileName);
                newFileUpload.setFileSavePath(newFilePath);
                newFileUpload.setFileUrl(fileServerURL + nextId);
                newFileUpload.setCreateTime(new Date());
                fileUploadMapper.insertSelective(newFileUpload);

                batchOperationLog.setResultFilePath(newFileUpload.getFileSavePath());
                batchOperationLog.setResultFileUrl(newFileUpload.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Successful.getValue());
                batchOperationLog.setResultDesc("批理销户成功");
            } else {
                logger.info("所有号码批量开户失败");
                FileUpload uploadFile = fileUploadMapper.selectByPrimaryKey(batchOperationLog.getFileId());
                batchOperationLog.setResultFilePath(uploadFile.getFileSavePath());
                batchOperationLog.setResultFileUrl(uploadFile.getFileUrl());
                batchOperationLog.setStatus(BatchOptStatusEnum.Fail.getValue());
                batchOperationLog.setResultDesc("所有号码批量开户失败");
            }
            batchOperationLog.setUpdateTime(new Date());
            batchOperationLog.setUpdateOperator(batchOperationLog.getCreateOperator());
            batchOperationLogMapper.updateByPrimaryKey(batchOperationLog);
            logger.info("批量销户结束");
        } catch (Exception ex) {
            ex.printStackTrace();
            Thread.interrupted();
            logger.error(ex.getMessage());
        }
    }

    public BatchOperationLog getBatchOperationLog() {
        return batchOperationLog;
    }

    public void setBatchOperationLog(BatchOperationLog batchOperationLog) {
        this.batchOperationLog = batchOperationLog;
    }
}
