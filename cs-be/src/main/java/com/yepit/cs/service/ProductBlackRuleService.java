package com.yepit.cs.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.domain.ProductBlackList;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.mapper.ProductBlackListMapper;
import com.yepit.cs.mapper.ProductInfoMapper;
import com.yepit.cs.vo.ProductBlackRuleVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/6/26 3:41 下午
 **/
@Service
public class ProductBlackRuleService {

    @Autowired
    private ProductBlackListMapper productBlackListMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    /**
     * 根据产品ID和区号获取黑名单规则
     *
     * @param productId
     * @param areaCode
     * @return
     */
    public ProductBlackRuleVO getProductBlackRule(String productId, String areaCode) {
        ProductBlackList queryCond = new ProductBlackList();
        queryCond.setProductId(productId);
        queryCond.setAreaCode(areaCode);
        List<ProductBlackRuleVO> result = productBlackListMapper.listByCond(queryCond);
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    /**
     * 创建黑名单规则
     *
     * @param productId
     * @param areaCode
     * @return
     */
    public BaseResponse createProductBlackRule(String productId, String areaCode) {
        if (StringUtils.isBlank(productId)) {
            throw new ServiceException("000001", "请选择一个产品");
        }
        if (StringUtils.isBlank(areaCode)) {
            throw new ServiceException("000002", "请选择一个区域");
        }
        if (getProductBlackRule(productId, areaCode) != null) {
            throw new ServiceException("000003", "黑名单规则已存在");
        }
        ProductInfo productInfo = productInfoMapper.selectByPrimaryKey(productId);
        if (productInfo == null) {
            throw new ServiceException("000004", "待配置黑名单的产品不存在");
        }
        ProductBlackList productBlackRule = new ProductBlackList();
        productBlackRule.setProductId(productId);
        productBlackRule.setAreaCode(areaCode);
        int result = productBlackListMapper.insert(productBlackRule);
        if (result > 0) {
            return new BaseResponse().setSuccessfulResp("创建黑名单规则成功");
        } else {
            return new BaseResponse().setFailResp("999999", "创建黑名单规则失败");
        }
    }

    /**
     * 删除黑名单规则
     *
     * @param productId
     * @param areaCode
     * @return
     */
    public BaseResponse deleteProductBlackRule(String productId, String areaCode) {
        ProductBlackList result = getProductBlackRule(productId, areaCode);
        if (result != null) {
            productBlackListMapper.deleteByPrimaryKey(result.getId());
        }
        return new BaseResponse().setSuccessfulResp("删除黑名单规则成功");
    }

    /**
     * 根据主键删除黑名单规则
     *
     * @param ruleId
     * @return
     */
    public BaseResponse deleteProductBlackRuleById(Integer ruleId) {
        productBlackListMapper.deleteByPrimaryKey(ruleId);
        return new BaseResponse().setSuccessfulResp("删除黑名单规则成功");
    }

    /**
     * 根据查询条件查询黑名单规则
     *
     * @param id
     * @param productId
     * @param areaCode
     * @return
     */
    public List<ProductBlackRuleVO> listByCond(Integer id, String productId, String areaCode) {
        ProductBlackList queryCond = new ProductBlackList();
        queryCond.setId(id);
        queryCond.setProductId(productId);
        queryCond.setAreaCode(areaCode);
        return productBlackListMapper.listByCond(queryCond);
    }
}
