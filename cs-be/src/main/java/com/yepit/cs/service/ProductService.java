package com.yepit.cs.service;

import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageHelper;
import com.yepit.cs.cache.ProductCache;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.common.PageArg;
import com.yepit.cs.common.PageResult;
import com.yepit.cs.constant.ProductStatusEnum;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.constant.SequenceDefConstant;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.product.DeleteProductRequest;
import com.yepit.cs.dto.product.PageSearchProductRequest;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.mapper.ProductInfoMapper;
import com.yepit.cs.util.SessionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by qianlong on 2017/8/20.
 */
@Service
public class ProductService {

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Autowired
    private ProductCache productCache;

    @Autowired
    private AdminProductService adminProductService;

    /**
     * 创建产品
     *
     * @param productInfo
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse<ProductInfo> createProduct(ProductInfo productInfo) throws Exception {
        BaseResponse<ProductInfo> resp = new BaseResponse<ProductInfo>();
        //判断必填信息
        if (StringUtils.isBlank(productInfo.getProductName())) {
            return new BaseResponse<ProductInfo>(false, "000001", "产品名称不能为空");
        }
        Integer productType = productInfo.getProductType();
        if(productType == null){
            return new BaseResponse<ProductInfo>(false, "000003", "请选择产品类型");
        }
        if(productType < 0 || productType > 5){
            return new BaseResponse<ProductInfo>(false, "000004", "产品类型不正确");
        }

        //判断产品编码是否重复
        ProductInfo cond = new ProductInfo();
        cond.setProductCode(productInfo.getProductCode());
        List<ProductInfo> productList = productInfoMapper.listByCond(cond);
        if (productList != null && productList.size() > 0) {
            return new BaseResponse<ProductInfo>(false, "000005", "产品编码不能重复");
        }

        //判断产品名称是否重复
        cond = new ProductInfo();
        cond.setProductName(productInfo.getProductName());
        productList = productInfoMapper.listByCond(cond);
        if (productList != null && productList.size() > 0) {
            return new BaseResponse<ProductInfo>(false, "000002", "产品名称不能重复");
        }

        //获取主键
        String tableName = SequenceDefConstant.ProductInfoDef.TABLE_NAME;
        String columnName = SequenceDefConstant.ProductInfoDef.COLUMN_NAME;
        String nextId = sequenceDefService.nextId(tableName, columnName);

        productInfo.setProductId(nextId);
        productInfo.setCreateTime(new Date());
        productInfo.setStatus(ProductStatusEnum.Normal.getValue());
        productInfoMapper.insertSelective(productInfo);

        //刷新缓存
        productCache.reload();

        resp = resp.setSuccessfulResp("创建产品成功");
        resp.setResult(productInfo);
        return resp;
    }

    /**
     * 更新产品
     *
     * @param productInfo
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse<ProductInfo> updateProduct(ProductInfo productInfo) throws Exception {
        BaseResponse<ProductInfo> resp = new BaseResponse<ProductInfo>();
        //判断必填信息
        if (StringUtils.isBlank(productInfo.getProductId())) {
            return new BaseResponse(false, "000001", "请选择一个产品");
        }

        //判断当前产品是否存在
        ProductInfo currentRecord = productInfoMapper.selectByPrimaryKey(productInfo.getProductId());
        if (currentRecord == null) {
            return new BaseResponse(false, "000002", "要修改的产品不存在");
        }

        //如果修改产品名称需要判断产品名称是否重复
        if (StringUtils.isNotBlank(productInfo.getProductName())) {
            ProductInfo cond = new ProductInfo();
            cond.setProductName(productInfo.getProductName());
            List<ProductInfo> productList = productInfoMapper.listByCond(cond);
            if (productList != null && productList.size() > 0) {
                for (ProductInfo product : productList) {
                    if (product.getProductId().equals(currentRecord.getProductId())) {
                        continue;
                    } else {
                        return new BaseResponse<ProductInfo>(false, "000002", "产品名称不能重复");
                    }
                }
            }
        }

        Integer productType = productInfo.getProductType();
        if(productType != null){
            if(productType < 0 || productType > 5){
                return new BaseResponse<ProductInfo>(false, "000004", "产品类型不正确");
            }
        }

        productInfo.setProductId(currentRecord.getProductId());//产品ID不能被修改
        productInfo.setProductCode(currentRecord.getProductCode());//产品编码不能被修改
        productInfo.setUpdateTime(new Date());
        productInfoMapper.updateByPrimaryKeySelective(productInfo);

        ProductInfo updateData = productInfoMapper.selectByPrimaryKey(productInfo.getProductId());

        //刷新缓存
        productCache.reload();

        resp = resp.setSuccessfulResp("更新产品成功");
        resp.setResult(updateData);
        return resp;
    }

    /**
     * 分页查询产品信息
     * @param request
     * @return
     */
    public BaseResponse<PageResult<ProductInfo>> listProductByCond(PageSearchProductRequest request) {
        BaseResponse<PageResult<ProductInfo>> resp = new BaseResponse<PageResult<ProductInfo>>();
        PageArg pageArg = request.getPage();
        if (pageArg != null) {
            PageHelper.startPage(pageArg.getPageNum(), pageArg.getPageSize());
        }

        //过滤掉当前用户没有权限查看的产品
        request.setAdminId(SessionUtils.getCurrentUser().getAdminId());
        List<String> bindProductList = adminProductService.ListCurrentUserBindProduct();

//        ProductInfo cond = new ProductInfo();
//        BeanUtils.copyProperties(request,cond);
        List<ProductInfo> productList = productInfoMapper.pageList(request);

        PageResult<ProductInfo> searchResult = new PageResult<ProductInfo>(productList);
        resp = resp.setSuccessfulResp("查询产品信息成功");
        resp.setResult(searchResult);
        return resp;
    }

    /**
     * 根据请求删除产品
     * @param request
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse deleteProduct(DeleteProductRequest request) throws Exception{
        String productId = request.getProductId();
        if(StringUtils.isNotBlank(productId)){
            productInfoMapper.deleteByPrimaryKey(productId);
        }else{
            String productCode = request.getProductCode();
            if(StringUtils.isNotBlank(productCode)){
                productInfoMapper.deleteByProductCode(productId);
            }
        }

        //刷新缓存
        productCache.reload();

        return new BaseResponse(true, ResultCodeConstant.SUCCESSFUL,"删除产品成功");
    }

    /**
     * 根据查询条件查询,不分页
     * @param request
     * @return
     */
    public List<ProductInfo> listProduct(PageSearchProductRequest request){
//        ProductInfo cond = new ProductInfo();
//        BeanUtils.copyProperties(request,cond);
//        request.setAdminId(SessionUtils.getCurrentUser().getAdminId());
        List<ProductInfo> productList = productInfoMapper.pageList(request);

        return productList;
    }

    /**
     * 根据产品Code查询产品
     * @param productCode
     * @return
     */
    public ProductInfo getProductByCode(String productCode){
        ProductInfo queryProductCond = new ProductInfo();
        queryProductCond.setProductCode(productCode);
        List<ProductInfo> productInfoList = productInfoMapper.listByCond(queryProductCond);
        if(CollUtil.isEmpty(productInfoList)){
            return null;
        }
        return productInfoList.get(0);
    }

    /**
     * 根据产品主键查询产品
     * @param productId
     * @return
     */
    public ProductInfo getProductById(String productId){
        return productInfoMapper.selectByPrimaryKey(productId);
    }

    /**
     * 根据ChargeId查询产品
     * @param chargeId
     * @return
     */
    public ProductInfo getProductByChargeId(String chargeId){
        ProductInfo queryProductCond = new ProductInfo();
        queryProductCond.setChargeId(chargeId);
        List<ProductInfo> productInfoList = productInfoMapper.listByCond(queryProductCond);
        if(CollUtil.isEmpty(productInfoList)){
            return null;
        }
        return productInfoList.get(0);    }
}
