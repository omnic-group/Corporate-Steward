package com.yepit.cs.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.dto.sms.SmsInfo;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.util.NumberUtils;
import com.yepit.cs.util.otp.JuHeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * @author qianlong
 * @Description //TODO
 * @Date 2019/10/26 2:48 PM
 **/
@Slf4j
@Service
public class SmsService {

    /**
     * 发送短信
     *
     * @param fileContent
     * @return
     */
    public BaseResponse sendSms(List<String> fileContent) {
        try{
            SmsInfo smsInfo = this.buildSmsInfo(fileContent);
            JuHeUtils.sendSmsByTplParams(smsInfo.getPhoneNumber(), smsInfo.getTplId(), smsInfo.getTplParams());
            return new BaseResponse().setSuccessfulResp("短信发送成功");
        }catch (ServiceException ex){
            return new BaseResponse().setFailResp(ex.getErrorCode(), ex.getMessage());
        }catch (Exception ex){
            return new BaseResponse().setFailResp("999999", ex.getMessage());
        }
    }

    private SmsInfo buildSmsInfo(List<String> fileContent){
        String phoneNumber = fileContent.get(0);
        String templateId = fileContent.get(1);//模板号

        log.info("用户号码:" + phoneNumber);
        log.info("模板号:" + templateId);

        if(StringUtils.isBlank(phoneNumber)){
            throw new ServiceException("号码不能为空");
        }
        if(StringUtils.isBlank(templateId)){
            throw new ServiceException("模板号");
        }
        if (!NumberUtils.isMobilePhone(phoneNumber)) {
            throw new ServiceException("号码必须是手机号码");
        }
        HashMap<String, Object> tplParams = new HashMap<String,Object>();

        if (templateId.equalsIgnoreCase("193894")) {
            tplParams.put("#code#", fileContent.get(2).trim());
        }

        if (templateId.equalsIgnoreCase("126488")) {
            tplParams.put("#code#", fileContent.get(2).trim());
        }

        if (templateId.equalsIgnoreCase("205439")) {
            tplParams.put("#code#", fileContent.get(2).trim());
        }

        SmsInfo smsInfo = new SmsInfo(phoneNumber,templateId,tplParams);
        return smsInfo;
    }
}
