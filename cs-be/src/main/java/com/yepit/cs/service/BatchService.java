package com.yepit.cs.service;

import com.github.pagehelper.PageHelper;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.common.PageArg;
import com.yepit.cs.common.PageResult;
import com.yepit.cs.component.ApplicationContextProvider;
import com.yepit.cs.constant.BatchOptStatusEnum;
import com.yepit.cs.constant.BatchOptTypeEnum;
import com.yepit.cs.constant.SequenceDefConstant;
import com.yepit.cs.domain.BatchOperationLog;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.dto.batch.CreateBatchRequest;
import com.yepit.cs.dto.batch.CreateBatchResponse;
import com.yepit.cs.dto.batch.SearchBatchLogRequest;
import com.yepit.cs.dto.batch.SearchBatchLogResponse;
import com.yepit.cs.mapper.BatchOperationLogMapper;
import com.yepit.cs.mapper.FileUploadMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by qianlong on 2017/9/2.
 */
@Service
public class BatchService {

    @Autowired
    private UserService userService;

    @Autowired
    private BatchOperationLogMapper batchOperationLogMapper;

    @Autowired
    private FileUploadMapper fileUploadMapper;

    @Autowired
    private SequenceDefService sequenceDefService;

    @Value("${system.batch.savepath}")
    private String batchFileSavePath;

    @Value("${system.fileServerURL}")
    private String fileServerURL;

    protected static Logger log = Logger.getLogger(BatchService.class);

    /**
     * 分页查询批操作日志
     *
     * @param request
     * @return
     */
    public BaseResponse<PageResult<SearchBatchLogResponse>> listBatchOptLogByCond(SearchBatchLogRequest request) {
        BaseResponse<PageResult<SearchBatchLogResponse>> resp = new BaseResponse<PageResult<SearchBatchLogResponse>>();
        PageArg pageArg = request.getPage();
        if (pageArg != null) {
            PageHelper.startPage(pageArg.getPageNum(), pageArg.getPageSize());
        }

        List<BatchOperationLog> batchOperationLogList = batchOperationLogMapper.listByCond(request);

        PageResult<BatchOperationLog> searchResult = new PageResult<BatchOperationLog>(batchOperationLogList);
        List<SearchBatchLogResponse> respList = new ArrayList<SearchBatchLogResponse>();
        if (batchOperationLogList != null && batchOperationLogList.size() > 0) {
            for (BatchOperationLog boLog : batchOperationLogList) {
                SearchBatchLogResponse sblResp = new SearchBatchLogResponse();
                BeanUtils.copyProperties(boLog, sblResp);
                sblResp.setBatchTypeName(BatchOptTypeEnum.getDescByValue(sblResp.getBatchType()));
                sblResp.setStatusName(BatchOptStatusEnum.getDescByValue(sblResp.getStatus()));
                respList.add(sblResp);
            }
        }
        PageResult<SearchBatchLogResponse> pageResult = new PageResult<SearchBatchLogResponse>(respList);

        resp = resp.setSuccessfulResp("查询批量操作记录成功");

//        long count = searchResult.getCount();
//        long pageCount = searchResult.getPageCount();
//        int pageNum = searchResult.getPageNum();
//        int pageSize = searchResult.getPageSize();

        BeanUtils.copyProperties(searchResult, pageResult);
        pageResult.setRows(respList);
//        pageResult.setCount(count);
//        pageResult.setPageNum(pageNum);
//        pageResult.setPageSize(pageSize);
//        pageResult.setPageCount(pageCount);
        resp.setResult(pageResult);
        return resp;
    }

    /**
     * 新增批处理
     *
     * @param request
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, Exception.class})
    public BaseResponse<CreateBatchResponse> createBatch(CreateBatchRequest request) throws Exception {
        String fileId = request.getFileId();
        if (StringUtils.isBlank(fileId)) {
            return new BaseResponse<CreateBatchResponse>(false, "000001", "请选择需要批处理的文件");
        }

        Integer batchType = request.getBatchType();
        if (batchType == null) {
            return new BaseResponse<CreateBatchResponse>(false, "000002", "请选择批处理操作类型");
        }

        //检查批处理文件是否上传成功了
        FileUpload uploadBatchFile = fileUploadMapper.selectByPrimaryKey(fileId);
        if (uploadBatchFile == null) {
            return new BaseResponse<CreateBatchResponse>(false, "000003", "批处理文件没有上传成功,请重新上传");
        }
        String batchFilePath = uploadBatchFile.getFileSavePath();
        File batchFile = new File(batchFilePath);
        if (!batchFile.exists()) {
            return new BaseResponse<CreateBatchResponse>(false, "000003", "批处理文件没有上传成功,请重新上传");
        }

        BatchOperationLog batchOperationLog = new BatchOperationLog();
        String batchId = sequenceDefService.nextId(SequenceDefConstant.BatchOperationLog.TABLE_NAME,
                SequenceDefConstant.BatchOperationLog.COLUMN_NAME);//获取主键
        batchOperationLog.setBatchId(batchId);
        batchOperationLog.setBatchType(batchType);
        batchOperationLog.setCreateOperator(request.getOperName());
        batchOperationLog.setCreateTime(new Date());
        batchOperationLog.setFileId(fileId);
        batchOperationLog.setStatus(BatchOptStatusEnum.Pending.getValue());
        batchOperationLog.setUploadFilePath(uploadBatchFile.getFileSavePath());
        batchOperationLog.setUploadFileUrl(uploadBatchFile.getFileUrl());

        batchOperationLogMapper.insertSelective(batchOperationLog);

        BaseResponse<CreateBatchResponse> resp = new BaseResponse<CreateBatchResponse>();
        resp = resp.setSuccessfulResp("批操作处理已经开始,请稍后来查看处理结果");

        //开启线程批处理
//        this.batchExecute(batchOperationLog);
        return resp;
    }

//    private void batchExecute(BatchOperationLog batchOperationLog) throws Exception {
//        Integer batchType = batchOperationLog.getBatchType();
//        Thread thread = null;
//        if (batchType == BatchOptTypeEnum.OpenAccount.getValue()) {
//            log.info("批量开户线程开始");
//
//        } else if (batchType == BatchOptTypeEnum.BlackUserImport.getValue()) {
//            log.info("批量导入黑名单线程开始");
//
//        } else if (batchType == BatchOptTypeEnum.BlackUserFilter.getValue()) {
//            log.info("黑名单过滤线程开始");
////            BlackUserFilterThread myThread = ApplicationContextProvider.getBean("blackUserFilterThread", BlackUserFilterThread.class);
////            BlackUserFilterThread myThread = new BlackUserFilterThread(batchOperationLog,batchFileSavePath,fileServerURL);
////            thread = new Thread(myThread);
////            BlackUserFilterTask task = new BlackUserFilterTask(batchOperationLog);
//            BlackUserFilterTask task = ApplicationContextProvider.getBean(BlackUserFilterTask.class);
//            task.setBatchOperationLog(batchOperationLog);
//            task.start();
//        }
////        thread.start();
//    }
}
