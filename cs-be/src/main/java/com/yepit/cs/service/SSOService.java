package com.yepit.cs.service;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.domain.SysAdmin;
import com.yepit.cs.dto.admin.AdminLoginRequest;
import com.yepit.cs.dto.admin.AdminLoginResponse;
import com.yepit.cs.dto.sso.SSOAuthResponse;
import com.yepit.cs.mapper.SysAdminMapper;
import com.yepit.cs.util.CusAccessObjectUtils;
import com.yepit.cs.util.RedisUtils;
import com.yepit.cs.util.encrypt.RSAUtils;
import com.yepit.cs.util.token.JWTUtils;
import com.yepit.cs.util.token.TokenDetailImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by qianlong on 2018/1/28.
 */
@Service
public class SSOService {

    @Value("${sso.token.expiration}")
    private Long expiration;

    protected static Logger LOGGER = Logger.getLogger(SSOService.class);

    @Autowired
    private JWTUtils jwtUtils;

    @Autowired
    private SysAdminMapper sysAdminMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private AdminService adminService;

    @Value("${sso.server.contextPath}")
    private String ssoServer;

    /**
     * 校验该用户是否可通过SSO登录
     *
     * @param loginName
     * @return
     */
    public BaseResponse<SSOAuthResponse> ssoRequest(String loginName, HttpServletRequest httpRequest)
            throws Exception {
        BaseResponse<SSOAuthResponse> resp = new BaseResponse<SSOAuthResponse>();
        /**
         * 校验消息头中是否带有应有的标识
         */
//        String channel = httpRequest.getHeader("channel");
//        if (StringUtils.isBlank(channel)) {
//            resp = resp.setFailResp("000001", "非法的请求来源");
//            return resp;
//        }
        /**
         * 校验该请求地址是否在白名单中
         */
        String remoteIP = CusAccessObjectUtils.getIpAddress(httpRequest);
        LOGGER.info("远程访问IP地址为:" + remoteIP);

        /**
         * 校验用户是否存在
         */
        SysAdmin cond = new SysAdmin();
        cond.setLoginName(loginName);
        List<SysAdmin> adminList = sysAdminMapper.listByCond(cond);
        if (adminList == null || adminList.size() == 0) {
            resp = resp.setFailResp("000003", "用户不存在");
            return resp;
        }
        SysAdmin admin = adminList.get(0);

        //生成token
        TokenDetailImpl tokenDetail = new TokenDetailImpl(admin.getLoginName());
        String accessToken = jwtUtils.generateToken(tokenDetail);
        /**
         * 对参数进行加密
         */
        String parameter = "loginName=" + admin.getLoginName() + "&loginPwd=" + admin.getLoginPwd();
        //初始化密钥
        Map<String, Object> keyMap = RSAUtils.initKey(accessToken);
        //公钥
        String publicKey = RSAUtils.getPublicKey(keyMap);
        //私钥
        String privateKey = RSAUtils.getPrivateKey(keyMap);
        //加密
        String encodedStr = RSAUtils.encryptByPublicKey(parameter, publicKey);

        Map<String, String> encryption = new HashMap<String, String>();
        encryption.put("privateKey", privateKey);
        encryption.put("loginName", loginName);
        encryption.put("loginPwd", admin.getLoginPwd());
        //将加密后的信息、私钥都放入缓存中
        redisUtils.set(encodedStr, encryption, expiration);

        String url = ssoServer + "/ssoAuth?msg=" + encodedStr;
        resp = resp.setSuccessfulResp("认证通过");
        SSOAuthResponse ssoAuthResponse = new SSOAuthResponse();
        ssoAuthResponse.setLoginUrl(url);
        resp.setResult(ssoAuthResponse);
        return resp;
    }

    /**
     * 对SSO信息进行认证
     *
     * @param msg
     * @return
     * @throws Exception
     */
    public BaseResponse<AdminLoginResponse> ssoAuth(String msg) throws Exception {
        BaseResponse<AdminLoginResponse> resp = new BaseResponse<AdminLoginResponse>();
        Object encryptionObj = redisUtils.get(msg);
        if (encryptionObj == null) {
            resp = resp.setFailResp("000001", "非法的请求");
            return resp;
        }
        Map<String, String> encryption = (HashMap<String, String>) redisUtils.get(msg);
        //取出私钥进行解密
        String privateKey = encryption.get("privateKey");
        String loginName = encryption.get("loginName");
        String loginPwd = encryption.get("loginPwd");
        String decodedStr = RSAUtils.decryptByPrivateKey(msg, privateKey);//解密
        String userInfo = "loginName=" + loginName + "&loginPwd=" + loginPwd;
        if (decodedStr.equals(userInfo)) {
            AdminLoginRequest adminLoginRequest = new AdminLoginRequest();
            adminLoginRequest.setLoginName(loginName);
            adminLoginRequest.setLoginPwd(loginPwd);
            resp = adminService.adminLogin(adminLoginRequest);
            return resp;
        } else {
            resp = resp.setFailResp("000001", "非法的请求");
            return resp;
        }
    }
}
