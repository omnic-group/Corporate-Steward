package com.yepit.cs.service;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.IsmpOrderStatusEnum;
import com.yepit.cs.constant.IsmpPlaceOrderRequestStatusEnum;
import com.yepit.cs.domain.IsmpOrder;
import com.yepit.cs.dto.ismp.IsmpPrePlaceOrderRequest;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.mapper.IsmpOrderMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.yepit.cs.mapper.IsmpPlaceOrderRequestMapper;
import com.yepit.cs.domain.IsmpPlaceOrderRequest;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.yepit.cs.rabbit.RabbitConstant.ISMP_PLACE_ORDER_QUEUE;

@Service
public class IsmpPlaceOrderRequestService{

    @Resource
    private IsmpPlaceOrderRequestMapper ismpPlaceOrderRequestMapper;

    @Autowired
    private IsmpOrderMapper ismpOrderMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    
    public int deleteByPrimaryKey(Long id) {
        return ismpPlaceOrderRequestMapper.deleteByPrimaryKey(id);
    }

    
    public int insert(IsmpPlaceOrderRequest record) {
        return ismpPlaceOrderRequestMapper.insert(record);
    }

    
    public int insertSelective(IsmpPlaceOrderRequest record) {
        return ismpPlaceOrderRequestMapper.insertSelective(record);
    }

    
    public IsmpPlaceOrderRequest selectByPrimaryKey(Long id) {
        return ismpPlaceOrderRequestMapper.selectByPrimaryKey(id);
    }

    
    public int updateByPrimaryKeySelective(IsmpPlaceOrderRequest record) {
        return ismpPlaceOrderRequestMapper.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(IsmpPlaceOrderRequest record) {
        return ismpPlaceOrderRequestMapper.updateByPrimaryKey(record);
    }

    /**
     * 根据状态查询
     * @param status
     * @return
     */
    public List<IsmpPlaceOrderRequest> findByStatus(Integer status){
        return ismpPlaceOrderRequestMapper.findByStatus(status);
    }

    /**
     * 保存ISMP正式下订单请求
     * @param ismpPrePlaceOrderRequest
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, rollbackFor = {RuntimeException.class, ServiceException.class, Exception.class})
    public BaseResponse savePrePlaceOrderReqeust(IsmpPrePlaceOrderRequest ismpPrePlaceOrderRequest){
        BaseResponse orderResp = null;
        //查询所有未确认过的预订单
        IsmpOrder queryCond = new IsmpOrder();
        queryCond.setIsmpSerial(ismpPrePlaceOrderRequest.getIsmpSerial());
        queryCond.setStatus(IsmpOrderStatusEnum.WaitingForConfirm.getValue());
        List<IsmpOrder> ismpOrders = ismpOrderMapper.listByCond(queryCond);
        if (ismpOrders == null || ismpOrders.size() == 0) {
            throw new ServiceException("没有找到预订单");
        }

        //将请求先放到中间表,并且发往消息队列
        IsmpPlaceOrderRequest ismpPlaceOrderRequest = new IsmpPlaceOrderRequest();
        ismpPlaceOrderRequest.setIsmpSerial(ismpPrePlaceOrderRequest.getIsmpSerial());
        ismpPlaceOrderRequest.setPhoneNumber(ismpPrePlaceOrderRequest.getPhoneNumber());
        ismpPlaceOrderRequest.setIsmpResult(ismpPrePlaceOrderRequest.getResult());
        ismpPlaceOrderRequest.setStatus(IsmpPlaceOrderRequestStatusEnum.WAITING_FOR_HANDLE.getValue());
        ismpPlaceOrderRequest.setRequestTime(new Date());
        ismpPlaceOrderRequest.setId(IdUtil.getSnowflake(1,1).nextId());
        ismpPlaceOrderRequestMapper.insert(ismpPlaceOrderRequest);
        orderResp = new BaseResponse();
        orderResp = orderResp.setSuccessfulResp("保存订单请求成功");

        // 同步发送消息
        String message = JSON.toJSONString(ismpPlaceOrderRequest);
        rabbitTemplate.convertAndSend(ISMP_PLACE_ORDER_QUEUE, message);
        return orderResp;
    }

}
