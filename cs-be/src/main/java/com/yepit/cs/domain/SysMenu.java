package com.yepit.cs.domain;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public class SysMenu {

    private String menuId;

    private String parentMenuId;

    private String menuName;

    private String menuUrl;

    private String icon;

    private Integer sort;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId == null ? null : menuId.trim();
    }

    public String getParentMenuId() {
        return parentMenuId;
    }

    public void setParentMenuId(String parentMenuId) {
        this.parentMenuId = parentMenuId == null ? null : parentMenuId.trim();
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName == null ? null : menuName.trim();
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl == null ? null : menuUrl.trim();
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(menuId);
        builder.append(menuName);
        builder.append(menuUrl);
        builder.append(parentMenuId);
        builder.append(sort);
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SysMenu))
            return false;
        final SysMenu other = (SysMenu)obj;
        if(!other.getMenuId().equals(menuId))
            return false;
        return true;
    }

}