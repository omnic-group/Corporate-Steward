package com.yepit.cs.domain;

import lombok.Data;

import java.util.Date;

@Data
public class UserSubscribeInfo {
    private String subscriptionId;

    private String userId;

    private String phonenumber;

    private Integer telecomOperator;

    private Integer phoneType;

    private String userName;

    private String area;

    private String areaName;

    private String productId;

    private String productCode;

    private String productName;

    private String chargeId;

    private Integer productPrice;

    private Integer productType;

    private Date openTime;

    private Date closeTime;

    private Integer status;

    private Integer operateOrigin;

    private Date createTime;

    private String createOperator;

    private Date updateTime;

    private String updateOperator;

    private String fileId;

    private String fileUrl;

    private String startDate;

    private String endDate;

    private Long adminId;
}