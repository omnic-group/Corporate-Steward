package com.yepit.cs.domain;

import java.util.Date;

public class OrderAttachment {
    private String orderAttachmentId;

    private String subscriptionId;

    private String fileId;

    private Date createTime;

    private String createOperator;

    public String getOrderAttachmentId() {
        return orderAttachmentId;
    }

    public void setOrderAttachmentId(String orderAttachmentId) {
        this.orderAttachmentId = orderAttachmentId == null ? null : orderAttachmentId.trim();
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId == null ? null : subscriptionId.trim();
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId == null ? null : fileId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateOperator() {
        return createOperator;
    }

    public void setCreateOperator(String createOperator) {
        this.createOperator = createOperator == null ? null : createOperator.trim();
    }
}