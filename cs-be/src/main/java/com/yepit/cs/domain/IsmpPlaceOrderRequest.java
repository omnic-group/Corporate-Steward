package com.yepit.cs.domain;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
    * ISMP发来的最终落订单的数据，处理完成后更新状态
    */
@Data
public class IsmpPlaceOrderRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String ismpSerial;

    private String phoneNumber;

    private Integer ismpResult;

    private Date requestTime;

    /**
    * 1-待处理
            2-已处理
    */
    private Integer status;

    private Date processTime;

    private String processResult;
}