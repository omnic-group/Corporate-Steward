package com.yepit.cs.domain;

import java.util.Date;

public class OrderLog {
    private String orderLogId;

    private String subscriptionId;

    private Integer orderType;

    private Integer operateOrigin;

    private Date createTime;

    private String createOperator;

    public String getOrderLogId() {
        return orderLogId;
    }

    public void setOrderLogId(String orderLogId) {
        this.orderLogId = orderLogId == null ? null : orderLogId.trim();
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId == null ? null : subscriptionId.trim();
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getOperateOrigin() {
        return operateOrigin;
    }

    public void setOperateOrigin(Integer operateOrigin) {
        this.operateOrigin = operateOrigin;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateOperator() {
        return createOperator;
    }

    public void setCreateOperator(String createOperator) {
        this.createOperator = createOperator == null ? null : createOperator.trim();
    }
}