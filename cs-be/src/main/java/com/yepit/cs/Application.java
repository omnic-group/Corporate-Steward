package com.yepit.cs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by qianlong on 2017/8/17.
 */
@SpringBootApplication
@EnableScheduling
@ComponentScan({ "com.yepit.cs" })
@ServletComponentScan
@EnableCaching
@Configuration
@EnableAutoConfiguration
@MapperScan(basePackages = "com.yepit.cs.mapper")
@EnableTransactionManagement
@EnableAsync
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) throws Exception {
        System.out.println("------------Application is start---------------");
        SpringApplication.run(Application.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        logger.info("----------Application configure-------");
        return builder.sources(this.getClass());
    }
}
