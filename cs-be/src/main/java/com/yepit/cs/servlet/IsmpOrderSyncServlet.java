package com.yepit.cs.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.domain.IsmpPlaceOrderRequest;
import com.yepit.cs.dto.ismp.IsmpPrePlaceOrderRequest;
import com.yepit.cs.dto.ismp.IsmpPreOrderRequest;
import com.yepit.cs.dto.order.SubscribeProductResponse;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.service.IsmpOrderService;
import com.yepit.cs.service.IsmpPlaceOrderRequestService;
import com.yepit.cs.util.HttpUtils;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author qianlong
 * @description //ISMP异步回单处理
 * @Date 2019/8/26 1:52 PM
 **/
@WebServlet(urlPatterns = "/ismp/order/sync")
@Log4j
public class IsmpOrderSyncServlet extends HttpServlet {

    @Autowired
    private IsmpOrderService ismpOrderService;

    @Autowired
    private IsmpPlaceOrderRequestService ismpPlaceOrderRequestService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doService(req, resp);
    }

    public void doService(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpUtils.showParams(req);
        String orderString = req.getParameter("order_string");
        String params = req.getParameter("params");
        String orderResult = req.getParameter("orderResult");
        log.info("收到ISMP请求(order_string):" + orderString);
        log.info("收到ISMP请求(params):" + params);
        log.info("收到ISMP请求(orderResult):" + orderResult);
        if(StringUtils.isNotBlank(orderString)){
            log.info("第一次平台处理结果:" + null);
            HttpUtils.returnHttpResonse(res,null);
        }
        //params不为空时进行回调处理,生成预订单
        if (StringUtils.isNotBlank(params)) {
            IsmpPreOrderRequest preOrderRequest = new IsmpPreOrderRequest();
            JSONObject result = null;
            BaseResponse resp = null;
            try {
                JSONObject requestParams = JSONObject.parseObject(params);
                preOrderRequest.setPhoneNumber(requestParams.getString("user_id"));
                preOrderRequest.setIsmpSerial(requestParams.getString("stream_no"));
                preOrderRequest.setProductCode(requestParams.getString("ismp_product_id"));
                preOrderRequest.setTradeType(requestParams.getInteger("op_type"));
                resp = ismpOrderService.createPreOrder(preOrderRequest);
                log.info("第二次平台处理结果[" + preOrderRequest.getPhoneNumber() + "]:" + JSON.toJSONString(resp));
                result = new JSONObject();
                if (resp.isSuccess()) {
                    result.put("content", "");
                    result.put("blackflag", "0");
                } else {
                    result.put("content", resp.getResultDesc());
                    result.put("blackflag", "0");
                }
            } catch (ServiceException ex) {
                log.error(ex.getMessage());
                resp = new BaseResponse<>().setFailResp(ex.getErrorCode(), ex.getMessage());
            } catch (Exception ex) {
                log.error(ex.getMessage());
                resp = new BaseResponse<>().setFailResp("999999", ex.getMessage());
            }
            if(resp.isSuccess()){
                log.info("第二次返回给ISMP平台结果[" + preOrderRequest.getPhoneNumber() + "]结果:" + JSON.toJSONString(result));
                HttpUtils.returnHttpResonse(res,JSON.toJSONString(result));
            }else{
                HttpUtils.returnHttpResonse(res,null);
            }
        }

        //orderResult不为空时进行回调处理,生成正式订单
        if (StringUtils.isNotBlank(orderResult)) {
            JSONObject orderResultParams = JSONObject.parseObject(orderResult);
            IsmpPrePlaceOrderRequest prePlaceOrderRequest = new IsmpPrePlaceOrderRequest();
            BaseResponse<SubscribeProductResponse> resp = null;
            try {
                prePlaceOrderRequest.setIsmpSerial(orderResultParams.getString("stream_no"));
                prePlaceOrderRequest.setPhoneNumber(orderResultParams.getString("user_id"));
                prePlaceOrderRequest.setResult(orderResultParams.getInteger("result"));
                resp = ismpPlaceOrderRequestService.savePrePlaceOrderReqeust(prePlaceOrderRequest);

            } catch (ServiceException ex) {
                log.error(ex.getMessage());
                resp = new BaseResponse<SubscribeProductResponse>().setFailResp(ex.getErrorCode(), ex.getMessage());
            } catch (Exception ex) {
                log.error(ex.getMessage());
                resp = new BaseResponse<SubscribeProductResponse>().setFailResp("999999", ex.getMessage());
            }
            log.info("第三次平台处理结果[" + prePlaceOrderRequest.getPhoneNumber() + "]:" + JSON.toJSONString(resp));
            log.info("第三次返回给ISMP平台结果[" + prePlaceOrderRequest.getPhoneNumber() + "]结果:null");
            HttpUtils.returnHttpResonse(res,null);
        }
    }
}
