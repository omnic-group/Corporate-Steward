package com.yepit.cs.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.yepit.cs.dto.system.AreaInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.awt.geom.Area;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by qianlong on 2017/9/3.
 */
public class AreaUtils {

    private static List<AreaInfo> allAreaList = null;

    public static HashMap<String, AreaInfo> areaInfoMap = null;

    public static HashMap<String,List<AreaInfo>> provinceCityMap = null;

    public static List<String> provinceNameList = new ArrayList<String>();
    static{
        provinceNameList.add("北京");
        provinceNameList.add("天津");
        provinceNameList.add("上海");
        provinceNameList.add("重庆");
        provinceNameList.add("河北");
        provinceNameList.add("山西");
        provinceNameList.add("陕西");
        provinceNameList.add("山东");
        provinceNameList.add("河南");
        provinceNameList.add("辽宁");
        provinceNameList.add("吉林");
        provinceNameList.add("黑龙江");
        provinceNameList.add("江苏");
        provinceNameList.add("浙江");
        provinceNameList.add("安徽");
        provinceNameList.add("江西");
        provinceNameList.add("福建");
        provinceNameList.add("湖北");
        provinceNameList.add("湖南");
        provinceNameList.add("四川");
        provinceNameList.add("贵州");
        provinceNameList.add("云南");
        provinceNameList.add("广东");
        provinceNameList.add("海南");
        provinceNameList.add("甘肃");
        provinceNameList.add("青海");
        provinceNameList.add("内蒙古");
        provinceNameList.add("新疆");
        provinceNameList.add("广西");
        provinceNameList.add("宁夏");
        provinceNameList.add("西藏");
        provinceNameList.add("香港");
        provinceNameList.add("澳门");
        provinceNameList.add("台湾");
    }

    /**
     * 返回所有区域信息
     * @return HashMap，key=areaCode,value=AreaInfo
     * @throws Exception
     */
    public static HashMap<String, AreaInfo> getAreaInfoMap() throws Exception {
        if (areaInfoMap == null || areaInfoMap.size() == 0) {
            areaInfoMap = getAllArea();
        }
        return areaInfoMap;
    }

    /**
     * 获取省份和地市的关系
     * @return
     * @throws Exception
     */
    public static HashMap<String,List<AreaInfo>> getProvinceCityMap() throws Exception{
        if (provinceCityMap != null && provinceCityMap.size() > 0) {
            return provinceCityMap;
        }

        allAreaList = getAllAreas();
        provinceCityMap = new HashMap<String,List<AreaInfo>>();
        for(String provinceName:provinceNameList){
            List<AreaInfo> cityList = new ArrayList<AreaInfo>();
            for(AreaInfo areaInfo:allAreaList){
                String province = areaInfo.getProvince();
                if(StringUtils.isNotBlank(province) && province.endsWith(provinceName)){
                    cityList.add(areaInfo);
                }
            }
            provinceCityMap.put(provinceName,cityList);
        }

        return provinceCityMap;
    }

    /**
     * 根据省份名称获取下级一级地市列表
     * @param provinceName 省份中文名称
     * @return
     * @throws Exception
     */
    public static List<AreaInfo> getCityListByProvince(String provinceName) throws Exception{
        if (provinceCityMap == null || provinceCityMap.size() == 0) {
            provinceCityMap = getProvinceCityMap();
        }

        return provinceCityMap.get(provinceName);
    }

    /**
     * 根据地区中文名称获取地区详细信息
     * @param areaName
     * @return
     * @throws Exception
     */
    public static AreaInfo getAreaByName(String areaName) throws Exception{
        if(allAreaList == null || allAreaList.size() > 0){
            allAreaList = getAllAreas();
        }

        for(AreaInfo areaInfo:allAreaList){
            if(areaName.equals(areaInfo.getCity())){
                return areaInfo;
            }
        }

        return null;
    }


    /**
     * 获取所有地区信息
     * @return
     * @throws Exception
     */
    public static List<AreaInfo> getAllAreas() throws Exception{
        if(allAreaList != null && allAreaList.size() > 0){
            return allAreaList;
        }

        allAreaList = new ArrayList<AreaInfo>();
        Resource resource = new ClassPathResource("areacode.json");
        BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()));
        JsonParser parser = new JsonParser();//创建解析器
        JsonArray jsonArray = (JsonArray) parser.parse(br);
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
            AreaInfo areaInfo = new AreaInfo();
            areaInfo.setCode(jsonObject.get("code").getAsString());
            areaInfo.setCity(jsonObject.get("city").getAsString());
            areaInfo.setPinyin(jsonObject.get("pinyin").getAsString());
            if (jsonObject.get("province") == null) {
                areaInfo.setProvince(jsonObject.get("city").getAsString());
            } else {
                areaInfo.setProvince(jsonObject.get("province").getAsString());
            }
            allAreaList.add(areaInfo);
        }
        return allAreaList;
    }

    public static HashMap<String, AreaInfo> getAllArea() throws Exception {
        allAreaList = getAllAreas();
        areaInfoMap = new HashMap<String, AreaInfo>();
        for(AreaInfo areaInfo:allAreaList){
            areaInfoMap.put(areaInfo.getCode(), areaInfo);
        }
        return areaInfoMap;
    }


    public static void main(String[] args) throws Exception {
        HashMap<String, AreaInfo> areaInfoMap = AreaUtils.getAreaInfoMap();
        System.out.println(areaInfoMap.size());
        HashMap<String,List<AreaInfo>> provinceCityMap = AreaUtils.getProvinceCityMap();
        System.out.println(provinceCityMap.size());
        System.out.println(provinceNameList.size());
//
//        List<AreaInfo> jiangsuCitys = getCityListByProvince("江苏");
//        for(AreaInfo city:jiangsuCitys){
//            System.out.println(city.getCity());
//        }

        AreaInfo nanjing = getAreaByName("南京");
        System.out.println((nanjing == null?"未知":nanjing.getCode()));
    }
}
