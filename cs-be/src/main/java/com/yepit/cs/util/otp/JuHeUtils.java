package com.yepit.cs.util.otp;

import com.alibaba.fastjson.JSONObject;
import com.voicetalk.CallResponseOfString;
import com.voicetalk.VoiceTask2SoapProxy;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.util.encrypt.MD5Utils;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


/**
 * 调用聚合数据平台发送短信
 *
 * @author qianlong
 */
public class JuHeUtils {

    protected static Logger log = Logger.getLogger(JuHeUtils.class);

    public static final String DEF_CHATSET = "UTF-8";
    public static final int DEF_CONN_TIMEOUT = 30000;
    public static final int DEF_READ_TIMEOUT = 30000;
    public static String userAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36";

    //配置您申请的KEY
    public static final String APPKEY_SMS = "cfc6d6a2916c119b9eead31033161f0b";
    public static final String APPKEY_VOICE = "8dfdad31257cebe9f5f0feb4d4f5f3a6";

    public static final String SMS_URL = "http://v.juhe.cn/sms/send";
    public static final String VOICE_URL = "http://op.juhe.cn/yuntongxun/voice";

    /**
     * 发送短信
     *
     * @param mobile
     * @param tpllId
     * @param paramValue
     */
    public static void sendSms(String mobile, String tpllId, String paramValue) {
        String result = null;
        Map params = new HashMap();//请求参数
        Map<String, Object> tplParams = new HashMap<String, Object>();
        tplParams.put("#code#", paramValue);
        String tplValue = urlencode(tplParams);
        params.put("mobile", mobile);//接收短信的手机号码
        params.put("tpl_id", tpllId);//短信模板ID，请参考个人中心短信模板设置
        params.put("tpl_value", tplValue);//变量名和变量值对。如果你的变量名或者变量值中带有#&=中的任意一个特殊符号，请先分别进行urlencode编码后再传递，<a href="http://www.juhe.cn/news/index/id/50" target="_blank">详细说明></a>
        params.put("key", APPKEY_SMS);//应用APPKEY(应用详细页查询)
        params.put("dtype", "json");//返回数据的格式,xml或json，默认json

        try {
            result = net(SMS_URL, params, "GET");
            System.out.println("send sms result:" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (object.getInteger("error_code") != 0) {
                throw new ServiceException(object.getInteger("error_code") + ":" + object.getString("reason"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送短信
     *
     * @param mobile 号码
     * @param tpllId 模板ID
     * @param tplParams 模板参数
     */
    public static void sendSmsByTplParams(String mobile, String tpllId, Map<String, Object> tplParams) {
        String result = null;
        Map params = new HashMap();//请求参数
        String tplValue = urlencode(tplParams);
        params.put("mobile", mobile);//接收短信的手机号码
        params.put("tpl_id", tpllId);//短信模板ID，请参考个人中心短信模板设置
        params.put("tpl_value", tplValue);//变量名和变量值对。如果你的变量名或者变量值中带有#&=中的任意一个特殊符号，请先分别进行urlencode编码后再传递，<a href="http://www.juhe.cn/news/index/id/50" target="_blank">详细说明></a>
        params.put("key", APPKEY_SMS);//应用APPKEY(应用详细页查询)
        params.put("dtype", "json");//返回数据的格式,xml或json，默认json

        send(SMS_URL,params,"GET");
    }

    private static void send(String smsUrl,Map params,String method){
        try {
            String result = net(SMS_URL, params, "GET");
            System.out.println("send sms result:" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (object.getInteger("error_code") != 0) {
                throw new ServiceException(object.getInteger("error_code") + ":" + object.getString("reason"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送语音验证码(聚合平台)
     *
     * @param mobile
     * @param otpCode
     */
    public static void sendVoiceByJuHe(String mobile, String otpCode) {
        String result = null;
        Map params = new HashMap();//请求参数
        params.put("valicode", otpCode);//验证码内容，字母、数字 4-8位
        params.put("to", mobile);//接收手机号码
        params.put("playtimes", "3");//验证码播放次数，默认3
        params.put("key", APPKEY_VOICE);//应用APPKEY(应用详细页查询)
        params.put("dtype", "json");//返回数据的格式,xml或json，默认json

        try {
            result = net(VOICE_URL, params, "GET");
            System.out.println("send voice result:" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (object.getInteger("error_code") != 0) {
                throw new ServiceException(object.getInteger("error_code") + ":" + object.getString("reason"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送语音验证码(音信通)
     *
     * @param mobile
     * @param otpCode
     */
    public static void sendVoiceByYXT(String mobile, String otpCode) {
        VoiceTask2SoapProxy voiceTask = new VoiceTask2SoapProxy();
        String userCode = "yxtitf";
        String pwd = "sP9VCwYU@e";
        long timeStampSec = System.currentTimeMillis() / 1000;
        String timestamp = String.format("%010d", timeStampSec);
        try{
            String encodePwd = MD5Utils.encode(MD5Utils.encode(userCode + MD5Utils.encode(pwd)) + timestamp);
            CallResponseOfString resp = voiceTask.sendVCode(userCode, encodePwd, timestamp, "voice", mobile, otpCode);
            log.info("向" + mobile + "发送语音验证码结果:" + resp.getMessage());
        }catch (Exception ex){
            throw new ServiceException("发送语音验证码失败",ex);
        }

    }


    /**
     * @param strUrl 请求地址
     * @param params 请求参数
     * @param method 请求方法
     * @return 网络请求字符串
     * @throws Exception
     */
    public static String net(String strUrl, Map params, String method) throws Exception {
        HttpURLConnection conn = null;
        BufferedReader reader = null;
        String rs = null;
        try {
            StringBuffer sb = new StringBuffer();
            if (method == null || method.equals("GET")) {
                strUrl = strUrl + "?" + urlencode(params);
            }
            URL url = new URL(strUrl);
            conn = (HttpURLConnection) url.openConnection();
            if (method == null || method.equals("GET")) {
                conn.setRequestMethod("GET");
            } else {
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
            }
            conn.setRequestProperty("User-agent", userAgent);
            conn.setUseCaches(false);
            conn.setConnectTimeout(DEF_CONN_TIMEOUT);
            conn.setReadTimeout(DEF_READ_TIMEOUT);
            conn.setInstanceFollowRedirects(false);
            conn.connect();
            if (params != null && method.equals("POST")) {
                try {
                    DataOutputStream out = new DataOutputStream(conn.getOutputStream());
                    out.writeBytes(urlencode(params));
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
            InputStream is = conn.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is, DEF_CHATSET));
            String strRead = null;
            while ((strRead = reader.readLine()) != null) {
                sb.append(strRead);
            }
            rs = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
        return rs;
    }

    //将map型转为请求参数型
    public static String urlencode(Map<String, Object> data) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry i : data.entrySet()) {
            try {
                sb.append(i.getKey()).append("=").append(URLEncoder.encode(i.getValue() + "", "UTF-8")).append("&");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) throws Exception {
        String mobile = "18551855920";
        String tplId = "148784";
        sendSms(mobile, tplId, null);
//        sendVoiceByYXT(mobile,"1234");
    }
}
