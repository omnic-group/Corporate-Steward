package com.yepit.cs.util.encrypt;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/2/20 9:23 AM
 **/
public class MD5Utils {

    /**
     * 利用MD5进行加密
     *
     * @param str 待加密的字符串
     * @return 加密后的字符串
     * @throws NoSuchAlgorithmException     没有这种产生消息摘要的算法
     * @throws UnsupportedEncodingException
     */
    public static String encode(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String encodeStr = DigestUtils.md5Hex(str);
        return encodeStr;
    }

}
