package com.yepit.cs.util;

import com.alibaba.fastjson.JSON;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/8/26 1:56 PM
 **/
@Log4j
public class HttpUtils {

    public static void returnHttpResonse(HttpServletResponse httpServletResponse, String resp) {
        try {
            httpServletResponse.setHeader("Content-type", "application/json;charset=UTF-8");
            httpServletResponse.setCharacterEncoding("UTF-8");
            if (resp == null) {
                httpServletResponse.getOutputStream().write("".getBytes());
            } else {
                httpServletResponse.getOutputStream().write(resp.getBytes());
            }
            httpServletResponse.setStatus(200);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

    }

    public static Map<String, Object> showParams(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        Enumeration paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = (String) paramNames.nextElement();

            String[] paramValues = request.getParameterValues(paramName);
            if (paramValues.length > 0) {
                String paramValue = paramValues[0];
                if (paramValue.length() != 0) {
                    map.put(paramName, paramValue);
                }
            }
        }

        Set<Map.Entry<String, Object>> set = map.entrySet();
        log.info("==============================================================");
        log.info(JSON.toJSONString(set));
//        for (Map.Entry entry : set) {
//            log.debug(entry.getKey() + ":" + entry.getValue());
//        }
        log.debug("=============================================================");
        return map;
    }

}
