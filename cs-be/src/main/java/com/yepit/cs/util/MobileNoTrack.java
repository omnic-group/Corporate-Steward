package com.yepit.cs.util;

import com.yepit.cs.constant.TelecomTypeEnum;
import com.yepit.cs.dto.system.AreaInfo;
import com.yepit.cs.dto.user.PhoneInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * 通过聚合API查手机号码归属地
 * Created by qianlong on 2017/8/27.
 */
public class MobileNoTrack {

    protected static Logger log = Logger.getLogger(MobileNoTrack.class);

    public static final String DEF_CHATSET = "UTF-8";
    public static final int DEF_CONN_TIMEOUT = 30000;
    public static final int DEF_READ_TIMEOUT = 30000;
    public static String userAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36";

    //配置您申请的KEY
    public static final String APPKEY = "6bb695025421d4df468ae63845290cac";
    public static final String REQUEST_URL = "http://apis.juhe.cn/mobile/get";//请求接口地址

    //1.手机归属地查询
    public static PhoneInfo requestTrack(String phone) throws Exception{
        String resultJson = null;
//        String url = "http://apis.juhe.cn/mobile/get";//请求接口地址
        Map params = new HashMap();//请求参数
        params.put("phone", phone);//需要查询的手机号码或手机号码前7位
        params.put("key", APPKEY);//应用APPKEY(应用详细页查询)
        params.put("dtype", "json");//返回数据的格式,xml或json，默认json

        try {
            resultJson = net(REQUEST_URL, params, "GET");
            log.info("请求聚合API返回结果:"+resultJson);
            Map<String,Object> respMap = JsonUtils.jsonToMap(resultJson);
            if(respMap.get("error_code") != null){
                if(respMap.get("error_code").toString().equals("0.0")){
//                    String phoneInfoStr = respMap.get("result").toString();
                    Map<String,Object> phoneInfoMap = (Map<String,Object>)respMap.get("result");

                    String areaCode = phoneInfoMap.get("areacode").toString();
                    String areaName = phoneInfoMap.get("city").toString();
                    if(StringUtils.isBlank(areaCode)){//如果areaCode为空,尝试使用areaName来获取区号
                        AreaInfo cityInfo = AreaUtils.getAreaByName(areaName);
                        if(cityInfo != null){
                            areaCode = cityInfo.getCode();
                        }
                    }

                    PhoneInfo phoneInfo = new PhoneInfo();
                    phoneInfo.setPhoneNumber(phone);
                    phoneInfo.setAreaCode(areaCode);
                    phoneInfo.setAreaName(areaName);
                    String company = phoneInfoMap.get("company").toString();
                    if(company.equals("移动")){
                        phoneInfo.setTelecomOperatorType(TelecomTypeEnum.CMC.getValue());
                    }else if(company.equals("电信")){
                        phoneInfo.setTelecomOperatorType(TelecomTypeEnum.CTC.getValue());
                    }else if(company.equals("联通")){
                        phoneInfo.setTelecomOperatorType(TelecomTypeEnum.CUC.getValue());
                    }else{
                        phoneInfo.setTelecomOperatorType(null);
                    }
                    phoneInfo.setTelecomOperatorName(company);
                    return phoneInfo;
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * @param strUrl 请求地址
     * @param params 请求参数
     * @param method 请求方法
     * @return 网络请求字符串
     * @throws Exception
     */
    public static String net(String strUrl, Map params, String method) throws Exception {
        HttpURLConnection conn = null;
        BufferedReader reader = null;
        String rs = null;
        try {
            StringBuffer sb = new StringBuffer();
            if (method == null || method.equals("GET")) {
                strUrl = strUrl + "?" + urlencode(params);
            }
            log.info("请求聚合API:"+strUrl);
            URL url = new URL(strUrl);
            conn = (HttpURLConnection) url.openConnection();
            if (method == null || method.equals("GET")) {
                conn.setRequestMethod("GET");
            } else {
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
            }
            conn.setRequestProperty("User-agent", userAgent);
            conn.setUseCaches(false);
            conn.setConnectTimeout(DEF_CONN_TIMEOUT);
            conn.setReadTimeout(DEF_READ_TIMEOUT);
            conn.setInstanceFollowRedirects(false);
            conn.connect();
            if (params != null && method.equals("POST")) {
                try {
                    DataOutputStream out = new DataOutputStream(conn.getOutputStream());
                    out.writeBytes(urlencode(params));
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
            InputStream is = conn.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is, DEF_CHATSET));
            String strRead = null;
            while ((strRead = reader.readLine()) != null) {
                sb.append(strRead);
            }
            rs = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
        return rs;
    }

    //将map型转为请求参数型
    public static String urlencode(Map<String, String> data) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry i : data.entrySet()) {
            try {
                sb.append(i.getKey()).append("=").append(URLEncoder.encode(i.getValue() + "", "UTF-8")).append("&");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) throws Exception{
        PhoneInfo phoneInfo = MobileNoTrack.requestTrack("19951717099");
        System.out.println(JsonUtils.Object2Json(phoneInfo));
        System.out.println(phoneInfo.getAreaCode());
    }
}
