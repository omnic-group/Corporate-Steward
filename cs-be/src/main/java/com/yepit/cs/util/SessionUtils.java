package com.yepit.cs.util;

import com.yepit.cs.constant.SessionConstant;
import com.yepit.cs.dto.admin.AdminDTO;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/5/30 7:01 下午
 **/
public class SessionUtils {

    /**
     * 获取当前登录用户
     * @return
     */
    public static AdminDTO getCurrentUser(){
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if(null != requestAttributes) {
            HttpServletResponse response = requestAttributes.getResponse();
            HttpServletRequest request = requestAttributes.getRequest();
            HttpSession session = request.getSession();

            Object currentUser = session.getAttribute(SessionConstant.CURRENT_ADMIN);
            if (currentUser != null) {
                return (AdminDTO)currentUser;
            }
        }
        return null;
    }
}
