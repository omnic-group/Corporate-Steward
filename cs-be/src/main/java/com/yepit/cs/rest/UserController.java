package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.dto.user.*;
import com.yepit.cs.service.UserService;
import com.yepit.cs.util.ExcelUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by qianlong on 2017/8/31.
 */
@RestController
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/blackUser", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<CreateBlackUserResponse> addBlackUser(@RequestBody CreateBlackUserRequest request) throws Exception {
        try {
            request.setOperName(getCurrentAdmin().getLoginName());
            BaseResponse<CreateBlackUserResponse> resp = userService.addBlackUser(request);
            return resp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<CreateBlackUserResponse>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @RequestMapping(value = "/blackUser/{phonenumber}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<CreateBlackUserResponse> delBlackUser(@PathVariable String phonenumber) throws Exception {
        try {
            BaseResponse resp = userService.delBlackUser(phonenumber);
            return resp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<CreateBlackUserResponse>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @RequestMapping(value = "/blackUsers/{phonenumber}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public BaseResponse<BlackUserInfo> getBlackUserByPhone(@PathVariable String phonenumber) throws Exception {
        try {
            BaseResponse<BlackUserInfo> resp = userService.getBlackUserByPhone(phonenumber);
            return resp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<BlackUserInfo>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @RequestMapping(value = "/userAuth", method = RequestMethod.POST, produces = "application/json")
    public BaseResponse userAuth(@RequestBody UserAuthRequest request){
        try{
            BaseResponse resp = userService.userAuth(request);
            return resp;
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @RequestMapping(value = "/blackUserFilter", method = RequestMethod.POST, produces = "application/json")
    public BaseResponse<BlackUserFilterResponse> blackUserFilter(BlackUserFilterRequest request) throws Exception{
        try{
            BaseResponse<BlackUserFilterResponse> resp = userService.blackUserFilter(request);
            return resp;
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse<BlackUserFilterResponse>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

}
