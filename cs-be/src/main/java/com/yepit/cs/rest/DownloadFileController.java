package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by qianlong on 2017/9/3.
 */
@Controller
public class DownloadFileController {

    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/files/{fileId}", method = RequestMethod.GET)
    public void testDownload(@PathVariable String fileId,HttpServletResponse res) throws Exception{
        BaseResponse<FileUpload> resp = fileService.getUploadFile(fileId);
        if(!resp.isSuccess()){
            throw new Exception(resp.getResultDesc());
        }

        FileUpload downloadFile = resp.getResult();
        String filePath = downloadFile.getFileSavePath();

        res.setHeader("content-type", "application/octet-stream");
        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=" + downloadFile.getFileName());
        byte[] buff = new byte[1024];
        BufferedInputStream bis = null;
        OutputStream os = null;
        try {
            os = res.getOutputStream();
            bis = new BufferedInputStream(new FileInputStream(new File(filePath)));
            int i = bis.read(buff);
            while (i != -1) {
                os.write(buff, 0, buff.length);
                os.flush();
                i = bis.read(buff);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
