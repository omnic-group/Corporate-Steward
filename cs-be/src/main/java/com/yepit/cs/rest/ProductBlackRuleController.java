package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.domain.ProductBlackList;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.order.SearchSubscriptionResponse;
import com.yepit.cs.dto.product.CreateProductBlackRuleRequest;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.service.ProductBlackRuleService;
import com.yepit.cs.vo.ProductBlackRuleVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/6/26 3:53 下午
 **/
@Api(tags = "产品黑名单配置")
@RestController
@Log4j
public class ProductBlackRuleController {

    @Autowired
    private ProductBlackRuleService prodctBlackRuleService;

    /**
     * 创建黑名单规则
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "创建黑名单规则")
    @PostMapping("/productBlackRule")
    @ResponseBody
    public BaseResponse createProductBlackRule(@RequestBody CreateProductBlackRuleRequest request) {
        try {
            return prodctBlackRuleService.createProductBlackRule(request.getProductId(), request.getAreaCode());
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    /**
     * 删除黑名单规则
     *
     * @param ruleId
     * @return
     */
    @ApiOperation(value = "删除黑名单规则")
    @DeleteMapping("/productBlackRule/{id}")
    @ResponseBody
    public BaseResponse deleteProductBlackRule(@PathVariable(value = "id") Integer ruleId) {
        try {
            return prodctBlackRuleService.deleteProductBlackRuleById(ruleId);
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }


    @ApiOperation(value = "查询黑名单规则")
    @GetMapping("/productBlackRule")
    @ResponseBody
    public BaseResponse<List<ProductBlackRuleVO>> listProductBlackRule(ProductBlackList queryCond) {
        try {
            List<ProductBlackRuleVO> result = prodctBlackRuleService.listByCond(queryCond.getId(), queryCond.getProductId(), queryCond.getAreaCode());
            BaseResponse<List<ProductBlackRuleVO>> resp = new BaseResponse<List<ProductBlackRuleVO>>();
            resp = resp.setSuccessfulResp("查询数据成功");
            resp.setResult(result);
            return resp;
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<List<ProductBlackRuleVO>>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }
}
