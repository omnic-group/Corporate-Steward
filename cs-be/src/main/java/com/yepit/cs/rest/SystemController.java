package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.domain.Area;
import com.yepit.cs.dto.system.ProvinceInfo;
import com.yepit.cs.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qianlong on 2017/10/8.
 */
@RestController
public class SystemController extends BaseController{

    @Autowired
    private AreaService areaService;

    @RequestMapping(value = "/provinces", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public BaseResponse<List<ProvinceInfo>> getAllProvince() throws Exception{
        try{
            List<ProvinceInfo> allProvince = new ArrayList<ProvinceInfo>();
            List<Area> provinces = areaService.getAllProvinces();
            if(provinces != null && provinces.size() > 0){
                for(Area province:provinces){
                    ProvinceInfo provinceInfo = new ProvinceInfo();
                    provinceInfo.setProvinceId(province.getAreaId());
                    provinceInfo.setProvinceName(province.getAreaName());
                    provinceInfo.setAreaCode(province.getAreaCode());
                    allProvince.add(provinceInfo);
                }
            }

            BaseResponse<List<ProvinceInfo>> resp = new BaseResponse<List<ProvinceInfo>>();
            resp = resp.setSuccessfulResp("查询数据成功");
            resp.setResult(allProvince);
            return resp;
        }catch (Exception ex){
            return new BaseResponse<List<ProvinceInfo>>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

}
