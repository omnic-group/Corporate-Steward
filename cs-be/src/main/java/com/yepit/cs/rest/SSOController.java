package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.OTPTypeEnum;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.dto.admin.AdminAuthResponse;
import com.yepit.cs.dto.admin.AdminLoginRequest;
import com.yepit.cs.dto.admin.AdminLoginResponse;
import com.yepit.cs.dto.sso.SSOAuthResponse;
import com.yepit.cs.service.AdminService;
import com.yepit.cs.service.OTPService;
import com.yepit.cs.service.SSOService;
import com.yepit.cs.util.CusAccessObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by qianlong on 2018/1/26.
 */
@RestController
public class SSOController extends BaseController{

    @Autowired
    private SSOService ssoService;

    @RequestMapping(value = "/sso/{loginName}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public BaseResponse<SSOAuthResponse> ssoRequest(@PathVariable String loginName, HttpServletRequest httpRequest){
        try{
            return ssoService.ssoRequest(loginName,httpRequest);
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse<SSOAuthResponse>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

    @RequestMapping(value = "/ssoAuth", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public BaseResponse<AdminLoginResponse> ssoAuth(@RequestParam String msg, HttpServletRequest httpRequest){
        try{
            String decodeMsg = msg.replaceAll(" ","+");
            return ssoService.ssoAuth(decodeMsg);
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse<AdminLoginResponse>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

}
