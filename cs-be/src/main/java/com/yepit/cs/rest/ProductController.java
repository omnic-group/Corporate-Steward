package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.common.PageResult;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.product.CreateProductRequest;
import com.yepit.cs.dto.product.PageSearchProductRequest;
import com.yepit.cs.dto.product.UpdateProductRequest;
import com.yepit.cs.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by qianlong on 2017/8/20.
 */
@RestController
public class ProductController extends BaseController{

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/product", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<ProductInfo> createProduct(@RequestBody CreateProductRequest request){
        BaseResponse<ProductInfo> resp = new BaseResponse<ProductInfo>();
        try{
            ProductInfo productInfo = new ProductInfo();
            BeanUtils.copyProperties(request,productInfo);
            productInfo.setCreateOperator(getCurrentAdmin().getLoginName());
            resp = productService.createProduct(productInfo);
            return resp;
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse<ProductInfo>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

    @RequestMapping(value = "/product/{productId}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<ProductInfo> updateProduct(@PathVariable String productId,@RequestBody UpdateProductRequest request){
        BaseResponse<ProductInfo> resp = new BaseResponse<ProductInfo>();
        try{
            ProductInfo productInfo = new ProductInfo();
            BeanUtils.copyProperties(request,productInfo);
            productInfo.setProductId(productId);
            productInfo.setUpdateOperator(getCurrentAdmin().getLoginName());
            resp = productService.updateProduct(productInfo);
            return resp;
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse<ProductInfo>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

    @RequestMapping(value = "/products", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<PageResult<ProductInfo>> pageSearchProduct(@RequestBody PageSearchProductRequest request){
        BaseResponse<PageResult<ProductInfo>> resp = new BaseResponse<PageResult<ProductInfo>>();
        try{
            resp = productService.listProductByCond(request);
            return resp;
        }catch (Exception ex){
            ex.printStackTrace();
            resp = new BaseResponse<PageResult<ProductInfo>>();
            resp.setFailResp(ResultCodeConstant.UNKNOWERR,"查询产品信息异常");
            return resp;
        }
    }
}
