package com.yepit.cs.rest;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.*;
import com.yepit.cs.dto.ismp.IsmpSubscribeProductRequest;
import com.yepit.cs.dto.order.*;
import com.yepit.cs.dto.user.UserAuthRequest;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.service.IsmpOrderService;
import com.yepit.cs.service.OTPService;
import com.yepit.cs.service.OrderService;
import com.yepit.cs.service.UserService;
import com.yepit.cs.util.HttpUtils;
import com.yepit.cs.util.NumberUtils;
import com.yepit.cs.util.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by qianlong on 2017/8/30.
 */
@Api(tags = "订单相关接口")
@Log4j
@RestController
public class OrderController extends BaseController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private OTPService otpService;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisUtils redisUtils;

    @Value("${system.file.savepath}")
    private String tmpFilePath;

    @Autowired
    private IsmpOrderService ismpOrderService;

    @ApiOperation(value = "发起订购")
    @RequestMapping(value = "/order", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<SubscribeProductResponse> subscribeProduct(@RequestBody SubscribeProductRequest request) throws Exception {
        BaseResponse<SubscribeProductResponse> resp = new BaseResponse<SubscribeProductResponse>();
        try {
            request.setOperName(getCurrentAdmin().getLoginName());
            Integer productType = request.getProductType();
            Integer devOps = request.getDevOps();
            if (devOps != null && devOps.equals(OrderConstant.DevOpsDef.DEV_OPS)) {
                resp = orderService.subscribeProduct(request);
            } else if (productType != null && productType.intValue() == ProductTypeEnum.ISMP.getValue()) {
                IsmpSubscribeProductRequest ismpSubProductRequest = new IsmpSubscribeProductRequest();
                ismpSubProductRequest.setOrderId(request.getIsmpOrderId());
                ismpSubProductRequest.setOtp(request.getOtp());
                ismpSubProductRequest.setPhoneNumber(request.getPhoneNumber());
                return ismpOrderService.subscribeProduct(ismpSubProductRequest);
            } else {
                resp = orderService.subscribeProduct(request);
            }
            return resp;
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse<SubscribeProductResponse>(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<SubscribeProductResponse>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    /**
     * 微信渠道发起的订购
     *
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/subscriberViaWechat")
    @ResponseBody
    public BaseResponse<SubscribeProductResponse> subscribeProductViaWechat(@RequestBody SubscribeProductViaWechatRequest request) throws Exception {
        BaseResponse<SubscribeProductResponse> resp = null;
        String phoneNumber = request.getPhoneNumber();
        try {
            if (StringUtils.isBlank(phoneNumber)) {
                throw new ServiceException("000001", "请输入用户号码");
            }
            Integer verifyType = request.getVerifyType();
            if (verifyType == null || (verifyType < 1 && verifyType > 2)) {
                throw new ServiceException("000002", "密码验证方式不正确");
            }
            String password = request.getPassword();
            if (StringUtils.isBlank(password)) {
                String errorMsg = null;
                if (verifyType == 1) {
                    errorMsg = "请输入10000密码";
                }
                if (verifyType == 2) {
                    errorMsg = "请输入短信或语音验证码";
                }
                throw new ServiceException("000003", errorMsg);
            }
            if (verifyType == 1) {//校验10000密码
                UserAuthRequest authRequest = new UserAuthRequest();
                authRequest.setPhoneNumber(request.getPhoneNumber());
                authRequest.setPassword(password);
                BaseResponse authResp = userService.userAuth(authRequest);
                if (!authResp.isSuccess()) {
                    throw new ServiceException(authResp.getResultCode(), authResp.getResultDesc());
                }
            } else if (verifyType == 2) {//校验OTP
                try {
                    otpService.verifyOTP(OTPTypeEnum.Order.getValue(), phoneNumber, password);
                } catch (Exception ex) {
                    throw new ServiceException("999999", "校验验证码失败:" + ex.getMessage());
                }
            }
            SubscribeProductRequest subscribeProductRequest = new SubscribeProductRequest();
            BeanUtils.copyProperties(request, subscribeProductRequest);
            //设置号码类型
            if (NumberUtils.isMobilePhone(phoneNumber)) {
                subscribeProductRequest.setPhoneType(PhoneTypeEnum.Mobile.getValue());
            }
            if (NumberUtils.isFixedPhone(phoneNumber)) {
                subscribeProductRequest.setPhoneType(PhoneTypeEnum.FixedPhone.getValue());
                subscribeProductRequest.setTelecomOperator(TelecomTypeEnum.CTC.getValue());//先默认中国电信
            }
            resp = orderService.subscribeProduct(subscribeProductRequest);
            return resp;
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse<SubscribeProductResponse>(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<SubscribeProductResponse>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        } finally {
            //清除缓存中的验证码
            try {
                String key = OTPTypeEnum.getDescByValue(OTPTypeEnum.Order.getValue()) + "_" + phoneNumber;
                redisUtils.remove(key);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @RequestMapping(value = "/order/{subscriptionId}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse unSubscribeProduct(@PathVariable String subscriptionId, @RequestBody UnsubscribeProductRequest request) throws Exception {
        BaseResponse resp = new BaseResponse();
        try {
            request.setOperName(getCurrentAdmin().getLoginName());
            request.setSubscriptionId(subscriptionId);
            resp = orderService.unSubscribeProduct(request);
            return resp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @RequestMapping(value = "/orders", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<List<SearchSubscriptionResponse>> searchUserSubscription(@RequestBody SearchSubscriptionRequest request) {
        try {
            request.setOperName(getCurrentAdmin().getLoginName());
            request.setAdminId(getCurrentAdmin().getAdminId());
            BaseResponse<List<SearchSubscriptionResponse>> resp = orderService.searchUserSubscription(request);
            return resp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<List<SearchSubscriptionResponse>>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @ApiOperation(value = "查询用户最新的订购记录")
    @RequestMapping(value = "/lastOrders", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<List<SearchSubscriptionResponse>> searchLastOrders(@RequestBody SearchSubscriptionRequest request) {
        try {
            request.setOperName(getCurrentAdmin().getLoginName());
            BaseResponse<List<SearchSubscriptionResponse>> resp = orderService.searchUserSubscription(request);
            HashMap<String, SearchSubscriptionResponse> subscriptionMap = new HashMap<String, SearchSubscriptionResponse>();
            List<SearchSubscriptionResponse> subscriptionList = resp.getResult();
            subscriptionList.forEach(subscriptionObj -> {
                String key = subscriptionObj.getPhonenumber() + "_" + subscriptionObj.getProductCode();
                SearchSubscriptionResponse value = subscriptionMap.get(key);
                if (value == null) {
                    subscriptionMap.put(key, subscriptionObj);
                } else if (value.getUpdateTime().compareTo(subscriptionObj.getUpdateTime()) < 0) {
                    subscriptionMap.put(key, value);
                }
            });

            List<SearchSubscriptionResponse> finalResult = new ArrayList<SearchSubscriptionResponse>();
            subscriptionMap.values().forEach(subscriptionObj -> {
                finalResult.add(subscriptionObj);
            });


            resp.setResult(finalResult);
            return resp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<List<SearchSubscriptionResponse>>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    /**
     * 导出订单记录
     */
    @ApiOperation(value = "导出订单记录")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "phoneNumber", value = "用户号码", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "订单状态", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "startDate", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endDate", value = "结束时间", required = false, dataType = "string", paramType = "query")
    })
    @GetMapping(value = "/orders/export", produces = "application/json")
    public void exportUserSubscriptions(String phoneNumber, Integer status, String startDate, String endDate, HttpServletResponse res) {
        try {
            SearchSubscriptionRequest request = new SearchSubscriptionRequest();
            request.setPhoneNumber(phoneNumber);
            request.setStatus(status);
            request.setStartDate(startDate);
            request.setEndDate(endDate);
            request.setOperName(getCurrentAdmin().getLoginName());
            request.setSuperFlag(1);
            BaseResponse<List<SearchSubscriptionResponse>> resp = this.searchLastOrders(request);
            log.info(JSON.toJSONString(resp));
            List<SearchSubscriptionResponse> userSubscriptions = resp.getResult();
            if (userSubscriptions != null) {
                List<ExportOrderInfo> exportOrderInfos = new ArrayList<ExportOrderInfo>();
                for (SearchSubscriptionResponse subscriptionInfo : userSubscriptions) {
                    ExportOrderInfo exportOrderInfo = new ExportOrderInfo();
                    BeanUtils.copyProperties(subscriptionInfo, exportOrderInfo);
                    exportOrderInfos.add(exportOrderInfo);
                }
                String fileName = System.currentTimeMillis() + ".xls";
                res.setContentType("application/vnd.ms-excel");
                res.setCharacterEncoding("utf-8");
                res.setHeader("Content-disposition", "attachment;filename=" + fileName);
                EasyExcel.write(res.getOutputStream(), ExportOrderInfo.class).sheet("订单记录").doWrite(exportOrderInfos);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 订购时根据用户号码获取验证码
     *
     * @param phoneNumber
     * @return
     */
    @ApiOperation(value = "订购时根据用户号码获取验证码")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "phoneNumber", value = "用户号码", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "tplId", value = "短信模板ID(ISMP平台开通填193894)", dataType = "string", paramType = "query")
    })
    @GetMapping(value = "/otp")
    @ResponseBody
    public BaseResponse<String> getOtpBeforeOrder(@RequestParam String phoneNumber,
                                                  @RequestParam(required = false) String tplId) {
        try {
            return otpService.getOTPViaJuhe(OTPTypeEnum.Order.getValue(), phoneNumber, tplId);
        } catch (Exception ex) {
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

}
