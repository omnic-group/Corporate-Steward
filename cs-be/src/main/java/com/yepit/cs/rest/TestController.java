package com.yepit.cs.rest;

import com.alibaba.fastjson.JSON;
import com.yepit.cs.domain.IsmpPlaceOrderRequest;
import com.yepit.cs.util.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.yepit.cs.rabbit.RabbitConstant.ISMP_PLACE_ORDER_QUEUE;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/8/6 4:50 下午
 **/
@Api(tags = "测试接口")
@RestController
@Slf4j
public class TestController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @ApiOperation(value = "测试响应接口")
    @PostMapping("/test")
    public String test(HttpServletRequest req){
        log.info("------------测试接口begin------------");
        HttpUtils.showParams(req);
        log.info("------------测试接口end------------");
        return "test";
    }

    @ApiOperation(value = "测试MQ接口")
    @PostMapping("/testMQ")
    public String testMq(@RequestBody IsmpPlaceOrderRequest ismpPlaceOrderRequest){
        String message = JSON.toJSONString(ismpPlaceOrderRequest);
        rabbitTemplate.convertAndSend(ISMP_PLACE_ORDER_QUEUE, message);
        return "发送成功";
    }
}
