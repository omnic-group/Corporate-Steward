package com.yepit.cs.rest;

import com.yepit.cs.cache.AdminCache;
import com.yepit.cs.cache.ProductCache;
import com.yepit.cs.cache.SysClientCache;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.exception.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/8/13 2:08 下午
 **/
@Api(tags = "缓存接口")
@RestController
@Log4j
public class CacheController {

    @Autowired
    private AdminCache adminCache;

    @Autowired
    private ProductCache productCache;

    @Autowired
    private SysClientCache sysClientCache;

    /**
     * 刷新管理员缓存
     *
     * @param
     * @return
     */
    @ApiOperation(value = "刷新管理员缓存")
    @GetMapping("/cache/admin")
    @ResponseBody
    public BaseResponse adminReload() {
        try {
            log.info("刷新管理员缓存");
            adminCache.reload();
            return new BaseResponse().setSuccessfulResp("刷新管理员缓存成功");
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse<String>(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<String>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    /**
     * 刷新产品缓存
     *
     * @param
     * @return
     */
    @ApiOperation(value = "刷新产品缓存")
    @GetMapping("/cache/product")
    @ResponseBody
    public BaseResponse productReload() {
        try {
            log.info("刷新产品缓存");
            productCache.reload();
            return new BaseResponse().setSuccessfulResp("刷新产品缓存成功");
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse<String>(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<String>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    /**
     * 刷新产品缓存
     *
     * @param
     * @return
     */
    @ApiOperation(value = "刷新客户端授权缓存")
    @GetMapping("/cache/sysClient")
    @ResponseBody
    public BaseResponse sysClientReload() {
        try {
            log.info("刷新产品缓存");
            sysClientCache.reload();
            return new BaseResponse().setSuccessfulResp("刷新客户端授权缓存成功");
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse<String>(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<String>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }
}
