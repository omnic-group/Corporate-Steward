package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.common.PageResult;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.dto.batch.CreateBatchRequest;
import com.yepit.cs.dto.batch.CreateBatchResponse;
import com.yepit.cs.dto.batch.SearchBatchLogRequest;
import com.yepit.cs.dto.batch.SearchBatchLogResponse;
import com.yepit.cs.service.BatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by qianlong on 2017/9/2.
 */
@RestController
public class BatchController extends BaseController {

    @Autowired
    private BatchService batchService;

    @RequestMapping(value = "/batchOptLogs", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<PageResult<SearchBatchLogResponse>> listBatchOptLogByCond(@RequestBody SearchBatchLogRequest request) {
        BaseResponse<PageResult<SearchBatchLogResponse>> resp = null;
        try {
            resp = batchService.listBatchOptLogByCond(request);
            return resp;
        } catch (Exception ex) {
            ex.printStackTrace();
            resp = new BaseResponse<PageResult<SearchBatchLogResponse>>();
            resp.setFailResp(ResultCodeConstant.UNKNOWERR, "查询批处理记录异常");
            return resp;
        }
    }

    @RequestMapping(value = "/batchOpt", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<CreateBatchResponse> createBatch(@RequestBody CreateBatchRequest request) throws Exception {
        BaseResponse<CreateBatchResponse> resp = null;
        try {
            request.setOperName(getCurrentAdmin().getLoginName());
            resp = batchService.createBatch(request);
            return resp;
        } catch (Exception ex) {
            ex.printStackTrace();
            resp = new BaseResponse<CreateBatchResponse>();
            resp.setFailResp(ResultCodeConstant.UNKNOWERR, "创建批处理操作异常");
            return resp;
        }
    }
}

