package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.domain.FileUpload;
import com.yepit.cs.dto.file.FileUploadRequest;
import com.yepit.cs.dto.file.FileUploadResponse;
import com.yepit.cs.service.FileService;
import com.yepit.cs.service.OrderService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by qianlong on 2017/8/23.
 */
@RestController
public class FileController {

    protected static Logger log = Logger.getLogger(FileController.class);

    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<FileUploadResponse> uploadFile(@RequestBody FileUploadRequest request){
        BaseResponse<FileUploadResponse> resp = new BaseResponse<FileUploadResponse>();
        try{
            resp = fileService.upload(request);
            return resp;
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse<FileUploadResponse>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }
}
