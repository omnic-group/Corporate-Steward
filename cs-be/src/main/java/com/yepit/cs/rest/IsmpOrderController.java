package com.yepit.cs.rest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.domain.ProductInfo;
import com.yepit.cs.dto.ismp.*;
import com.yepit.cs.dto.order.SubscribeProductResponse;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.service.IsmpOrderService;
import com.yepit.cs.service.IsmpPlaceOrderRequestService;
import com.yepit.cs.service.ProductService;
import com.yepit.cs.util.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/8/3 5:18 PM
 **/
@Api(tags = "ISMP相关接口(V1)")
@RestController
@Log4j
public class IsmpOrderController {

    @Autowired
    private IsmpOrderService ismpOrderService;

    @Autowired
    private ProductService productService;

    @Autowired
    private IsmpPlaceOrderRequestService ismpPlaceOrderRequestService;

    @ApiOperation(value = "获取产品信息")
    @GetMapping("/ismp/orderInfo")
    @ApiImplicitParam(name = "chargeId", value = "产品ID", required = true, dataType = "string", paramType = "query")
    @ResponseBody
    public BaseResponse<HashMap<String, Object>> getOrderInfo(@RequestParam String chargeId) {
        GetIsmpOrderRequest getOrderRequest = new GetIsmpOrderRequest();
        getOrderRequest.setChargeId(chargeId);
        return ismpOrderService.getOrderInfo(getOrderRequest);
    }

    @ApiOperation(value = "获取订单ID")
    @GetMapping("/ismp/orderId")
    @ApiImplicitParam(name = "chargeId", value = "产品ID", required = true, dataType = "string", paramType = "query")
    @ResponseBody
    public BaseResponse<String> getOrderId(@RequestParam String chargeId) {
        ProductInfo productInfo = productService.getProductByChargeId(chargeId);
        if(productInfo == null){
            BaseResponse<String> resp = new BaseResponse<String>();
            resp = resp.setFailResp("999999","ChargeId对应的产品不存在");
            return resp;
        }
        return ismpOrderService.getOrderId(chargeId, productInfo.getSpid(), null);
    }

    @ApiOperation(value = "获取验证码")
    @GetMapping("/ismp/order/otp")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "chargeId", value = "产品ID", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phoneNumber", value = "用户号码", required = true, dataType = "string", paramType = "query")
    })
    @ResponseBody
    public BaseResponse<String> getOTP(@RequestParam String chargeId, @RequestParam String phoneNumber) {
        ValidSubRequest request = new ValidSubRequest();
        request.setChargeId(chargeId);
        request.setPhoneNumber(phoneNumber);
        try {
            return ismpOrderService.getOTP(request);
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse<String>(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<String>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @ApiOperation(value = "订购产品")
    @PostMapping("/ismp/product/subscribe")
    @ResponseBody
    public BaseResponse subscribeProduct(@RequestBody IsmpSubscribeProductRequest request) {
        try {
            return ismpOrderService.subscribeProduct(request);
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @ApiOperation(value = "直接订购产品")
    @PostMapping("/ismp/product/directSubscribe")
    @ResponseBody
    public BaseResponse directSubscribeProduct(@RequestBody IsmpDirectSubProductRequest request) {
        try {
            return ismpOrderService.directSubscribeProduct(request);
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @ApiOperation(value = "第三方平台直接订购产品")
    @PostMapping("/thirdParty/ismp/product/directSubscribe")
    @ResponseBody
    public BaseResponse directSubscribeProductBy3rd(@RequestBody IsmpDirectSubProductRequest request) {
        try {
            return ismpOrderService.subscribeProductBy3rd(request);
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @ApiOperation(value = "退订产品")
    @PostMapping("/ismp/product/unsubscribe")
    @ResponseBody
    public BaseResponse unsubscribeProduct(@RequestBody IsmpUnSubscribeProductRequest request) {
        try {
            return ismpOrderService.unSubscribeProduct(request);
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @ApiOperation(value = "直接退订产品")
    @PostMapping("/ismp/product/directUnsubscribe")
    @ResponseBody
    public BaseResponse directUnsubscribeProduct(@RequestBody IsmpDirectUnSubProductRequest request) {
        try {
            return ismpOrderService.directUnSubscribeProduct(request);
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @ApiOperation(value = "异步回单处理,仅供测试使用")
    @RequestMapping(value = "/ismp/order/syncTest", method = {RequestMethod.POST, RequestMethod.GET})
    public String sync(@RequestParam(value = "order_string", required = false) String orderString,
                       @RequestParam(value = "params", required = false) String params,
                       @RequestParam(value = "orderResult", required = false) String orderResult,
                       HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
        HttpUtils.showParams(servletRequest);
        log.info("收到ISMP请求(order_string):" + orderString);
        log.info("收到ISMP请求(params):" + params);
        log.info("收到ISMP请求(orderResult):" + orderResult);
        if (StringUtils.isNotBlank(orderString)) {
            log.info("第一次平台处理结果:" + null);
            return null;
        }
        //params不为空时进行回调处理,生成预订单
        if (StringUtils.isNotBlank(params)) {
            IsmpPreOrderRequest preOrderRequest = new IsmpPreOrderRequest();
            JSONObject result = null;
            BaseResponse resp = null;
            try {
                JSONObject requestParams = JSONObject.parseObject(params);
                preOrderRequest.setPhoneNumber(requestParams.getString("user_id"));
                preOrderRequest.setIsmpSerial(requestParams.getString("stream_no"));
                preOrderRequest.setProductCode(requestParams.getString("ismp_product_id"));
                preOrderRequest.setTradeType(requestParams.getInteger("op_type"));
                resp = ismpOrderService.createPreOrder(preOrderRequest);
                log.info("第二次平台处理结果[" + preOrderRequest.getPhoneNumber() + "]:" + JSON.toJSONString(resp));
                result = new JSONObject();
                if (resp.isSuccess()) {
                    result.put("content", "ok");
                    result.put("blackflag", "0");
                } else {
                    result.put("content", resp.getResultDesc());
                    result.put("blackflag", "0");
                }
            } catch (ServiceException ex) {
                log.error(ex.getMessage());
                resp = new BaseResponse<>().setFailResp(ex.getErrorCode(), ex.getMessage());
            } catch (Exception ex) {
                log.error(ex.getMessage());
                resp = new BaseResponse<>().setFailResp("999999", ex.getMessage());
            }
            if (resp.isSuccess()) {
                log.info("第二次返回给ISMP平台结果[" + preOrderRequest.getPhoneNumber() + "]结果:" + JSON.toJSONString(result));
                return result.toJSONString();
            } else {
                return null;
            }
        }

        //orderResult不为空时进行回调处理,生成正式订单
        if (StringUtils.isNotBlank(orderResult)) {
            JSONObject orderResultParams = JSONObject.parseObject(orderResult);
            IsmpPrePlaceOrderRequest placeOrderRequest = new IsmpPrePlaceOrderRequest();
            BaseResponse<SubscribeProductResponse> resp = null;
            try {
                placeOrderRequest.setIsmpSerial(orderResultParams.getString("stream_no"));
                placeOrderRequest.setPhoneNumber(orderResultParams.getString("user_id"));
                placeOrderRequest.setResult(orderResultParams.getInteger("result"));
                resp = ismpPlaceOrderRequestService.savePrePlaceOrderReqeust(placeOrderRequest);
//                resp = ismpOrderService.placeOrder(placeOrderRequest);
            } catch (ServiceException ex) {
                log.error(ex.getMessage());
                resp = new BaseResponse<SubscribeProductResponse>().setFailResp(ex.getErrorCode(), ex.getMessage());
            } catch (Exception ex) {
                log.error(ex.getMessage());
                resp = new BaseResponse<SubscribeProductResponse>().setFailResp("999999", ex.getMessage());
            }
            log.info("第三次平台处理结果[" + placeOrderRequest.getPhoneNumber() + "]:" + JSON.toJSONString(resp));
            log.info("第三次返回给ISMP平台结果[" + placeOrderRequest.getPhoneNumber() + "]结果:null");
            return null;
//            if (resp.isSuccess()) {
//                log.info("第三次返回给ISMP平台结果[" + placeOrderRequest.getPhoneNumber() + "]:null");
//                return null;
//            } else {
//                log.info("第三次返回给ISMP平台结果[" + placeOrderRequest.getPhoneNumber() + "]:" + JSON.toJSONString(resp));
//                return JSON.toJSONString(resp);
//            }
        }

        return null;
    }
}
