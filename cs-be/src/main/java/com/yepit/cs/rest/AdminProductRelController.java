package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.dto.admin.AdminProductDTO;
import com.yepit.cs.dto.admin.AdminProductRelRequest;
import com.yepit.cs.service.AdminProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2020/5/30 9:49 上午
 **/
@Api(tags = "操作员产品绑定关系接口")
@RestController
@Log4j
public class AdminProductRelController {

    @Autowired
    private AdminProductService adminProductService;

    @ApiOperation(value = "操作员绑定产品")
    @PostMapping("/admin/bindProduct")
    @ResponseBody
    public BaseResponse bindProduct(@RequestBody AdminProductRelRequest request) {
        return adminProductService.bindProduct(request.getAdminId(),request.getProductList());
    }

    @ApiOperation(value = "操作员解绑产品")
    @DeleteMapping("/admin/bindProduct")
    @ResponseBody
    public BaseResponse unBindProduct(@RequestBody AdminProductRelRequest request) {
        return adminProductService.unBindProduct(request.getAdminId(),request.getProductList());
    }

    @ApiOperation(value = "获取操作员绑定的产品")
    @GetMapping("/admin/{adminId}/products")
    @ResponseBody
    public BaseResponse<List<AdminProductDTO>> getBindProducts(@PathVariable Long adminId) {
        BaseResponse<List<AdminProductDTO>> resp = new BaseResponse<List<AdminProductDTO>>();
        List<AdminProductDTO> list = adminProductService.ListAdminProduct(adminId);
        resp = resp.setSuccessfulResp("查询数据成功",list);
        return resp;
    }
}
