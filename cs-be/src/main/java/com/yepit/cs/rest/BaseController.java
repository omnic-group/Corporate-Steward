package com.yepit.cs.rest;

import com.yepit.cs.constant.SessionConstant;
import com.yepit.cs.domain.SysAdmin;
import com.yepit.cs.dto.admin.AdminDTO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;

/**
 * Created by qianlong on 2017/8/20.
 */
public class BaseController {

    @Autowired
    private HttpSession httpSession;

    public AdminDTO getCurrentAdmin(){
        if(httpSession == null){
            return null;
        }
        Object admin = httpSession.getAttribute(SessionConstant.CURRENT_ADMIN);
        if(admin != null){
            return (AdminDTO)admin;
        }
        return null;
    }
}
