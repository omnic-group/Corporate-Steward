package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.common.PageResult;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.domain.SysAdmin;
import com.yepit.cs.dto.admin.*;
import com.yepit.cs.service.AdminService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by qianlong on 2017/8/20.
 */
@RestController
public class AdminController extends BaseController{

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<AdminLoginResponse> adminLogin(@RequestBody AdminLoginRequest request){
        try{
            return adminService.adminLogin(request);
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse<AdminLoginResponse>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

    @RequestMapping(value = "/auth", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<AdminAuthResponse> adminAuth(@RequestBody AdminLoginRequest request){
        try{
            return adminService.adminAuth(request);
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse<AdminAuthResponse>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

    @RequestMapping(value = "/admin", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse createAdmin(@RequestBody CreateAdminRequest request){
        try{
            SysAdmin sysAdmin = new SysAdmin();
            BeanUtils.copyProperties(request,sysAdmin);
            sysAdmin.setCreateOperator(getCurrentAdmin().getLoginName());
            return adminService.createAdmin(sysAdmin,request.getRoleId());
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse<AdminLoginResponse>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

    @RequestMapping(value = "/admin/{adminId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public BaseResponse<SysAdmin> adminDetail(@PathVariable Long adminId){
        try{
            return adminService.getAdminById(adminId);
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse<SysAdmin>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

    @RequestMapping(value = "/admin/{adminId}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse updateAdmin(@PathVariable Long adminId,@RequestBody UpdateAdminRequest request){
        try{
            SysAdmin admin = new SysAdmin();
            BeanUtils.copyProperties(request,admin);
            admin.setAdminId(adminId);
            return adminService.updateAdmin(admin);
        }catch (Exception ex){
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

    @RequestMapping(value = "/admins", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<PageResult<PageSearchAdminResponse>> pageSearchAdmin(@RequestBody PageSearchAdminRequest request){
        BaseResponse<PageResult<PageSearchAdminResponse>> resp = new BaseResponse<PageResult<PageSearchAdminResponse>>();
        try{
            resp = adminService.listAdminByCond(request);
            return resp;
        }catch (Exception ex){
            ex.printStackTrace();
            resp = new BaseResponse<PageResult<PageSearchAdminResponse>>();
            resp.setFailResp(ResultCodeConstant.UNKNOWERR,"查询管理员信息异常");
            return resp;
        }
    }
}
