package com.yepit.cs.rest;

import com.alibaba.fastjson.JSON;
import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.IsmpOrderTradeTypeEnum;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.domain.SysClient;
import com.yepit.cs.dto.ismp.*;
import com.yepit.cs.exception.ServiceException;
import com.yepit.cs.service.IsmpOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author qianlong
 * @description //TODO
 * @Date 2019/8/3 5:18 PM
 **/
@Api(tags = "ISMP相关接口(V2)")
@RestController
@Log4j
public class NewIsmpOrderController {

    @Autowired
    private IsmpOrderService ismpOrderService;

    /**
     * 订单请求校验,校验成功后返回ISMP订单号,发送验证码给用户
     *
     * @param ismpPreOrderRequest
     * @return
     */
    @ApiOperation(value = "获取订购验证码和订单号")
    @PostMapping("/ismp/v2/getOtp")
    @ResponseBody
    public BaseResponse<String> getOtp(@RequestHeader(value = "auth") String auth, @RequestBody NewIsmpPreOrderRequest ismpPreOrderRequest) {
        try {
            log.info("收到获取验证码请求:" + JSON.toJSONString(ismpPreOrderRequest));
            SysClient sysClient = ismpOrderService.validRequestAuth(auth);
            if (!NewIsmpPreOrderRequest.checkSign(sysClient.getSecret(), ismpPreOrderRequest)) {
                throw new ServiceException("请求签名不正确");
            }
            BaseResponse resp = ismpOrderService.partnerGetOTP(ismpPreOrderRequest.getPhoneNumber(), ismpPreOrderRequest.getProductCode(), ismpPreOrderRequest.getOrderTimestamp(), sysClient);
            return resp;
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse<String>(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse<String>(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    /**
     * 正式发起订购请求
     *
     * @param auth
     * @param ismpOrderRequest
     * @return
     */
    @ApiOperation(value = "发起订购")
    @PostMapping("/ismp/v2/subscribeProduct")
    @ResponseBody
    public BaseResponse subscribeProduct(@RequestHeader(value = "auth") String auth, @RequestBody NewIsmpOrderRequest ismpOrderRequest) {
        try {
            log.info("收到订购产品请求:" + JSON.toJSONString(ismpOrderRequest));
            SysClient sysClient = ismpOrderService.validRequestAuth(auth);
            if (!ismpOrderRequest.validRequestSign(sysClient.getSecret())) {
                throw new ServiceException("请求签名不正确");
            }
            IsmpSubscribeProductRequest request = new IsmpSubscribeProductRequest();
            request.setOrderId(ismpOrderRequest.getOrderId());
            request.setOtp(ismpOrderRequest.getOtp());
            request.setPhoneNumber(ismpOrderRequest.getPhoneNumber());
            request.setOrderType(IsmpOrderTradeTypeEnum.OrderMonthly.getValue());
            BaseResponse resp = ismpOrderService.subscribeProduct(request);
            return resp;
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }

    @ApiOperation(value = "退订产品")
    @PostMapping("/ismp/v2/product/unsubscribe")
    @ResponseBody
    public BaseResponse unsubscribeProduct(@RequestHeader(value = "auth") String auth, @RequestBody NewIsmpUnSubRequest request) {
        try {
            log.info("收到退订产品请求:" + JSON.toJSONString(request));
            SysClient sysClient = ismpOrderService.validRequestAuth(auth);
            if (!request.validRequestSign(sysClient.getSecret())) {
                throw new ServiceException("请求签名不正确");
            }
            return ismpOrderService.partnerUnSubscribeProduct(sysClient, request);
        } catch (ServiceException ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ex.getErrorCode(), ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BaseResponse(false, ResultCodeConstant.UNKNOWERR, ex.getMessage());
        }
    }
}
