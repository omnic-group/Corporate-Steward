package com.yepit.cs.rest;

import com.yepit.cs.common.BaseResponse;
import com.yepit.cs.constant.ResultCodeConstant;
import com.yepit.cs.dto.stat.StatByChannelRequest;
import com.yepit.cs.dto.stat.StatByChannelResponse;
import com.yepit.cs.dto.stat.StatByProductRequest;
import com.yepit.cs.dto.stat.StatByProductResponse;
import com.yepit.cs.service.StatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by qianlong on 2017/10/8.
 */
@RestController
public class StatController extends BaseController{

    @Autowired
    private StatService statService;

    @RequestMapping(value = "/stat/product", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<StatByProductResponse> searchStatResultByProduct(@RequestBody StatByProductRequest request) throws Exception{
        try{
            BaseResponse<StatByProductResponse> resp = statService.searchStatResultByProduct(request);
            return resp;
        }catch (Exception ex){
            return new BaseResponse<StatByProductResponse>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }

    @RequestMapping(value = "/stat/channel", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public BaseResponse<StatByChannelResponse> searchStatResultByChannel(@RequestBody StatByChannelRequest request) throws Exception{
        try{
            BaseResponse<StatByChannelResponse> resp = statService.searchStatResultByChannel(request);
            return resp;
        }catch (Exception ex){
            return new BaseResponse<StatByChannelResponse>(false, ResultCodeConstant.UNKNOWERR,ex.getMessage());
        }
    }
}
