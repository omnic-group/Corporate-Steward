/**
 * ClientObject.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.common;

public class ClientObject  implements java.io.Serializable {
    private java.lang.String ipAddress;

    private java.lang.String operator;

    private java.lang.String optFlag1;

    private java.lang.String optFlag2;

    private java.lang.String optFlag3;

    private int origin;

    private java.lang.String timeStamp;

    public ClientObject() {
    }

    public ClientObject(
           java.lang.String ipAddress,
           java.lang.String operator,
           java.lang.String optFlag1,
           java.lang.String optFlag2,
           java.lang.String optFlag3,
           int origin,
           java.lang.String timeStamp) {
           this.ipAddress = ipAddress;
           this.operator = operator;
           this.optFlag1 = optFlag1;
           this.optFlag2 = optFlag2;
           this.optFlag3 = optFlag3;
           this.origin = origin;
           this.timeStamp = timeStamp;
    }


    /**
     * Gets the ipAddress value for this ClientObject.
     * 
     * @return ipAddress
     */
    public java.lang.String getIpAddress() {
        return ipAddress;
    }


    /**
     * Sets the ipAddress value for this ClientObject.
     * 
     * @param ipAddress
     */
    public void setIpAddress(java.lang.String ipAddress) {
        this.ipAddress = ipAddress;
    }


    /**
     * Gets the operator value for this ClientObject.
     * 
     * @return operator
     */
    public java.lang.String getOperator() {
        return operator;
    }


    /**
     * Sets the operator value for this ClientObject.
     * 
     * @param operator
     */
    public void setOperator(java.lang.String operator) {
        this.operator = operator;
    }


    /**
     * Gets the optFlag1 value for this ClientObject.
     * 
     * @return optFlag1
     */
    public java.lang.String getOptFlag1() {
        return optFlag1;
    }


    /**
     * Sets the optFlag1 value for this ClientObject.
     * 
     * @param optFlag1
     */
    public void setOptFlag1(java.lang.String optFlag1) {
        this.optFlag1 = optFlag1;
    }


    /**
     * Gets the optFlag2 value for this ClientObject.
     * 
     * @return optFlag2
     */
    public java.lang.String getOptFlag2() {
        return optFlag2;
    }


    /**
     * Sets the optFlag2 value for this ClientObject.
     * 
     * @param optFlag2
     */
    public void setOptFlag2(java.lang.String optFlag2) {
        this.optFlag2 = optFlag2;
    }


    /**
     * Gets the optFlag3 value for this ClientObject.
     * 
     * @return optFlag3
     */
    public java.lang.String getOptFlag3() {
        return optFlag3;
    }


    /**
     * Sets the optFlag3 value for this ClientObject.
     * 
     * @param optFlag3
     */
    public void setOptFlag3(java.lang.String optFlag3) {
        this.optFlag3 = optFlag3;
    }


    /**
     * Gets the origin value for this ClientObject.
     * 
     * @return origin
     */
    public int getOrigin() {
        return origin;
    }


    /**
     * Sets the origin value for this ClientObject.
     * 
     * @param origin
     */
    public void setOrigin(int origin) {
        this.origin = origin;
    }


    /**
     * Gets the timeStamp value for this ClientObject.
     * 
     * @return timeStamp
     */
    public java.lang.String getTimeStamp() {
        return timeStamp;
    }


    /**
     * Sets the timeStamp value for this ClientObject.
     * 
     * @param timeStamp
     */
    public void setTimeStamp(java.lang.String timeStamp) {
        this.timeStamp = timeStamp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientObject)) return false;
        ClientObject other = (ClientObject) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ipAddress==null && other.getIpAddress()==null) || 
             (this.ipAddress!=null &&
              this.ipAddress.equals(other.getIpAddress()))) &&
            ((this.operator==null && other.getOperator()==null) || 
             (this.operator!=null &&
              this.operator.equals(other.getOperator()))) &&
            ((this.optFlag1==null && other.getOptFlag1()==null) || 
             (this.optFlag1!=null &&
              this.optFlag1.equals(other.getOptFlag1()))) &&
            ((this.optFlag2==null && other.getOptFlag2()==null) || 
             (this.optFlag2!=null &&
              this.optFlag2.equals(other.getOptFlag2()))) &&
            ((this.optFlag3==null && other.getOptFlag3()==null) || 
             (this.optFlag3!=null &&
              this.optFlag3.equals(other.getOptFlag3()))) &&
            this.origin == other.getOrigin() &&
            ((this.timeStamp==null && other.getTimeStamp()==null) || 
             (this.timeStamp!=null &&
              this.timeStamp.equals(other.getTimeStamp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIpAddress() != null) {
            _hashCode += getIpAddress().hashCode();
        }
        if (getOperator() != null) {
            _hashCode += getOperator().hashCode();
        }
        if (getOptFlag1() != null) {
            _hashCode += getOptFlag1().hashCode();
        }
        if (getOptFlag2() != null) {
            _hashCode += getOptFlag2().hashCode();
        }
        if (getOptFlag3() != null) {
            _hashCode += getOptFlag3().hashCode();
        }
        _hashCode += getOrigin();
        if (getTimeStamp() != null) {
            _hashCode += getTimeStamp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientObject.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ipAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ipAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optFlag1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "optFlag1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optFlag2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "optFlag2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optFlag3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "optFlag3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("origin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "origin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "timeStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
