/**
 * MusicSetsSubscribeInterfaceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.client.MusicSetsSubscribeInterface;

public interface MusicSetsSubscribeInterfaceService extends javax.xml.rpc.Service {
    public String getMusicSetsSubscribeInterfaceAddress();

    public MusicSetsSubscribeInterface getMusicSetsSubscribeInterface() throws javax.xml.rpc.ServiceException;

    public MusicSetsSubscribeInterface getMusicSetsSubscribeInterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
