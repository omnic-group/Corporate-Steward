/**
 * MusicSetsSubscribeInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.client.MusicSetsSubscribeInterface;

public interface MusicSetsSubscribeInterface extends java.rmi.Remote {
    public com.linkage.lccmp.webservice.encpdata.user.MusicSetsSubscribeInfo[] getMusicSetsSubscribe(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject addMusicSetsSubscribe(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo userInfo, String setsid, String opendate, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject delMusicSetsSubscribe(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo userInfo, String setsid, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
}
