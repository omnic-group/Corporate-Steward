/**
 * UserInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.client.UserInterface;

public interface UserInterface extends java.rmi.Remote {
    public com.linkage.lccmp.webservice.encpdata.user.UserInfo getUserInfo(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject wyOpenAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject pauseAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject resumeAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject openOCSUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject closeOCSUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject feeStop(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject changeNumber(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String newNumber, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject openAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject closeAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject modPassword(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String oPassword, String nPassword, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject authUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String password, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject modAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject open4GAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, String offerSpecId, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject exceptionOpenAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject wyCloseAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public int isCMPUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
    public com.linkage.lccmp.common.ResultObject modUserStatus(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String status, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException;
}
