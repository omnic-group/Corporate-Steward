/**
 * MusicSetsSubscribeInterfaceSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.client.MusicSetsSubscribeInterface;

public class MusicSetsSubscribeInterfaceSoapBindingStub extends org.apache.axis.client.Stub implements MusicSetsSubscribeInterface {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[3];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getMusicSetsSubscribe");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "ArrayOffUserDet"));
        oper.setReturnClass(com.linkage.lccmp.webservice.encpdata.user.MusicSetsSubscribeInfo[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "getMusicSetsSubscribeReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addMusicSetsSubscribe");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "setsid"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "opendate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "addMusicSetsSubscribeReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("delMusicSetsSubscribe");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "setsid"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "delMusicSetsSubscribeReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[2] = oper;

    }

    public MusicSetsSubscribeInterfaceSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public MusicSetsSubscribeInterfaceSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public MusicSetsSubscribeInterfaceSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject");
            cachedSerQNames.add(qName);
            cls = com.linkage.lccmp.common.ClientObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject");
            cachedSerQNames.add(qName);
            cls = com.linkage.lccmp.common.ResultObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "ArrayOffUserDet");
            cachedSerQNames.add(qName);
            cls = com.linkage.lccmp.webservice.encpdata.user.MusicSetsSubscribeInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "MusicSetsSubscribeInfo");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "MusicSetsSubscribeInfo");
            cachedSerQNames.add(qName);
            cls = com.linkage.lccmp.webservice.encpdata.user.MusicSetsSubscribeInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo");
            cachedSerQNames.add(qName);
            cls = com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
                    _call.setEncodingStyle(org.apache.axis.Constants.URI_SOAP11_ENC);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        Class cls = (Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            Class sf = (Class)
                                 cachedSerFactories.get(i);
                            Class df = (Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.linkage.lccmp.webservice.encpdata.user.MusicSetsSubscribeInfo[] getMusicSetsSubscribe(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/MusicSetsSubscribeInterface", "getMusicSetsSubscribe"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {userInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.webservice.encpdata.user.MusicSetsSubscribeInfo[]) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.webservice.encpdata.user.MusicSetsSubscribeInfo[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.webservice.encpdata.user.MusicSetsSubscribeInfo[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject addMusicSetsSubscribe(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo userInfo, String setsid, String opendate, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/MusicSetsSubscribeInterface", "addMusicSetsSubscribe"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {userInfo, setsid, opendate, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject delMusicSetsSubscribe(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo userInfo, String setsid, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/MusicSetsSubscribeInterface", "delMusicSetsSubscribe"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {userInfo, setsid, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
