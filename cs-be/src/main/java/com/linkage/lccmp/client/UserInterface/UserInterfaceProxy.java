package com.linkage.lccmp.client.UserInterface;

public class UserInterfaceProxy implements UserInterface {
  private String _endpoint = null;
  private UserInterface userInterface = null;
  
  public UserInterfaceProxy() {
    _initUserInterfaceProxy();
  }
  
  public UserInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initUserInterfaceProxy();
  }
  
  private void _initUserInterfaceProxy() {
    try {
      userInterface = (new UserInterfaceServiceLocator()).getUserInterface();
      if (userInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)userInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)userInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (userInterface != null)
      ((javax.xml.rpc.Stub)userInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public UserInterface getUserInterface() {
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface;
  }
  
  public com.linkage.lccmp.webservice.encpdata.user.UserInfo getUserInfo(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.getUserInfo(simpleUserInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject wyOpenAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.wyOpenAccount(userInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject pauseAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.pauseAccount(simpleUserInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject resumeAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.resumeAccount(simpleUserInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject openOCSUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.openOCSUser(simpleUserInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject closeOCSUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.closeOCSUser(simpleUserInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject feeStop(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.feeStop(simpleUserInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject changeNumber(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String newNumber, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.changeNumber(simpleUserInfo, newNumber, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject openAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.openAccount(userInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject closeAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.closeAccount(simpleUserInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject modPassword(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String oPassword, String nPassword, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.modPassword(simpleUserInfo, oPassword, nPassword, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject authUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String password, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.authUser(simpleUserInfo, password, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject modAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.modAccount(userInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject open4GAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, String offerSpecId, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.open4GAccount(userInfo, offerSpecId, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject exceptionOpenAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.exceptionOpenAccount(userInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject wyCloseAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.wyCloseAccount(simpleUserInfo, clientObject);
  }
  
  public int isCMPUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.isCMPUser(simpleUserInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject modUserStatus(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String status, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (userInterface == null)
      _initUserInterfaceProxy();
    return userInterface.modUserStatus(simpleUserInfo, status, clientObject);
  }
  
  
}