/**
 * UserInterfaceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.client.UserInterface;

public class UserInterfaceServiceLocator extends org.apache.axis.client.Service implements UserInterfaceService {

    public UserInterfaceServiceLocator() {
    }


    public UserInterfaceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public UserInterfaceServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for UserInterface
    private String UserInterface_address = "http://61.160.149.149:7001/services/UserInterface";

    public String getUserInterfaceAddress() {
        return UserInterface_address;
    }

    // The WSDD service name defaults to the port name.
    private String UserInterfaceWSDDServiceName = "UserInterface";

    public String getUserInterfaceWSDDServiceName() {
        return UserInterfaceWSDDServiceName;
    }

    public void setUserInterfaceWSDDServiceName(String name) {
        UserInterfaceWSDDServiceName = name;
    }

    public UserInterface getUserInterface() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(UserInterface_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getUserInterface(endpoint);
    }

    public UserInterface getUserInterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            UserInterfaceSoapBindingStub _stub = new UserInterfaceSoapBindingStub(portAddress, this);
            _stub.setPortName(getUserInterfaceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setUserInterfaceEndpointAddress(String address) {
        UserInterface_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (UserInterface.class.isAssignableFrom(serviceEndpointInterface)) {
                UserInterfaceSoapBindingStub _stub = new UserInterfaceSoapBindingStub(new java.net.URL(UserInterface_address), this);
                _stub.setPortName(getUserInterfaceWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("UserInterface".equals(inputPortName)) {
            return getUserInterface();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "UserInterfaceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "UserInterface"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {
        
if ("UserInterface".equals(portName)) {
            setUserInterfaceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
