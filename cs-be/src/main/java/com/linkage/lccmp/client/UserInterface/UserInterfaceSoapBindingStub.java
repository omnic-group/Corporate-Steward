/**
 * UserInterfaceSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.client.UserInterface;

public class UserInterfaceSoapBindingStub extends org.apache.axis.client.Stub implements UserInterface {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[18];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getUserInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "UserInfo"));
        oper.setReturnClass(com.linkage.lccmp.webservice.encpdata.user.UserInfo.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "getUserInfoReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("wyOpenAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "UserInfo"), com.linkage.lccmp.webservice.encpdata.user.UserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "wyOpenAccountReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("pauseAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "pauseAccountReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("resumeAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "resumeAccountReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("openOCSUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "openOCSUserReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("closeOCSUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "closeOCSUserReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("feeStop");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "feeStopReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("changeNumber");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "newNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "changeNumberReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("openAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "UserInfo"), com.linkage.lccmp.webservice.encpdata.user.UserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "openAccountReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("closeAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "closeAccountReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("modPassword");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "oPassword"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "nPassword"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "modPasswordReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("authUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "authUserReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("modAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "UserInfo"), com.linkage.lccmp.webservice.encpdata.user.UserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "modAccountReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("open4GAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "UserInfo"), com.linkage.lccmp.webservice.encpdata.user.UserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "offerSpecId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "open4GAccountReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("exceptionOpenAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "UserInfo"), com.linkage.lccmp.webservice.encpdata.user.UserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "exceptionOpenAccountReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("wyCloseAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "wyCloseAccountReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("isCMPUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        oper.setReturnClass(int.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "isCMPUserReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("modUserStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "simpleUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"), com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "status"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "clientObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject"), com.linkage.lccmp.common.ClientObject.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject"));
        oper.setReturnClass(com.linkage.lccmp.common.ResultObject.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "modUserStatusReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[17] = oper;

    }

    public UserInterfaceSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public UserInterfaceSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public UserInterfaceSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ClientObject");
            cachedSerQNames.add(qName);
            cls = com.linkage.lccmp.common.ClientObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://common.lccmp.linkage.com", "ResultObject");
            cachedSerQNames.add(qName);
            cls = com.linkage.lccmp.common.ResultObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo");
            cachedSerQNames.add(qName);
            cls = com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "UserInfo");
            cachedSerQNames.add(qName);
            cls = com.linkage.lccmp.webservice.encpdata.user.UserInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
                    _call.setEncodingStyle(org.apache.axis.Constants.URI_SOAP11_ENC);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        Class cls = (Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            Class sf = (Class)
                                 cachedSerFactories.get(i);
                            Class df = (Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.linkage.lccmp.webservice.encpdata.user.UserInfo getUserInfo(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "getUserInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.webservice.encpdata.user.UserInfo) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.webservice.encpdata.user.UserInfo) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.webservice.encpdata.user.UserInfo.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject wyOpenAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "wyOpenAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {userInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject pauseAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "pauseAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject resumeAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "resumeAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject openOCSUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "openOCSUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject closeOCSUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "closeOCSUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject feeStop(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "feeStop"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject changeNumber(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String newNumber, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "changeNumber"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, newNumber, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject openAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "openAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {userInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject closeAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "closeAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject modPassword(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String oPassword, String nPassword, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "modPassword"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, oPassword, nPassword, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject authUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String password, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "authUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, password, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject modAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "modAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {userInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject open4GAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, String offerSpecId, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "open4GAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {userInfo, offerSpecId, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject exceptionOpenAccount(com.linkage.lccmp.webservice.encpdata.user.UserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "exceptionOpenAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {userInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject wyCloseAccount(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "wyCloseAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public int isCMPUser(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "isCMPUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((Integer) _resp).intValue();
            } catch (Exception _exception) {
                return ((Integer) org.apache.axis.utils.JavaUtils.convert(_resp, int.class)).intValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.linkage.lccmp.common.ResultObject modUserStatus(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo simpleUserInfo, String status, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/UserInterface", "modUserStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {simpleUserInfo, status, clientObject});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.linkage.lccmp.common.ResultObject) _resp;
            } catch (Exception _exception) {
                return (com.linkage.lccmp.common.ResultObject) org.apache.axis.utils.JavaUtils.convert(_resp, com.linkage.lccmp.common.ResultObject.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
