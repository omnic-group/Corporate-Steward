/**
 * MusicSetsSubscribeInterfaceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.client.MusicSetsSubscribeInterface;

public class MusicSetsSubscribeInterfaceServiceLocator extends org.apache.axis.client.Service implements MusicSetsSubscribeInterfaceService {

    public MusicSetsSubscribeInterfaceServiceLocator() {
    }


    public MusicSetsSubscribeInterfaceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MusicSetsSubscribeInterfaceServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MusicSetsSubscribeInterface
    private String MusicSetsSubscribeInterface_address = "http://61.160.149.149:7001/services/MusicSetsSubscribeInterface";

    public String getMusicSetsSubscribeInterfaceAddress() {
        return MusicSetsSubscribeInterface_address;
    }

    // The WSDD service name defaults to the port name.
    private String MusicSetsSubscribeInterfaceWSDDServiceName = "MusicSetsSubscribeInterface";

    public String getMusicSetsSubscribeInterfaceWSDDServiceName() {
        return MusicSetsSubscribeInterfaceWSDDServiceName;
    }

    public void setMusicSetsSubscribeInterfaceWSDDServiceName(String name) {
        MusicSetsSubscribeInterfaceWSDDServiceName = name;
    }

    public MusicSetsSubscribeInterface getMusicSetsSubscribeInterface() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MusicSetsSubscribeInterface_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMusicSetsSubscribeInterface(endpoint);
    }

    public MusicSetsSubscribeInterface getMusicSetsSubscribeInterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            MusicSetsSubscribeInterfaceSoapBindingStub _stub = new MusicSetsSubscribeInterfaceSoapBindingStub(portAddress, this);
            _stub.setPortName(getMusicSetsSubscribeInterfaceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMusicSetsSubscribeInterfaceEndpointAddress(String address) {
        MusicSetsSubscribeInterface_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (MusicSetsSubscribeInterface.class.isAssignableFrom(serviceEndpointInterface)) {
                MusicSetsSubscribeInterfaceSoapBindingStub _stub = new MusicSetsSubscribeInterfaceSoapBindingStub(new java.net.URL(MusicSetsSubscribeInterface_address), this);
                _stub.setPortName(getMusicSetsSubscribeInterfaceWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("MusicSetsSubscribeInterface".equals(inputPortName)) {
            return getMusicSetsSubscribeInterface();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://61.160.149.149:7001/services/MusicSetsSubscribeInterface", "MusicSetsSubscribeInterfaceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://61.160.149.149:7001/services/MusicSetsSubscribeInterface", "MusicSetsSubscribeInterface"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {
        
if ("MusicSetsSubscribeInterface".equals(portName)) {
            setMusicSetsSubscribeInterfaceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
