/**
 * UserInterfaceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.client.UserInterface;

public interface UserInterfaceService extends javax.xml.rpc.Service {
    public String getUserInterfaceAddress();

    public UserInterface getUserInterface() throws javax.xml.rpc.ServiceException;

    public UserInterface getUserInterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
