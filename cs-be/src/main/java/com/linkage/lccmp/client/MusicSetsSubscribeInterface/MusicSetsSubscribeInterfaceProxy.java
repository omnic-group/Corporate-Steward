package com.linkage.lccmp.client.MusicSetsSubscribeInterface;

public class MusicSetsSubscribeInterfaceProxy implements MusicSetsSubscribeInterface {
  private String _endpoint = null;
  private MusicSetsSubscribeInterface musicSetsSubscribeInterface = null;
  
  public MusicSetsSubscribeInterfaceProxy() {
    _initMusicSetsSubscribeInterfaceProxy();
  }
  
  public MusicSetsSubscribeInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initMusicSetsSubscribeInterfaceProxy();
  }
  
  private void _initMusicSetsSubscribeInterfaceProxy() {
    try {
      musicSetsSubscribeInterface = (new MusicSetsSubscribeInterfaceServiceLocator()).getMusicSetsSubscribeInterface();
      if (musicSetsSubscribeInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)musicSetsSubscribeInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)musicSetsSubscribeInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (musicSetsSubscribeInterface != null)
      ((javax.xml.rpc.Stub)musicSetsSubscribeInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public MusicSetsSubscribeInterface getMusicSetsSubscribeInterface() {
    if (musicSetsSubscribeInterface == null)
      _initMusicSetsSubscribeInterfaceProxy();
    return musicSetsSubscribeInterface;
  }
  
  public com.linkage.lccmp.webservice.encpdata.user.MusicSetsSubscribeInfo[] getMusicSetsSubscribe(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo userInfo, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (musicSetsSubscribeInterface == null)
      _initMusicSetsSubscribeInterfaceProxy();
    return musicSetsSubscribeInterface.getMusicSetsSubscribe(userInfo, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject addMusicSetsSubscribe(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo userInfo, String setsid, String opendate, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (musicSetsSubscribeInterface == null)
      _initMusicSetsSubscribeInterfaceProxy();
    return musicSetsSubscribeInterface.addMusicSetsSubscribe(userInfo, setsid, opendate, clientObject);
  }
  
  public com.linkage.lccmp.common.ResultObject delMusicSetsSubscribe(com.linkage.lccmp.webservice.encpdata.user.SimpleUserInfo userInfo, String setsid, com.linkage.lccmp.common.ClientObject clientObject) throws java.rmi.RemoteException{
    if (musicSetsSubscribeInterface == null)
      _initMusicSetsSubscribeInterfaceProxy();
    return musicSetsSubscribeInterface.delMusicSetsSubscribe(userInfo, setsid, clientObject);
  }
  
  
}