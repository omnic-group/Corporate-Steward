/**
 * SimpleUserInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.webservice.encpdata.user;

public class SimpleUserInfo  implements java.io.Serializable {
    private java.lang.String callusertype;

    private java.lang.String phonenumber;

    public SimpleUserInfo() {
    }

    public SimpleUserInfo(
           java.lang.String callusertype,
           java.lang.String phonenumber) {
           this.callusertype = callusertype;
           this.phonenumber = phonenumber;
    }


    /**
     * Gets the callusertype value for this SimpleUserInfo.
     * 
     * @return callusertype
     */
    public java.lang.String getCallusertype() {
        return callusertype;
    }


    /**
     * Sets the callusertype value for this SimpleUserInfo.
     * 
     * @param callusertype
     */
    public void setCallusertype(java.lang.String callusertype) {
        this.callusertype = callusertype;
    }


    /**
     * Gets the phonenumber value for this SimpleUserInfo.
     * 
     * @return phonenumber
     */
    public java.lang.String getPhonenumber() {
        return phonenumber;
    }


    /**
     * Sets the phonenumber value for this SimpleUserInfo.
     * 
     * @param phonenumber
     */
    public void setPhonenumber(java.lang.String phonenumber) {
        this.phonenumber = phonenumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SimpleUserInfo)) return false;
        SimpleUserInfo other = (SimpleUserInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.callusertype==null && other.getCallusertype()==null) || 
             (this.callusertype!=null &&
              this.callusertype.equals(other.getCallusertype()))) &&
            ((this.phonenumber==null && other.getPhonenumber()==null) || 
             (this.phonenumber!=null &&
              this.phonenumber.equals(other.getPhonenumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCallusertype() != null) {
            _hashCode += getCallusertype().hashCode();
        }
        if (getPhonenumber() != null) {
            _hashCode += getPhonenumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SimpleUserInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "SimpleUserInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("callusertype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "callusertype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phonenumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "phonenumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
