/**
 * MusicSetsSubscribeInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.webservice.encpdata.user;

public class MusicSetsSubscribeInfo  implements java.io.Serializable {
    private java.lang.String areano;

    private java.lang.String closedate;

    private java.lang.String downid;

    private java.lang.String downmethod;

    private java.lang.String downtype;

    private java.lang.String effectdate;

    private java.lang.String fee;

    private java.lang.String feetype;

    private java.lang.String lastpaytime;

    private java.lang.String modoperator;

    private java.lang.String modorigin;

    private java.lang.String opendate;

    private java.lang.String openoperator;

    private java.lang.String openorigin;

    private java.lang.String paytype;

    private java.lang.String phonenumber;

    private java.lang.String setsid;

    private java.lang.String spid;

    private java.lang.String status;

    private java.lang.String statustime;

    private java.lang.String subscribemonth;

    private java.lang.String terminatortype;

    private java.lang.String userinfoid;

    private java.lang.String usertype;

    private java.lang.String validdate;

    public MusicSetsSubscribeInfo() {
    }

    public MusicSetsSubscribeInfo(
           java.lang.String areano,
           java.lang.String closedate,
           java.lang.String downid,
           java.lang.String downmethod,
           java.lang.String downtype,
           java.lang.String effectdate,
           java.lang.String fee,
           java.lang.String feetype,
           java.lang.String lastpaytime,
           java.lang.String modoperator,
           java.lang.String modorigin,
           java.lang.String opendate,
           java.lang.String openoperator,
           java.lang.String openorigin,
           java.lang.String paytype,
           java.lang.String phonenumber,
           java.lang.String setsid,
           java.lang.String spid,
           java.lang.String status,
           java.lang.String statustime,
           java.lang.String subscribemonth,
           java.lang.String terminatortype,
           java.lang.String userinfoid,
           java.lang.String usertype,
           java.lang.String validdate) {
           this.areano = areano;
           this.closedate = closedate;
           this.downid = downid;
           this.downmethod = downmethod;
           this.downtype = downtype;
           this.effectdate = effectdate;
           this.fee = fee;
           this.feetype = feetype;
           this.lastpaytime = lastpaytime;
           this.modoperator = modoperator;
           this.modorigin = modorigin;
           this.opendate = opendate;
           this.openoperator = openoperator;
           this.openorigin = openorigin;
           this.paytype = paytype;
           this.phonenumber = phonenumber;
           this.setsid = setsid;
           this.spid = spid;
           this.status = status;
           this.statustime = statustime;
           this.subscribemonth = subscribemonth;
           this.terminatortype = terminatortype;
           this.userinfoid = userinfoid;
           this.usertype = usertype;
           this.validdate = validdate;
    }


    /**
     * Gets the areano value for this MusicSetsSubscribeInfo.
     * 
     * @return areano
     */
    public java.lang.String getAreano() {
        return areano;
    }


    /**
     * Sets the areano value for this MusicSetsSubscribeInfo.
     * 
     * @param areano
     */
    public void setAreano(java.lang.String areano) {
        this.areano = areano;
    }


    /**
     * Gets the closedate value for this MusicSetsSubscribeInfo.
     * 
     * @return closedate
     */
    public java.lang.String getClosedate() {
        return closedate;
    }


    /**
     * Sets the closedate value for this MusicSetsSubscribeInfo.
     * 
     * @param closedate
     */
    public void setClosedate(java.lang.String closedate) {
        this.closedate = closedate;
    }


    /**
     * Gets the downid value for this MusicSetsSubscribeInfo.
     * 
     * @return downid
     */
    public java.lang.String getDownid() {
        return downid;
    }


    /**
     * Sets the downid value for this MusicSetsSubscribeInfo.
     * 
     * @param downid
     */
    public void setDownid(java.lang.String downid) {
        this.downid = downid;
    }


    /**
     * Gets the downmethod value for this MusicSetsSubscribeInfo.
     * 
     * @return downmethod
     */
    public java.lang.String getDownmethod() {
        return downmethod;
    }


    /**
     * Sets the downmethod value for this MusicSetsSubscribeInfo.
     * 
     * @param downmethod
     */
    public void setDownmethod(java.lang.String downmethod) {
        this.downmethod = downmethod;
    }


    /**
     * Gets the downtype value for this MusicSetsSubscribeInfo.
     * 
     * @return downtype
     */
    public java.lang.String getDowntype() {
        return downtype;
    }


    /**
     * Sets the downtype value for this MusicSetsSubscribeInfo.
     * 
     * @param downtype
     */
    public void setDowntype(java.lang.String downtype) {
        this.downtype = downtype;
    }


    /**
     * Gets the effectdate value for this MusicSetsSubscribeInfo.
     * 
     * @return effectdate
     */
    public java.lang.String getEffectdate() {
        return effectdate;
    }


    /**
     * Sets the effectdate value for this MusicSetsSubscribeInfo.
     * 
     * @param effectdate
     */
    public void setEffectdate(java.lang.String effectdate) {
        this.effectdate = effectdate;
    }


    /**
     * Gets the fee value for this MusicSetsSubscribeInfo.
     * 
     * @return fee
     */
    public java.lang.String getFee() {
        return fee;
    }


    /**
     * Sets the fee value for this MusicSetsSubscribeInfo.
     * 
     * @param fee
     */
    public void setFee(java.lang.String fee) {
        this.fee = fee;
    }


    /**
     * Gets the feetype value for this MusicSetsSubscribeInfo.
     * 
     * @return feetype
     */
    public java.lang.String getFeetype() {
        return feetype;
    }


    /**
     * Sets the feetype value for this MusicSetsSubscribeInfo.
     * 
     * @param feetype
     */
    public void setFeetype(java.lang.String feetype) {
        this.feetype = feetype;
    }


    /**
     * Gets the lastpaytime value for this MusicSetsSubscribeInfo.
     * 
     * @return lastpaytime
     */
    public java.lang.String getLastpaytime() {
        return lastpaytime;
    }


    /**
     * Sets the lastpaytime value for this MusicSetsSubscribeInfo.
     * 
     * @param lastpaytime
     */
    public void setLastpaytime(java.lang.String lastpaytime) {
        this.lastpaytime = lastpaytime;
    }


    /**
     * Gets the modoperator value for this MusicSetsSubscribeInfo.
     * 
     * @return modoperator
     */
    public java.lang.String getModoperator() {
        return modoperator;
    }


    /**
     * Sets the modoperator value for this MusicSetsSubscribeInfo.
     * 
     * @param modoperator
     */
    public void setModoperator(java.lang.String modoperator) {
        this.modoperator = modoperator;
    }


    /**
     * Gets the modorigin value for this MusicSetsSubscribeInfo.
     * 
     * @return modorigin
     */
    public java.lang.String getModorigin() {
        return modorigin;
    }


    /**
     * Sets the modorigin value for this MusicSetsSubscribeInfo.
     * 
     * @param modorigin
     */
    public void setModorigin(java.lang.String modorigin) {
        this.modorigin = modorigin;
    }


    /**
     * Gets the opendate value for this MusicSetsSubscribeInfo.
     * 
     * @return opendate
     */
    public java.lang.String getOpendate() {
        return opendate;
    }


    /**
     * Sets the opendate value for this MusicSetsSubscribeInfo.
     * 
     * @param opendate
     */
    public void setOpendate(java.lang.String opendate) {
        this.opendate = opendate;
    }


    /**
     * Gets the openoperator value for this MusicSetsSubscribeInfo.
     * 
     * @return openoperator
     */
    public java.lang.String getOpenoperator() {
        return openoperator;
    }


    /**
     * Sets the openoperator value for this MusicSetsSubscribeInfo.
     * 
     * @param openoperator
     */
    public void setOpenoperator(java.lang.String openoperator) {
        this.openoperator = openoperator;
    }


    /**
     * Gets the openorigin value for this MusicSetsSubscribeInfo.
     * 
     * @return openorigin
     */
    public java.lang.String getOpenorigin() {
        return openorigin;
    }


    /**
     * Sets the openorigin value for this MusicSetsSubscribeInfo.
     * 
     * @param openorigin
     */
    public void setOpenorigin(java.lang.String openorigin) {
        this.openorigin = openorigin;
    }


    /**
     * Gets the paytype value for this MusicSetsSubscribeInfo.
     * 
     * @return paytype
     */
    public java.lang.String getPaytype() {
        return paytype;
    }


    /**
     * Sets the paytype value for this MusicSetsSubscribeInfo.
     * 
     * @param paytype
     */
    public void setPaytype(java.lang.String paytype) {
        this.paytype = paytype;
    }


    /**
     * Gets the phonenumber value for this MusicSetsSubscribeInfo.
     * 
     * @return phonenumber
     */
    public java.lang.String getPhonenumber() {
        return phonenumber;
    }


    /**
     * Sets the phonenumber value for this MusicSetsSubscribeInfo.
     * 
     * @param phonenumber
     */
    public void setPhonenumber(java.lang.String phonenumber) {
        this.phonenumber = phonenumber;
    }


    /**
     * Gets the setsid value for this MusicSetsSubscribeInfo.
     * 
     * @return setsid
     */
    public java.lang.String getSetsid() {
        return setsid;
    }


    /**
     * Sets the setsid value for this MusicSetsSubscribeInfo.
     * 
     * @param setsid
     */
    public void setSetsid(java.lang.String setsid) {
        this.setsid = setsid;
    }


    /**
     * Gets the spid value for this MusicSetsSubscribeInfo.
     * 
     * @return spid
     */
    public java.lang.String getSpid() {
        return spid;
    }


    /**
     * Sets the spid value for this MusicSetsSubscribeInfo.
     * 
     * @param spid
     */
    public void setSpid(java.lang.String spid) {
        this.spid = spid;
    }


    /**
     * Gets the status value for this MusicSetsSubscribeInfo.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this MusicSetsSubscribeInfo.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the statustime value for this MusicSetsSubscribeInfo.
     * 
     * @return statustime
     */
    public java.lang.String getStatustime() {
        return statustime;
    }


    /**
     * Sets the statustime value for this MusicSetsSubscribeInfo.
     * 
     * @param statustime
     */
    public void setStatustime(java.lang.String statustime) {
        this.statustime = statustime;
    }


    /**
     * Gets the subscribemonth value for this MusicSetsSubscribeInfo.
     * 
     * @return subscribemonth
     */
    public java.lang.String getSubscribemonth() {
        return subscribemonth;
    }


    /**
     * Sets the subscribemonth value for this MusicSetsSubscribeInfo.
     * 
     * @param subscribemonth
     */
    public void setSubscribemonth(java.lang.String subscribemonth) {
        this.subscribemonth = subscribemonth;
    }


    /**
     * Gets the terminatortype value for this MusicSetsSubscribeInfo.
     * 
     * @return terminatortype
     */
    public java.lang.String getTerminatortype() {
        return terminatortype;
    }


    /**
     * Sets the terminatortype value for this MusicSetsSubscribeInfo.
     * 
     * @param terminatortype
     */
    public void setTerminatortype(java.lang.String terminatortype) {
        this.terminatortype = terminatortype;
    }


    /**
     * Gets the userinfoid value for this MusicSetsSubscribeInfo.
     * 
     * @return userinfoid
     */
    public java.lang.String getUserinfoid() {
        return userinfoid;
    }


    /**
     * Sets the userinfoid value for this MusicSetsSubscribeInfo.
     * 
     * @param userinfoid
     */
    public void setUserinfoid(java.lang.String userinfoid) {
        this.userinfoid = userinfoid;
    }


    /**
     * Gets the usertype value for this MusicSetsSubscribeInfo.
     * 
     * @return usertype
     */
    public java.lang.String getUsertype() {
        return usertype;
    }


    /**
     * Sets the usertype value for this MusicSetsSubscribeInfo.
     * 
     * @param usertype
     */
    public void setUsertype(java.lang.String usertype) {
        this.usertype = usertype;
    }


    /**
     * Gets the validdate value for this MusicSetsSubscribeInfo.
     * 
     * @return validdate
     */
    public java.lang.String getValiddate() {
        return validdate;
    }


    /**
     * Sets the validdate value for this MusicSetsSubscribeInfo.
     * 
     * @param validdate
     */
    public void setValiddate(java.lang.String validdate) {
        this.validdate = validdate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MusicSetsSubscribeInfo)) return false;
        MusicSetsSubscribeInfo other = (MusicSetsSubscribeInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.areano==null && other.getAreano()==null) || 
             (this.areano!=null &&
              this.areano.equals(other.getAreano()))) &&
            ((this.closedate==null && other.getClosedate()==null) || 
             (this.closedate!=null &&
              this.closedate.equals(other.getClosedate()))) &&
            ((this.downid==null && other.getDownid()==null) || 
             (this.downid!=null &&
              this.downid.equals(other.getDownid()))) &&
            ((this.downmethod==null && other.getDownmethod()==null) || 
             (this.downmethod!=null &&
              this.downmethod.equals(other.getDownmethod()))) &&
            ((this.downtype==null && other.getDowntype()==null) || 
             (this.downtype!=null &&
              this.downtype.equals(other.getDowntype()))) &&
            ((this.effectdate==null && other.getEffectdate()==null) || 
             (this.effectdate!=null &&
              this.effectdate.equals(other.getEffectdate()))) &&
            ((this.fee==null && other.getFee()==null) || 
             (this.fee!=null &&
              this.fee.equals(other.getFee()))) &&
            ((this.feetype==null && other.getFeetype()==null) || 
             (this.feetype!=null &&
              this.feetype.equals(other.getFeetype()))) &&
            ((this.lastpaytime==null && other.getLastpaytime()==null) || 
             (this.lastpaytime!=null &&
              this.lastpaytime.equals(other.getLastpaytime()))) &&
            ((this.modoperator==null && other.getModoperator()==null) || 
             (this.modoperator!=null &&
              this.modoperator.equals(other.getModoperator()))) &&
            ((this.modorigin==null && other.getModorigin()==null) || 
             (this.modorigin!=null &&
              this.modorigin.equals(other.getModorigin()))) &&
            ((this.opendate==null && other.getOpendate()==null) || 
             (this.opendate!=null &&
              this.opendate.equals(other.getOpendate()))) &&
            ((this.openoperator==null && other.getOpenoperator()==null) || 
             (this.openoperator!=null &&
              this.openoperator.equals(other.getOpenoperator()))) &&
            ((this.openorigin==null && other.getOpenorigin()==null) || 
             (this.openorigin!=null &&
              this.openorigin.equals(other.getOpenorigin()))) &&
            ((this.paytype==null && other.getPaytype()==null) || 
             (this.paytype!=null &&
              this.paytype.equals(other.getPaytype()))) &&
            ((this.phonenumber==null && other.getPhonenumber()==null) || 
             (this.phonenumber!=null &&
              this.phonenumber.equals(other.getPhonenumber()))) &&
            ((this.setsid==null && other.getSetsid()==null) || 
             (this.setsid!=null &&
              this.setsid.equals(other.getSetsid()))) &&
            ((this.spid==null && other.getSpid()==null) || 
             (this.spid!=null &&
              this.spid.equals(other.getSpid()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.statustime==null && other.getStatustime()==null) || 
             (this.statustime!=null &&
              this.statustime.equals(other.getStatustime()))) &&
            ((this.subscribemonth==null && other.getSubscribemonth()==null) || 
             (this.subscribemonth!=null &&
              this.subscribemonth.equals(other.getSubscribemonth()))) &&
            ((this.terminatortype==null && other.getTerminatortype()==null) || 
             (this.terminatortype!=null &&
              this.terminatortype.equals(other.getTerminatortype()))) &&
            ((this.userinfoid==null && other.getUserinfoid()==null) || 
             (this.userinfoid!=null &&
              this.userinfoid.equals(other.getUserinfoid()))) &&
            ((this.usertype==null && other.getUsertype()==null) || 
             (this.usertype!=null &&
              this.usertype.equals(other.getUsertype()))) &&
            ((this.validdate==null && other.getValiddate()==null) || 
             (this.validdate!=null &&
              this.validdate.equals(other.getValiddate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAreano() != null) {
            _hashCode += getAreano().hashCode();
        }
        if (getClosedate() != null) {
            _hashCode += getClosedate().hashCode();
        }
        if (getDownid() != null) {
            _hashCode += getDownid().hashCode();
        }
        if (getDownmethod() != null) {
            _hashCode += getDownmethod().hashCode();
        }
        if (getDowntype() != null) {
            _hashCode += getDowntype().hashCode();
        }
        if (getEffectdate() != null) {
            _hashCode += getEffectdate().hashCode();
        }
        if (getFee() != null) {
            _hashCode += getFee().hashCode();
        }
        if (getFeetype() != null) {
            _hashCode += getFeetype().hashCode();
        }
        if (getLastpaytime() != null) {
            _hashCode += getLastpaytime().hashCode();
        }
        if (getModoperator() != null) {
            _hashCode += getModoperator().hashCode();
        }
        if (getModorigin() != null) {
            _hashCode += getModorigin().hashCode();
        }
        if (getOpendate() != null) {
            _hashCode += getOpendate().hashCode();
        }
        if (getOpenoperator() != null) {
            _hashCode += getOpenoperator().hashCode();
        }
        if (getOpenorigin() != null) {
            _hashCode += getOpenorigin().hashCode();
        }
        if (getPaytype() != null) {
            _hashCode += getPaytype().hashCode();
        }
        if (getPhonenumber() != null) {
            _hashCode += getPhonenumber().hashCode();
        }
        if (getSetsid() != null) {
            _hashCode += getSetsid().hashCode();
        }
        if (getSpid() != null) {
            _hashCode += getSpid().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getStatustime() != null) {
            _hashCode += getStatustime().hashCode();
        }
        if (getSubscribemonth() != null) {
            _hashCode += getSubscribemonth().hashCode();
        }
        if (getTerminatortype() != null) {
            _hashCode += getTerminatortype().hashCode();
        }
        if (getUserinfoid() != null) {
            _hashCode += getUserinfoid().hashCode();
        }
        if (getUsertype() != null) {
            _hashCode += getUsertype().hashCode();
        }
        if (getValiddate() != null) {
            _hashCode += getValiddate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MusicSetsSubscribeInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "MusicSetsSubscribeInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("areano");
        elemField.setXmlName(new javax.xml.namespace.QName("", "areano"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("closedate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "closedate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("downid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "downid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("downmethod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "downmethod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("downtype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "downtype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("effectdate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "effectdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fee");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feetype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "feetype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastpaytime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastpaytime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modoperator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modoperator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modorigin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modorigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opendate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "opendate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("openoperator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "openoperator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("openorigin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "openorigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paytype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paytype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phonenumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "phonenumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("setsid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "setsid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("spid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "spid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statustime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statustime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscribemonth");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscribemonth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminatortype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "terminatortype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userinfoid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userinfoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usertype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usertype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validdate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "validdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
