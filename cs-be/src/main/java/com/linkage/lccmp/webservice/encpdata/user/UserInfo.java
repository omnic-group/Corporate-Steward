/**
 * UserInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.linkage.lccmp.webservice.encpdata.user;

public class UserInfo  implements java.io.Serializable {
    private String areano;

    private String callusertype;

    private String closedate;

    private String corpinfoid;

    private String deptinfoid;

    private String moddate;

    private String modoperator;

    private String modorigin;

    private String opendate;

    private String openoperator;

    private String openorigin;

    private String pausedate;

    private String paytype;

    private String phonenumber;

    private String reserved1;

    private String reserved2;

    private String status;

    private String terminatortype;

    private String userinfoid;

    private String usertype;

    public UserInfo() {
    }

    public UserInfo(
           String areano,
           String callusertype,
           String closedate,
           String corpinfoid,
           String deptinfoid,
           String moddate,
           String modoperator,
           String modorigin,
           String opendate,
           String openoperator,
           String openorigin,
           String pausedate,
           String paytype,
           String phonenumber,
           String reserved1,
           String reserved2,
           String status,
           String terminatortype,
           String userinfoid,
           String usertype) {
           this.areano = areano;
           this.callusertype = callusertype;
           this.closedate = closedate;
           this.corpinfoid = corpinfoid;
           this.deptinfoid = deptinfoid;
           this.moddate = moddate;
           this.modoperator = modoperator;
           this.modorigin = modorigin;
           this.opendate = opendate;
           this.openoperator = openoperator;
           this.openorigin = openorigin;
           this.pausedate = pausedate;
           this.paytype = paytype;
           this.phonenumber = phonenumber;
           this.reserved1 = reserved1;
           this.reserved2 = reserved2;
           this.status = status;
           this.terminatortype = terminatortype;
           this.userinfoid = userinfoid;
           this.usertype = usertype;
    }


    /**
     * Gets the areano value for this UserInfo.
     * 
     * @return areano
     */
    public String getAreano() {
        return areano;
    }


    /**
     * Sets the areano value for this UserInfo.
     * 
     * @param areano
     */
    public void setAreano(String areano) {
        this.areano = areano;
    }


    /**
     * Gets the callusertype value for this UserInfo.
     * 
     * @return callusertype
     */
    public String getCallusertype() {
        return callusertype;
    }


    /**
     * Sets the callusertype value for this UserInfo.
     * 
     * @param callusertype
     */
    public void setCallusertype(String callusertype) {
        this.callusertype = callusertype;
    }


    /**
     * Gets the closedate value for this UserInfo.
     * 
     * @return closedate
     */
    public String getClosedate() {
        return closedate;
    }


    /**
     * Sets the closedate value for this UserInfo.
     * 
     * @param closedate
     */
    public void setClosedate(String closedate) {
        this.closedate = closedate;
    }


    /**
     * Gets the corpinfoid value for this UserInfo.
     * 
     * @return corpinfoid
     */
    public String getCorpinfoid() {
        return corpinfoid;
    }


    /**
     * Sets the corpinfoid value for this UserInfo.
     * 
     * @param corpinfoid
     */
    public void setCorpinfoid(String corpinfoid) {
        this.corpinfoid = corpinfoid;
    }


    /**
     * Gets the deptinfoid value for this UserInfo.
     * 
     * @return deptinfoid
     */
    public String getDeptinfoid() {
        return deptinfoid;
    }


    /**
     * Sets the deptinfoid value for this UserInfo.
     * 
     * @param deptinfoid
     */
    public void setDeptinfoid(String deptinfoid) {
        this.deptinfoid = deptinfoid;
    }


    /**
     * Gets the moddate value for this UserInfo.
     * 
     * @return moddate
     */
    public String getModdate() {
        return moddate;
    }


    /**
     * Sets the moddate value for this UserInfo.
     * 
     * @param moddate
     */
    public void setModdate(String moddate) {
        this.moddate = moddate;
    }


    /**
     * Gets the modoperator value for this UserInfo.
     * 
     * @return modoperator
     */
    public String getModoperator() {
        return modoperator;
    }


    /**
     * Sets the modoperator value for this UserInfo.
     * 
     * @param modoperator
     */
    public void setModoperator(String modoperator) {
        this.modoperator = modoperator;
    }


    /**
     * Gets the modorigin value for this UserInfo.
     * 
     * @return modorigin
     */
    public String getModorigin() {
        return modorigin;
    }


    /**
     * Sets the modorigin value for this UserInfo.
     * 
     * @param modorigin
     */
    public void setModorigin(String modorigin) {
        this.modorigin = modorigin;
    }


    /**
     * Gets the opendate value for this UserInfo.
     * 
     * @return opendate
     */
    public String getOpendate() {
        return opendate;
    }


    /**
     * Sets the opendate value for this UserInfo.
     * 
     * @param opendate
     */
    public void setOpendate(String opendate) {
        this.opendate = opendate;
    }


    /**
     * Gets the openoperator value for this UserInfo.
     * 
     * @return openoperator
     */
    public String getOpenoperator() {
        return openoperator;
    }


    /**
     * Sets the openoperator value for this UserInfo.
     * 
     * @param openoperator
     */
    public void setOpenoperator(String openoperator) {
        this.openoperator = openoperator;
    }


    /**
     * Gets the openorigin value for this UserInfo.
     * 
     * @return openorigin
     */
    public String getOpenorigin() {
        return openorigin;
    }


    /**
     * Sets the openorigin value for this UserInfo.
     * 
     * @param openorigin
     */
    public void setOpenorigin(String openorigin) {
        this.openorigin = openorigin;
    }


    /**
     * Gets the pausedate value for this UserInfo.
     * 
     * @return pausedate
     */
    public String getPausedate() {
        return pausedate;
    }


    /**
     * Sets the pausedate value for this UserInfo.
     * 
     * @param pausedate
     */
    public void setPausedate(String pausedate) {
        this.pausedate = pausedate;
    }


    /**
     * Gets the paytype value for this UserInfo.
     * 
     * @return paytype
     */
    public String getPaytype() {
        return paytype;
    }


    /**
     * Sets the paytype value for this UserInfo.
     * 
     * @param paytype
     */
    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }


    /**
     * Gets the phonenumber value for this UserInfo.
     * 
     * @return phonenumber
     */
    public String getPhonenumber() {
        return phonenumber;
    }


    /**
     * Sets the phonenumber value for this UserInfo.
     * 
     * @param phonenumber
     */
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }


    /**
     * Gets the reserved1 value for this UserInfo.
     * 
     * @return reserved1
     */
    public String getReserved1() {
        return reserved1;
    }


    /**
     * Sets the reserved1 value for this UserInfo.
     * 
     * @param reserved1
     */
    public void setReserved1(String reserved1) {
        this.reserved1 = reserved1;
    }


    /**
     * Gets the reserved2 value for this UserInfo.
     * 
     * @return reserved2
     */
    public String getReserved2() {
        return reserved2;
    }


    /**
     * Sets the reserved2 value for this UserInfo.
     * 
     * @param reserved2
     */
    public void setReserved2(String reserved2) {
        this.reserved2 = reserved2;
    }


    /**
     * Gets the status value for this UserInfo.
     * 
     * @return status
     */
    public String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this UserInfo.
     * 
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }


    /**
     * Gets the terminatortype value for this UserInfo.
     * 
     * @return terminatortype
     */
    public String getTerminatortype() {
        return terminatortype;
    }


    /**
     * Sets the terminatortype value for this UserInfo.
     * 
     * @param terminatortype
     */
    public void setTerminatortype(String terminatortype) {
        this.terminatortype = terminatortype;
    }


    /**
     * Gets the userinfoid value for this UserInfo.
     * 
     * @return userinfoid
     */
    public String getUserinfoid() {
        return userinfoid;
    }


    /**
     * Sets the userinfoid value for this UserInfo.
     * 
     * @param userinfoid
     */
    public void setUserinfoid(String userinfoid) {
        this.userinfoid = userinfoid;
    }


    /**
     * Gets the usertype value for this UserInfo.
     * 
     * @return usertype
     */
    public String getUsertype() {
        return usertype;
    }


    /**
     * Sets the usertype value for this UserInfo.
     * 
     * @param usertype
     */
    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof UserInfo)) return false;
        UserInfo other = (UserInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.areano==null && other.getAreano()==null) || 
             (this.areano!=null &&
              this.areano.equals(other.getAreano()))) &&
            ((this.callusertype==null && other.getCallusertype()==null) || 
             (this.callusertype!=null &&
              this.callusertype.equals(other.getCallusertype()))) &&
            ((this.closedate==null && other.getClosedate()==null) || 
             (this.closedate!=null &&
              this.closedate.equals(other.getClosedate()))) &&
            ((this.corpinfoid==null && other.getCorpinfoid()==null) || 
             (this.corpinfoid!=null &&
              this.corpinfoid.equals(other.getCorpinfoid()))) &&
            ((this.deptinfoid==null && other.getDeptinfoid()==null) || 
             (this.deptinfoid!=null &&
              this.deptinfoid.equals(other.getDeptinfoid()))) &&
            ((this.moddate==null && other.getModdate()==null) || 
             (this.moddate!=null &&
              this.moddate.equals(other.getModdate()))) &&
            ((this.modoperator==null && other.getModoperator()==null) || 
             (this.modoperator!=null &&
              this.modoperator.equals(other.getModoperator()))) &&
            ((this.modorigin==null && other.getModorigin()==null) || 
             (this.modorigin!=null &&
              this.modorigin.equals(other.getModorigin()))) &&
            ((this.opendate==null && other.getOpendate()==null) || 
             (this.opendate!=null &&
              this.opendate.equals(other.getOpendate()))) &&
            ((this.openoperator==null && other.getOpenoperator()==null) || 
             (this.openoperator!=null &&
              this.openoperator.equals(other.getOpenoperator()))) &&
            ((this.openorigin==null && other.getOpenorigin()==null) || 
             (this.openorigin!=null &&
              this.openorigin.equals(other.getOpenorigin()))) &&
            ((this.pausedate==null && other.getPausedate()==null) || 
             (this.pausedate!=null &&
              this.pausedate.equals(other.getPausedate()))) &&
            ((this.paytype==null && other.getPaytype()==null) || 
             (this.paytype!=null &&
              this.paytype.equals(other.getPaytype()))) &&
            ((this.phonenumber==null && other.getPhonenumber()==null) || 
             (this.phonenumber!=null &&
              this.phonenumber.equals(other.getPhonenumber()))) &&
            ((this.reserved1==null && other.getReserved1()==null) || 
             (this.reserved1!=null &&
              this.reserved1.equals(other.getReserved1()))) &&
            ((this.reserved2==null && other.getReserved2()==null) || 
             (this.reserved2!=null &&
              this.reserved2.equals(other.getReserved2()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.terminatortype==null && other.getTerminatortype()==null) || 
             (this.terminatortype!=null &&
              this.terminatortype.equals(other.getTerminatortype()))) &&
            ((this.userinfoid==null && other.getUserinfoid()==null) || 
             (this.userinfoid!=null &&
              this.userinfoid.equals(other.getUserinfoid()))) &&
            ((this.usertype==null && other.getUsertype()==null) || 
             (this.usertype!=null &&
              this.usertype.equals(other.getUsertype())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAreano() != null) {
            _hashCode += getAreano().hashCode();
        }
        if (getCallusertype() != null) {
            _hashCode += getCallusertype().hashCode();
        }
        if (getClosedate() != null) {
            _hashCode += getClosedate().hashCode();
        }
        if (getCorpinfoid() != null) {
            _hashCode += getCorpinfoid().hashCode();
        }
        if (getDeptinfoid() != null) {
            _hashCode += getDeptinfoid().hashCode();
        }
        if (getModdate() != null) {
            _hashCode += getModdate().hashCode();
        }
        if (getModoperator() != null) {
            _hashCode += getModoperator().hashCode();
        }
        if (getModorigin() != null) {
            _hashCode += getModorigin().hashCode();
        }
        if (getOpendate() != null) {
            _hashCode += getOpendate().hashCode();
        }
        if (getOpenoperator() != null) {
            _hashCode += getOpenoperator().hashCode();
        }
        if (getOpenorigin() != null) {
            _hashCode += getOpenorigin().hashCode();
        }
        if (getPausedate() != null) {
            _hashCode += getPausedate().hashCode();
        }
        if (getPaytype() != null) {
            _hashCode += getPaytype().hashCode();
        }
        if (getPhonenumber() != null) {
            _hashCode += getPhonenumber().hashCode();
        }
        if (getReserved1() != null) {
            _hashCode += getReserved1().hashCode();
        }
        if (getReserved2() != null) {
            _hashCode += getReserved2().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getTerminatortype() != null) {
            _hashCode += getTerminatortype().hashCode();
        }
        if (getUserinfoid() != null) {
            _hashCode += getUserinfoid().hashCode();
        }
        if (getUsertype() != null) {
            _hashCode += getUsertype().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UserInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://user.encpdata.webservice.lccmp.linkage.com", "UserInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("areano");
        elemField.setXmlName(new javax.xml.namespace.QName("", "areano"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("callusertype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "callusertype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("closedate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "closedate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("corpinfoid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "corpinfoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deptinfoid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "deptinfoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moddate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "moddate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modoperator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modoperator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modorigin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modorigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opendate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "opendate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("openoperator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "openoperator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("openorigin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "openorigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pausedate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pausedate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paytype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paytype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phonenumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "phonenumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reserved1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reserved1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reserved2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reserved2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminatortype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "terminatortype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userinfoid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userinfoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usertype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usertype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
